package com.postepay.acceptance.test.ui;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import java.lang.reflect.Constructor;
import java.net.URISyntaxException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.FilenameUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.logging.LogEntry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cucumber.api.Scenario;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import test.automation.core.UIUtils;
import test.automation.core.videorecorder.ExecFfmpeg;
import ui.core.support.Costants;
import ui.core.support.page.Page;
import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.repository.LocatorRepo;
import ui.core.support.uiobject.repository.PageRepo;

public class TestEnvironment 
{
	private static TestEnvironment singleton;
	private static final Logger logger = LoggerFactory.getLogger(TestEnvironment.class);

	private static final String LOCATORS_FOLDER = "locators";
	private static final String LANGUAGES_FOLDER = "languages";
	private static final String LOADERS_FOLDER = "loaders";
	private static final String MOLECULES_FILE = "molecules.csv";
	private static final String CLASS_CSV_FIELD = "class";
	private static final String CSV_CSV_FIELD = "csv";
	private static final String MOLECULES_FOLDER = "molecules";
	private static final String KEY_CSV_FIELD = "key";
	private static final String PAGES_FILE = "pages.csv";

	private String testId;
	private HashMap<String,WebDriver> drivers;
	private HashMap<String,Properties> driversProp;
	private HashMap<String, Properties> languages;

	private EnvironmentSettings settings;

	public String baseVideoNamePath;
	private String videoName;

	private boolean emu;
	
	private Properties appConfiguration;
	private String currentDriver;
	
	private TestEnvironment()
	{
		testId=null;
		drivers = new HashMap<>();
		languages = new HashMap<>();
		driversProp = new HashMap<>();
		appConfiguration = new Properties();
		try {
			appConfiguration.load(TestEnvironment.class.getClassLoader().getResourceAsStream("ExecutionTest.properties"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(appConfiguration.toString());

	}
	

	public Properties getAppConfiguration() {
		return appConfiguration;
	}

	public static TestEnvironment get()
	{
		if(singleton == null)
			singleton = new TestEnvironment();

		return singleton;
	}

	public Properties getDriverProperties(String config)
	{
		return driversProp.get(config);
	}
	
	/**
	 * metodo di inizializzazione ambiente
	 * @param setting
	 */
	public void setEnvironment(EnvironmentSettings setting) 
	{
		this.settings=setting;

		SimpleDateFormat format=new SimpleDateFormat("yyyyMMdd_hhmmss");

		//carico il driver
		String driver=setting.getDriver();

		String[] D=driver.split(",");
		for (String d: D) {
			driversProp.put(d, UIUtils.ui().getProperties(d));
			drivers.put(d, UIUtils.ui().driver(d));
		}
		
		this.currentDriver=D[0];
		
//		drivers.put(driver, UIUtils.ui().driver(driver));

		Properties d=UIUtils.ui().getCurrentProperties();

		String emulator=d.getProperty("emulator");

		emu = emulator != null && emulator.equals("true");

		if (emu) {
			this.baseVideoNamePath=this.settings.getTestIdJira()+"-"+this.currentDriver.toUpperCase()
					+"_"+format.format(Calendar.getInstance().getTime())+".mp4";
		}else {
			this.baseVideoNamePath=this.settings.getTestIdJira()+"-"+this.currentDriver.toUpperCase();
		}

		File[] listOfFiles;
		//carico i languages
		try {
			listOfFiles = TestUtils.getFilesFromFolder(TestEnvironment.class.getClassLoader().getResource(LANGUAGES_FOLDER).toURI());

			for(File file : listOfFiles)
			{
				Properties p=new Properties();
				p.load(new FileInputStream(file));
				languages.put(FilenameUtils.removeExtension(file.getName()), p);
				logger.info(file.getAbsolutePath());
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.error(e.getMessage());
		}


		//caricamento locators
		LocatorRepo.get().clearRepository();

		try {
			listOfFiles = TestUtils.getFilesFromFolder(TestEnvironment.class.getClassLoader().getResource(LOCATORS_FOLDER).toURI());
			ArrayList<String> files=new ArrayList<>();

			for (File file : listOfFiles) {
				if (file.isFile()) {
					System.out.println(file.getAbsolutePath());
					files.add(file.getAbsolutePath());
				}
			}

			LocatorRepo.get().loadElements((String[]) files.toArray(new String[files.size()]));
			//System.out.println(LocatorRepo.get().getLocator("gotoHomePage404"));

		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.error(e.getMessage());
		}

		//carico le molecole, organismi e pagine
		loadMolecules();

		loadPages();
	}

	private void loadPages() 
	{
		try {
			Path file=Paths.get(TestEnvironment.class.getClassLoader().getResource(LOADERS_FOLDER+FileSystems.getDefault().getSeparator()+PAGES_FILE).toURI());
			Reader reader=null;
			CSVParser csvParser=null;

			reader = Files.newBufferedReader(file);

			csvParser= new CSVParser(reader, CSVFormat.EXCEL
					.withFirstRecordAsHeader()
					.withDelimiter(';')
					.withIgnoreHeaderCase()
					.withTrim());

			for (CSVRecord csvRecord : csvParser)
			{
				String _class=csvRecord.get(CLASS_CSV_FIELD);
				String csv=csvRecord.get(CSV_CSV_FIELD);
				String key=csvRecord.get(KEY_CSV_FIELD);

				Class<?> clazz=Class.forName(_class);
				Constructor<?> constructor=clazz.getConstructor(WebDriver.class,Properties.class);
				Page o=(Page) constructor.newInstance(drivers.get(this.currentDriver),languages.get(settings.getLanguage()));

				o.init();

				PageRepo.get().set(key, o);
			}

			csvParser.close();
			reader.close();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	private void loadMolecules() 
	{
		try {
			Path file=Paths.get(TestEnvironment.class.getClassLoader().getResource(LOADERS_FOLDER+FileSystems.getDefault().getSeparator()+MOLECULES_FILE).toURI());
			Reader reader=null;
			CSVParser csvParser=null;

			reader = Files.newBufferedReader(file);

			csvParser= new CSVParser(reader, CSVFormat.EXCEL
					.withFirstRecordAsHeader()
					.withDelimiter(';')
					.withIgnoreHeaderCase()
					.withTrim());

			for (CSVRecord csvRecord : csvParser)
			{
				String _class=csvRecord.get(CLASS_CSV_FIELD);
				String csv=csvRecord.get(CSV_CSV_FIELD);

				Class<?> clazz=Class.forName(_class);
				Constructor<?> constructor=clazz.getConstructor(WebDriver.class,Properties.class);
				Molecule o=(Molecule) constructor.newInstance(drivers.get(this.currentDriver),languages.get(settings.getLanguage()));

				o.init();

				//carico da csv
				o.loadFromCsv(Paths.get(TestEnvironment.class.getClassLoader().getResource(MOLECULES_FOLDER+FileSystems.getDefault().getSeparator()+csv).toURI()).toString());
			}

			csvParser.close();
			reader.close();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	public WebDriver getDriver(String config)
	{
		return drivers.get(config);
	}

	public void setDriver(String config)
	{
		drivers.get(config);
	}
	
	public Properties getLanguage(String name)
	{
		return languages.get(name);
	}

	public EnvironmentSettings getEnvironmentSettings()
	{
		return this.settings;
	}

	public void startVideoRecording(String config)
	{
		WebDriver d=drivers.get(this.currentDriver);

		if(d != null)
		{
			Class<?> type=d.getClass();

			if (emu) {

				videoName=UIUtils.ui().videoRecoder(this.baseVideoNamePath,this.currentDriver);
				UIUtils.ui().startVideoRecording();

			}
//			else if(!type.equals(Costants.ANDROID))
//			{
//				videoName=UIUtils.ui().videoRecoder(this.baseVideoNamePath,this.currentDriver);
//				UIUtils.ui().startVideoRecording();
//			}

			else
			{
				videoName=UIUtils.ui().mobileVideoRecorder(this.baseVideoNamePath,config);

				//				((AndroidDriver) d).context("NATIVE_APP");

				UIUtils.ui().startMobileVideoRecording((AppiumDriver<?>) d);

				//			    ((AndroidDriver) d).context("CHROMIUM");
			}
		}
	}

	public void stopVideoRecording(Scenario scenario)
	{

		WebDriver d=drivers.get(this.currentDriver);

		if(d != null)
		{
			Class<?> type=d.getClass();

			if (emu) {
				UIUtils.ui().stopVideoRecording();
			}
//			else if(!type.equals(Costants.ANDROID))
//			{
//				UIUtils.ui().stopVideoRecording();
//			}
			else
			{
				//				((AndroidDriver) d).context("NATIVE_APP");

				UIUtils.ui().stopMobileVideoRecording();

				//			    ((AndroidDriver) d).context("CHROMIUM");
			}

		}

		scenario.write("video:"+videoName);


		String workingDir = System.getProperty("user.dir");
		workingDir = workingDir + File.separatorChar + "target" + File.separatorChar + "embedded-videos";

		Properties currentProperties=UIUtils.ui().getCurrentProperties();

		if (emu) {
			try {
				ExecFfmpeg.changeFrameRate(currentProperties, workingDir, this.baseVideoNamePath);
				ExecFfmpeg.cropVideo(currentProperties, workingDir, this.baseVideoNamePath);
			} catch (Exception e) {
				e.printStackTrace();
			}			
		}

	}

	public static void captureLog(AppiumDriver driver, String testName)
			throws Exception {
		DateFormat df = new SimpleDateFormat("dd_MM_yyyy_HH-mm-ss");
		Date today = Calendar.getInstance().getTime();
		String reportDate = df.format(today);
		String logPath = UIUtils.ui().getCurrentProperties().getProperty("log.path");
		List<LogEntry> logEntries = driver.manage().logs().get("logcat").getAll();
		File logFile = new File(logPath + reportDate + "_" + testName + ".txt");
		PrintWriter log_file_writer = new PrintWriter(logFile);
		for(LogEntry l : logEntries)
		{
			log_file_writer.write(l.getMessage());
			log_file_writer.write(System.lineSeparator());
		}
		log_file_writer.flush();
		log_file_writer.close();
	}
	
	public String getVideoNamePath()
	{
//		return this.baseVideoNamePath;
		return this.videoName;
	}
}



