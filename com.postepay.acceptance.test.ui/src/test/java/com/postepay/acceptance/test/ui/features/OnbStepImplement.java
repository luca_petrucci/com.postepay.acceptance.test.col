package com.postepay.acceptance.test.ui.features;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;

import com.postepay.acceptance.test.ui.EnvironmentSettings;
import com.postepay.acceptance.test.ui.TestEnvironment;
import com.postepay.automation.core.ui.common.datatable.UserDataCredential;
import com.postepay.automation.core.ui.pages.abilitainapppage.AbilitaInAppPage;
import com.postepay.automation.core.ui.pages.abilitaquestodispositivopage.AbilitaQuestoDispositivoPage;
import com.postepay.automation.core.ui.pages.autorizzaonbpage.AutorizzaOnbPage;
import com.postepay.automation.core.ui.pages.bachecapage.BachecaPage;
import com.postepay.automation.core.ui.pages.creaposteid.CreaPosteId;
import com.postepay.automation.core.ui.pages.hamburgermenuhomepage.HamburgerMenuHomePage;
import com.postepay.automation.core.ui.pages.homepage.HomePage;
import com.postepay.automation.core.ui.pages.loginpostepage.LoginPostePage;
import com.postepay.automation.core.ui.pages.loginwithposteid.LoginWithPosteId;
import com.postepay.automation.core.ui.pages.notifichesettingpage.NotificheSettingPage;
import com.postepay.automation.core.ui.pages.onbqrcodereaderpage.OnbQrCodeReaderPage;
import com.postepay.automation.core.ui.pages.rechargemypostepaypagestep5.RechargeMyPostepayPageStep5;
import com.postepay.automation.core.ui.pages.settingpage.SettingPage;
import com.postepay.automation.core.ui.pages.typonbpage.TypOnbPage;
import com.postepay.automation.core.ui.verifytools.StringAndNumberOperationTools;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.waitutil.WaitManager;

public class OnbStepImplement {

	private String scenarioID;

	@Given("^Vengono avviati i due devices$")
	public void vengono_avviati_i_due_devices(List<EnvironmentSettings> dataTable) throws Throwable {
		EnvironmentSettings setting=dataTable.get(0);
		this.scenarioID="Test-";

		setting.setScenarioId(this.scenarioID);

		TestEnvironment.get().setEnvironment(setting);

	}

	@Given("^Viene effettuata la login sul device preferito$")
	public void viene_effettuata_la_login_sul_device_preferito(List<UserDataCredential> dataTable) throws Throwable {
		UserDataCredential table=dataTable.get(0);

		AppiumDriver<?> driver = (AppiumDriver<?>) TestEnvironment.get().getDriver(table.getDevice());

		// Pagina del PosteID
		try {
			LoginWithPosteId loginPosteId = (LoginWithPosteId) PageRepo.get().get(LoginWithPosteId.NAME);
			loginPosteId.setDriver(driver);
			loginPosteId.get();
//			loginPosteId.clickNotIAm();;
			WaitManager.get().waitLongTime();

		} catch (Exception e) {

		}

		LoginPostePage loginPage=(LoginPostePage) PageRepo.get().get(LoginPostePage.NAME);
		loginPage.setDriver(driver);
		loginPage.get();

		loginPage.insertCredential(table.getUsername(), table.getPassword());
		loginPage.clickOnAccedi();
		WaitManager.get().waitLongTime();

		LoginWithPosteId loginPosteId = (LoginWithPosteId) PageRepo.get().get(LoginWithPosteId.NAME);
		loginPosteId.setDriver(driver);
		loginPosteId.get();
		loginPosteId.insertPosteId(table.getPosteId());
		loginPosteId.clickOnConfirmButton();
		WaitManager.get().waitLongTime();
	}

	@Given("^Utente verifica che il \"([^\"]*)\" e il preferito altrimenti lo imposta come tale$")
	public void utente_verifica_che_il_e_il_preferito_altrimenti_lo_imposta_come_tale(String arg1, List<UserDataCredential> dataTable) throws Throwable {
		UserDataCredential table=dataTable.get(0);

		AppiumDriver<?> driver = (AppiumDriver<?>) TestEnvironment.get().getDriver(table.getDevice());

		HomePage home = (HomePage) PageRepo.get().get(HomePage.NAME);
		home.setDriver(driver);
		home.get();

		home.clickOnHamburgerMenu();
		WaitManager.get().waitShortTime();

		HamburgerMenuHomePage menu=(HamburgerMenuHomePage) PageRepo.get().get(HamburgerMenuHomePage.NAME);
		menu.setDriver(driver);
		//		menu.get();

		menu.clickOnSettings();
		WaitManager.get().waitShortTime();

		// Pagina di Impostazioni
		SettingPage setting = (SettingPage) PageRepo.get().get(SettingPage.NAME);
		setting.setDriver(driver);
		//		setting.get();

		setting.clickOnNotification();
		WaitManager.get().waitShortTime();

		NotificheSettingPage notifiche = (NotificheSettingPage) PageRepo.get().get(NotificheSettingPage.NAME);
		notifiche.setDriver(driver);
		//		notifiche.get();

		//		notifiche.verifyHeaderpage("Notifiche");
		if (notifiche.isPreferito() == false) {
			// Allora sono il preferito
			notifiche.verifyLayoutPreferito();
		} else {
			notifiche.verifyLayoutNonPreferito();
			notifiche.clickOnImpostaComePreferito();

			// Pagina di inserimento PosteID
			RechargeMyPostepayPageStep5 newPosteId=(RechargeMyPostepayPageStep5) PageRepo.get().get(RechargeMyPostepayPageStep5.NAME);
			newPosteId.setDriver(driver);
			newPosteId.insertPosteId(table.getPosteId());
			newPosteId.clickOnConfirmButton();
		}
	}

	@Given("^Viene effettuata la login sul device non onboardato$")
	public void viene_effettuata_la_login_sul_device_non_onboardato(List<UserDataCredential> dataTable) throws Throwable {
		UserDataCredential table=dataTable.get(0);

		AppiumDriver<?> driver = (AppiumDriver<?>) TestEnvironment.get().getDriver(table.getDevice());

		// Pagina del PosteID DEVICE NON ONB

		try {
			LoginWithPosteId loginPosteId = (LoginWithPosteId) PageRepo.get().get(LoginWithPosteId.NAME);
			loginPosteId.setDriver(driver);
			loginPosteId.get();
//			loginPosteId.clickNotIAm();;
			WaitManager.get().waitLongTime();

		} catch (Exception e) {

		}
		LoginPostePage loginPage=(LoginPostePage) PageRepo.get().get(LoginPostePage.NAME);
		loginPage.setDriver(driver);
		loginPage.get();

		loginPage.insertCredential(table.getUsername(), table.getPassword());
		loginPage.clickOnAccedi();
		WaitManager.get().waitLongTime();
	}

	@Given("^Utente inizia il funnel di SYNC sul secondo device e clicca su INIZIA$")
	public void utente_inizia_il_funnel_di_SYNC_sul_secondo_device_e_clicca_su_INIZIA(List<UserDataCredential> dataTable) throws Throwable {
		UserDataCredential table=dataTable.get(0);
		AppiumDriver<?> driver = (AppiumDriver<?>) TestEnvironment.get().getDriver(table.getDevice());

		AbilitaQuestoDispositivoPage abilita = (AbilitaQuestoDispositivoPage) PageRepo.get().get(AbilitaQuestoDispositivoPage.NAME);
		abilita.setDriver(driver);
		abilita.get();

		abilita.verifyHeaderpage(TestEnvironment.get().getLanguage("header").getProperty("AbilitaInAppPage"));
		abilita.verifyLayout();
		abilita.clickOnInizia();
	}

	@Given("^Utente atterra sulla schermata Abilita in App e sceglie la sync da dispositivo preferito$")
	public void utente_atterra_sulla_schermata_Abilita_in_App_e_sceglie_la_sync_da_dispositivo_preferito(List<UserDataCredential> dataTable) throws Throwable {
		UserDataCredential table=dataTable.get(0);
		AppiumDriver<?> driver = (AppiumDriver<?>) TestEnvironment.get().getDriver(table.getDevice());

		AbilitaInAppPage abilita = (AbilitaInAppPage) PageRepo.get().get(AbilitaInAppPage.NAME);
		abilita.setDriver(driver);
		abilita.get();

		abilita.verifyHeaderpage(TestEnvironment.get().getLanguage("header").getProperty("AbilitaInAppPage"));
		abilita.verifyLayout();
		abilita.clickOnAutorizzaConPreferito();
	}

	@Then("^Utente atterra sulla schermata di QR Code Reader$")
	public void utente_atterra_sulla_schermata_di_QR_Code_Reader(List<UserDataCredential> dataTable) throws Throwable {
		UserDataCredential table=dataTable.get(0);
		AppiumDriver<?> driver = (AppiumDriver<?>) TestEnvironment.get().getDriver(table.getDevice());

		OnbQrCodeReaderPage qrPage = (OnbQrCodeReaderPage) PageRepo.get().get(OnbQrCodeReaderPage.NAME);
		qrPage.setDriver(driver);
		qrPage.get();

		qrPage.verifyHeaderpage(TestEnvironment.get().getLanguage("header").getProperty("AbilitaInAppPage"));
		qrPage.verifyLayout();
	}

	// Variabile di controllo per verificare che arrivi la notifica push
	// In caso controtario si procede verso la sezione Bacheca
	// All'ultimo step del test si solleva l'eccezione che la notifica non è arrivata entro tot minuti
	// Eccezione anche nel caso in cui non sia presente nella sezione bacheca
	boolean isNotificaArrived = false;

	@Then("^Sul \"([^\"]*)\" viene ricevuta la notifica di SYNC$")
	public void sul_viene_ricevuta_la_notifica_di_SYNC(String arg1, List<UserDataCredential> dataTable) throws Throwable {
		UserDataCredential table=dataTable.get(0);
		AppiumDriver<?> driver = (AppiumDriver<?>) TestEnvironment.get().getDriver(table.getDevice());

//		((AppiumDriver<?>) driver).openNotifications();
		NotificheSettingPage notifiche = (NotificheSettingPage) PageRepo.get().get(NotificheSettingPage.NAME);
		notifiche.setDriver(driver);
		WaitManager.get().waitLongTime();

		long time=TimeUnit.MINUTES.toMillis(2);
		while(time > 0)
		{
			try
			{
				notifiche.clickOnNotificaPush((AppiumDriver<?>) driver, "Richiesta di autorizzazione da altro dispositivo");
				this.isNotificaArrived = true;
				break;
				//			controllo la presenza della notifica se presente clicco ed esco del while con un break
			}
			catch(Exception err)
			{
				Thread.sleep(TimeUnit.SECONDS.toMillis(10));
				time -=TimeUnit.SECONDS.toMillis(10);
				System.out.println("notifica non trovata attendo 10 sec...");
			}
		}

		if (this.isNotificaArrived == false) {
			System.out.println("La notifica push non è arrivata, si procede con la verifica in bacheca");
//			((AppiumDriver<?>) driver).pressKey(new KeyEvent(AndroidKey.BACK));
			WaitManager.get().waitMediumTime();
//			((AppiumDriver<?>) driver).pressKey(new KeyEvent(AndroidKey.BACK));
			WaitManager.get().waitMediumTime();

			HomePage home = (HomePage) PageRepo.get().get(HomePage.NAME);
			home.setDriver(driver);
			home.clickAllMessageNumber();
			WaitManager.get().waitLongTime();

			StringAndNumberOperationTools tool = new StringAndNumberOperationTools();
			String data = tool.stringDATEforBachecaMessageTab("toDay");

			BachecaPage bacheca = (BachecaPage) PageRepo.get().get(BachecaPage.NAME);
			bacheca.clickOnSpecificNotification((AppiumDriver<?>) driver,
					"RICHIESTA DI AUTORIZZAZIONE DA ALTRO DISPOSITIVO", 
					"E' stata richiesta un' autorizzazione dal device Android SDK built for x86_64", 
					data);
		}
	}

	@Then("^Utente autorizza la SYNC$")
	public void utente_autorizza_la_SYNC(List<UserDataCredential> dataTable) throws Throwable {
		UserDataCredential table=dataTable.get(0);
		double discrepanza = StringAndNumberOperationTools.convertStringToDouble(table.getDiscrepanza());

		AppiumDriver<?> driver = (AppiumDriver<?>) TestEnvironment.get().getDriver(table.getDevice());

		AutorizzaOnbPage autorizzaPage = (AutorizzaOnbPage) PageRepo.get().get(AutorizzaOnbPage.NAME);
		autorizzaPage.setDriver(driver);
//		autorizzaPage.get();

//		autorizzaPage.verifyHeaderpage("Autorizza dispositivo");
		autorizzaPage.verifyLayout((AppiumDriver<?>) driver, discrepanza);
		autorizzaPage.clickOnAutorizza();

		// Inserimento posteID, inserita nel vecchio formato se this.isNotificaArrived == true
		// Inserimento posteID, inserita nel nuovo formato se this.isNotificaArrived == false
		if (this.isNotificaArrived == true) {
			LoginWithPosteId posteIDold = (LoginWithPosteId) PageRepo.get().get(LoginWithPosteId.NAME);
			posteIDold.setDriver(driver);
			posteIDold.insertPosteId(table.getPosteId());
			posteIDold.clickOnConfirmButton();

		} else {
			RechargeMyPostepayPageStep5 newPosteId=(RechargeMyPostepayPageStep5) PageRepo.get().get(RechargeMyPostepayPageStep5.NAME);
			newPosteId.setDriver(driver);
			newPosteId.insertPosteId(table.getPosteId());
			newPosteId.clickOnConfirmButton();
		}
	}

	@Then("^Viene generato il codice QR per autorizzare la SYNC$")
	public void viene_generato_il_codice_QR_per_autorizzare_la_SYNC(List<UserDataCredential> dataTable) throws Throwable {
		UserDataCredential table=dataTable.get(0);
		AppiumDriver<?> driver = (AppiumDriver<?>) TestEnvironment.get().getDriver(table.getDevice());
	}

	@Then("^Il \"([^\"]*)\" legge il QR Code$")
	public void il_legge_il_QR_Code(String arg1, List<UserDataCredential> dataTable) throws Throwable {
		UserDataCredential table=dataTable.get(0);
		AppiumDriver<?> driver = (AppiumDriver<?>) TestEnvironment.get().getDriver(table.getDevice());
	}

	@Then("^Viene mostrata la pagina di creazione del posteID$")
	public void viene_mostrata_la_pagina_di_creazione_del_posteID(List<UserDataCredential> dataTable) throws Throwable {
		UserDataCredential table=dataTable.get(0);
		AppiumDriver<?> driver = (AppiumDriver<?>) TestEnvironment.get().getDriver(table.getDevice());
		
		CreaPosteId posteID = (CreaPosteId) PageRepo.get().get(CreaPosteId.NAME);
		posteID.setDriver(driver);
		
//		posteID.verifyHeaderpage("Abilita prodotti in app");
		posteID.verifyLayout();
		posteID.createPosteID(table.getPosteId());
		posteID.clickOnConferma();
	}

	@Then("^Inizia il countdown e alla fine l utente atterra sulla TYP$")
	public void inizia_il_countdown_e_alla_fine_l_utente_atterra_sulla_TYP(List<UserDataCredential> dataTable) throws Throwable {
		UserDataCredential table=dataTable.get(0);
		double discrepanza = StringAndNumberOperationTools.convertStringToDouble(table.getDiscrepanza());

		AppiumDriver<?> driver = (AppiumDriver<?>) TestEnvironment.get().getDriver(table.getDevice());
		
		AutorizzaOnbPage autorizzaPage = (AutorizzaOnbPage) PageRepo.get().get(AutorizzaOnbPage.NAME);
		autorizzaPage.setDriver(driver);
		autorizzaPage.verifyLayoutCountdown();
		
		WaitManager.get().waitMediumTime();
		
		TypOnbPage typ = (TypOnbPage) PageRepo.get().get(TypOnbPage.NAME);
		
		typ.setDriver(driver);
		typ.get();
		
		typ.verifyLayout(((AppiumDriver<?>)driver), discrepanza);
		typ.clickOnChiudi();
	}

	@Then("^Utente naviga nell App$")
	public void utente_naviga_nell_App(List<UserDataCredential> dataTable) throws Throwable {
		UserDataCredential table=dataTable.get(0);
		AppiumDriver<?> driver = (AppiumDriver<?>) TestEnvironment.get().getDriver(table.getDevice());
	}

	@Then("^Viene eseguita la cancellazione del posteID$")
	public void viene_eseguita_la_cancellazione_del_posteID(List<UserDataCredential> dataTable) throws Throwable {
		UserDataCredential table=dataTable.get(0);
		AppiumDriver<?> driver = (AppiumDriver<?>) TestEnvironment.get().getDriver(table.getDevice());
	}

	@Then("^Vengono chiuse entrambe le App$")
	public void vengono_chiuse_entrambe_le_App(List<UserDataCredential> dataTable) throws Throwable {
		UserDataCredential table=dataTable.get(0);
		AppiumDriver<?> driver = (AppiumDriver<?>) TestEnvironment.get().getDriver(table.getDevice());
	}

}
