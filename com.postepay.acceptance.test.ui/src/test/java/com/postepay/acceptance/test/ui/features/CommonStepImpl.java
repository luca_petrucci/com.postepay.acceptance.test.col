package com.postepay.acceptance.test.ui.features;

import static java.util.stream.Collectors.toList;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.Console;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream.GetField;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Driver;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.html5.Location;
import org.openqa.selenium.html5.LocationContext;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.mobile.NetworkConnection;
import org.openqa.selenium.mobile.NetworkConnection.ConnectionType;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.CommandExecutionHelper;
import io.appium.java_client.ExecutesMethod;
import io.appium.java_client.HasSettings;
import io.appium.java_client.Setting;
import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.*;
import cucumber.runtime.Utils;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.HidesKeyboard;
import io.appium.java_client.InteractsWithApps;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.Activity;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.StartsActivity;
import io.appium.java_client.android.connection.ConnectionState;
import io.appium.java_client.android.connection.ConnectionStateBuilder;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import junit.framework.Test;
import test.automation.bug.reporter.BugReporter;
import test.automation.core.UIUtils;
import test.automation.core.UIUtils.SCROLL_DIRECTION;
import test.automation.core.cmd.CommandArgs;
import test.automation.core.cmd.adb.AdbCommandPrompt;
import ui.core.support.Costants;
import ui.core.support.connectionutil.ConnectionUtils;
import ui.core.support.page.Page;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.repository.PageRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;
import ui.core.support.waitutil.WaitManager;
import utility.CoreUtility;
import utility.Parser;
import utility.SoftAssertion;
import cucumber.api.PendingException;
import cucumber.api.java.en.*;

import com.postepay.acceptance.test.ui.ConnectionsEmulatorTools;
import com.postepay.acceptance.test.ui.EnvironmentSettings;
import com.postepay.acceptance.test.ui.PopUpTheread;
import com.postepay.acceptance.test.ui.TestEnvironment;
import com.postepay.acceptance.test.ui.VideoManagerTool;
import com.postepay.acceptance.test.ui.VideoUtilities;
import static io.appium.java_client.MobileCommand.setSettingsCommand;
//import com.postepay.automation.core.ui.common.*;
import com.postepay.automation.core.ui.common.datatable.*;
import com.postepay.automation.core.ui.molecules.AutomaticRechargePostepaySectionPageMolecola;
import com.postepay.automation.core.ui.molecules.BodyMapsGenericScreenFromHomePage;
import com.postepay.automation.core.ui.molecules.BodyRechargePaymentSection;
import com.postepay.automation.core.ui.molecules.CredentialsCardLogin;
import com.postepay.automation.core.ui.molecules.GenericErrorPopUpMolecola;
import com.postepay.automation.core.ui.molecules.ThankYouPageForTelephoneRecharge;
import com.postepay.automation.core.ui.pages.abilitainapppage.AbilitaInAppPage;
import com.postepay.automation.core.ui.pages.abilitaquestodispositivopage.AbilitaQuestoDispositivoPage;
import com.postepay.automation.core.ui.pages.accessandauthorizationpage.AccessAndAuthorizationPage;
import com.postepay.automation.core.ui.pages.acquistoparcheggiopage.AcquistoParcheggioPage;
import com.postepay.automation.core.ui.pages.automaticrechargeonzerorechargepage.AutomaticRechargeOnZeroRechargePage;
import com.postepay.automation.core.ui.pages.automaticrechargepostepay.AutomaticRechargePostepay;
import com.postepay.automation.core.ui.pages.automaticrechargepostepaysectionpage.AutomaticRechargePostepaySectionPage;
import com.postepay.automation.core.ui.pages.automaticrechargepostepaysummary.AutomaticRechargePostepaySummary;
import com.postepay.automation.core.ui.pages.banktranfertpostagiropagestep1.BankTranfertPostagiroPageStep1;
import com.postepay.automation.core.ui.pages.banktranfertsepapagestep1.BankTranfertSEPAPageStep1;
import com.postepay.automation.core.ui.pages.banktranfertsepapagestep2.BankTranfertSEPAPageStep2;
import com.postepay.automation.core.ui.pages.banktranfertsepapagestep3.BankTranfertSEPAPageStep3;
import com.postepay.automation.core.ui.pages.banktranfertsepapagestep4.BankTranfertSEPAPageStep4;
import com.postepay.automation.core.ui.pages.banktranfertsepapagestep5.BankTranfertSEPAPageStep5;
import com.postepay.automation.core.ui.pages.bolloautomotot139.BolloAutoMotoT139;
import com.postepay.automation.core.ui.pages.bpolpage.BPOLPage;
import com.postepay.automation.core.ui.pages.carburantestep1.CarburanteStep1;
import com.postepay.automation.core.ui.pages.carburantestep2.CarburanteStep2;
import com.postepay.automation.core.ui.pages.carburantestep3.CarburanteStep3;
import com.postepay.automation.core.ui.pages.carburantetyp.CarburanteTYP;
import com.postepay.automation.core.ui.pages.commentrechargep2ppage.CommentRechargeP2pPage;
import com.postepay.automation.core.ui.pages.communitycardonepage.CommunityCardOnePage;
import com.postepay.automation.core.ui.pages.communitycardtwopage.CommunityCardTwoPage;
import com.postepay.automation.core.ui.pages.credentialspid.CredentialSPID;
import com.postepay.automation.core.ui.pages.deleteautomaticrechargepage.DeleteAutomaticRechargePage;
import com.postepay.automation.core.ui.pages.detailsconnectsimpurchaseandsendgigapage.DetailsConnectSimPurchaseAndSendGigaPage;
import com.postepay.automation.core.ui.pages.detailsconnectsimpurchasegigapage.DetailsConnectSimPurchaseGigaPage;
import com.postepay.automation.core.ui.pages.dettailsmsgrmautomaticrech.DettailsMessageConfirmRemovingAutomaticRecharge;
import com.postepay.automation.core.ui.pages.devicenativopage.DeviceNativoPage;
import com.postepay.automation.core.ui.pages.extraandurbanoandextramieiacquisti.ExtraAndurbanoAndExtraMieiAcquisti;
import com.postepay.automation.core.ui.pages.extraurbanpurchasepagestep1.ExtraurbanPurchasePageStep1;
import com.postepay.automation.core.ui.pages.extraurbanpurchasepagestep2.ExtraurbanPurchasePageStep2;
import com.postepay.automation.core.ui.pages.extraurbanpurchasepagestep3.ExtraurbanPurchasePageStep3;
import com.postepay.automation.core.ui.pages.extraurbanpurchasepagestep4.ExtraurbanPurchasePageStep4;
import com.postepay.automation.core.ui.pages.extraurbanpurchasepagestep5.ExtraurbanPurchasePageStep5;
import com.postepay.automation.core.ui.pages.extraurbanpurchasepagestep6.ExtraurbanPurchasePageStep6;
import com.postepay.automation.core.ui.pages.fuelpurchasepagestep1.FuelPurchasePageStep1;
import com.postepay.automation.core.ui.pages.genericerrorpopup.GenericErrorPopUp;
import com.postepay.automation.core.ui.pages.hamburgermenuhomepage.HamburgerMenuHomePage;
import com.postepay.automation.core.ui.pages.homepage.HomePage;
import com.postepay.automation.core.ui.pages.homepage.HomePageManager;
import com.postepay.automation.core.ui.pages.iosdevicepage.IOSDevicePage;
import com.postepay.automation.core.ui.pages.loginpostepage.LoginPostePage;
import com.postepay.automation.core.ui.pages.loginwithposteid.LoginWithPosteId;
import com.postepay.automation.core.ui.pages.mapsgenericscreenfromhomepage.MapsGenericScreenFromHomePage;
import com.postepay.automation.core.ui.pages.mapspaywithqrcode.MapsPayWithQRCode;
import com.postepay.automation.core.ui.pages.mieiacquistipage.MieiAcquistiPage;
import com.postepay.automation.core.ui.pages.ministerotrasportipage.MinisteroTrasportiPage;
import com.postepay.automation.core.ui.pages.ministerotrasportisummarypage.MinisteroTrasportiSummaryPage;
import com.postepay.automation.core.ui.pages.newaccesspage.NewAccessPage;
import com.postepay.automation.core.ui.pages.nop2pcontactthankyoupage.NoP2pContactThankYouPage;
import com.postepay.automation.core.ui.pages.notificheautorizzative.NotificheAutorizzativePage;
import com.postepay.automation.core.ui.pages.notifichesettingpage.NotificheSettingPage;
import com.postepay.automation.core.ui.pages.notifichesettingpage.NotificheSettingPageManager;
import com.postepay.automation.core.ui.pages.pagabollettinobiancot135.PagaBollettinoBiancoT135;
import com.postepay.automation.core.ui.pages.pagabollettinobiancot136.RiepilogoPagamentoSummaryT136;
import com.postepay.automation.core.ui.pages.pagabollettinot133.PagaBollettinoT133;
import com.postepay.automation.core.ui.pages.pagopapage.PagoPaPage;
import com.postepay.automation.core.ui.pages.parkingpurchasepagestep1.ParkingPurchasePageStep1;
import com.postepay.automation.core.ui.pages.parkingpurchasepagestep1.ParkingPurchasePageStep1Manager;
import com.postepay.automation.core.ui.pages.parkingpurchasepagestep2.ParkingPurchasePageStep2;
import com.postepay.automation.core.ui.pages.parkingpurchasepagestep3.ParkingPurchasePageStep3;
import com.postepay.automation.core.ui.pages.paymentpaymentpage.PaymentPaymentPage;
import com.postepay.automation.core.ui.pages.preloginpostepage.PreLoginPostePage;
import com.postepay.automation.core.ui.pages.productsconnectcarddetailspage.ProductsConnectCardDetailsPage;
import com.postepay.automation.core.ui.pages.productsstandardpage.ProductsStandardPage;
import com.postepay.automation.core.ui.pages.rechargemypostepaypagestep1.RechargeMyPostepayPageStep1;
import com.postepay.automation.core.ui.pages.rechargemypostepaypagestep2.RechargeMyPostepayPageStep2;
import com.postepay.automation.core.ui.pages.rechargemypostepaypagestep3.RechargeMyPostepayPageStep3;
import com.postepay.automation.core.ui.pages.rechargemypostepaypagestep4.RechargeMyPostepayPageStep4;
import com.postepay.automation.core.ui.pages.rechargemypostepaypagestep5.RechargeMyPostepayPageStep5;
import com.postepay.automation.core.ui.pages.rechargemypostepaypagestep6.RechargeMyPostepayPageStep6;
import com.postepay.automation.core.ui.pages.rechargeotherpostepaypagestep1.RechargeOtherPostepayPageStep1;
import com.postepay.automation.core.ui.pages.rechargeotherpostepaypagestep2.RechargeOtherPostepayPageStep2;
import com.postepay.automation.core.ui.pages.rechargepaymentpage.RechargePaymentPage;
import com.postepay.automation.core.ui.pages.rechargepostepayusingtelephoncontactspage.RechargePostepayUsingTelephonContactsPage;
import com.postepay.automation.core.ui.pages.requestp2ppagestep1.RequestP2PPageStep1;
import com.postepay.automation.core.ui.pages.requestp2ppagestep2.RequestP2PPageStep2;
import com.postepay.automation.core.ui.pages.requestp2ppagestep3.RequestP2PPageStep3;
import com.postepay.automation.core.ui.pages.requestp2ppagestep4.RequestP2PPageStep4;
import com.postepay.automation.core.ui.pages.rubricabollettinopage.RubricaBollettinoPage;
import com.postepay.automation.core.ui.pages.salvadanaioaccantonamentodettaglio.SalvadanaioAccantonamentoDettaglio;
import com.postepay.automation.core.ui.pages.salvadanaioobiettivosummary.SalvadanaioObiettivoSummary;
import com.postepay.automation.core.ui.pages.salvadanaioopzionisettaggio.SalvadanaioOpzioniSettaggio;
import com.postepay.automation.core.ui.pages.salvadanaiopage.SalvadanaioPage;
import com.postepay.automation.core.ui.pages.salvadanaioselezionatipologiapage.SalvadanaioSelezionaTipologiaPage;
import com.postepay.automation.core.ui.pages.salvadanaiopichercalender.SalvadanaioPicherCalender;
import com.postepay.automation.core.ui.pages.salvadanaiotyp.SalvadanaioTYP;
import com.postepay.automation.core.ui.pages.salvadanaioversasuobiettivo.SalvadanaioVersaSuObiettivo;
import com.postepay.automation.core.ui.pages.salvadanaioversasuobiettivoricorrente.SalvadanaioVersaSuObiettivoRicorrente;
import com.postepay.automation.core.ui.pages.scontipostedettagliot137.ScontiPosteDettaglioT137;
import com.postepay.automation.core.ui.pages.scontipostedettagliot137.ScontiPosteDettaglioT137Manager;
import com.postepay.automation.core.ui.pages.selectpostepayrecharge.SelectPostepayRecharge;
import com.postepay.automation.core.ui.pages.selezionabollettinot134.SelezionaBollettinoT134;
import com.postepay.automation.core.ui.pages.selezionatipobonificopage.SelezionaTipoBonificoPage;
import com.postepay.automation.core.ui.pages.selezionafiltribachecapage.SelezionaFiltriBachecaPage;
import com.postepay.automation.core.ui.pages.sendg2gpagestep1.SendG2GPageStep1;
import com.postepay.automation.core.ui.pages.sendg2gpagestep2.SendG2GPageStep2;
import com.postepay.automation.core.ui.pages.sendg2gpagestep3.SendG2GPageStep3;
import com.postepay.automation.core.ui.pages.sendg2gpagestep5.SendG2GPageStep5;
import com.postepay.automation.core.ui.pages.sendp2ppagestep1.SendP2PPageStep1;
import com.postepay.automation.core.ui.pages.sendp2ppagestep2.SendP2PPageStep2;
import com.postepay.automation.core.ui.pages.sendp2ppagestep3.SendP2PPageStep3;
import com.postepay.automation.core.ui.pages.sendp2ppagestep4.SendP2PPageStep4;
import com.postepay.automation.core.ui.pages.sendp2ppagestep5.SendP2PPageStep5;
import com.postepay.automation.core.ui.pages.servicepaymentpage.ServicePaymentPage;
import com.postepay.automation.core.ui.pages.servizityp.ServiziTYP;
import com.postepay.automation.core.ui.pages.settingpage.SettingPage;
import com.postepay.automation.core.ui.pages.settingpage.SettingPageManager;
import com.postepay.automation.core.ui.pages.settingprofile.SettingProfile;
import com.postepay.automation.core.ui.pages.settingprofilecarburante.SettingProfileCarburante;
import com.postepay.automation.core.ui.pages.thankyoupageconnectsimpurchasegigapage.ThankYouPageConnectSimPurchaseGigaPage;
import com.postepay.automation.core.ui.pages.thankyoupageconnectsimpurchasegigapage.ThankYouPageConnectSimPurchaseGigaPageManager;
import com.postepay.automation.core.ui.pages.thelephonerechargepagestep1.ThelephoneRechargePageStep1;
import com.postepay.automation.core.ui.pages.thelephonerechargepagestep3.ThelephoneRechargePageStep3;
import com.postepay.automation.core.ui.pages.urbanoricevutatyp.UrbanoRicevutaTYP;
import com.postepay.automation.core.ui.pages.urbanpurchasepagestep1.UrbanPurchasePageStep1;
import com.postepay.automation.core.ui.pages.urbanpurchasepagestep2.UrbanPurchasePageStep2;
import com.postepay.automation.core.ui.pages.urbanpurchasepagestep3.UrbanPurchasePageStep3;
import com.postepay.automation.core.ui.pages.urbanpurchasepagestep4.UrbanPurchasePageStep4;
import com.postepay.automation.core.ui.pages.automaticrechargepostepaythankyoupage.AutomaticRechargePostepayThankYouPage;
import com.postepay.automation.core.ui.pages.bachecapage.BachecaPage;
import com.postepay.automation.core.ui.pages.webviewgenericpage.WebviewGenericPage;
import com.postepay.automation.core.ui.verifytools.LayoutTools;
import com.postepay.automation.core.ui.verifytools.StringAndNumberOperationTools;
//import com.postepay.automation.core.ui.verifytools.WaitManager;

import aj.org.objectweb.asm.Type;
import ch.qos.logback.classic.Logger;
import io.appium.java_client.android.nativekey.PressesKey;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.touch.offset.PointOption;
import io.restassured.config.ConnectionConfig;

public class CommonStepImpl {
	private String driverName;
	private String scenarioID;
	WebDriver driverAndroid;
	private Point pointSignIn;
	int pointSignInX;
	int pointSignInY;
	WebDriver driverWeb;
	private String executionDate;
	WebDriver driverBPOL;
	String adbExe;
	boolean isIOS = false;
	// public WebDriver getAndroidDriver() {
	// return driverAndroid;
	// }

	@Before
	public void setup(Scenario scenario) throws IOException {
		System.out.println(scenario.getId());
		System.out.println(scenario.getName());

		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		// format.setTimeZone(TimeZone.getTimeZone("UTC"));
		Calendar c = Calendar.getInstance();
		this.executionDate = format.format(c.getTime());
		System.out.println(this.executionDate);

		System.setProperty(UIUtils.SCENARIO_ID, Integer.toString(scenario.getId().hashCode()));
		this.scenarioID = "Test-";

	}

	Thread threadPopup;
	private Location location;

	@After
	public void runHook(Scenario scenario) {
		// stop del thread dei popup random per i biglietti urbani e extraurbani
		System.out.println("Stop del thread dei popup random");
		// threadPopup.interrupt();

		// VideoManagerTool tool=new VideoManagerTool();
		// tool.stopVideoRecorder();
		// VideoUtilities tool=new VideoUtilities();
		// tool.stopVideoRecorder();
		// tool.pullFileVideoRecorder();

		TestEnvironment.get().stopVideoRecording(scenario);

		Properties p = UIUtils.ui().getCurrentProperties();
		JSONObject obj = new JSONObject();
		obj.put("name", p.getProperty("device.name"));
		obj.put("os", p.getProperty("device.os"));
		obj.put("version", p.getProperty("device.version"));
		obj.put("brand", p.getProperty("device.brand"));
		obj.put("screen_size", p.getProperty("device.screen_size"));
		obj.put("resolution", p.getProperty("device.resolution"));
		obj.put("category", p.getProperty("device.category"));
		obj.put("icon", p.getProperty("device.icon"));
		obj.put("image", p.getProperty("device.image"));
		scenario.write("device:" + obj.toString());

		// if (scenario.isFailed()){
		// takeScreenshotOnFailure(scenario);
		// }else
		// {
		// embedScreenshot(scenario);
		// }
		scenario.write("execDate:" + this.executionDate);

		try {
			TestEnvironment.captureLog((AppiumDriver) driverAndroid, TestEnvironment.get().baseVideoNamePath);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//		((LocationContext) driverAndroid).setLocation(location); // Must be a driver that implements LocationContext
		Properties devName = UIUtils.ui().getCurrentProperties();
		if (isIOS == false) {
			AdbCommandPrompt adbCmd = new AdbCommandPrompt(adbExe, devName.getProperty("ios.udid"));
			//adbCmd.enableGps();
		}
		// List<LogEntry> logEntries =
		// driverAndroid.manage().logs().get("logcat").getAll();
		if(this.isIOS) {
			((HasSettings) driverAndroid).setSetting(Setting.SHOULD_USE_COMPACT_RESPONSES, true);
			((HasSettings) driverAndroid).setSetting(Setting.WAIT_FOR_SELECTOR_TIMEOUT, 600000);
			CommandExecutionHelper.execute((ExecutesMethod) driverAndroid, setSettingsCommand("customSnapshotTimeout", 15f));
			CommandExecutionHelper.execute((ExecutesMethod) driverAndroid, setSettingsCommand("snapshotMaxDepth", 50));
		}

		closeDriver();
		try {
			if (scenario.isFailed())
				BugReporter.get(this.getClass().getClassLoader().getResourceAsStream("bug.reporter.config.properties"))
				.reportBug(scenario, "com.postepay", AppVersion.version,
						TestEnvironment.get().getVideoNamePath());
		} catch (Exception e) {
			// TODO: handle exception
		}

		try {
			SoftAssertion.get().getAssertions().assertAll();
		} catch (AssertionError e) {
			BugReporter.get(this.getClass().getClassLoader().getResourceAsStream("bug.reporter.config.properties"))
			.reportBug(scenario, e, AppVersion.version, TestEnvironment.get().getVideoNamePath());
			throw e;
		}
	}

	private void closeDriver() {
		EnvironmentSettings s = TestEnvironment.get().getEnvironmentSettings();
		this.driverWeb = TestEnvironment.get().getDriver(s.getDriver());

		if (driverWeb != null)

			if (driverWeb.getClass().equals(Costants.ANDROID)) {
				// d.close();
				driverWeb.quit();
			} else {
				driverWeb.quit();
			}
		if (driverBPOL != null)
			driverBPOL.quit();

	}

	private void takeScreenshotOnFailure(Scenario scenario) {
		try {
			WebDriver currentDriver = UIUtils.ui().getCurrentDriver();
			String fileName = UIUtils.ui().takeScreenshot(currentDriver);
			scenario.write("<img src=\"embeddings\\" + fileName + " \" >");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void embedScreenshot(Scenario scenario) {
		int scenarioID = scenario.getId().hashCode();
		String reportImagesDir = System.getProperty(UIUtils.REPORT_IMAGES_DIR);
		try {
			List<Path> listImagesFile = Files.walk(Paths.get(reportImagesDir)).filter(Files::isRegularFile)
					.filter(f -> StringUtils.contains(f.getFileName().toString(), Integer.toString(scenarioID)))
					.collect(toList());
			for (Path path : listImagesFile) {
				scenario.write("<img src=\"embeddings\\" + path.toFile().getName() + " \" >");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Given("^App PP viene avviata$")
	public void app_PP_viene_avviata(List<EnvironmentSettings> dataTable) throws Throwable {

		EnvironmentSettings setting = dataTable.get(0);
		setting.setScenarioId(this.scenarioID);
		TestEnvironment.get().setEnvironment(setting);

		driverName = setting.getDriver();
		// VideoUtilities tool=new VideoUtilities();
		// tool.startVideoRecorder(
		// setting.getDriver(),
		// setting.getTestIdCheck(),
		// setting.getUsername(),
		// setting.getLanguage());

		// TestEnvironment.get().startVideoRecording(driverName);

		// WebDriver driverWeb = (WebDriver)
		// TestEnvironment.get().getDriver(driverName);
		WebDriver driver = (WebDriver) TestEnvironment.get().getDriver(setting.getDriver());
		driverAndroid = driver;
		//		((InteractsWithApps) driverAndroid).resetApp();
		//		location = ((LocationContext) driver).location(); // Must be a driver that implements LocationContext
		Properties pro = UIUtils.ui().getCurrentProperties();
		System.out.println("\n\nNOTE ##### \n" + pro);
		String emu = pro.getProperty("emulator");
		// String emuName = pro.getProperty("android.device.name");
		String emuName = pro.getProperty("ios.udid");
		this.adbExe = pro.getProperty("path.cmd.adb");
		String osType = pro.getProperty("device.os");
		System.out.println("\n\nosType ##### \n" + osType);

		if (osType.equals("ios")) {
			this.isIOS = true;
			((HasSettings) driver).setSetting(Setting.SHOULD_USE_COMPACT_RESPONSES, true);
			((HasSettings) driver).setSetting(Setting.WAIT_FOR_SELECTOR_TIMEOUT, 600000);
			CommandExecutionHelper.execute((ExecutesMethod) driver, setSettingsCommand("customSnapshotTimeout", 0f));
			CommandExecutionHelper.execute((ExecutesMethod) driver, setSettingsCommand("snapshotMaxDepth", 20));
			driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		}

		// ## POPUP # Start # di errore per emulatore con device model fittizio
		//		clickOkPopupEmaulator(emu);
		// ## POPUP # End #

		TestEnvironment.get().startVideoRecording(driverName);

		if (!(driver instanceof IOSDriver)) {
			if (emu.equals("true")) {
				// Ciclo emulatore

				//ConnectionUtils.disableAirModeADB(adbExe, emuName);
				//ConnectionUtils.enableDataADB(adbExe, emuName);

				AdbCommandPrompt adbCmd = new AdbCommandPrompt(adbExe, emuName);
				boolean isWiFi = adbCmd.isWifiEnable();
				if (isWiFi == false) {
					System.out.println("isWiFi==false");
					ConnectionsEmulatorTools.enableWiFiEmulatorNotifiche((AndroidDriver<?>) driverAndroid);
					// ConnectionsEmulatorTools.enableCustomWifi(adbExe, emuName);
					// ConnectionUtils.enableWiFiADB(emuName);
				}

			} else {

				// connectionTools.disableAirModeADB(emuName);
				//ConnectionUtils.enableDataADB(adbExe, emuName);
				// connectionTools.enableWiFiADB(emuName);
				// connectionTools.disableAirMode(driverAndroid);
				// connectionTools.enableData(driverAndroid);
				//ConnectionUtils.enableWiFi((AndroidDriver<?>) driverAndroid);
			}
		} else {
			// Clicchiamo su Consenti una volta

			// Apriamo i Setting
			//			IOSDevicePage setPage = (IOSDevicePage) PageRepo.get().get(IOSDevicePage.NAME);
			//
			//			WaitManager.get().waitMediumTime();
			//			((InteractsWithApps) driverAndroid).runAppInBackground(Duration.ofSeconds(1));
			//			WaitManager.get().waitMediumTime();
			//			((InteractsWithApps) driverAndroid).activateApp("com.apple.Preferences");
			////			setPage.openSetting();
			//			// Verifica e click su Air Mode
			//			if (setPage.isAirModeEnable()) {
			//				setPage.clickOnAirModeToggle();
			//			}
			//			setPage.clickOnWiFiLabel();
			//			if (!setPage.isWifiEnable()) {
			//				setPage.clickOnWifiToggle();
			//			}
			//			setPage.clickOnBackFromWifiSetting();
			//			((InteractsWithApps) driverAndroid).runAppInBackground(Duration.ofSeconds(1));
			//
			//			((InteractsWithApps) driverAndroid).activateApp(pro.getProperty("ios.bundle.id"));
			//			WaitManager.get().waitShortTime();
		}
		try {
			//			WebDriverWait wait = new WebDriverWait(driver, 5);
			//			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@resource-id='posteitaliane.posteapp.apppostepay:id/picker_text_value' and @text='OK']"))).click();
			GenericErrorPopUp gError = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);

			if(!gError.isUpdate()) {
				gError.clickOnNotUpdte();
			}

		} catch (Exception e) {
			// TODO: handle exception
		}
		//		SoftAssertion.get().getAssertions().assertThat("Pippo").isEqualTo("pluto");
	}

	private void clickOkPopupEmaulator(String emuBoolean) {
		// ## POPUP # Start # di errore per emulatore con device model fittizio
		if (!(driverAndroid instanceof IOSDriver)) {
			if (emuBoolean.equals("true")) {
				try {
					WebElement buttonOK = UIUtils.ui().waitForCondition(driverAndroid,
							ExpectedConditions.visibilityOfElementLocated(By.xpath(
									"//*[@resource-id='posteitaliane.posteapp.apppostepay:id/md_buttonDefaultPositive' and @text='OK']")));
					buttonOK.click();
				} catch (Exception e) {
					e.printStackTrace();
				}
			} // ## POPUP # End #
		}
	}

	@Given("^App PP viene avviata versione due$")
	public void app_PP_viene_avviata_versione_due(List<EnvironmentSettings> dataTable) throws Throwable {

		EnvironmentSettings setting = dataTable.get(0);
		setting.setScenarioId(this.scenarioID);

		TestEnvironment.get().setEnvironment(setting);

		WebDriver driver = (WebDriver) TestEnvironment.get().getDriver(setting.getDriver());
		driverAndroid = driver;

		String deviceUsed = setting.getDriver();

		// VideoManagerTool tool=new VideoManagerTool();
		//
		// // CA PART
		// tool.clickOnRECbuttonSTART(
		// driver,
		// setting.getDriver(),
		// setting.getTestIdCheck(),
		// setting.getUsername(),
		// setting.getLanguage());

		VideoUtilities tool = new VideoUtilities();
		tool.startVideoRecorder(setting.getDriver(), setting.getTestIdCheck(), setting.getUsername(),
				setting.getLanguage());

	}

	@When("^Utente inserisce le credenziali nella login page$")
	public void utente_inserisce_le_credenziali_nella_login_page(List<UserDataCredential> dataTable) throws Throwable {

		UserDataCredential table = dataTable.get(0);
		double discrepanza = StringAndNumberOperationTools.convertStringToDouble(table.getDiscrepanza());

		// ## Controlli login # Start # Controlli sulla login, su quale delle tre pagine
		// appare.
		WaitManager.get().waitHighTime();
		try {
			GenericErrorPopUp popupAuth = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);
			if (this.isIOS) {
				//					popupAuth.verifyLayoutOfGpsPopupIos(driverAndroid,
				//							TestEnvironment.get().getLanguage("generic-popup").getProperty("GpsPopupIOSTitle"),
				//							TestEnvironment.get().getLanguage("generic-popup").getProperty("GpsPopUpIOS"), isIOS);
				try {
					String permissionTextButton = "Consenti una volta";
					WaitManager.get().waitShortTime();
					popupAuth.isPermissionRequiredSamsung(permissionTextButton);
				}catch(Exception e) {

				}
				try {
					String permissionTextButton2 = "Consenti";
					WaitManager.get().waitShortTime();
					popupAuth.isPermissionRequiredSamsung(permissionTextButton2);
					WaitManager.get().waitShortTime();
				}catch(Exception e) {

				}
				try {
					String permissionTextButton2 = "Consenti";
					WaitManager.get().waitShortTime();
					popupAuth.isPermissionRequiredSamsung(permissionTextButton2);
					WaitManager.get().waitShortTime();
				}catch(Exception e) {

				}
				try {
					String permissionTextButton3 = "OK";
					WaitManager.get().waitShortTime();
					popupAuth.isPermissionRequiredSamsung(permissionTextButton3);
				}catch(Exception e) {

				}
				try {
					String permissionTextButton2 = "Usa";
					WaitManager.get().waitShortTime();
					popupAuth.isPermissionClickGPSUsa(permissionTextButton2);
					WaitManager.get().waitShortTime();
				}catch(Exception e) {

				}

			} else {
				popupAuth.verifyLayoutOfPopup(driverAndroid, null,
						TestEnvironment.get().getLanguage("generic-popup").getProperty("GpsPopUp"), isIOS);
				String permissionTextButton = "OK";
				WaitManager.get().waitShortTime();
				popupAuth.isPermissionRequiredSamsung(permissionTextButton);
			}

		} catch (Exception e) {
			System.out.println("Non ho cliccato sul popup");
		}
		// System.out.println(driverAndroid.getPageSource());
		try {
			System.out.println("IS - New Access Page");
			loginWithNewAccess(table);
			return;
		} catch (Exception e) {
			e.printStackTrace();
		}
		WaitManager.get().waitMediumTime();
		try {
			System.out.println("IS - Normal Login");
			loginWithLoginPage(table);
			return;
		} catch (Exception e) {
			e.printStackTrace();
		}
		WaitManager.get().waitMediumTime();
		try {
			System.out.println("IS - Pre Login");
			loginWithPrelogin(table);
			return;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void loginWithPrelogin(UserDataCredential table) {
		// System.out.println(driverAndroid.getPageSource());
		PreLoginPostePage accessPage = (PreLoginPostePage) PageRepo.get().get(PreLoginPostePage.NAME);

		//		accessPage.get();
		accessPage.clickOnAccedi();

		LoginPostePage loginPage = (LoginPostePage) PageRepo.get().get(LoginPostePage.NAME);

		loginPage.get();
		loginPage.insertCredential(table.getUsername(), table.getPassword());

	}

	private void loginWithLoginPage(UserDataCredential table) {
		// System.out.println(driverAndroid.getPageSource());
		LoginPostePage loginPage = (LoginPostePage) PageRepo.get().get(LoginPostePage.NAME);

		loginPage.get();
		loginPage.insertCredential(table.getUsername(), table.getPassword());

	}

	private void loginWithNewAccess(UserDataCredential table) {
		System.out.println(driverAndroid.getPageSource());
		// UIUtils.ui().waitForCondition(driverAndroid,
		// ExpectedConditions.visibilityOfElementLocated(
		// By.xpath("(//*[@*='XCUIElementTypeButton' and
		// ./parent::*[@*='XCUIElementTypeOther']])[3]/following-sibling::*[1]")));
		// System.out.println(driverAndroid.findElements(By.xpath("(//*[@*='XCUIElementTypeButton'
		// and
		// ./parent::*[@*='XCUIElementTypeOther']])[3]/following-sibling::*[1]")).get(0).getTagName());
		NewAccessPage accessPage = (NewAccessPage) PageRepo.get().get(NewAccessPage.NAME);

		System.out.println("New Access page GET");
		accessPage.get();
		System.out.println("Click su Non sei Tu");
		accessPage.verifyLayout();
		accessPage.clickOnNonSeiTu();

		LoginPostePage loginPage = (LoginPostePage) PageRepo.get().get(LoginPostePage.NAME);

		loginPage.get();
		loginPage.insertCredential(table.getUsername(), table.getPassword());
	}

	@Given("^Utente esegue la login in App$")
	public void utente_esegue_la_login_in_App(List<UserDataCredential> dataTable) throws Throwable {
		UserDataCredential table = dataTable.get(0);

		LoginPostePage loginPage = (LoginPostePage) PageRepo.get().get(LoginPostePage.NAME);
		loginPage.clickOnAccedi();

		// o esce il posteid o la schermata di abilita prodotto
		boolean abilita = false;
		AbilitaQuestoDispositivoPage abilitaPage = (AbilitaQuestoDispositivoPage) PageRepo.get().get(AbilitaQuestoDispositivoPage.NAME).get();
		abilita = abilitaPage.checkOnb();
		System.out.println("Abilita _> "+abilita);

		//TODO: A.F. - Inserisco caso onb vada ko dopo inserimento cvv, 
		//e venga mostrata la pagina di creazione PosteID

		// Gestisco i due casi
		if (!abilita) {

			boolean creaPosteId=false;

			AbilitaInAppPage onbPage = (AbilitaInAppPage) PageRepo.get().get(AbilitaInAppPage.NAME);

			System.out.println("Controllo presenza crea Poste ID");
			creaPosteId=onbPage.isCreaPosteIdVisible();

			if(creaPosteId)
			{
				System.out.println("crea poste Id presente --> procedo con la creazione del posteId");
				WaitManager.get().waitMediumTime();
				onbPage.createPosteID(table.getPosteId());
			}
			else
			{
				System.out.println("abilita == null && creaPosteId == null mi aspetto l'inserimento del posteId 657");

				LoginWithPosteId loginPosteId = (LoginWithPosteId) PageRepo.get().get(LoginWithPosteId.NAME);

				loginPosteId.verifyHeaderPage("Inserisci Codice PosteID");
				loginPosteId.verifyLayout();
				//					loginPosteId.verifyHelloName(table.getName());
				loginPosteId.insertPosteId(table.getPosteid());

				loginPosteId.clickOnConfirmButton();
			}

		} else {
			System.out.println("abilita != null mi aspetto l'onbrding");
			// Se abilita è diverso da Null allora sono sulla pagina di onboarding					
			//					Abbiamo due scenario
			//					1 Bisogna buttare a terra un device
			//					2 L'utenza è pulita
			AbilitaInAppPage onbPage = (AbilitaInAppPage) PageRepo.get().get(AbilitaInAppPage.NAME);
			try {
				abilitaPage.clickOnInizia();
				//						caso in cui prima devo cancellare il device
				//						onbPage.clickOnNonHoaccessoAlPreferito();
			} catch (Exception e) {
				// TODO: handle exception
			}

			try {
				onbPage.clickOnDeleteDevice();
			} catch (Exception e) {
				// TODO: handle exception
			}

			// Continuo il flusso di onbording da Inizia (Schermata Onb con descrizione)
			//					AbilitaInAppPage onbPage = (AbilitaInAppPage) PageRepo.get().get(AbilitaInAppPage.NAME);

			try {
				System.out.println("--> start onbPage.clickOnOnbDevice");
				onbPage.clickOnOnbDevice(TestEnvironment.get().getLanguage(table.getName()).getProperty("carta"));
				WaitManager.get().waitMediumTime();

			} catch (Exception e) {
				System.out.println(">>>> ERROE Su clickOnOnbDevice");
			}

			try {
				onbPage.clickOnProseguiButton();
			} catch (Exception e) {
				System.out.println(">>>> ERROE Su clickOnProseguiButton");
			}

			try {
				onbPage.createPosteID(table.getPosteId());
			} catch (Exception e) {
				System.out.println(">>>> ERROE Su createPosteID");
			}
		}

		//		System.out.println("abilita == null mi aspetto l'inserimento del posteId");
		//		
		//		LoginWithPosteId loginPosteId = (LoginWithPosteId) PageRepo.get().get(LoginWithPosteId.NAME).get();
		//		loginPosteId.verifyHeaderPage("Inserisci Codice PosteID");
		//		loginPosteId.insertPosteId(table.getPosteid());
		//		loginPosteId.clickOnConfirmButton();


		// WaitManager.get().waitHighTime();
		WaitManager.get().waitMediumTime();

		try {
			WaitManager.get().waitShortTime();
			//			driverAndroid.findElement(By.xpath("//*[contains(@text,'NON MOSTRARE')]")).click();
			CoreUtility.visibilityOfElement(driverAndroid, By.xpath("//*[contains(@text,'NON MOSTRARE')]"), 20).click();
			WaitManager.get().waitShortTime();
		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			GenericErrorPopUp errorPopUp = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);
			WaitManager.get().waitShortTime();
			errorPopUp.clickOnNonMostrarePiu();
			WaitManager.get().waitShortTime();
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	@Given("^Utente effettua il logout dall app$")
	public void utente_effettua_il_logout_dall_app() throws Throwable {
		// ((PressesKey) driverAndroid).pressKey(new KeyEvent(AndroidKey.HOME));
		// WaitManager.get().waitShortTime();
		// ((PressesKey) driverAndroid).pressKey(new KeyEvent(AndroidKey.APP_SWITCH));
		// WaitManager.get().waitMediumTime();
		//
		// Properties devName = UIUtils.ui().getCurrentProperties();
		// AdbCommandPrompt cmd=new AdbCommandPrompt(adbExe,
		// devName.getProperty("ios.udid"));
		// cmd.swipe(300, 1200, 300, 600, 10);
		// WaitManager.get().waitShortTime();

		driverAndroid.quit();
	}

	@Then("^Compare il messaggio di errore per la connessione assente$")
	public void then_compare_il_messaggio_di_errore_per_la_connessione_assente() throws Throwable {
		GenericErrorPopUp errorPopUp = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);

		//		errorPopUp.verifyHeaderPopUp();
		//		errorPopUp.verifyCopyPopUp();
		if (this.isIOS) {
			try {
				errorPopUp.verifyLayoutOfPopupIOS(driverAndroid, "Precisione della posizione",
						"Attivando il Wi-Fi, la precisione della posizione sarà migliore.", this.isIOS);
				errorPopUp.closeAtacPopup();
			}catch(Exception e) {

			}
			errorPopUp.verifyLayoutOfPopupIOS(driverAndroid, "Avviso",
					"Il servizio non è al momento disponibile. Ti preghiamo di riprovare più tardi", this.isIOS);
		} else {
			errorPopUp.verifyLayoutOfPopup(driverAndroid, "Avviso",
					"Sembra che la tua connessione internet non sia attiva (404). Ti invitiamo a verificare le impostazioni del dispositivo o a riprovare più tardi. ",
					this.isIOS);
		}
		errorPopUp.clickOnOKPopUp();
	}

	@When("^Utente atterra sulla pagina del posteId dopo il resume$")
	public void utente_atterra_sulla_pagina_del_posteId_dopo_il_resume(List<UserDataCredential> dataTable)
			throws Throwable {
		UserDataCredential dataLayout = dataTable.get(0);
		double discrepanza = StringAndNumberOperationTools.convertStringToDouble(dataLayout.getDiscrepanza());

		try {
			// Controllo dopo il resume
			HomePage home = (HomePage) PageRepo.get().get(HomePage.NAME);

			home.clickOnPaymentTabIcon();

		} catch (Exception e) {
			// TODO: handle exception
		}

		// ## POPUP # Start # di errore per emulatore con device model fittizio
		Properties pro = UIUtils.ui().getCurrentProperties();
		System.out.println("\n\nNOTE ##### \n" + pro);
		String emu = pro.getProperty("emulator");
		clickOkPopupEmaulator(emu);
		// ## POPUP # End #

		NewAccessPage newAccessPage = (NewAccessPage) PageRepo.get().get(NewAccessPage.NAME);
		newAccessPage.get();
		newAccessPage.verifyLayout();

		WaitManager.get().waitMediumTime();

	}

	@When("^Utente atterra sulla pagina del posteId$")
	public void utente_atterra_sulla_pagina_del_posteId() throws Throwable {

		NewAccessPage accessPage = (NewAccessPage) PageRepo.get().get(NewAccessPage.NAME);

		accessPage.clickOnAccedi();

		LoginWithPosteId loginPosteId = (LoginWithPosteId) PageRepo.get().get(LoginWithPosteId.NAME).get();

		loginPosteId.verifyHeaderPage(TestEnvironment.get().getLanguage("header").getProperty("LoginWithPosteId"));

		WaitManager.get().waitMediumTime();

	}

	@When("^Utente clicca su accedi$")
	public void utente_clicca_su_accedi() throws Throwable {
		LoginPostePage loginPage = (LoginPostePage) PageRepo.get().get(LoginPostePage.NAME);

		loginPage.clickOnAccedi();
	}

	@When("^Utente clicca su accedi da offline$")
	public void utente_clicca_su_accedi_da_offline() throws Throwable {

		Properties pro = UIUtils.ui().getCurrentProperties();
		String emu = pro.getProperty("emulator");
		// String emuName = pro.getProperty("android.device.name");
		String emuName = pro.getProperty("ios.udid");
		this.adbExe = pro.getProperty("path.cmd.adb");

		if (this.isIOS) {
			LoginPostePage loginPage = (LoginPostePage) PageRepo.get().get(LoginPostePage.NAME);
			loginPage.clickOnAccedi();

		} else {
			if (emu.equals("true")) {
				// new
				// TouchAction(driverAndroid).tap(PointOption.point(p.x,p.y)).release().perform();

				Runtime.getRuntime()
				.exec("adb -s " + emuName + " shell input tap " + pointSignInX + " " + pointSignInY);

				// LoginPostePage loginPage=(LoginPostePage)
				// PageRepo.get().get(LoginPostePage.NAME);
				// loginPage.clickOnAccedi();

				WaitManager.get().waitLongTime();

			} else {
				LoginPostePage loginPage = (LoginPostePage) PageRepo.get().get(LoginPostePage.NAME);

				loginPage.clickOnAccedi();
			}
		}
	}

	@When("^Utente inserisce il posteId$")
	public void utente_inserisce_il_posteId(List<UserDataCredential> dataDable) throws Throwable {

		UserDataCredential table = dataDable.get(0);

		// o esce il posteid o la schermata di abilita prodotto
		boolean abilita = false;
		AbilitaQuestoDispositivoPage abilitaPage = (AbilitaQuestoDispositivoPage) PageRepo.get().get(AbilitaQuestoDispositivoPage.NAME);
		abilita = abilitaPage.checkOnb();
		System.out.println("Abilita _> "+abilita);

		// Gestisco i due casi
		if (!abilita) {

			boolean creaPosteId=false;

			AbilitaInAppPage onbPage = (AbilitaInAppPage) PageRepo.get().get(AbilitaInAppPage.NAME);

			System.out.println("Controllo presenza crea Poste ID");
			creaPosteId=onbPage.isCreaPosteIdVisible();

			if(creaPosteId)
			{
				System.out.println("crea poste Id presente --> procedo con la creazione del posteId");
				WaitManager.get().waitMediumTime();
				onbPage.createPosteID(table.getPosteId());
			}
			else
			{
				System.out.println("abilita == null & creaPosteId==null mi aspetto l'inserimento del posteId 857");

				LoginWithPosteId loginPosteId = (LoginWithPosteId) PageRepo.get().get(LoginWithPosteId.NAME);

				loginPosteId.verifyHeaderPage("Inserisci Codice PosteID");
				loginPosteId.verifyLayout();
				loginPosteId.verifyHelloName(table.getName());
				loginPosteId.insertPosteId(table.getPosteid());

				loginPosteId.clickOnConfirmButton();
			}

		} else {
			System.out.println("abilita != null mi aspetto l'onbrding");
			// Se abilita è diverso da Null allora sono sulla pagina di onboarding					
			//			Abbiamo due scenario
			//			1 Bisogna buttare a terra un device
			//			2 L'utenza è pulita
			AbilitaInAppPage onbPage = (AbilitaInAppPage) PageRepo.get().get(AbilitaInAppPage.NAME);
			try {
				abilitaPage.clickOnInizia();
				//				caso in cui prima devo cancellare il device
				//				onbPage.clickOnNonHoaccessoAlPreferito();
				onbPage.clickOnDeleteDevice();
			} catch (Exception e) {
				// TODO: handle exception
			}
			// Continuo il flusso di onbording da Inizia (Schermata Onb con descrizione)
			//			AbilitaInAppPage onbPage = (AbilitaInAppPage) PageRepo.get().get(AbilitaInAppPage.NAME);
			System.out.println("--> start onbPage.clickOnOnbDevice");

				onbPage.clickOnOnbDevice(TestEnvironment.get().getLanguage(table.getName()).getProperty("carta"));

			WaitManager.get().waitMediumTime();
			//	onbPage.createPosteID(table.getPosteId());
			try {
				onbPage.clickOnProseguiButton();
			} catch (Exception e) {
				// TODO: handle exception
			}
			try {
				onbPage.createPosteID(table.getPosteId());
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
	}

	@When("^Utente clicca sul bottone conferma$")
	public void utente_clicca_sul_bottone_conferma() throws Throwable {

		//		LoginWithPosteId loginPosteId = (LoginWithPosteId) PageRepo.get().get(LoginWithPosteId.NAME);
		//
		//		loginPosteId.clickOnConfirmButton();

		// WaitManager.get().waitHighTime();
		WaitManager.get().waitHighTime();

		PopUpTheread popupRandom = new PopUpTheread();
		threadPopup = new Thread((Runnable) popupRandom);

		try {
			driverAndroid.findElement(By.xpath("//*[contains(@text,'NON MOSTRARE')]")).click();
			WaitManager.get().waitShortTime();
		} catch (Exception e) {
			// TODO: handle exception
		}
		// threadPopup.start();
		GenericErrorPopUp popup=(GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);
		if(isIOS) {
			popup.clickOnApplePayPopupCloseBtn();			
		}

		try {	
			popup.clickOnChiudiRicaricaDopoOnb();
		} catch (Exception e) {
			// TODO: handle exception
		}
		try {	
			popup.clickOnNonMostrarePiu();
		} catch (Exception e) {
			// TODO: handle exception
		}


	}

	@Then("^Utente atterra sulla \"([^\"]*)\"$")
	public void utente_atterra_sulla(String landingPage, List<LayoutDataBean> dataDable) throws Throwable {
		GenericErrorPopUp rating = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);

		// List<UserDataCredential> dataTable, List<LayoutDataBean> dataDable
		/*
		 * HomePage
		 */
		if (landingPage.equals("Homepage")) {
			// try {Thread.sleep(3000);} catch (Exception err ) {}
			WaitManager.get().waitLongTime();

			HomePage homePage = (HomePage) PageRepo.get().get(HomePage.NAME);
			// try {Thread.sleep(1000);} catch (Exception err ) {}
			GenericErrorPopUp popup=(GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);
			popup.clickOnApplePayPopupCloseBtn();
			homePage.verifyHeaderLogoHomepage();

		}
		/*
		 * WebView di Registrazione
		 */
		else if (landingPage.equals("WebView")) {
			WaitManager.get().waitLongTime();

			WebviewGenericPage webviewGenericPage = (WebviewGenericPage) PageRepo.get().get(WebviewGenericPage.NAME);
			WaitManager.get().waitShortTime();
		}
		/*
		 * Inviare P2P
		 */
		else if (landingPage.equals("TnkP2p") || landingPage.equals("TnkP2Speed")) {
			WaitManager.get().waitShortTime();

			SendP2PPageStep5 tnkYouPageP2p = (SendP2PPageStep5) PageRepo.get().get(SendP2PPageStep5.NAME);

			tnkYouPageP2p.clickOnCloseButton();

			WaitManager.get().waitMediumTime();

			try {
				rating.isRatingPopUp();
			} catch (Exception e) {
				// TODO: handle exception
			}

			// Ciclo di controllo per la pagina dove si atterra
			if (landingPage.equals("TnkP2p")) {
				CommunityCardOnePage communityPage = (CommunityCardOnePage) PageRepo.get()
						.get(CommunityCardOnePage.NAME);

				communityPage.verifyHeaderPage();
			} else if (landingPage.equals("TnkP2pSpeed")) {
				HomePage homePage = (HomePage) PageRepo.get().get(HomePage.NAME).get();

				homePage.verifyHeaderLogoHomepage();
			} else {
				System.out.println("La pagina dopo la TNK PAGE non e definita correttamente");
			}
		}
		/*
		 * Ricevere P2P
		 */
		else if (landingPage.equals("TnkP2pRequest")) {
			WaitManager.get().waitShortTime();

			RequestP2PPageStep3 tnkYouPageP2pRequest = (RequestP2PPageStep3) PageRepo.get()
					.get(RequestP2PPageStep3.NAME);

			tnkYouPageP2pRequest.clickOnCloseButton();

			WaitManager.get().waitMediumTime();

			try {
				rating.isRatingPopUp();
			} catch (Exception e) {
				// TODO: handle exception
			}

			CommunityCardOnePage communityPage = (CommunityCardOnePage) PageRepo.get().get(CommunityCardOnePage.NAME);

			communityPage.verifyHeaderPage();
		}
		/*
		 * Ricevere P2P Da Utente NON P2P - THK Y PAGE
		 */
		else if (landingPage.equals("TnkNoP2pUser")) {
			WaitManager.get().waitMediumTime();

			NoP2pContactThankYouPage tnkYouPageNoP2pUser = (NoP2pContactThankYouPage) PageRepo.get()
					.get(NoP2pContactThankYouPage.NAME);

			tnkYouPageNoP2pUser.verifyLayoutOfTkp();

			tnkYouPageNoP2pUser.clickOnInviteYouFriend();

			// Clicca sul buttone indietro del device per chiudere la popup di scelta di
			// invito
			if (this.isIOS) {
				tnkYouPageNoP2pUser.clickOnChiudiInvitaAmici();
			} else {
				WebDriver driver = (WebDriver) TestEnvironment.get().getDriver(driverName);
				((PressesKey) driverAndroid).pressKey(new KeyEvent(AndroidKey.BACK));
			}
			tnkYouPageNoP2pUser.clickOnBankButton();
		}
		/*
		 * THK Y PAGE GENERICA - Per diversi flussi - Bonifico SEPA
		 */
		else if (landingPage.equals("TnkGeneric")) {
			WaitManager.get().waitLongTime();
			System.out.println(driverAndroid.getPageSource());

			BankTranfertSEPAPageStep5 tnkYouPage = (BankTranfertSEPAPageStep5) PageRepo.get()
					.get(BankTranfertSEPAPageStep5.NAME);

			WaitManager.get().waitShortTime();

			tnkYouPage.verifyLayout();

			tnkYouPage.clickOnButtonClose();

			try {
				rating.isRatingPopUp();
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		/*
		 * THK Y PAGE MIT 
		 */
		else if (landingPage.equals("TnkMIT")) {
			WaitManager.get().waitLongTime();
			System.out.println(driverAndroid.getPageSource());

			BankTranfertSEPAPageStep5 tnkYouPage = (BankTranfertSEPAPageStep5) PageRepo.get()
					.get(BankTranfertSEPAPageStep5.NAME);

			WaitManager.get().waitShortTime();

			tnkYouPage.typPraticaMIT();
		}
		/*
		 * THK Y PAGE GENERICA - Per diversi flussi - RICARICA LA MIA E ALTRA POSTEPAY
		 */
		else if (landingPage.equals("TnkRecharge")) {
			WaitManager.get().waitLongTime();

			RechargeMyPostepayPageStep6 tnkYouPage = (RechargeMyPostepayPageStep6) PageRepo.get()
					.get(RechargeMyPostepayPageStep6.NAME);

			tnkYouPage.verifyLayout(TestEnvironment.get().getLanguage("typ-text"));

			tnkYouPage.clickOnButtonClose();

			try {
				rating.isRatingPopUp();
			} catch (Exception e) {
				// TODO: handle exception
			}
		}

		else if (landingPage.equals("TnkTelephoneRecharge")) {
			WaitManager.get().waitLongTime();

			ThelephoneRechargePageStep3 tnkYouPage = (ThelephoneRechargePageStep3) PageRepo.get()
					.get(ThelephoneRechargePageStep3.NAME);

			tnkYouPage.verifySummaryTypTelephone();

			try {
				rating.isRatingPopUp();
			} catch (Exception e) {
				// TODO: handle exception
			}
		} else if (landingPage.equals("TnkBolloAuto")) {
			System.out.println("TODO CONTROLLO SU TYP QUANDO SARA VISIBILE PER BOLLO AUTO");
		}
		/*
		 * THK Y PAGE RIACARICA ALTRA RICORRENTE
		 */
		else if (landingPage.equals("TnkRechargeAutomatic")) {
			WaitManager.get().waitLongTime();

			AutomaticRechargePostepayThankYouPage tnkYouPage = (AutomaticRechargePostepayThankYouPage) PageRepo.get()
					.get(AutomaticRechargePostepayThankYouPage.NAME);

			tnkYouPage.verifyLayout();

			tnkYouPage.clickOnButtonClose();

			try {
				rating.isRatingPopUp();
			} catch (Exception e) {
				// TODO: handle exception
			}
		}

		/*
		 * FIRST PAGE DI RIACARICA ALTRA RICORRENTE
		 */
		else if (landingPage.equals("firtRechargeAutomatic")) {
			WaitManager.get().waitLongTime();

			AutomaticRechargeOnZeroRechargePage firstRechargeAutom = (AutomaticRechargeOnZeroRechargePage) PageRepo
					.get().get(AutomaticRechargeOnZeroRechargePage.NAME);

			firstRechargeAutom.verifyLayout();

			firstRechargeAutom.clickOnBackButton();
		}

		/*
		 * TNK page per il G2G
		 */

		else if (landingPage.equals("TnkG2G")) {
			LayoutDataBean dataLayout = dataDable.get(0);
			double discrepanza = StringAndNumberOperationTools.convertStringToDouble(dataLayout.getDiscrepanza());

			SendG2GPageStep5 tnkG2G = (SendG2GPageStep5) PageRepo.get().get(SendG2GPageStep5.NAME);

			tnkG2G.verifyLayoutPage();
			WaitManager.get().waitMediumTime();;
			// tnkG2G.verifyImageCard(driverAndroid, discrepanza);
			tnkG2G.clickOnClose();

			try {
				rating.isRatingPopUp();
			} catch (Exception e) {
				// TODO: handle exception
			}
		} else if (landingPage.equals("TkpG2GPurchase")) {

			ThankYouPageConnectSimPurchaseGigaPage tnkG2G = (ThankYouPageConnectSimPurchaseGigaPage) PageRepo.get()
					.get(ThankYouPageConnectSimPurchaseGigaPage.NAME);

			tnkG2G.verifyLayoutPage();
			tnkG2G.clickOnClose();

			try {
				rating.isRatingPopUp();
			} catch (Exception e) {
				// TODO: handle exception
			}

			try {
				tnkG2G.clickOnClose();
			} catch (Exception e) {
				// TODO: handle exception
			}
		} else if (landingPage.equals("TnkNoG2gUser")) {
			SendG2GPageStep5 noG2G = (SendG2GPageStep5) PageRepo.get().get(SendG2GPageStep5.NAME);
			noG2G.verifyTYPNoG2g();
		}
		else if (landingPage.equals("TnkTelephoneRecharge")) {
			WaitManager.get().waitLongTime();
			System.out.println(driverAndroid.getPageSource());

			BankTranfertSEPAPageStep5 tnkYouPage = (BankTranfertSEPAPageStep5) PageRepo.get()
					.get(BankTranfertSEPAPageStep5.NAME);

			WaitManager.get().waitShortTime();

			tnkYouPage.verifyLayoutDinamic(
					TestEnvironment.get().getLanguage("typ-text").getProperty("TypTitleTelefono"),
					TestEnvironment.get().getLanguage("typ-text").getProperty("TypDescTelefono"),
					TestEnvironment.get().getLanguage("typ-text").getProperty("btnChiudi")
					);
			tnkYouPage.verifyLinkPresenza(TestEnvironment.get().getLanguage("typ-text").getProperty("LinkRicaricaTelefonica"));


			tnkYouPage.clickOnButtonClose();

			try {
				rating.isRatingPopUp();
			} catch (Exception e) {
				// TODO: handle exception
			}
		}

	}

	@Then("^Utente chiude App PP$")
	public void utente_chiude_App_PP() throws Throwable {
		// // Logica per stoppare il video recorder
		// /////////////////////////////////////////////////////////////////////////////////////////
		// EnvironmentSettings setting=dataTable.get(0);
		//
		// WebDriver driver=(WebDriver)
		// TestEnvironment.get().getDriver(setting.getDriver());
		//
		// String deviceUsed=setting.getDriver();
		// String testId=setting.getTestIdCheck();
		// String user=setting.getUsername();
		// String language=setting.getLanguage();
		//
		// VideoManagerTool tool=new VideoManagerTool();
		// tool.clickOnRECbuttonSTOP(driver, deviceUsed, testId, user, language);
		// ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	}

	@When("^Utente mette offline il device$")
	public void utente_mette_offline_il_device(List<EnvironmentSettings> dataTable) throws Throwable {

		WaitManager.get().waitShortTime();

		Particle btnSignIn = (Particle) UiObjectRepo.get().get(CredentialsCardLogin.SIGNINBUTTONLOGIN);
		pointSignIn = btnSignIn.getElement().getLocation();
		Dimension sizeSignIn = btnSignIn.getElement().getSize();

		pointSignInX = pointSignIn.x + (sizeSignIn.getWidth() / 2);
		pointSignInY = pointSignIn.y + (sizeSignIn.getHeight() / 2);

		ConnectionUtils connectionTools = new ConnectionUtils();

		Properties pro = UIUtils.ui().getCurrentProperties();
		String emu = pro.getProperty("emulator");
		// String emuName = pro.getProperty("android.device.name");
		String emuName = pro.getProperty("ios.udid");

		if (this.isIOS) {
			System.out.println("isIOS == true");

			IOSDevicePage setPage = (IOSDevicePage) PageRepo.get().get(IOSDevicePage.NAME);

			((InteractsWithApps) driverAndroid).runAppInBackground(Duration.ofSeconds(-1));
			WaitManager.get().waitMediumTime();
			((InteractsWithApps) driverAndroid).activateApp("com.apple.Preferences");
			//			setPage.openSetting();

			setPage.clickOnWiFiLabel();
			if (setPage.isWifiEnable()) {
				setPage.clickOnWifiToggle();
			}
			setPage.clickOnBackFromWifiSetting();
			((InteractsWithApps) driverAndroid).runAppInBackground(Duration.ofSeconds(-1));

			((InteractsWithApps) driverAndroid).activateApp(pro.getProperty("ios.bundle.id"));
			WaitManager.get().waitShortTime();

		} else {
			if (emu.equals("true")) {
				// Ciclo emulatore
				// connectionTools.disableWiFiADB(emuName);
				connectionTools.disableDataADB(adbExe, emuName);
				// connectionTools.disableWiFiADB(adbExe, emuName);
				ConnectionsEmulatorTools.disableWiFiEmulatorNotifiche((AndroidDriver<?>) driverAndroid);
				// ConnectionsEmulatorTools.disableCustomWifi(adbExe, emuName);
			} else {
				// Ciclo device fisico
				connectionTools.disableWiFi((AndroidDriver<?>) driverAndroid);
				connectionTools.disableDataADB(adbExe, emuName);
			}
		}

		WaitManager.get().waitLongTime();
		// new TouchAction(driver).tap(PointOption.point(p.x,p.y)).release().perform();
	}

	@Then("^Utente mette online il device$")
	public void utente_mette_online_il_device(List<EnvironmentSettings> dataTable) throws Throwable {
		EnvironmentSettings setting = dataTable.get(0);

		// WebDriver driver=(WebDriver)
		// TestEnvironment.get().getDriver(setting.getDriver());

		WaitManager.get().waitShortTime();

		ConnectionUtils connectionTools = new ConnectionUtils();

		Properties pro = UIUtils.ui().getCurrentProperties();
		String emu = pro.getProperty("emulator");
		// String emuName = pro.getProperty("android.device.name");
		String emuName = pro.getProperty("ios.udid");

		if (this.isIOS) {
			System.out.println("isIOS == true");

			IOSDevicePage setPage = (IOSDevicePage) PageRepo.get().get(IOSDevicePage.NAME);

			((InteractsWithApps) driverAndroid).runAppInBackground(Duration.ofSeconds(-1));
			WaitManager.get().waitMediumTime();
			((InteractsWithApps) driverAndroid).activateApp("com.apple.Preferences");
			//			setPage.openSetting();

			setPage.clickOnWiFiLabel();
			if (!setPage.isWifiEnable()) {
				setPage.clickOnWifiToggle();
			}
			setPage.clickOnBackFromWifiSetting();
			((InteractsWithApps) driverAndroid).runAppInBackground(Duration.ofSeconds(-1));

			((InteractsWithApps) driverAndroid).activateApp(pro.getProperty("ios.bundle.id"));
			WaitManager.get().waitShortTime();

		} else {
			if (emu.equals("true")) {
				// Ciclo emulatore
				// connectionTools.enableWiFiADB(emuName);
				connectionTools.enableDataADB(adbExe, emuName);
				// connectionTools.enableWiFiADB(adbExe, emuName);
				ConnectionsEmulatorTools.enableWiFiEmulatorNotifiche((AndroidDriver<?>) driverAndroid);
				// ConnectionsEmulatorTools.enableCustomWifi(adbExe, emuName);
				// connectionTools.enableWiFi(driverAndroid);
			} else {
				// Ciclo device fisico
				connectionTools.enableWiFi((AndroidDriver<?>) driverAndroid);
			}
		}
	}

	@Then("^Utente mette online il device da modalita aerea$")
	public void utente_mette_online_il_device_da_modalita_aerea(List<EnvironmentSettings> dataTable) throws Throwable {
		EnvironmentSettings setting = dataTable.get(0);

		// WebDriver driver=(WebDriver)
		// TestEnvironment.get().getDriver(setting.getDriver());

		WaitManager.get().waitShortTime();

		Properties pro = UIUtils.ui().getCurrentProperties();
		String emu = pro.getProperty("emulator");
		// String emuName = pro.getProperty("android.device.name");
		String emuName = pro.getProperty("ios.udid");

		if (this.isIOS) {
			IOSDevicePage setPage = (IOSDevicePage) PageRepo.get().get(IOSDevicePage.NAME);
			GenericErrorPopUp popup = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);

			WaitManager.get().waitMediumTime();
			((InteractsWithApps) driverAndroid).activateApp("com.apple.Preferences");
			//			setPage.openSetting();
			// Verifica e click su Air Mode
			if (setPage.isAirModeEnable()) {
				setPage.clickOnAirModeToggle();
				popup.isPermissionGeneric();
			}

			((InteractsWithApps) driverAndroid).runAppInBackground(Duration.ofSeconds(-1));

			WaitManager.get().waitShortTime();

		} else {
			if (emu.equals("true")) {
				// Ciclo emulatore
				ConnectionsEmulatorTools connectionTools = new ConnectionsEmulatorTools();
				// connectionTools.disableAirModeADB(emuName);
				// connectionTools.enableDataADB(emuName);
				connectionTools.disableAirModeEmulator((AndroidDriver<?>) driverAndroid);

			} else {
				// Ciclo device fisico
				// ConnectionUtils.disableAirMode(driverAndroid);
				// ConnectionUtils.enableWiFi(driverAndroid);
				//ConnectionUtils.disableAirModeADB(adbExe, emuName);
			}
		}
	}

	@Then("^Utente mette in modalità aereo il device per quindici minuti$")
	public void utente_mette_in_modalità_aereo_il_device_per_quindici_minuti(List<EnvironmentSettings> dataTable)
			throws Throwable {
		// EnvironmentSettings setting=dataTable.get(0);

		// WebDriver driver=(WebDriver)
		// TestEnvironment.get().getDriver(setting.getDriver());

		WaitManager.get().waitShortTime();

		Properties pro = UIUtils.ui().getCurrentProperties();
		String emu = pro.getProperty("emulator");
		// String emuName = pro.getProperty("android.device.name");
		String emuName = pro.getProperty("ios.udid");

		if (this.isIOS) {

			IOSDevicePage setPage = (IOSDevicePage) PageRepo.get().get(IOSDevicePage.NAME);

			WaitManager.get().waitMediumTime();
			((InteractsWithApps) driverAndroid).runAppInBackground(Duration.ofSeconds(-1));
			WaitManager.get().waitMediumTime();
			((InteractsWithApps) driverAndroid).activateApp("com.apple.Preferences");
			//			setPage.openSetting();
			// Verifica e click su Air Mode
			if (!setPage.isAirModeEnable()) {
				setPage.clickOnAirModeToggle();
			}
			((InteractsWithApps) driverAndroid).runAppInBackground(Duration.ofSeconds(-1));

			WaitManager.get().waitShortTime();

		} else {
			if (emu.equals("true")) {
				// Ciclo emulatore
				ConnectionsEmulatorTools connectionTools = new ConnectionsEmulatorTools();
				// System.out.println(emuName);
				// connectionTools.disableDataADB(emuName);
				// connectionTools.enableAirModeADB(emuName);
				// connectionTools.enableAirMode(driverAndroid);
				connectionTools.enableAirModeEmulator((AndroidDriver<?>) driverAndroid);
			} else {
				// Ciclo device fisico
				//ConnectionUtils.enableAirModeADB(adbExe, emuName);
			}
			((PressesKey) driverAndroid).pressKey(new KeyEvent(AndroidKey.HOME));
		}

		WaitManager.get().waitFor(TimeUnit.MINUTES, 15);
	}

	@Then("^Utente esegue il resume del App$")
	public void utente_esegue_il_resume_del_App(List<EnvironmentSettings> dataTable) throws Throwable {
		EnvironmentSettings table = dataTable.get(0);
		Properties pro = UIUtils.ui().getCurrentProperties();
		String emu = pro.getProperty("emulator");

		if (this.isIOS) {
			WaitManager.get().waitShortTime();
			((InteractsWithApps) driverAndroid).activateApp(pro.getProperty("ios.bundle.id"));
			WaitManager.get().waitShortTime();
		} else {
			// Fare il resume dell' app
			((PressesKey) driverAndroid).pressKey(new KeyEvent(AndroidKey.APP_SWITCH));
			WaitManager.get().waitLongTime();

			if (emu.equals("true")) {
				WebElement card = UIUtils.ui().waitForCondition(driverAndroid,
						ExpectedConditions.visibilityOfElementLocated(
								By.xpath("//*[@resource-id='com.android.systemui:id/task_view_thumbnail']")));
				card.click();
				WaitManager.get().waitLongTime();
			} else {
				WebElement card = null;
				switch (table.getDriver()) {

				case "OnePlus":
					card = UIUtils.ui().waitForCondition(driverAndroid, ExpectedConditions.visibilityOfElementLocated(
							By.xpath("//*[@resource-id='net.oneplus.launcher:id/snapshot']")));
					card.click();
					WaitManager.get().waitLongTime();
					break;

				case "RedMiNote8":

					card = UIUtils.ui().waitForCondition(driverAndroid, ExpectedConditions.visibilityOfElementLocated(
							By.xpath("//*[@resource-id='com.android.systemui:id/task_view_thumbnail']")));
					card.click();
					WaitManager.get().waitLongTime();
					break;

				default:
					System.out.println("Device Default");
					Properties devName = UIUtils.ui().getCurrentProperties();

					AdbCommandPrompt cmd = new AdbCommandPrompt(adbExe, devName.getProperty("ios.udid"));
					cmd.tap(300, 1200);

					WaitManager.get().waitLongTime();
					break;
				}
			}
		}
	}

	@Then("^Utente mette in background il device per quindici minuti$")
	public void utente_mette_in_background_il_device_per_quindici_minuti(List<EnvironmentSettings> dataTable)
			throws Throwable {
		EnvironmentSettings setting = dataTable.get(0);
		Properties pro = UIUtils.ui().getCurrentProperties();
		String emu = pro.getProperty("emulator");

		//		if(this.isIOS) {
		//			WaitManager.get().waitMediumTime();
		//			((InteractsWithApps) driverAndroid).runAppInBackground(Duration.ofSeconds(-1));
		//			
		//			WaitManager.get().waitFor(TimeUnit.MINUTES, 15);
		//			
		//			((InteractsWithApps) driverAndroid).activateApp(pro.getProperty("ios.bundle.id"));
		//			WaitManager.get().waitShortTime();
		//			
		//		} else {
		//			((PressesKey) driverAndroid).pressKey(new KeyEvent(AndroidKey.HOME));
		//			WaitManager.get().waitFor(TimeUnit.MINUTES, 15);
		//
		//			((PressesKey) driverAndroid).pressKey(new KeyEvent(AndroidKey.APP_SWITCH));
		//			WaitManager.get().waitLongTime();
		//
		//			if (emu.equals("true")) {
		//				WebElement card=UIUtils.ui().waitForCondition(driverAndroid, 
		//						ExpectedConditions.visibilityOfElementLocated(
		//								By.xpath(
		//										"//*[@resource-id='com.android.systemui:id/task_view_thumbnail']"
		//										)));
		//				card.click();
		//				WaitManager.get().waitLongTime();
		//			} 
		//			else {
		//				switch (setting.getDriver()) {
		//				case "OnePlus":		
		//					WebElement card=UIUtils.ui().waitForCondition(driverAndroid, 
		//							ExpectedConditions.visibilityOfElementLocated(
		//									By.xpath(
		//											"//*[@resource-id='net.oneplus.launcher:id/snapshot']"
		//											)));
		//					card.click();
		//					WaitManager.get().waitLongTime();
		//
		//					break;
		//
		//				case "RedMiNote8":
		//					card=UIUtils.ui().waitForCondition(driverAndroid, 
		//							ExpectedConditions.visibilityOfElementLocated(
		//									By.xpath(
		//											"//*[@resource-id='com.android.systemui:id/task_view_thumbnail']"
		//											)));
		//					card.click();
		//					WaitManager.get().waitLongTime();
		//
		//					break; 
		//
		//				case "GalaxyS10":
		//					card=UIUtils.ui().waitForCondition(driverAndroid, 
		//							ExpectedConditions.visibilityOfElementLocated(
		//									By.xpath(
		//											"//*[@resource-id='com.sec.android.app.launcher:id/icon']"
		//											)));
		//					card.click();
		//					WaitManager.get().waitLongTime();
		//
		//					break;
		//
		//				default: 
		//					System.out.println("Device Default");
		//					Properties devName = UIUtils.ui().getCurrentProperties();
		//
		//					AdbCommandPrompt cmd=new AdbCommandPrompt(adbExe, devName.getProperty("ios.udid"));
		//					cmd.tap(300, 1200);
		//
		//					WaitManager.get().waitLongTime();
		//					break;
		//				} 
		//			}
		//		}

		WaitManager.get().waitMediumTime();
		((InteractsWithApps) driverAndroid).runAppInBackground(Duration.ofSeconds(-1));
		WaitManager.get().waitFor(TimeUnit.MINUTES, 15);
		if (this.isIOS) {
			((InteractsWithApps) driverAndroid).activateApp(pro.getProperty("ios.bundle.id"));
			WaitManager.get().waitShortTime();
		} else {
			((InteractsWithApps) driverAndroid).activateApp(pro.getProperty("android.app.package"));
			WaitManager.get().waitShortTime();
		}

	}

	@When("^Compare il pop-up che informa non si può accedere con le credenziali SPID$")
	public void compare_il_pop_up_che_informa_non_si_può_accedere_con_le_credenziali_SPID() throws Throwable {

		CredentialSPID spidPopUp = (CredentialSPID) PageRepo.get().get(CredentialSPID.NAME).get();

		spidPopUp.verifyLayoutPopUp();
	}

	@When("^Utente clicca su Ok$")
	public void utente_clicca_su_Ok() throws Throwable {

		CredentialSPID spidPopUp = (CredentialSPID) PageRepo.get().get(CredentialSPID.NAME);

		spidPopUp.clickOnOk();

	}

	@When("^Utente clicca su Non ho un account Poste$")
	public void utente_clicca_su_Non_ho_un_account_Poste() throws Throwable {
		WaitManager.get().waitHighTime();

		CredentialSPID spidPopUp = (CredentialSPID) PageRepo.get().get(CredentialSPID.NAME);

		spidPopUp.clickOnIDonHaveAccount();
	}

	/*
	 * Step implementazione P2P
	 */

	@When("^Utente sulla homepage clicca sul tab community$")
	public void utente_sulla_homepage_clicca_sul_tab_community() throws Throwable {

		HomePage homePage = (HomePage) PageRepo.get().get(HomePage.NAME);
		GenericErrorPopUp popup=(GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);
		popup.clickOnApplePayPopupCloseBtn();

		homePage.clickOnCommunityTabIcon();

		CommunityCardOnePage communityCardOnePage = (CommunityCardOnePage) PageRepo.get()
				.get(CommunityCardOnePage.NAME);

		communityCardOnePage.verifyHeaderPage();
	}

	@When("^Utente clicca su P(\\d+)P invia denaro$")
	public void utente_clicca_su_P_P_invia_denaro(int arg1) throws Throwable {
		CommunityCardOnePage communityCardOnePage = (CommunityCardOnePage) PageRepo.get()
				.get(CommunityCardOnePage.NAME);

		communityCardOnePage.clickOnSendP2p();

		WaitManager.get().waitMediumTime();

	}

	@Then("^Utente verifica che il tab Goal&Win e presente$")
	public void utente_verifica_che_il_tab_Goal_Win_e_presente() throws Throwable {
		CommunityCardOnePage communityCardOnePage = (CommunityCardOnePage) PageRepo.get()
				.get(CommunityCardOnePage.NAME);

		communityCardOnePage.verifyGoalAndWinCard();
	}

	@When("^Utente seleziona il destinatario abilitato a ricevere il P(\\d+)P$")
	public void utente_seleziona_il_destinatario_abilitato_a_ricevere_il_P_P(int arg1,
			List<UserDataCredential> dataTable) throws Throwable {
		UserDataCredential table = dataTable.get(0);

		String cardChoose = table.getCardNumber();
		String nCards = table.getUserNumberCards();

		SendP2PPageStep2 step2 = (SendP2PPageStep2) PageRepo.get().get(SendP2PPageStep2.NAME);
		GenericErrorPopUp permissionContacts = null;
		// Popup per permessi di OS
		String permissionTextButton = "Consenti";
		if (table.getDriver().equals("GalaxyS8PlusAnd8Luca")) {
			permissionTextButton = "Allow";
		}
		System.out.println("Controllo device generico");

		try {
			System.out.println("PopUp di permessi Rubrica - OK");
			GenericErrorPopUp popupAuth = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);
			permissionTextButton = "OK";
			popupAuth.isPermissionRequiredSamsung(permissionTextButton);
		} catch (Exception e) {
			// TODO: handle exception
		}
		// Gestione nel caso di una o piu carte in possesso dell'utente
		// vincenzo fortunato : 2
		// luca petrucci: 1

		// GenericErrorPopUp errorPopUpSamsung=(GenericErrorPopUp)
		// PageRepo.get().get(GenericErrorPopUp.NAME);

		if (nCards.equals("2")) {
			SendP2PPageStep1 howPay = (SendP2PPageStep1) PageRepo.get().get(SendP2PPageStep1.NAME);
			String titleInput = "Invia denaro";
			howPay.verifyHeaderPage(titleInput);
			howPay.clickOnCard(driverAndroid, cardChoose);


			AbilitaInAppPage onbPage = (AbilitaInAppPage) PageRepo.get().get(AbilitaInAppPage.NAME);

			if(onbPage.isOnb()) {
				System.out.println("--> start onbPage.clickOnOnbDeviceDurinOperations");
				//				onbPage = (AbilitaInAppPage) PageRepo.get().get(AbilitaInAppPage.NAME);
				onbPage.clickOnOnbDeviceDurinOperations(TestEnvironment.get().getLanguage(table.getName()).getProperty("carta"));

			}
			try {
				permissionContacts = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);
				permissionContacts.isPermissionRequired();
				System.out.println("CHECK PERMISSION");
			} catch (Exception e) {
				// TODO: handle exception
			}
			try {
				permissionContacts.isPermissionRequiredSamsung(permissionTextButton);
				System.out.println("CLICKED");
			} catch (Exception e) {
				// TODO: handle exception
			}


			//// Controllo samsung
			// try {
			// UIUtils.ui().waitForCondition(errorPopUpSamsung.getDriver(),
			//// ExpectedConditions.visibilityOfElementLocated(.getXPath()));
			// errorPopUpSamsung.isPermissionRequiredSamsung();
			// } catch (Exception e) {
			// // TODO: handle exception
			// }
			// Controllo device generico
			System.out.println("Controllo device generico");
			try {
				permissionContacts = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);
				//				permissionContacts.isPermissionRequired();
				//				System.out.println("CHECK PERMISSION");
				permissionContacts.isPermissionRequiredSamsung(permissionTextButton);
				System.out.println("CLICKED");
			} catch (Exception e) {
				// TODO: handle exception
			}

			step2.searchContact(table.getTargetUser());
		} else if (nCards.equals("1")) {
			AbilitaInAppPage onbPage = (AbilitaInAppPage) PageRepo.get().get(AbilitaInAppPage.NAME);

			if(onbPage.isOnb()) {
				System.out.println("--> start onbPage.clickOnOnbDeviceDurinOperations");
				//				onbPage = (AbilitaInAppPage) PageRepo.get().get(AbilitaInAppPage.NAME);
				onbPage.clickOnOnbDeviceDurinOperations(TestEnvironment.get().getLanguage(table.getName()).getProperty("carta"));

			}

			try {
				permissionContacts = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);
				permissionContacts.isPermissionRequired();
				System.out.println("CHECK PERMISSION");
			} catch (Exception e) {
				// TODO: handle exception
			}
			try {
				permissionContacts.isPermissionRequiredSamsung(permissionTextButton);
				System.out.println("CLICKED");
			} catch (Exception e) {
				// TODO: handle exception
			}

			// Controllo device virtuale
			try {
				permissionContacts = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);
				permissionContacts.isPermissionRequired();
			} catch (Exception e) {
				// TODO: handle exception
			}
			step2.searchContact(table.getTargetUser());

		} else {
			System.out.println("Nessun input sul numero di carte... riprovo la ricerca del utente P2P.");

			AbilitaInAppPage onbPage = (AbilitaInAppPage) PageRepo.get().get(AbilitaInAppPage.NAME);

			if(onbPage.isOnb()) {
				System.out.println("--> start onbPage.clickOnOnbDeviceDurinOperations");
				//				onbPage = (AbilitaInAppPage) PageRepo.get().get(AbilitaInAppPage.NAME);
				onbPage.clickOnOnbDeviceDurinOperations(TestEnvironment.get().getLanguage(table.getName()).getProperty("carta"));

			}

			WaitManager.get().waitMediumTime();
			try {
				permissionContacts = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);
				permissionContacts.isPermissionRequired();
				System.out.println("CHECK PERMISSION");
			} catch (Exception e) {
				// TODO: handle exception
			}
			try {
				permissionContacts.isPermissionRequiredSamsung(permissionTextButton);
				System.out.println("CLICKED");
			} catch (Exception e) {
				// TODO: handle exception
			}
			step2.searchContact(table.getTargetUser());
		}
	}

	@When("^Utente seleziona il destinatario del P(\\d+)P tal tab CONTATTI P(\\d+)P$")
	public void utente_seleziona_il_destinatario_del_P_P_tal_tab_CONTATTI_P_P(int arg1, int arg2,
			List<UserDataCredential> dataTable) throws Throwable {
		UserDataCredential table = dataTable.get(0);

		try {
			System.out.println("PopUp di permessi Rubrica - Allow");
			GenericErrorPopUp popupAuth = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);

			String permissionTextButton = "Allow";
			popupAuth.isPermissionRequiredSamsung(permissionTextButton);
		} catch (Exception e) {
			// TODO: handle exception
		}

		try {
			System.out.println("PopUp di permessi Rubrica - ALLOW");
			GenericErrorPopUp popupAuth = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);

			String permissionTextButton = "ALLOW";
			popupAuth.isPermissionRequiredSamsung(permissionTextButton);
			//			popupAuth.isPermissionGeneric();

		} catch (Exception e) {
			// TODO: handle exception
		}

		try {
			System.out.println("PopUp di permessi Rubrica - CONSENTI");
			GenericErrorPopUp popupAuth = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);

			String permissionTextButton = "CONSENTI";
			popupAuth.isPermissionRequiredSamsung(permissionTextButton);
		} catch (Exception e) {
			// TODO: handle exception
		}

		try {
			System.out.println("PopUp di permessi Rubrica - Consenti");
			GenericErrorPopUp popupAuth = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);

			String permissionTextButton = "Consenti";
			popupAuth.isPermissionRequiredSamsung(permissionTextButton);
		} catch (Exception e) {
			// TODO: handle exception
		}


		try {
			System.out.println("PopUp di permessi Rubrica - OK");
			GenericErrorPopUp popupAuth = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);

			String permissionTextButton = "OK";
			popupAuth.isPermissionRequiredSamsung(permissionTextButton);
		} catch (Exception e) {
			// TODO: handle exception
		}

		SendP2PPageStep2 step2 = (SendP2PPageStep2) PageRepo.get().get(SendP2PPageStep2.NAME);
		step2.searchContact(table.getTargetUser());

	}

	@When("^Utente inserisce importo da inviare e clicca su conferma$")
	public void utente_inserisce_importo_da_inviare_e_clicca_su_conferma(List<UserDataCredential> dataTable)
			throws Throwable {

		UserDataCredential table = dataTable.get(0);

		SendP2PPageStep3 step3 = (SendP2PPageStep3) PageRepo.get().get(SendP2PPageStep3.NAME);

		step3.insetAmoutToSend(table.getImporto());

		WaitManager.get().waitMediumTime();

		step3.clickOnConfirmButton();
	}

	@When("^Utente verifica i dati del riepilogo del P(\\d+)P e lo invia$")
	public void utente_verifica_i_dati_del_riepilogo_del_P_P_e_lo_invia(int arg1) throws Throwable {

		SendP2PPageStep4 step4 = (SendP2PPageStep4) PageRepo.get().get(SendP2PPageStep4.NAME);

		WaitManager.get().waitMediumTime();

		step4.clickOnSendButton();

		WaitManager.get().waitShortTime();
	}

	@When("^Utente inserisce il posteId nel nuovo format e clicca su conferma$")
	public void utente_inserisce_il_posteId_nel_nuovo_format_e_clicca_su_conferma(List<UserDataCredential> dataTable)
			throws Throwable {

		UserDataCredential posteId = dataTable.get(0);

		String configuration = TestEnvironment.get().getAppConfiguration().getProperty("app");

		//		if (posteId.getCarburantePage() != null && posteId.getCarburantePage().equals("true") && configuration != null
		//				&& configuration.equals("BS")) {
		//			return;
		//		}

		RechargeMyPostepayPageStep5 newPosteIdPage = (RechargeMyPostepayPageStep5) PageRepo.get()
				.get(RechargeMyPostepayPageStep5.NAME);

		newPosteIdPage.insertPosteId(posteId.getPosteid());

		WaitManager.get().waitShortTime();

		newPosteIdPage.clickOnConfirmButton();
		WaitManager.get().waitHighTime();
		WaitManager.get().waitShortTime();
	}

	@When("^Utente e sulla homepage e clicca sulla card invia P(\\d+)P operazione veloce$")
	public void utente_e_sulla_homepage_e_clicca_sulla_card_invia_P_P_operazione_veloce(int arg1,
			List<UserDataCredential> dataTable) throws Throwable {
		HomePage homePage = (HomePage) PageRepo.get().get(HomePage.NAME);
		GenericErrorPopUp popup=(GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);
		popup.clickOnApplePayPopupCloseBtn();
		homePage.isP2PDisplayed();
		homePage.clickOnSentP2PSpeedOperation();

		// Ciclo di controllo per utente con una o più carte attive
		// 1 : luca.petrucci-abmo
		// 2 : vincenzo.fortunato-4869
		UserDataCredential table = dataTable.get(0);
		String cardNumber = table.getUserNumberCards();
		String cardChoose = table.getCardNumber();

		if (cardNumber.equals("2")) {
			SendP2PPageStep1 howPay = (SendP2PPageStep1) PageRepo.get().get(SendP2PPageStep1.NAME);
			String titleInput = "Invia denaro";
			howPay.verifyHeaderPage(titleInput);
			howPay.clickOnCard(driverAndroid, cardChoose);
		}

		AbilitaInAppPage onbPage = (AbilitaInAppPage) PageRepo.get().get(AbilitaInAppPage.NAME);

		if(onbPage.isOnb()) {
			System.out.println("--> start onbPage.clickOnOnbDeviceDurinOperations");
			//			onbPage = (AbilitaInAppPage) PageRepo.get().get(AbilitaInAppPage.NAME);
			onbPage.clickOnOnbDeviceDurinOperations(TestEnvironment.get().getLanguage(table.getName()).getProperty("carta"));

		}

	}

	@When("^Utente clicca sul tab P(\\d+)P richiedi denaro$")
	public void utente_clicca_sul_tab_P_P_richiedi_denaro(int arg1) throws Throwable {
		CommunityCardOnePage communityPage = (CommunityCardOnePage) PageRepo.get().get(CommunityCardOnePage.NAME);

		communityPage.clickOnRequestP2p();
	}

	@When("^Utente seleziona il contatto per richiedere il pagamento da no poste$")
	public void utente_seleziona_il_contatto_per_richiedere_il_pagamento_da_no_poste(List<UserDataCredential> dataTable)
			throws Throwable {
		UserDataCredential table = dataTable.get(0);
		String usr = table.getTargetUser();

		try {
			System.out.println("PopUp di permessi Rubrica - Allow");
			GenericErrorPopUp popupAuth = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);

			String permissionTextButton = "Allow";
			popupAuth.isPermissionRequiredSamsung(permissionTextButton);
		} catch (Exception e) {
			// TODO: handle exception
		}

		try {
			System.out.println("PopUp di permessi Rubrica - ALLOW");
			GenericErrorPopUp popupAuth = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);

			String permissionTextButton = "ALLOW";
			popupAuth.isPermissionRequiredSamsung(permissionTextButton);
			//			popupAuth.isPermissionGeneric();

		} catch (Exception e) {
			// TODO: handle exception
		}

		try {
			System.out.println("PopUp di permessi Rubrica - Consenti");
			GenericErrorPopUp popupAuth = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);

			String permissionTextButton = "Consenti";
			popupAuth.isPermissionRequiredSamsung(permissionTextButton);
			//			popupAuth.isPermissionGeneric();

		} catch (Exception e) {
			// TODO: handle exception
		}

		try {
			System.out.println("PopUp di permessi Rubrica - CONSENTI");
			GenericErrorPopUp popupAuth = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);

			String permissionTextButton = "CONSENTI";
			popupAuth.isPermissionRequiredSamsung(permissionTextButton);
			//			popupAuth.isPermissionGeneric();

		} catch (Exception e) {
			// TODO: handle exception
		}

		try {
			System.out.println("PopUp di permessi Rubrica - OK iOS");
			GenericErrorPopUp popupAuth = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);

			String permissionTextButton = "OK";
			popupAuth.isPermissionRequiredSamsung(permissionTextButton);

		} catch (Exception e) {
			// TODO: handle exception
		}	

		RequestP2PPageStep1 step1 = (RequestP2PPageStep1) PageRepo.get().get(RequestP2PPageStep1.NAME);

		step1.clickOnContactTab();
		// RequestP2PPageStep1 step1b=(RequestP2PPageStep1)
		// PageRepo.get().get(RequestP2PPageStep1.NAME);
		step1.searchContactNoP2p(driverAndroid, usr);
	}

	@When("^Utente seleziona il contatto per richiedere il pagamento$")
	public void utente_seleziona_il_contatto_per_richiedere_il_pagamento(List<UserDataCredential> dataTable)
			throws Throwable {
		UserDataCredential table = dataTable.get(0);
		String usr = table.getTargetUser();

		// Popup per permessi di OS
		GenericErrorPopUp permission = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);
		try {

			permission.isPermissionRequiredSamsung("Consenti");
		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			permission.isPermissionRequiredSamsung("ALLOW");
		} catch (Exception e) {
			// TODO: handle exception
		}

		try {
			permission.isPermissionRequiredSamsung("OK");
		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			permission.isPermissionGeneric();
		} catch (Exception e) {
			// TODO: handle exception
		}

		RequestP2PPageStep1 step1 = (RequestP2PPageStep1) PageRepo.get().get(RequestP2PPageStep1.NAME);

		step1.searchContact(usr);

	}

	@When("^Utente inserisce importo da richiedere e clicca su conferma$")
	public void utente_inserisce_importo_da_richiedere_e_clicca_su_conferma(List<UserDataCredential> dataTable)
			throws Throwable {
		UserDataCredential table = dataTable.get(0);

		RequestP2PPageStep4 step2 = (RequestP2PPageStep4) PageRepo.get().get(RequestP2PPageStep4.NAME);

		step2.insetAmoutToRequest(table.getImporto());

		step2.clickOnConfirmButton();
	}

	@When("^Utente verifica i dati del riepilogo del P(\\d+)P e clicca su conferma$")
	public void utente_verifica_i_dati_del_riepilogo_del_P_P_e_clicca_su_conferma(int arg1) throws Throwable {

		RequestP2PPageStep2 step3 = (RequestP2PPageStep2) PageRepo.get().get(RequestP2PPageStep2.NAME);

		step3.clickOnRequestButton();
	}

	@When("^Utente sul riepiologo del P(\\d+)P aggiunge un commento e clicca su conferma$")
	public void utente_sul_riepiologo_del_P_P_aggiunge_un_commento_e_clicca_su_conferma(int arg1,
			List<UserDataCredential> dataTable) throws Throwable {

		UserDataCredential table = dataTable.get(0);

		RequestP2PPageStep2 step3 = (RequestP2PPageStep2) PageRepo.get().get(RequestP2PPageStep2.NAME);

		step3.clickOnAddComment();

		CommentRechargeP2pPage comment = (CommentRechargeP2pPage) PageRepo.get().get(CommentRechargeP2pPage.NAME);

		comment.inserComment(table.getComment());

		comment.clickOnConfirm();

		step3.clickOnRequestButton();

	}

	/*
	 * Step implementazione Bonifico SEPA
	 */

	@When("^Utente sulla homepage clicca sul tab PAGA$")
	public void utente_sulla_homepage_clicca_sul_tab_PAGA() throws Throwable {

		HomePage home = (HomePage) PageRepo.get().get(HomePage.NAME);
		GenericErrorPopUp popup=(GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);
		popup.clickOnApplePayPopupCloseBtn();

		try {

			popup.clickOnNonMostrarePiu();
		} catch (Exception e) {
			// TODO: handle exception
		}
		try {

			popup.clickOnChiudiRicaricaDopoOnb();
		} catch (Exception e) {
			// TODO: handle exception
		}

		home.clickOnPaymentTabIcon();

		try {
			GenericErrorPopUp popupAuth = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);
			popupAuth.closeAtacPopup();
		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			GenericErrorPopUp popupAuth = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);
			popupAuth.clickOnOKPopUp();
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	@When("^Utente sulla homepage clicca sul icona NOTIFICHE E MESSAGGI$")
	public void utente_sulla_homepage_clicca_sul_icona_NOTIFICHE_E_MESSAGGI() throws Throwable {
		HomePage home = (HomePage) PageRepo.get().get(HomePage.NAME);
		GenericErrorPopUp popup=(GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);
		popup.clickOnApplePayPopupCloseBtn();

		home.clickAllMessageNumber();
	}

	@When("^Utente clicca su PAGAMENTI$")
	public void utente_clicca_su_PAGAMENTI() throws Throwable {
		try {
			GenericErrorPopUp popup = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);
			popup.clickOnOKPopUp();
		} catch (Exception e) {

		}
		try {
			GenericErrorPopUp popup = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);
			popup.isPermissionRequiredSamsung("NO");
			;
		} catch (Exception e) {

		}

		RechargePaymentPage payPage = (RechargePaymentPage) PageRepo.get().get(RechargePaymentPage.NAME);
		payPage.verifyHeaderpage(TestEnvironment.get().getLanguage("header").getProperty("RechargePaymentPage"));
		payPage.clickOnTabPayment();
	}

	@When("^Utente clicca su Bonifico SEPA$")
	public void utente_clicca_su_Bonifico_SEPA() throws Throwable {
		if(isIOS) {
			CommandExecutionHelper.execute((ExecutesMethod) driverAndroid, setSettingsCommand("snapshotMaxDepth", 100));
		}

		PaymentPaymentPage payPage = (PaymentPaymentPage) PageRepo.get().get(PaymentPaymentPage.NAME);
		payPage.verifyHeaderpage(TestEnvironment.get().getLanguage("header").getProperty("RechargePaymentPage"));
		payPage.clickOnTransfertSepa();

		if(isIOS) {
			CommandExecutionHelper.execute((ExecutesMethod) driverAndroid, setSettingsCommand("snapshotMaxDepth", 20));
		}

	}

	@When("^Utente seleziona la carta da utilizzare per il pagamento su \"([^\"]*)\"$")
	public void utente_seleziona_la_carta_da_utilizzare_per_il_pagamento_su(String arg1,
			List<UserDataCredential> dataTable) throws Throwable {
		UserDataCredential table = dataTable.get(0);
		double discrepanza = StringAndNumberOperationTools.convertStringToDouble(table.getDiscrepanza());

		String operation = table.getTargetOperation();
		String cardChoose = table.getCardNumber();
		String nCard = table.getUserNumberCards();

		if (operation.equals("SEPA") || operation.equals("Postagiro")) {
			System.out.println("nCard: " + nCard);

			if (nCard.equals("2")) {
				System.out.println("Utenti con più carte");
				BankTranfertSEPAPageStep1 step1 = (BankTranfertSEPAPageStep1) PageRepo.get()
						.get(BankTranfertSEPAPageStep1.NAME);
				step1.clickOnCard(driverAndroid, cardChoose);
				step1.verifyHeaderpage(
						TestEnvironment.get().getLanguage("header").getProperty("BankTranfertSEPAPageStep1"));
			} else {
				System.out.println("Utente con una sola carta");
			}
		}
		// else if (operation.equals("Postagiro")) {
		// BankTranfertSEPAPageStep1 step2=(BankTranfertSEPAPageStep1)
		// PageRepo.get().get(BankTranfertSEPAPageStep1.NAME);
		//
		// step2.clickOnCard(driverAndroid, cardChoose);
		//
		// }
		else if (operation.equals("OtherPP") || operation.contentEquals("AutomaRechargePP")
				|| operation.equals("AutomaRechargeSoglia")) {
			RechargeOtherPostepayPageStep2 step3 = (RechargeOtherPostepayPageStep2) PageRepo.get()
					.get(RechargeOtherPostepayPageStep2.NAME);

			System.out.println("string username:   " + table.getUsername());

			//			step3.verifyHeaderpage(
			//					TestEnvironment.get().getLanguage("header").getProperty("RechargeOtherPostepayPageStep2"));
			// step3.verifyLayoutPage(driverAndroid,
			// table.getUsername(),
			// "Scegli come vuoi pagare",
			// "EVOLUTION",
			// table.getCardNumber(),
			// discrepanza);
			step3.clickOnCard(driverAndroid, table.getCardNumber());
		} else if (operation.equals("MyPP")) {
			RechargeMyPostepayPageStep3 step3 = (RechargeMyPostepayPageStep3) PageRepo.get()
					.get(RechargeMyPostepayPageStep3.NAME);

			//			step3.verifyHeaderpage(TestEnvironment.get().getLanguage("header").getProperty("RechargeMyPostepayPageStep3"));
			if (this.isIOS) {
				step3.verifyLayoutPage(driverAndroid, "Scegli lo strumento di pagamento per effettuare la ricarica", "CONNECT", table.getCardNumber(),
						discrepanza);
			} else {
				step3.verifyLayoutPage(driverAndroid, "Scegli come vuoi pagare", "CONNECT", table.getCardNumber(),
						discrepanza);
			}
			step3.selectCardToRecharge(driverAndroid, table.getCardNumber());
		} else if (operation.equals("Bollettino")) {
			if (nCard.equals("2")) {
				System.out.println("Utenti con più carte");
				RechargeMyPostepayPageStep3 step3 = (RechargeMyPostepayPageStep3) PageRepo.get()
						.get(RechargeMyPostepayPageStep3.NAME);

				//				step3.verifyHeaderpage(TestEnvironment.get().getLanguage("header").getProperty("PagaBollettinoT133"));
				//				step3.verifyLayoutPage(driverAndroid, "Come vuoi pagare?", "POSTEPAY DIGITAL", table.getCardNumber(),
				//						discrepanza);
				step3.selectCardToRecharge(driverAndroid, table.getCardNumber());

			} else {
				System.out.println("Utente con una sola carta");
			}
		} else if (operation.equals("BolloAuto")) {
			if (nCard.equals("2")) {
				System.out.println("Utenti con più carte");
				RechargeMyPostepayPageStep3 step3 = (RechargeMyPostepayPageStep3) PageRepo.get()
						.get(RechargeMyPostepayPageStep3.NAME);

				//				step3.verifyHeaderpage(TestEnvironment.get().getLanguage("header").getProperty("bolloAutoTitle"));
				//				step3.verifyLayoutPage(driverAndroid, "Come vuoi pagare?", "CONNECT", table.getCardNumber(),
				//						discrepanza);
				step3.selectCardToRecharge(driverAndroid, table.getCardNumber());

			} else {
				System.out.println("Utente con una sola carta");
			}
		}

		else if (operation.equals("Ministero")) {
			if (nCard.equals("2")) {
				System.out.println("Utenti con più carte");
				RechargeMyPostepayPageStep3 step3 = (RechargeMyPostepayPageStep3) PageRepo.get()
						.get(RechargeMyPostepayPageStep3.NAME);

				step3.verifyHeaderpage(
						TestEnvironment.get().getLanguage("header").getProperty("MinisteroDeiTrasportiPage"));
				if (this.isIOS) {
					step3.verifyLayoutPage(driverAndroid, "Come vuoi pagare?", "DIGITAL", table.getCardNumber(),
							discrepanza);
				} else {
					step3.verifyLayoutPage(driverAndroid, "Come vuoi pagare?", "CONNECT", table.getCardNumber(),
							discrepanza);
				}
				step3.selectCardToRecharge(driverAndroid, table.getCardNumber());

			} else {
				System.out.println("Utente con una sola carta");
			}
		}
		else if (operation.equals("PagoPa")) {
			if (nCard.equals("2")) {
				System.out.println("Utenti con più carte");
				RechargeMyPostepayPageStep3 step3 = (RechargeMyPostepayPageStep3) PageRepo.get()
						.get(RechargeMyPostepayPageStep3.NAME);
				try {
					PagaBollettinoT133 bollPage = (PagaBollettinoT133) PageRepo.get().get(PagaBollettinoT133.NAME);
					bollPage.onClickBollettinoManuale();
				} catch (Exception e) {
					// TODO: handle exception
				}
				step3.verifyHeaderpage(
						TestEnvironment.get().getLanguage("header").getProperty("PagoPa"));
				if (this.isIOS) {
					step3.verifyLayoutPage(driverAndroid, "Come vuoi pagare?", "DIGITAL", table.getCardNumber(),
							discrepanza);
				} else {
					step3.verifyLayoutPage(driverAndroid, "Come vuoi pagare?", "CONNECT", table.getCardNumber(),
							discrepanza);
				}
				step3.selectCardToRecharge(driverAndroid, table.getCardNumber());

			} else {
				System.out.println("Utente con una sola carta");
			}
		}

		AbilitaInAppPage onbPage = (AbilitaInAppPage) PageRepo.get().get(AbilitaInAppPage.NAME);
		System.out.println("Sono qui prima dell'onboarding ");
		if(onbPage.isOnb()) {
			System.out.println("--> start onbPage.clickOnOnbDeviceDurinOperations");
			//			onbPage = (AbilitaInAppPage) PageRepo.get().get(AbilitaInAppPage.NAME);
			onbPage.clickOnOnbDeviceDurinOperations(TestEnvironment.get().getLanguage(table.getName()).getProperty("carta"));

		}

	}

	@When("^Utente seleziona il tipo di bonifico o postagiro da effettuare$")
	public void utente_seleziona_il_tipo_di_bonifico_o_postagiro_da_effettuare(List<UserDataCredential> dataTable)
			throws Throwable {
		UserDataCredential table = dataTable.get(0);
		SelezionaTipoBonificoPage step2 = (SelezionaTipoBonificoPage) PageRepo.get()
				.get(SelezionaTipoBonificoPage.NAME);
		System.out.println(step2);
		step2.verifyLayout();
		step2.clickOnBonificoPostagiro(table.getTargetOperation());
	}

	@When("^Utente compila i dati per il bonifico$")
	public void utente_compila_i_dati_per_il_bonifico(List<UserDataCredential> dataTable) throws Throwable {

		UserDataCredential table = dataTable.get(0);
		String cardChoose = table.getCardNumber();
		try {
			BankTranfertSEPAPageStep1 step1 = (BankTranfertSEPAPageStep1) PageRepo.get()
					.get(BankTranfertSEPAPageStep1.NAME);
			step1.clickOnCard(driverAndroid, cardChoose);
			AbilitaInAppPage onbPage = (AbilitaInAppPage) PageRepo.get().get(AbilitaInAppPage.NAME);

			onbPage.inserisciMeseanno(TestEnvironment.get().getLanguage(table.getName()).getProperty("carta"));
			onbPage.inserisciCvv(TestEnvironment.get().getLanguage(table.getName()).getProperty("carta"));

		} catch (Exception e) {
			e.printStackTrace();
		}

		if(isIOS) {
			CommandExecutionHelper.execute((ExecutesMethod) driverAndroid, setSettingsCommand("snapshotMaxDepth", 100));
		}

		BankTranfertSEPAPageStep2 step2 = (BankTranfertSEPAPageStep2) PageRepo.get()
				.get(BankTranfertSEPAPageStep2.NAME);

		step2.verifyHeaderpage(TestEnvironment.get().getLanguage("header").getProperty("BankTranfertSEPAPageStep1"));
		step2.compileAllStandardForm(table.getIban(), table.getTargetUser(), table.getImporto(), table.getComment());
	}

	@When("^Utente clicca su PROCEDI della \"([^\"]*)\"$")
	public void utente_clicca_su_PROCEDI_della(String arg1, List<UserDataCredential> dataTable) throws Throwable {
		UserDataCredential table = dataTable.get(0);
		String operation = table.getTargetOperation();

		if (operation.equals("SEPA")) {

			BankTranfertSEPAPageStep2 step2 = (BankTranfertSEPAPageStep2) PageRepo.get()
					.get(BankTranfertSEPAPageStep2.NAME);

			step2.clickOnProceed();

			GenericErrorPopUp popup = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);

			boolean isVisiblePopUp = popup.checkIfElementIsPresent();

			if (isVisiblePopUp) {
				// Stringe del popup specifiche per le operazioni ripetute
				String title = "Avviso";
				String body = "Nella giornata odierna hai gia’ effettuato un pagamento con gli stessi estremi. Vuoi procedere comunque?";

				popup.verifyLayoutOfPopup(driverAndroid, title, body, this.isIOS);
				if (this.isIOS) {
					popup.isPermissionRequiredSamsung("OK");
				} else
					popup.clickOnOKPopUp();

			} else {
				System.out.println("## PASS ## Popup non presente");
			}

		} else if (operation.equals("Postagiro")) {
			BankTranfertSEPAPageStep2 step2 = (BankTranfertSEPAPageStep2) PageRepo.get()
					.get(BankTranfertSEPAPageStep2.NAME);

			step2.clickOnProceed();

			GenericErrorPopUp popup = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);

			boolean isVisiblePopUp = popup.checkIfElementIsPresent();

			try {
				if (isVisiblePopUp) {
					// Stringe del popup specifiche per le operazioni ripetute
					//					String title = "Avviso";
					//					String body = "Attenzione! Un postagiro con gli stessi estremi e' gia' stato preso in carico";
					//
					//					popup.verifyLayoutOfPopup(driverAndroid, title, body, this.isIOS);

					popup.clickOnOKPopUp();
					//					step2.clickOnProceed();
				} else {
					System.out.println("## PASS ## Popup non presente");
				}
			} catch(Exception e) {

			}
			try {
				if (isVisiblePopUp) {
					// Stringe del popup specifiche per le operazioni ripetute
					String title = "Avviso";
					String body = "Nella giornata odierna hai gia’ effettuato un pagamento con gli stessi estremi. Vuoi procedere comunque?";

					popup.verifyLayoutOfPopup(driverAndroid, title, body, this.isIOS);

					popup.clickOnOKPopUp();
					//					step2.clickOnProceed();
				} else {
					System.out.println("## PASS ## Popup non presente");
				}
			} catch(Exception e) {

			}

		} else if (operation.equals("OtherPP") || operation.contentEquals("AutomaRechargePP")
				|| operation.contentEquals("AutomaRechargeSoglia")) {
			RechargeOtherPostepayPageStep1 step3 = (RechargeOtherPostepayPageStep1) PageRepo.get()
					.get(RechargeOtherPostepayPageStep1.NAME);

			step3.clickOnProceedButton();
		} else if (operation.equals("MyPP")) {
			RechargeMyPostepayPageStep2 step3 = (RechargeMyPostepayPageStep2) PageRepo.get()
					.get(RechargeMyPostepayPageStep2.NAME);

			step3.clickOnProceedButton();
		}
	}

	@When("^Utente verifica il riepilogo del \"([^\"]*)\"$")
	public void utente_verifica_il_riepilogo_del(String arg1, List<UserDataCredential> dataTable) throws Throwable {

		UserDataCredential table = dataTable.get(0);
		String operation = table.getTargetOperation();

		if (operation.equals("SEPA")) {
			BankTranfertSEPAPageStep3 step3 = (BankTranfertSEPAPageStep3) PageRepo.get()
					.get(BankTranfertSEPAPageStep3.NAME);
			step3.verifyHeaderPage(
					TestEnvironment.get().getLanguage("header").getProperty("BankTranfertSEPAPageStep3"));
			step3.verifySummarySepa(table.getTargetUser(), table.getIban(), table.getImporto(), table.getComment(),
					table.getMyIban());
		} else if (operation.equals("Postagiro")) {
			BankTranfertPostagiroPageStep1 postagiroStep1 = (BankTranfertPostagiroPageStep1) PageRepo.get()
					.get(BankTranfertPostagiroPageStep1.NAME);
			postagiroStep1.verifyHeaderPage(
					TestEnvironment.get().getLanguage("header").getProperty("BankTranfertPostagiroPageStep1"));
			postagiroStep1.verifySummarySepa(table.getTargetUser(), table.getIban(), table.getImporto(),
					table.getComment(), table.getMyIban(), table.getCardNumber());
		} else if (operation.equals("OtherPP") || operation.equals("MyPP")) {
			RechargeMyPostepayPageStep4 otherPPsummary = (RechargeMyPostepayPageStep4) PageRepo.get()
					.get(RechargeMyPostepayPageStep4.NAME);
			otherPPsummary.verifyHeaderPage(
					TestEnvironment.get().getLanguage("header").getProperty("RechargeMyPostepayPageStep4"));
			otherPPsummary.verifySummaryRecharge(table.getTargetOperation(), table.getTargetUser().toUpperCase(),
					table.getCardNumberDestinatario(), table.getImporto(), table.getComment());
		} else if (operation.equals("AutomaRechargePP")) {
			AutomaticRechargePostepaySummary automaPP = (AutomaticRechargePostepaySummary) PageRepo.get()
					.get(AutomaticRechargePostepaySummary.NAME);

			automaPP.verifyHeaderPage(
					TestEnvironment.get().getLanguage("header").getProperty("AutomaticRechargePostepaySummary"));
			automaPP.verifySummaryRecharge(table.getTargetOperation(), table.getTargetUser(),
					table.getCardNumberDestinatario(), table.getImporto(), table.getComment(),
					table.getRecurrencyRechargeName());

			// Opzionali di verifica
			automaPP.verifyCopyMessage();
			// automaPP.verifyRealTimeMessage();
		} else if (operation.equals("AutomaRechargeSoglia")) {
			AutomaticRechargePostepaySummary automaPP = (AutomaticRechargePostepaySummary) PageRepo.get()
					.get(AutomaticRechargePostepaySummary.NAME);
			automaPP.verifyHeaderPage(
					TestEnvironment.get().getLanguage("header").getProperty("RechargeMyPostepayPageStep3"));
			automaPP.verifySummariRicaricaAutomaticaSOGLIA(table.getTargetUser(), table.getCardNumberDestinatario(),
					table.getComment(), table.getImporto());
		}
	}

	@When("^Utente clicca su PAGA del \"([^\"]*)\"$")
	public void utente_clicca_su_PAGA_del(String arg1, List<UserDataCredential> dataTable) throws Throwable {
		UserDataCredential table = dataTable.get(0);
		String operation = table.getTargetOperation();

		if (operation.equals("SEPA") || operation.equals("Postagiro")) {
			BankTranfertSEPAPageStep3 step3 = (BankTranfertSEPAPageStep3) PageRepo.get()
					.get(BankTranfertSEPAPageStep3.NAME);

			step3.clickOnPay();
		} else if (operation.equals("OtherPP")) {
			RechargeMyPostepayPageStep4 step3 = (RechargeMyPostepayPageStep4) PageRepo.get()
					.get(RechargeMyPostepayPageStep4.NAME);

			step3.clickOnPayButton();
		} else if (operation.equals("MyPP")) {
			RechargeMyPostepayPageStep4 step3 = (RechargeMyPostepayPageStep4) PageRepo.get()
					.get(RechargeMyPostepayPageStep4.NAME);

			step3.clickOnPayButton();
		} else if (operation.contentEquals("AutomaRechargePP") || operation.equals("AutomaRechargeSoglia")) {
			AutomaticRechargePostepaySummary step3 = (AutomaticRechargePostepaySummary) PageRepo.get()
					.get(AutomaticRechargePostepaySummary.NAME);

			step3.clickAutorizationButton();
		}

	}

	@When("^Utente inserisce il posteId durante la \"([^\"]*)\" e clicca conferma$")
	public void utente_inserisce_il_posteId_durante_la_e_clicca_conferma(String arg1,
			List<UserDataCredential> dataTable) throws Throwable {
		UserDataCredential table = dataTable.get(0);
		String operation = table.getTargetOperation();

		if (operation.equals("SEPA")) {
			BankTranfertSEPAPageStep4 step4 = (BankTranfertSEPAPageStep4) PageRepo.get()
					.get(BankTranfertSEPAPageStep4.NAME);
			step4.verifyHeaderpage(
					TestEnvironment.get().getLanguage("header").getProperty("BankTranfertSEPAPageStep4"));
			step4.insertPosteId(table.getPosteid());

			step4.clickOnConfirmButton();
		}

		else if (operation.equals("Postagiro")) {
			BankTranfertSEPAPageStep4 step4 = (BankTranfertSEPAPageStep4) PageRepo.get()
					.get(BankTranfertSEPAPageStep4.NAME);
			// step4.verifyHeaderpage(TestEnvironment.get().getLanguage("header").getProperty("BankTranfertPostagiroPageStep2"));
			step4.insertPosteId(table.getPosteid());
			((HidesKeyboard) driverAndroid).hideKeyboard();
			step4.clickOnConfirmButton();
		}

		else if (operation.equals("Bollettino")) {
			BankTranfertSEPAPageStep4 step4 = (BankTranfertSEPAPageStep4) PageRepo.get()
					.get(BankTranfertSEPAPageStep4.NAME);
			step4.verifyHeaderpage(TestEnvironment.get().getLanguage("header").getProperty("BollettinoPageHeader"));
			step4.insertPosteId(table.getPosteid());
			step4.clickOnConfirmButton();
		}

		else if (operation.equals("OtherPP") || operation.equals("MyPP") || operation.contentEquals("AutomaRechargePP")
				|| operation.contentEquals("AutomaRechargeSoglia") || (operation.contains("Ministero"))) {
			RechargeMyPostepayPageStep5 step3 = (RechargeMyPostepayPageStep5) PageRepo.get()
					.get(RechargeMyPostepayPageStep5.NAME);

			switch (operation) {
			case "OtherPP":
				step3.verifyHeaderPage(
						TestEnvironment.get().getLanguage("header").getProperty("RechargeOtherPostepayPageStep1"));
				break;
			case "MyPP":
				step3.verifyHeaderPage(
						TestEnvironment.get().getLanguage("header").getProperty("Ricarica la mia Postepay"));
				break;
			case "AutomaRechargePP":
				step3.verifyHeaderPage(TestEnvironment.get().getLanguage("header").getProperty("Ricarica Postepay"));
				break;
			case "AutomaRechargeSoglia":
				step3.verifyHeaderPage(TestEnvironment.get().getLanguage("header").getProperty("Ricarica Postepay"));
				break;
			case "Ministero":
				step3.verifyHeaderPage("Ministero dei Trasporti");
				break;
			default:
				break;
			}
			step3.insertPosteId(table.getPosteid());

			step3.clickOnConfirmButton();

		}

		else if (operation.contentEquals("DeleteAutomaRechargePP")) {
			RechargeMyPostepayPageStep5 step3 = (RechargeMyPostepayPageStep5) PageRepo.get()
					.get(RechargeMyPostepayPageStep5.NAME);

			step3.insertPosteId(table.getPosteid());

			step3.clickOnConfirmButton();

			step3.verifyHeaderPage("Elimina ricarica automatica");
		}

		else if (operation.equals("BolloAuto")) {
			BankTranfertSEPAPageStep4 step4 = (BankTranfertSEPAPageStep4) PageRepo.get()
					.get(BankTranfertSEPAPageStep4.NAME);
			step4.verifyHeaderpage(TestEnvironment.get().getLanguage("header").getProperty("bolloAutoTitle"));
			step4.insertPosteId(table.getPosteid());
			step4.clickOnConfirmButton();
		} else if (operation.equals("AutorizzazioneBPOL")) {
			BankTranfertSEPAPageStep4 step4 = (BankTranfertSEPAPageStep4) PageRepo.get()
					.get(BankTranfertSEPAPageStep4.NAME);
			step4.verifyHeaderAutorizzaWEB(
					TestEnvironment.get().getLanguage("header").getProperty("AutorizzazioneBPOLPosteid"));
			step4.insertPosteIdAutorizzaWEB(table.getPosteid());
			step4.clickOnConfirmButtonAutorizzaWEB();
		} else if (operation.equals("GPay")) {
			BankTranfertSEPAPageStep4 step4 = (BankTranfertSEPAPageStep4) PageRepo.get()
					.get(BankTranfertSEPAPageStep4.NAME);
			step4.verifyHeaderAutorizzaWEB(
					TestEnvironment.get().getLanguage("header").getProperty("AutorizzazioneBPOLPosteid"));
			step4.insertPosteIdAutorizzaWEB(table.getPosteid());
			step4.clickOnConfirmButton();
		}
	}

	/*
	 * Step implementazione RICARICA ALTRA POSTEPAY
	 */

	@When("^Utente clicca su Ricarica Altra Postepay$")
	public void utente_clicca_su_Ricarica_Altra_Postepay() throws Throwable {

		// RechargePaymentPage step1=(RechargePaymentPage)
		// PageRepo.get().get(RechargePaymentPage.NAME);
		// step1.clickOnRechargeOtherPostePay();
		SelectPostepayRecharge selectPostePay = (SelectPostepayRecharge) PageRepo.get()
				.get(SelectPostepayRecharge.NAME);

		selectPostePay.clickOtherPostePay();
	}

	@When("^Utente compila il form della dispositiva$")
	public void utente_compila_il_form_della_dispositiva(List<UserDataCredential> dataTable) throws Throwable {
		UserDataCredential table = dataTable.get(0);
		double discrepanza = StringAndNumberOperationTools.convertStringToDouble(table.getDiscrepanza());
		RechargeOtherPostepayPageStep1 step2 = (RechargeOtherPostepayPageStep1) PageRepo.get()
				.get(RechargeOtherPostepayPageStep1.NAME);
		try {
			AbilitaInAppPage onbPage = (AbilitaInAppPage) PageRepo.get().get(AbilitaInAppPage.NAME);

			onbPage.inserisciMeseanno(TestEnvironment.get().getLanguage(table.getName()).getProperty("carta"));
			onbPage.inserisciCvv(TestEnvironment.get().getLanguage(table.getName()).getProperty("carta"));

		} catch(Exception e) {
			e.printStackTrace();
		}

		step2.verifyHeaderpage(
				TestEnvironment.get().getLanguage("header").getProperty("RechargeOtherPostepayPageStep1"));
		// step2.verifyImageCard(driverAndroid, discrepanza);
		//		step2.verifyTitle(driverAndroid, "Scegli da rubrica");
		step2.compileAllStandardForm(table.getCardNumberDestinatario(), table.getTargetUser(), table.getImporto(),
				table.getComment());
	}

	@When("^Utente clicca sull icona RUBRICA$")
	public void utente_clicca_sull_icona_RUBRICA() throws Throwable {
		RechargeOtherPostepayPageStep1 step1 = (RechargeOtherPostepayPageStep1) PageRepo.get()
				.get(RechargeOtherPostepayPageStep1.NAME);

		step1.clickOnContactNumberMenu();
	}

	@When("^Utente completa il form della dispositiva inserendo importo e causale$")
	public void utente_completa_il_form_della_dispositiva_inserendo_importo_e_causale(
			List<UserDataCredential> dataTable) throws Throwable {
		UserDataCredential table = dataTable.get(0);

		RechargeOtherPostepayPageStep1 step2 = (RechargeOtherPostepayPageStep1) PageRepo.get()
				.get(RechargeOtherPostepayPageStep1.NAME);
		step2.insertAmountToSendTargetUser(table.getImporto());
		step2.insertReasonOfRecharge(table.getComment());
	}

	@When("^Utente seleziona il contatto per inviare il pagamento e salva$")
	public void utente_seleziona_il_contatto_per_inviare_il_pagamento_e_salva(List<UserDataCredential> dataTable)
			throws Throwable {
		UserDataCredential table = dataTable.get(0);

		RechargePostepayUsingTelephonContactsPage step1 = (RechargePostepayUsingTelephonContactsPage) PageRepo.get()
				.get(RechargePostepayUsingTelephonContactsPage.NAME);

		if (table.getTargetOperation().equals("AutomaRechargeSoglia")) {
			step1.clickOnTabMyCARD();
			step1.selectUserToSendRechargeUsingCard(table.getCardNumberDestinatario());
		} else {
			step1.selectUserToSendRecharge(table.getTargetUser());
		}

		step1.clickOnSaveButton();
	}

	@When("^Utente verifica che i campi NUMERO CARTA e INTESTATARIO si siano \"([^\"]*)\"$")
	public void utente_verifica_che_i_campi_NUMERO_CARTA_e_INTESTATARIO_si_siano(String valueStatus,
			List<UserDataCredential> dataTable) throws Throwable {
		UserDataCredential table = dataTable.get(0);

		RechargeOtherPostepayPageStep1 step2 = (RechargeOtherPostepayPageStep1) PageRepo.get()
				.get(RechargeOtherPostepayPageStep1.NAME);

		step2.verifyDestinatarioAndCardNumber(valueStatus, table.getTargetOperation(), table.getTargetUser(),
				table.getCardNumberDestinatario());
	}

	/*
	 * Step implementazione RICARICA LA MIA POSTEPAY
	 */

	@When("^Utente clicca sulla Card Ricarica Postepay$")
	public void utente_clicca_sulla_Card_Ricarica_Postepay() throws Throwable {
		RechargePaymentPage step1 = (RechargePaymentPage) PageRepo.get().get(RechargePaymentPage.NAME);
		try {
			GenericErrorPopUp popup = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);
			popup.clickOnOKPopUp();
		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			GenericErrorPopUp popup = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);
			popup.isPermissionRequiredSamsung("NO");
		} catch (Exception e) {
			// TODO: handle exception
		}

		step1.clickOnRechargePostePay();
	}

	@When("^Utente clicca su Ricarica La mia Postepay$")
	public void utente_clicca_su_Ricarica_La_mia_Postepay() throws Throwable {
		// RechargePaymentPage step1=(RechargePaymentPage)
		// PageRepo.get().get(RechargePaymentPage.NAME);
		//
		// step1.clickOnRechargeMyPostePay();
		SelectPostepayRecharge selectPostePay = (SelectPostepayRecharge) PageRepo.get()
				.get(SelectPostepayRecharge.NAME);

		selectPostePay.clickMyPostePay();
	}

	@When("^Utente clicca su Ricarica Postepay intestata ad altri$")
	public void utente_clicca_su_Ricarica_Postepay_intestata_ad_altri() throws Throwable {
		SelectPostepayRecharge selectPostePay = (SelectPostepayRecharge) PageRepo.get()
				.get(SelectPostepayRecharge.NAME);

		selectPostePay.clickOtherPostePay();
	}

	@When("^Utente seleziona la propria carta da ricaricare$")
	public void utente_seleziona_la_propria_carta_da_ricaricare(List<UserDataCredential> dataTable) throws Throwable {
		UserDataCredential table = dataTable.get(0);
		double discrepanza = StringAndNumberOperationTools.convertStringToDouble(table.getDiscrepanza());

		RechargeMyPostepayPageStep1 step2 = (RechargeMyPostepayPageStep1) PageRepo.get()
				.get(RechargeMyPostepayPageStep1.NAME);

		if (this.isIOS) {
			step2.verifyLayoutPage(driverAndroid, "Numero carta", "EVOLUTION", table.getCardNumberDestinatario(),
					discrepanza);
		} else {
			step2.verifyHeaderpage(
					TestEnvironment.get().getLanguage("header").getProperty("RechargeMyPostepayPageStep3"));
			step2.verifyLayoutPage(driverAndroid, "Scegli la carta da ricaricare", "CONNECT",
					table.getCardNumberDestinatario(), discrepanza);
		}
		step2.selectCardToRecharge(driverAndroid, table.getCardNumberDestinatario());
	}

	@When("^Utente compila il form della ricarica mia postepay$")
	public void utente_compila_il_form_della_ricarica_mia_postepay(List<UserDataCredential> dataTable)
			throws Throwable {
		UserDataCredential table = dataTable.get(0);

		RechargeMyPostepayPageStep2 step3 = (RechargeMyPostepayPageStep2) PageRepo.get()
				.get(RechargeMyPostepayPageStep2.NAME);

		step3.insertAmountToSendTargetUser(table.getImporto());

		step3.insertReasonOfRecharge(table.getComment());
	}

	/*
	 * Step implementazione RICARICA LA ALTRA POSTEPAY RICORRENTE
	 */

	@When("^Utente clicca sul toggle Ricarica automatica$")
	public void utente_clicca_sul_toggle_Ricarica_automatica() throws Throwable {
		RechargeOtherPostepayPageStep1 step2 = (RechargeOtherPostepayPageStep1) PageRepo.get()
				.get(RechargeOtherPostepayPageStep1.NAME);

		step2.clickOnToggleAutomatickRecharge();
	}

	@When("^Utente compila il form ricarica automatica$")
	public void utente_compila_il_form_ricarica_automatica(List<UserDataCredential> dataTable) throws Throwable {
		UserDataCredential table = dataTable.get(0);

		AutomaticRechargePostepay step3 = (AutomaticRechargePostepay) PageRepo.get()
				.get(AutomaticRechargePostepay.NAME);

		if (table.getTargetOperation().equals("AutomaRechargeSoglia")) {
			step3.clickOnRechargSOGLIA();
			step3.sendSaldoMinimo(table.getImporto());
			step3.selectRecurrencyRechargeDate(table.getRecurrencyRechargeDate());
			step3.selectRecurrencyRechargeName(table.getRecurrencyRechargeName());
		} else {
			step3.compileFormAutomaticRecharge(table.getRecurrencyRecharge(), table.getRecurrencyRechargeDate(),
					table.getRecurrencyRechargeName());
		}
	}

	@When("^Utente clicca sulla card RICARICA IN AUTOMATICO$")
	public void utente_clicca_sulla_card_RICARICA_IN_AUTOMATICO() throws Throwable {
		RechargePaymentPage page = (RechargePaymentPage) PageRepo.get().get(RechargePaymentPage.NAME);

		page.clickOnCardAutomaticRechargeSection();

	}

	@When("^Utente effettua lo swipe sul movimento della ricarica ricorrente$")
	public void utente_effettua_lo_swipe_sul_movimento_della_ricarica_ricorrente(List<UserDataCredential> dataTable)
			throws Throwable {
		UserDataCredential table = dataTable.get(0);

		AutomaticRechargePostepaySectionPage rechargePage = (AutomaticRechargePostepaySectionPage) PageRepo.get()
				.get(AutomaticRechargePostepaySectionPage.NAME);

		rechargePage.swipOnSpecificRecharge(table.getRecurrencyRechargeName());
	}

	@When("^Utente clicca su Cestino$")
	public void utente_clicca_su_Cestino() throws Throwable {
		AutomaticRechargePostepaySectionPage rechargePage = (AutomaticRechargePostepaySectionPage) PageRepo.get()
				.get(AutomaticRechargePostepaySectionPage.NAME);

		rechargePage.clickOnDeleteAutomaticRecharge();
	}

	@When("^Utente verifica la conformita del popup di \"([^\"]*)\" e clicca su \"([^\"]*)\"$")
	public void utente_verifica_la_conformita_del_popup_di_e_clicca_su(String popUpTipe, String OK) throws Throwable {

		WaitManager.get().waitShortTime();

		if (popUpTipe.equals("ConfirmDeleteAutoRecharge")) {
			DeleteAutomaticRechargePage popUp = (DeleteAutomaticRechargePage) PageRepo.get()
					.get(DeleteAutomaticRechargePage.NAME);

			popUp.verifyHeaderPopUp();

			popUp.verifyCopyPopUp();

			if (OK.equals("ANNULLA")) {
				popUp.clickOnANNULLAbutton();

			} else {
				popUp.clickOnOKbutton();
			}
		}
	}

	@Then("^Utente clicca sul tab HOME dalla schermata \"([^\"]*)\"$")
	public void utente_clicca_sul_tab_HOME_dalla_schermata(String fromPage) throws Throwable {
		try {
			GenericErrorPopUp popupAuth = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);
			popupAuth.closeAtacPopup();
		} catch (Exception e) {
			// TODO: handle exception
		}
		if (fromPage.equals("PAGA")) {
			PaymentPaymentPage payPage = (PaymentPaymentPage) PageRepo.get().get(PaymentPaymentPage.NAME);

			payPage.clickOnMenuTabIcon();
		}
	}

	@Then("^Utente clicca sull icona dei messaggi$")
	public void utente_clicca_sull_icona_dei_messaggi() throws Throwable {
		HomePage homePage = (HomePage) PageRepo.get().get(HomePage.NAME).get();
		GenericErrorPopUp popup=(GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);
		popup.clickOnApplePayPopupCloseBtn();

		homePage.clickAllMessageNumber();
	}

	@When("^Utente cliccca sul tab dei messaggi e cerca \"([^\"]*)\" e \"([^\"]*)\"$")
	public void utente_cliccca_sul_tab_dei_messaggi_e_cerca_e(String tipeMessage, String tipeDate,
			List<UserDataCredential> dataTable) throws Throwable {
		UserDataCredential table = dataTable.get(0);
		String msg = table.getTipeMessage();
		String date = table.getDateOfOperation();

		if (msg.equals("Conferma revoca Ricarica Automatica")) {

			BachecaPage rechargePage = (BachecaPage) PageRepo.get().get(BachecaPage.NAME);

			rechargePage.clickOnMESSAGGItab();

			rechargePage.verifyThereIsASpecificAutomaticRecharge(msg, date);
		} else if (msg.equals("Ricarica Carta Postepay da App")) {
			BachecaPage rechargePage = (BachecaPage) PageRepo.get().get(BachecaPage.NAME);
			rechargePage.clickOnMESSAGGItab();
			rechargePage.verifyThereIsASpecificAutomaticRecharge(msg, date);
		}

	}

	@Then("^E presente il messaggio di revoca della ricarica ricorrente precedentemente eliminata$")
	public void e_presente_il_messaggio_di_revoca_della_ricarica_ricorrente_precedentemente_eliminata()
			throws Throwable {
		DettailsMessageConfirmRemovingAutomaticRecharge msgPage = (DettailsMessageConfirmRemovingAutomaticRecharge) PageRepo
				.get().get(DettailsMessageConfirmRemovingAutomaticRecharge.NAME);

		msgPage.verifyHeaderPage();
		msgPage.verifyMessageDetailDescription();

		WaitManager.get().waitLongTime();
		msgPage.clickOnCloseButton();
	}

	@When("^Utente clicca sul link BANCOPOSTA$")
	public void utente_clicca_sul_link_BANCOPOSTA() throws Throwable {
		RechargeOtherPostepayPageStep2 page = (RechargeOtherPostepayPageStep2) PageRepo.get()
				.get(RechargeOtherPostepayPageStep2.NAME);

		page.clickOnLinkBancoPosta();

		WebviewGenericPage pageWeb = (WebviewGenericPage) PageRepo.get().get(WebviewGenericPage.NAME);

		// pageWeb.isLinkToBancopostaStore();
		pageWeb.verifyHeaderBancopostaApp(driverAndroid);
	}

	/*
	 * Step implementazione G2G
	 */

	@When("^Utente clicca sul tab G(\\d+)G invia Giga$")
	public void utente_clicca_sul_tab_G_G_invia_Giga(int arg1) throws Throwable {

		CommunityCardOnePage com1 = (CommunityCardOnePage) PageRepo.get().get(CommunityCardOnePage.NAME);
		// com1.swipOnG2g();
		com1.swipOnG2g(driverAndroid);

		CommunityCardTwoPage com2 = (CommunityCardTwoPage) PageRepo.get().get(CommunityCardTwoPage.NAME);
		com2.clickOnSendG2G(driverAndroid);
	}

	@When("^Utente verifica il layout della pagina Invio Giga e clicca su Procedi$")
	public void utente_verifica_il_layout_della_pagina_Invio_Giga_e_clicca_su_Procedi(List<LayoutDataBean> dataTable)
			throws Throwable {
		LayoutDataBean data = dataTable.get(0);
		double discrepanza = StringAndNumberOperationTools.convertStringToDouble(data.getDiscrepanza());

		SendG2GPageStep1 page = (SendG2GPageStep1) PageRepo.get().get(SendG2GPageStep1.NAME);

		page.verifyHeaderpage(TestEnvironment.get().getLanguage("header").getProperty("SendG2GPageStep1"));
		page.verifyLayoutPage();
		// page.verifyImageCard(driverAndroid, discrepanza);
		page.clickOnProcedi();
	}

	@When("^Utente seleziona il destinatario del G(\\d+)G tal tab CONTATTI G(\\d+)G$")
	public void utente_seleziona_il_destinatario_del_G_G_tal_tab_CONTATTI_G_G(int arg1, int arg2,
			List<UserDataCredential> dataTable) throws Throwable {

		UserDataCredential table = dataTable.get(0);
		try {
			System.out.println("PopUp di permessi Rubrica - Allow");
			GenericErrorPopUp popupAuth = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);

			String permissionTextButton = "Allow";
			popupAuth.isPermissionRequiredSamsung(permissionTextButton);
		} catch (Exception e) {
			// TODO: handle exception
		}

		try {
			System.out.println("PopUp di permessi Rubrica - ALLOW");
			GenericErrorPopUp popupAuth = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);

			String permissionTextButton = "ALLOW";
			popupAuth.isPermissionRequiredSamsung(permissionTextButton);

		} catch (Exception e) {
			// TODO: handle exception
		}

		try {
			System.out.println("PopUp di permessi Rubrica - CONSENTI");
			GenericErrorPopUp popupAuth = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);

			String permissionTextButton = "Consenti";
			popupAuth.isPermissionRequiredSamsung(permissionTextButton);

		} catch (Exception e) {
			// TODO: handle exception
		}
		SendG2GPageStep2 step2 = (SendG2GPageStep2) PageRepo.get().get(SendG2GPageStep2.NAME);
		try {
			System.out.println("PopUp di permessi Rubrica - OK");
			GenericErrorPopUp popupAuth = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);

			String permissionTextButton = "OK";
			popupAuth.isPermissionRequiredSamsung(permissionTextButton);
		} catch (Exception e) {
			// TODO: handle exception
		}

		step2.searchContact(table.getTargetUser());

	}

	@When("^Utente seleziona il destinatario del G(\\d+)G dal tab CONTATTI generici$")
	public void utente_seleziona_il_destinatario_del_G_G_dal_tab_CONTATTI_generici(int arg1,
			List<UserDataCredential> dataTable) throws Throwable {
		UserDataCredential table = dataTable.get(0);
		try {
			System.out.println("PopUp di permessi Rubrica - Allow");
			GenericErrorPopUp popupAuth = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);

			String permissionTextButton = "Allow";
			popupAuth.isPermissionRequiredSamsung(permissionTextButton);
		} catch (Exception e) {
			// TODO: handle exception
		}

		try {
			System.out.println("PopUp di permessi Rubrica - ALLOW");
			GenericErrorPopUp popupAuth = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);

			String permissionTextButton = "ALLOW";
			popupAuth.isPermissionRequiredSamsung(permissionTextButton);

		} catch (Exception e) {
			// TODO: handle exception
		}

		try {
			System.out.println("PopUp di permessi Rubrica - CONSENTI");
			GenericErrorPopUp popupAuth = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);

			String permissionTextButton = "CONSENTI";
			popupAuth.isPermissionRequiredSamsung(permissionTextButton);

		} catch (Exception e) {
			// TODO: handle exception
		}

		SendG2GPageStep2 step2 = (SendG2GPageStep2) PageRepo.get().get(SendG2GPageStep2.NAME);
		step2.clickOnContattiTab();
		step2.searchContact(table.getTargetUser());

	}

	@When("^Utente verifica il layout della pagina G(\\d+)G e clicca su procedi$")
	public void utente_verifica_il_layout_della_pagina_G_G_e_clicca_su_procedi(int arg1, List<LayoutDataBean> dataTable)
			throws Throwable {
		LayoutDataBean data = dataTable.get(0);
		double discrepanza = StringAndNumberOperationTools.convertStringToDouble(data.getDiscrepanza());

		SendG2GPageStep3 step3 = (SendG2GPageStep3) PageRepo.get().get(SendG2GPageStep3.NAME);

		// step3.verifyImageCard(driverAndroid, discrepanza);

		step3.verifyHeaderpage(TestEnvironment.get().getLanguage("header").getProperty("SendG2GPageStep1"));
		step3.verifyLayoutPage();
		step3.verifyGigaDifferentAmount();
		step3.clickOnSendG2G();
	}

	@When("^Utente sulla homepage clicca sulla card della Carta$")
	public void utente_sulla_homepage_clicca_sulla_card_della_Carta() throws Throwable {
		HomePage home = (HomePage) PageRepo.get().get(HomePage.NAME);
		GenericErrorPopUp popup=(GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);
		popup.clickOnApplePayPopupCloseBtn();

		home.scrollSbrodolo(driverAndroid);
		home.clickOnCard();

		WaitManager.get().waitShortTime();
		try {
			GenericErrorPopUp errorPopUp = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);
			errorPopUp.closeAtacPopup();
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	@When("^Utente clicca sulla card della Carta dalla sezione PRODOTTI$")
	public void utente_clicca_sulla_card_della_Carta_dalla_sezione_PRODOTTI(List<UserDataCredential> dataTable)
			throws Throwable {
		UserDataCredential table = dataTable.get(0);

		if(isIOS) {
			CommandExecutionHelper.execute((ExecutesMethod) driverAndroid, setSettingsCommand("snapshotMaxDepth", 100));
		}


		ProductsStandardPage product = (ProductsStandardPage) PageRepo.get().get(ProductsStandardPage.NAME);

		product.verifyHeader(TestEnvironment.get().getLanguage("header").getProperty("ProductsStandardPage"));
		product.verifyLayoutPage();

		if (table.getUsername().equals("vincenzo.fortunato-4869")) {
			product.verifyPresenceOfG2GCard();
		}

		product.clickOnCard();

		if(isIOS) {
			CommandExecutionHelper.execute((ExecutesMethod) driverAndroid, setSettingsCommand("snapshotMaxDepth", 20));
		}

	}

	// Varibili per controllo su decremento importi
	double oldContabile;
	double oldDisponibile;
	double amountSpendGigaPurchase;

	@When("^Utente memorizza importo disponibile ed importo contabile$")
	public void utente_memorizza_importo_disponibile_ed_importo_contabile() throws Throwable {
		ProductsConnectCardDetailsPage detailAmount = (ProductsConnectCardDetailsPage) PageRepo.get()
				.get(ProductsConnectCardDetailsPage.NAME);

		detailAmount.verifyHeader(
				TestEnvironment.get().getLanguage("header").getProperty("ProductsConnectCardDetailsPage"));
		detailAmount.verifyLayoutPage();

		this.oldContabile = detailAmount.getAmountContabile();
		this.oldDisponibile = detailAmount.getAmountDisponibile();
	}

	@When("^Utente clicca su back$")
	public void utente_clicca_su_back() throws Throwable {
		WaitManager.get().waitShortTime();
		if (this.isIOS) {
			ProductsConnectCardDetailsPage product = (ProductsConnectCardDetailsPage) PageRepo.get()
					.get(ProductsConnectCardDetailsPage.NAME);
			product.clickOnBack();
		} else {
			((PressesKey) driverAndroid).pressKey(new KeyEvent(AndroidKey.BACK));
		}
		WaitManager.get().waitMediumTime();
	}

	@When("^Utente clicca su Gestisci SIM e invia Giga$")
	public void utente_clicca_su_Gestisci_SIM_e_invia_Giga() throws Throwable {
		ProductsStandardPage product = (ProductsStandardPage) PageRepo.get().get(ProductsStandardPage.NAME);
		try {
			GenericErrorPopUp errorPopUp = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);
			errorPopUp.closeAtacPopup();
		} catch (Exception e) {
			// TODO: handle exception
		}
		if(isIOS) {
			CommandExecutionHelper.execute((ExecutesMethod) driverAndroid, setSettingsCommand("snapshotMaxDepth", 100));
			product.clickOnG2GLabel();
		}
		else {
			product.clickOnG2GLabel();
		}
		WaitManager.get().waitMediumTime();
	}

	@When("^Utente clicca su Acquista nella sezione SIM Connect$")
	public void utente_clicca_su_Acquista_nella_sezione_SIM_Connect() throws Throwable {
		DetailsConnectSimPurchaseAndSendGigaPage g2gPage = (DetailsConnectSimPurchaseAndSendGigaPage) PageRepo.get()
				.get(DetailsConnectSimPurchaseAndSendGigaPage.NAME);

		g2gPage.verifyLayoutPage();
		g2gPage.clickOnPurchaseGiga();
	}

	@When("^Utente clicca sul bottone indietro dalla toolbar$")
	public void utente_clicca_sul_bottone_indietro_dalla_toolbar() throws Throwable {
		DetailsConnectSimPurchaseAndSendGigaPage g2gPage = (DetailsConnectSimPurchaseAndSendGigaPage) PageRepo.get()
				.get(DetailsConnectSimPurchaseAndSendGigaPage.NAME);
		g2gPage.clickOnBack();
	}

	@When("^Verifica il riepilogo del acquisto dei Giga e clicca su paga$")
	public void verifica_il_riepilogo_del_acquisto_dei_Giga_e_clicca_su_paga() throws Throwable {
		DetailsConnectSimPurchaseGigaPage page = (DetailsConnectSimPurchaseGigaPage) PageRepo.get()
				.get(DetailsConnectSimPurchaseGigaPage.NAME);

		page.verifyHeader(TestEnvironment.get().getLanguage("header").getProperty("DetailsConnectSimPurchaseGigaPage"));
		page.verifyLayoutPage();
		this.amountSpendGigaPurchase = page.getAmountToPay();

		page.clickOnPurchaseGiga();
	}

	@Then("^Utente torna nella sezione PRODOTTI$")
	public void utente_torna_nella_sezione_PRODOTTI() throws Throwable {
		WaitManager.get().waitMediumTime();
		if (this.isIOS) {
			ProductsConnectCardDetailsPage products = (ProductsConnectCardDetailsPage) PageRepo.get()
					.get(ProductsConnectCardDetailsPage.NAME);
			products.clickOnBack();
		} else {
			((PressesKey) driverAndroid).pressKey(new KeyEvent(AndroidKey.BACK));
		}
		WaitManager.get().waitMediumTime();
	}

	@Then("^Utente verifica che importo disponibile ed importo contabile siano stati decrementati del costo dei giga$")
	public void utente_verifica_che_importo_disponibile_ed_importo_contabile_siano_stati_decrementati_del_costo_dei_giga()
			throws Throwable {
		// Effettuo il calcolo utilizzando i dati catturati prima dell'operazione
		double newDisponibile = this.oldDisponibile - this.amountSpendGigaPurchase;
		double newContabile = this.oldContabile - this.amountSpendGigaPurchase;

		// Confrontiamo i dati con quelli che vediamo a video
		ProductsConnectCardDetailsPage detailAmount = (ProductsConnectCardDetailsPage) PageRepo.get()
				.get(ProductsConnectCardDetailsPage.NAME);
		ProductsStandardPage product = (ProductsStandardPage) PageRepo.get().get(ProductsStandardPage.NAME);
		product.clickOnCard();
		detailAmount.verifyHeader(
				TestEnvironment.get().getLanguage("header").getProperty("ProductsConnectCardDetailsPage"));
		detailAmount.verifyLayoutPage();
		detailAmount.verifyDecrementoSaldo(newContabile, newDisponibile);

	}

	@And("^Utente sulla homepage clicca sul tab prodotti$")
	public void utente_sulla_homepage_clicca_sul_tab_prodotti() throws Throwable {
		HomePage home = (HomePage) PageRepo.get().get(HomePage.NAME);
		GenericErrorPopUp popup=(GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);
		popup.clickOnApplePayPopupCloseBtn();
		home.clickOnProductsTabIcon();
		WaitManager.get().waitShortTime();
		try {
			popup = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);
			popup.closeAtacPopup();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@And("^Utente clicca su bottone G2G per inviare giga$")
	public void utente_clicca_su_bottone_g2g_per_inviare_giga() throws Throwable {
		DetailsConnectSimPurchaseAndSendGigaPage details = (DetailsConnectSimPurchaseAndSendGigaPage) PageRepo.get()
				.get(DetailsConnectSimPurchaseAndSendGigaPage.NAME);
		// details.verifyLayoutPage();
		details.clickOnSendG2G();
		WaitManager.get().waitShortTime();
	}

	/*
	 * Step implementazione Lgin con Fingerprint
	 */

	@Then("^Utente atterra sulla home page e clicca sul hamburger menu$")
	public void utente_atterra_sulla_home_page_e_clicca_sul_hamburger_menu() throws Throwable {
		HomePage home = (HomePage) PageRepo.get().get(HomePage.NAME);
		GenericErrorPopUp popup=(GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);

		if(isIOS) {
			popup.clickOnApplePayPopupCloseBtn();			
		}


		popup.clickOnNonMostrarePiu();

		home.clickOnHamburgerMenu();
	}

	@Then("^Utente clicca su impostazioni$")
	public void utente_clicca_su_impostazioni() throws Throwable {
		HamburgerMenuHomePage menu = (HamburgerMenuHomePage) PageRepo.get().get(HamburgerMenuHomePage.NAME);

		menu.clickOnSettings();
	}

	@Then("^Utente accede alla sezione Accessi e Autorizzazioni$")
	public void utente_accede_alla_sezione_Accessi_e_Autorizzazioni() throws Throwable {
		SettingPage setting = (SettingPage) PageRepo.get().get(SettingPage.NAME);

		setting.verifyHeaderPage(driverAndroid, TestEnvironment.get().getLanguage("header").getProperty("SettingPage"));

		setting.clickOnAccessAndAuthorization();

	}

	@Then("^Utente imposta su \"([^\"]*)\" accesso con impronta$")
	public void utente_imposta_su_accesso_con_impronta(String targetStatus) throws Throwable {
		AccessAndAuthorizationPage page = (AccessAndAuthorizationPage) PageRepo.get()
				.get(AccessAndAuthorizationPage.NAME);

		if (targetStatus.equals("si")) {

			page.verifyIfAccessToggle("disable");

			page.clickOnAccessWithFingerprint();

		} else if (targetStatus.equals("no")) {

			page.verifyIfAccessToggle("enable");

			page.clickOnAccessWithFingerprint();
		}
	}

	@Then("^Utente clicca su ABILITA al popup abilita impronta digitale$")
	public void utente_clicca_su_ABILITA_al_popup_abilita_impronta_digitale() throws Throwable {
		AccessAndAuthorizationPage page = (AccessAndAuthorizationPage) PageRepo.get()
				.get(AccessAndAuthorizationPage.NAME);

		page.verifyLayoutPopupEnableFingerprint();
		page.clickOnEnableOnPopupFingerprint();
	}

	@Then("^Utente inserisce il posteID$")
	public void utente_inserisce_il_posteID(List<UserDataCredential> dataDable) throws Throwable {
		UserDataCredential table = dataDable.get(0);

		RechargeMyPostepayPageStep5 posteId = (RechargeMyPostepayPageStep5) PageRepo.get()
				.get(RechargeMyPostepayPageStep5.NAME);

		posteId.verifyHeaderPage(
				TestEnvironment.get().getLanguage("header").getProperty("RechargeMyPostepayPageStep5"));

		posteId.insertPosteId(table.getPosteid());

		posteId.clickOnConfirmButton();

	}

	@Then("^Utente inserisce il finger print per l'abilitazione$")
	public void utente_inserisce_il_finger_print_per_l_abilitazione(List<UserDataCredential> dataDable)
			throws Throwable {
		UserDataCredential table = dataDable.get(0);

		AccessAndAuthorizationPage page = (AccessAndAuthorizationPage) PageRepo.get()
				.get(AccessAndAuthorizationPage.NAME);

		Properties pro = UIUtils.ui().getCurrentProperties();
		this.adbExe = pro.getProperty("path.cmd.adb");
		String deviceName = pro.getProperty("ios.udid");

		System.out.println("path di ADB ---->     " + this.adbExe);

		page.verifyLayoutPopupSendFingerprint();
		int fingerID = Integer.parseInt(table.getFingerprintID());
		page.sendFingerprint(this.adbExe, deviceName, fingerID);
	}

	@Then("^Utente esce dalle impostazioni$")
	public void utente_esce_dalle_impostazioni() throws Throwable {
		WaitManager.get().waitShortTime();
		((PressesKey) driverAndroid).pressKey(new KeyEvent(AndroidKey.BACK));
		WaitManager.get().waitMediumTime();
		((PressesKey) driverAndroid).pressKey(new KeyEvent(AndroidKey.BACK));
		WaitManager.get().waitMediumTime();
	}

	@Then("^Utente effettua la logout$")
	public void utente_effettua_la_logout() throws Throwable {
		HomePage home = (HomePage) PageRepo.get().get(HomePage.NAME).get();

		home.clickOnHamburgerMenu();
		HamburgerMenuHomePage menu = (HamburgerMenuHomePage) PageRepo.get().get(HamburgerMenuHomePage.NAME);

		LayoutTools tool = new LayoutTools();
		try {
			tool.makeAScrollOnPage(menu, 500);
		} catch (Exception e) {
			// TODO: handle exception
		}
		menu.clickOnLogout();
	}

	@Then("^Utente effettua l'accesso con fingerprint$")
	public void utente_effettua_l_accesso_con_fingerprint(List<UserDataCredential> dataDable) throws Throwable {
		UserDataCredential table = dataDable.get(0);

		Properties pro = UIUtils.ui().getCurrentProperties();
		// System.out.println("\n\nNOTE ##### \n" + pro);

		// Popup device model fittizzio
		String emu = pro.getProperty("emulator");
		clickOkPopupEmaulator(emu);
		WaitManager.get().waitLongTime();

		// Click su Accedi
		NewAccessPage accessPage = (NewAccessPage) PageRepo.get().get(NewAccessPage.NAME);
		accessPage.clickOnAccedi();

		// Invia il fingerpritn
		LoginWithPosteId posteId = (LoginWithPosteId) PageRepo.get().get(LoginWithPosteId.NAME);
		posteId.verifyLayoutPopupSendFingerprint();

		int fingerID = Integer.parseInt(table.getFingerprintID());

		this.adbExe = pro.getProperty("path.cmd.adb");
		String deviceName = pro.getProperty("ios.udid");

		posteId.sendFingerprint(this.adbExe, deviceName, fingerID);
	}

	/*
	 * Ricarica Telefonica
	 */
	@When("^Utente clicca su \"([^\"]*)\"$")
	public void utente_clicca_su(String arg1, List<UserDataCredential> dataDable) throws Throwable {
		try {
			GenericErrorPopUp errorPopUp = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);
			errorPopUp.clickOnOKPopUp();
		} catch (Exception e) {
			// TODO: handle exception
		}
		UserDataCredential table = dataDable.get(0);
		String gestore = table.getGestoreRicarica();

		RechargePaymentPage payPage = (RechargePaymentPage) PageRepo.get().get(RechargePaymentPage.NAME);
		payPage.verifyHeaderpage(TestEnvironment.get().getLanguage("header").getProperty("RechargePaymentPage"));
		payPage.scrollToOtherSIM(driverAndroid);

		switch (gestore) {
		case "poste":
			payPage.clickOnRechargeTelephonePosteSim();
			break;
		case "vodafone":
			payPage.clickOnRechargeTelephoneVodafone();
			break;
		case "windTre":
			//			payPage.cl
			break;
		default:
			System.out.println("E' stato selezionato il default case con PosteMobile");
			payPage.clickOnRechargeTelephonePosteSim();
			break;
		}

		// Controllo sul numero di carte a disposizione dell'utente
		String cardChoose = table.getCardNumber();
		String nCards = table.getUserNumberCards();

		// Gestione nel caso di una o piu carte in possesso dell'utente
		// vincenzo fortunato : 2
		// luca petrucci: 1
		SendP2PPageStep2 step2 = (SendP2PPageStep2) PageRepo.get().get(SendP2PPageStep2.NAME);

		if (nCards.equals("2")) {
			SendP2PPageStep1 howPay = (SendP2PPageStep1) PageRepo.get().get(SendP2PPageStep1.NAME);
			howPay.verifyHeaderPage(TestEnvironment.get().getLanguage("header").getProperty("SendP2PPageStep1"));
			howPay.clickOnCard(driverAndroid, cardChoose);
		}
	}

	@When("^Utente inserisce il numero di telefono da ricaricare$")
	public void utente_inserisce_il_numero_di_telefono_da_ricaricare(List<UserDataCredential> dataDable)
			throws Throwable {
		UserDataCredential table = dataDable.get(0);

		ThelephoneRechargePageStep1 page = (ThelephoneRechargePageStep1) PageRepo.get()
				.get(ThelephoneRechargePageStep1.NAME);

		page.verifyHeader(TestEnvironment.get().getLanguage("header").getProperty("ThelephoneRechargePageStep1"));
		page.verifyLayoutPage();
		System.out.println("Telephone: " + table.getNumeroTel());

		Properties pro = UIUtils.ui().getCurrentProperties();
		this.adbExe = pro.getProperty("path.cmd.adb");

		page.sendTelephoneNumber(table.getNumeroTel(), adbExe);
		System.out.println("Telephone inviato");
	}

	@When("^Utente verifica che il campo operatore e valorizzato e seleziona importo e clicca su procedi$")
	public void utente_verifica_che_il_campo_operatore_e_valorizzato_e_seleziona_importo_e_clicca_su_procedi(
			List<UserDataCredential> dataDable) throws Throwable {
		UserDataCredential table = dataDable.get(0);

		ThelephoneRechargePageStep1 page = (ThelephoneRechargePageStep1) PageRepo.get()
				.get(ThelephoneRechargePageStep1.NAME);

		page.isOperatorePreValorizedString(table.getGestoreRicarica());
		page.sendAmount(table.getImporto());

		page.clickOnProceed();
	}

	@When("^Utente verifica il riepilogo della ricarica telefonica e clicca su paga$")
	public void utente_verifica_il_riepilogo_della_ricarica_telefonica_e_clicca_su_paga(
			List<UserDataCredential> dataDable) throws Throwable {
		UserDataCredential table = dataDable.get(0);

		ThelephoneRechargePageStep3 page = (ThelephoneRechargePageStep3) PageRepo.get()
				.get(ThelephoneRechargePageStep3.NAME);

		page.verifyHeader(TestEnvironment.get().getLanguage("header").getProperty("ThelephoneRechargePageStep3"));
		page.verifyLayoutPage();
		page.verifySummary(table.getCardNumber(), table.getNumeroTel(), table.getGestoreRicarica(), table.getImporto());
		page.clickOnPay();
	}

	/*
	 * Metodi Servizi
	 */

	@When("^Utente clicca su SERVIZI$")
	public void utente_clicca_su_SERVIZI() throws Throwable {
		RechargePaymentPage page = (RechargePaymentPage) PageRepo.get().get(RechargePaymentPage.NAME);
		page.verifyHeaderpage(TestEnvironment.get().getLanguage("header").getProperty("RechargePaymentPage"));
		try {
			GenericErrorPopUp errorPopUp = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);
			errorPopUp.clickOnOKPopUp();
		} catch (Exception e) {
			// TODO: handle exception
		}
		page.clickOnTabService();
	}

	@When("^Utente clicca sul servizio \"([^\"]*)\"$")
	public void utente_clicca_sul_servizio(String arg1, List<UserDataCredential> dataDable) throws Throwable {
		UserDataCredential table = dataDable.get(0);

		ServicePaymentPage page = (ServicePaymentPage) PageRepo.get().get(ServicePaymentPage.NAME);
		Properties devName; 
		AdbCommandPrompt adbCmd;
		switch (table.getServiceType()) {
		case "Biglietto Extraurbano":
			if(this.isIOS) {
				System.out.println("Step utile per IOS");
			} 
			else {
				devName = UIUtils.ui().getCurrentProperties();
				adbCmd = new AdbCommandPrompt(adbExe, devName.getProperty("ios.udid"));
				adbCmd.enableGps();
			}
			page.clickOnCardExtraurbano(driverAndroid);
			break;
		case "Biglietto Urbano":
			page.clickOnCardUrbano();
			break;
		case "Carburante":
			// driverAndroid.setLocation(new Location(41.8935, 12.5159, 48)); // Coordinate
			// pompa di benzina ROMA Scalo san lorenzo
			// driverAndroid.setLocation(new Location(40.8370234, 14.1893528, 80)); //
			// Coordinate pompa di benzina NAPOLI Via Vicinale Cupa Cintia
			((LocationContext) driverAndroid).setLocation(new Location(40.850664, 14.356630, 69)); // Coordinate pompa
			// di benzina
			// Cercola Via
			// Europa
			// driverAndroid.setLocation(new Location(41.903971, 12.443081, 39)); //
			// Coordinate pompa di benzina ROMA Via Anastasio II
			// driverAndroid.setLocation(new Location(45.453746, 9.220855, 113)); //
			// Coordinate pompa di benzina MILANO Via Monte Cimone, 9, 20137 Milano MI
			WaitManager.get().waitMediumTime();
			page.clickOnCardCarburante(driverAndroid);

			GenericErrorPopUp permissionContacts = null;
			try {
				permissionContacts = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);
				permissionContacts.isPermissionRequired();

			} catch (Exception e) {
				// TODO: handle exception
			}

			break;
		case "Parcheggio":
			if(this.isIOS) {
				((LocationContext) driverAndroid).setLocation(new Location(41.90770163390122, 12.44511031609513, 108));
				System.out.println("Step utile per IOS");
			} 
			else {
				devName = UIUtils.ui().getCurrentProperties();
				//adbCmd = new AdbCommandPrompt(adbExe, devName.getProperty("ios.udid"));
				adbCmd = new AdbCommandPrompt(adbExe, 
						devName.containsKey("ios.udid") ? 
								devName.getProperty("ios.udid") : 
									devName.getProperty("capability.udid.string"));
				adbCmd.enableGps();
				((LocationContext) driverAndroid).setLocation(new Location(41.90770163390122, 12.44511031609513, 108));
			}
			page.clickOnCardParcheggio();
			break;
		default:
			break;
		}

		GenericErrorPopUp permissionContacts = null;
		try {
			permissionContacts = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);
			String permissionTextButton = "permission_allow";
			//			permissionContacts.isPermissionRequired();	
			System.out.println("CLICK ON POPUP");
			permissionContacts.genericPermission(permissionTextButton);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@When("^Utente seleziona il luogo di \"([^\"]*)\"$")
	public void utente_seleziona_il_luogo_di(String set, List<UserDataCredential> dataDable) throws Throwable {
		UserDataCredential table = dataDable.get(0);

		ExtraurbanPurchasePageStep1 page = (ExtraurbanPurchasePageStep1) PageRepo.get()
				.get(ExtraurbanPurchasePageStep1.NAME);
		page.verifyHeaderpage(TestEnvironment.get().getLanguage("header").getProperty("ExtraurbanPurchasePageStep1"));
		page.verifyLayoutPage();

		switch (set) {
		case "partenza":
			page.clickOnChooseStartPlace();
			page.sendStartPlace(driverAndroid, table.getFromCity());
			WaitManager.get().waitMediumTime();
			break;
		case "destinazione":
			page.clickOnChooseDestinationPlace();
			page.sendDestinationPlace(driverAndroid, table.getToCity());
			WaitManager.get().waitMediumTime();
			break;
		default:
			break;
		}
	}

	@When("^Utente inserisce le informazioni aggiuntive del viaggio$")
	public void utente_inserisce_le_informazioni_aggiuntive_del_viaggio(List<UserDataCredential> dataDable)
			throws Throwable {
		UserDataCredential table = dataDable.get(0);

		ExtraurbanPurchasePageStep1 page = (ExtraurbanPurchasePageStep1) PageRepo.get()
				.get(ExtraurbanPurchasePageStep1.NAME);

		// Scalabili a seconda se moreInfo è:
		// si : nel caso di modifica della data e del numero di passeggeri
		// no : condizione standard con data di oggi, 1 passeggero adulto e 0 passeggeri
		// ragazzi
		if (table.getMoreInfo().equals("si")) {
			// TODO
		} else {
			page.verifyPassenger(table.getMoreInfo());
			page.verifyData(table.getMoreInfo());
		}
	}

	@When("^Utente clicca su CERCA BIGLIETTO$")
	public void utente_clicca_su_CERCA_BIGLIETTO() throws Throwable {
		ExtraurbanPurchasePageStep1 page = (ExtraurbanPurchasePageStep1) PageRepo.get()
				.get(ExtraurbanPurchasePageStep1.NAME);

		page.clickOnSearchTicket();
		WaitManager.get().waitMediumTime();
		WaitManager.get().waitLongTime();
	}

	@When("^Utente seleziona il biglietto tra quelli disponibili e clicca su SCEGLI$")
	public void utente_seleziona_il_biglietto_tra_quelli_disponibili_e_clicca_su_SCEGLI() throws Throwable {
		ExtraurbanPurchasePageStep2 page = (ExtraurbanPurchasePageStep2) PageRepo.get()
				.get(ExtraurbanPurchasePageStep2.NAME);

		WaitManager.get().waitMediumTime();
		WaitManager.get().waitMediumTime();
		page.verifyHeaderpage(TestEnvironment.get().getLanguage("header").getProperty("ExtraurbanPurchasePageStep2"));
		page.verifyLayoutPage();
		page.clickOnChooseTicket();
		WaitManager.get().waitMediumTime();
	}

	@When("^Utente clicca sceglie l opzione di biglietto \"([^\"]*)\"$")
	public void utente_clicca_sceglie_l_opzione_di_biglietto(String arg1) throws Throwable {
		ExtraurbanPurchasePageStep3 page = (ExtraurbanPurchasePageStep3) PageRepo.get()
				.get(ExtraurbanPurchasePageStep3.NAME);

		page.verifyHeaderPage(TestEnvironment.get().getLanguage("header").getProperty("ExtraurbanPurchasePageStep3"));
		page.verifyLayoutPage();
		page.clickOnExpandArrow();
		page.clickOnOrdinaryTicket();
		page.clickOnProceed();
	}

	@When("^Utente inserisce i dati dei passeggeri$")
	public void utente_inserisce_i_dati_dei_passeggeri(List<UserDataCredential> dataDable) throws Throwable {
		UserDataCredential table = dataDable.get(0);

		ExtraurbanPurchasePageStep4 page = (ExtraurbanPurchasePageStep4) PageRepo.get()
				.get(ExtraurbanPurchasePageStep4.NAME);

		page.verifyHeaderpage(TestEnvironment.get().getLanguage("header").getProperty("ExtraurbanPurchasePageStep4"));

		page.sendName(table.getNamePassenger());
		page.sendSurname(table.getSurnamePassenger());
		page.sendCard(table.getCardPassenger());
		page.sendEmail(table.getEmailPassenger());
		page.sendTelephono(table.getPhonePassenger());

		page.clickOnProceed();
	}

	@When("^Utente accetta le condizioni del \"([^\"]*)\" e clicca sul PAGA$")
	public void utente_accetta_le_condizioni_del_e_clicca_sul_PAGA(String arg1, List<UserDataCredential> dataDable)
			throws Throwable {
		UserDataCredential table = dataDable.get(0);

		ExtraurbanPurchasePageStep5 page = (ExtraurbanPurchasePageStep5) PageRepo.get()
				.get(ExtraurbanPurchasePageStep5.NAME);

		page.verifyHeaderpage(TestEnvironment.get().getLanguage("header").getProperty("ExtraurbanPurchasePageStep5"));
		page.verifyDestination(table.getFromCity(), table.getToCity());
		page.verifyName(table.getNamePassenger(), table.getSurnamePassenger());

		page.isPayButtonEnabled("disabled");
		page.clickOnPrivacy();
		page.isPayButtonEnabled("enabled");

		page.clickOnPay();
	}

	@When("^Utente verifica il riepilogo dell acquito del biglietto e clicca su paga$")
	public void utente_verifica_il_riepilogo_dell_acquito_del_biglietto_e_clicca_su_paga(
			List<UserDataCredential> dataDable) throws Throwable {
		UserDataCredential table = dataDable.get(0);

		if (table.getServiceType().equals("Biglietto Extraurbano")) {
			ExtraurbanPurchasePageStep6 page = (ExtraurbanPurchasePageStep6) PageRepo.get()
					.get(ExtraurbanPurchasePageStep6.NAME);

			page.verifyHeaderpage(
					TestEnvironment.get().getLanguage("header").getProperty("ExtraurbanPurchasePageStep6"));

			page.verifyLayoutPage();
			page.verifyDateOf();
			page.verifyPayWithCard(table.getCardNumber());

			page.clickOnPay();
		} else if (table.getServiceType().equals("Biglietto Urbano")) {
			UrbanPurchasePageStep4 page = (UrbanPurchasePageStep4) PageRepo.get().get(UrbanPurchasePageStep4.NAME);

			page.verifyHeaderpage(TestEnvironment.get().getLanguage("header").getProperty("UrbanPurchasePageStep4"));

			page.verifyLayoutPage();
			page.verifyDateOf();
			//page.verifyPayWithCard(table.getCardNumber());
			if(!page.isCurrentPayment(table.getCardNumber()))
			{
				page.clickOnCambiaMetodoPagamento();
				BankTranfertSEPAPageStep1 step1 = (BankTranfertSEPAPageStep1) PageRepo.get()
						.get(BankTranfertSEPAPageStep1.NAME);
				step1.clickOnCard(driverAndroid, table.getCardNumber());
			}

			page.clickOnPay();
		}
	}

	@When("^Utente cerca e seleziona la \"([^\"]*)\" in cui utilizzare il biglietto$")
	public void utente_cerca_e_seleziona_la_in_cui_utilizzare_il_biglietto(String arg1,
			List<UserDataCredential> dataDable) throws Throwable {
		UserDataCredential table = dataDable.get(0);

		UrbanPurchasePageStep1 page = (UrbanPurchasePageStep1) PageRepo.get().get(UrbanPurchasePageStep1.NAME);

		page.verifyHeaderpage(TestEnvironment.get().getLanguage("header").getProperty("UrbanPurchasePageStep1"));
		page.verifyLayoutPage();

		if (table.getSearchBy().equals("citta")) {
			page.searchCity(table.getToCity());
			page.clickOnSearchedCity();
		} else if (table.getSearchBy().equals("vettore")) {
			// TODO
		}

		UrbanPurchasePageStep2 page2 = (UrbanPurchasePageStep2) PageRepo.get().get(UrbanPurchasePageStep2.NAME);

		page2.verifyHeaderpage();
		page2.verifyLayoutPage();
		page2.clickOnTicketCard();
	}

	@When("^Utente verifica il riepilogo del biglietto urbano$")
	public void utente_verifica_il_riepilogo_del_biglietto_urbano(List<UserDataCredential> dataDable) throws Throwable {
		UserDataCredential table = dataDable.get(0);

		UrbanPurchasePageStep3 page = (UrbanPurchasePageStep3) PageRepo.get().get(UrbanPurchasePageStep3.NAME);

		page.verifyHeaderpage();
		page.verifyLayoutPage();

		// Scalabili a seconda se moreInfo è:
		// si : nel caso di modifica numero di passeggeri
		// no : 1 passeggero adulto e 0 passeggeri ragazzi
		if (table.getMoreInfo().equals("si")) {
			// TODO
		} else {
			page.verifyDefaultSettings();
		}
		page.clickPay();
	}

	// Salvataggio dati per controllo nella Ricevuta di pagamento
	String importoBigliettoUrbano;
	String dataBigliettoUrbano;

	@When("^Utente verifica il riepilogo dell acquito del biglietto urbano e clicca su ANNULLA$")
	public void utente_verifica_il_riepilogo_dell_acquito_del_biglietto_urbano_e_clicca_su_ANNULLA(
			List<UserDataCredential> dataDable) throws Throwable {
		UserDataCredential table = dataDable.get(0);
		UrbanPurchasePageStep4 page = (UrbanPurchasePageStep4) PageRepo.get().get(UrbanPurchasePageStep4.NAME);

		page.verifyHeaderpage(TestEnvironment.get().getLanguage("header").getProperty("UrbanPurchasePageStep4"));

		page.verifyLayoutPage();
		page.verifyDateOf();
		//page.verifyPayWithCard(table.getCardNumber());

		importoBigliettoUrbano = page.getImporto();
		dataBigliettoUrbano = page.getDate();

		page.clickOnAnnulla();
	}

	@When("^Utente verifica il riepilogo dell acquito del biglietto e clicca su ANNULLA$")
	public void utente_verifica_il_riepilogo_dell_acquito_del_biglietto_e_clicca_su_ANNULLA(
			List<UserDataCredential> dataDable) throws Throwable {
		ExtraurbanPurchasePageStep6 page = (ExtraurbanPurchasePageStep6) PageRepo.get()
				.get(ExtraurbanPurchasePageStep6.NAME);
		UserDataCredential table = dataDable.get(0);

		page.verifyHeaderpage(TestEnvironment.get().getLanguage("header").getProperty("ExtraurbanPurchasePageStep6"));

		page.verifyLayoutPage();
		page.verifyDateOf();
		page.verifyPayWithCard(table.getCardNumber());

		page.clickOnAnnulla();
	}

	@When("^Utente verifica il popup di conferma cancellazione e clicca su OK$")
	public void utente_verifica_il_popup_di_conferma_cancellazione_e_clicca_su_OK() throws Throwable {
		String popupTextLocator = "//*[@resource-id='android:id/message' and @text=\"Hai rifiutato l'operazione. Il pagamento non è stato eseguito\"]";
		// String popupTextLocator = "//*[@resource-id='android:id/message' and
		// @text=\"Hai rifiutato l'operazione. Il pagamento non è stato eseguito (Codice
		// errore: PPE-PPS-JE-8)\"]";

		String popupOkButtonLocator = "//*[@resource-id='android:id/button1' and @text='OK']";

		WebElement popupText = driverAndroid.findElement(By.xpath(popupTextLocator));
		WebElement popupOkButton = driverAndroid.findElement(By.xpath(popupOkButtonLocator));

		assertTrue(popupText.isDisplayed());

		popupOkButton.click();
		WaitManager.get().waitLongTime();
	}

	@When("^Utente ricomincia il funnel dalla schermata di riepilogo del biglietto extraurbano$")
	public void utente_ricomincia_il_funnel_dalla_schermata_di_riepilogo_del_biglietto_extraurbano(
			List<UserDataCredential> dataDable) throws Throwable {
		UserDataCredential table = dataDable.get(0);

		ExtraurbanPurchasePageStep5 page = (ExtraurbanPurchasePageStep5) PageRepo.get()
				.get(ExtraurbanPurchasePageStep5.NAME);

		page.verifyHeaderpage(TestEnvironment.get().getLanguage("header").getProperty("ExtraurbanPurchasePageStep5"));
		page.verifyDestination(table.getFromCity(), table.getToCity());
		page.verifyName(table.getNamePassenger(), table.getSurnamePassenger());

		page.isPayButtonEnabled("enabled");
		// page.clickOnPrivacy();

		page.clickOnPay();
	}

	@Then("^Utente atterra sulla TYP del Biglietto Extraurbano$")
	public void utente_atterra_sulla_TYP_del_Biglietto_Extraurbano(List<LayoutDataBean> dataDable) throws Throwable {
		LayoutDataBean dataLayout = dataDable.get(0);
		double discrepanza = StringAndNumberOperationTools.convertStringToDouble(dataLayout.getDiscrepanza());

		ServiziTYP page = (ServiziTYP) PageRepo.get().get(ServiziTYP.NAME);

		page.verifyLayoutPageGeneric(driverAndroid, dataLayout.getTargetOperation(), discrepanza);

		page.clickOnVaiAllaRicevuta();

	}

	@Then("^Utente atterra sulla TYP del biglietto urbano e clicca sul CTA Vai alla Ricevuta$")
	public void utente_atterra_sulla_TYP_del_biglietto_urbano_e_clicca_sul_CTA_Vai_alla_Ricevuta(
			List<LayoutDataBean> dataDable) throws Throwable {
		LayoutDataBean dataLayout = dataDable.get(0);
		double discrepanza = StringAndNumberOperationTools.convertStringToDouble(dataLayout.getDiscrepanza());

		ServiziTYP page = (ServiziTYP) PageRepo.get().get(ServiziTYP.NAME);

		page.verifyLayoutPageGeneric(driverAndroid, dataLayout.getTargetOperation(), discrepanza);

		page.clickOnVaiAllaRicevuta();
	}

	@Then("^Utente verifica il layout della schermata Ricevuta di pagnamento e clicca su Vai ai miei Acquisti$")
	public void utente_verifica_il_layout_della_schermata_Ricevuta_di_pagnamento_e_clicca_su_Vai_ai_miei_Acquisti(
			List<LayoutDataBean> dataDable) throws Throwable {
		LayoutDataBean dataLayout = dataDable.get(0);
		double discrepanza = StringAndNumberOperationTools.convertStringToDouble(dataLayout.getDiscrepanza());
		String cardIn = dataLayout.getCardNumber();

		UrbanoRicevutaTYP ricevutaPage = (UrbanoRicevutaTYP) PageRepo.get().get(UrbanoRicevutaTYP.NAME);

		ricevutaPage.verifyHeaderPage(TestEnvironment.get().getLanguage("header").getProperty("UrbanoRicevutaTYP"));
		// ricevutaPage.verifyImageCard(driverAndroid, discrepanza);
		ricevutaPage.verifyLayoutPage();
		ricevutaPage.verifyDataInRicevuta(importoBigliettoUrbano, dataBigliettoUrbano, cardIn);

		ricevutaPage.clickOnMieiAcquisti();
	}

	@Then("^Utente atterra nella sezione degli acquisti e apre il messaggio di conferma acquisto TRASPORTO$")
	public void utente_atterra_nella_sezione_degli_acquisti_e_apre_il_messaggio_di_conferma_acquisto_TRASPORTO(
			List<LayoutDataBean> dataDable) throws Throwable {
		LayoutDataBean dataLayout = dataDable.get(0);

		MieiAcquistiPage myPage = (MieiAcquistiPage) PageRepo.get().get(MieiAcquistiPage.NAME);

		myPage.verifyHeaderPage(TestEnvironment.get().getLanguage("header").getProperty("MieiAcquistiPage"));
		myPage.verifyLayoutPage(driverAndroid);
		myPage.clickToElementInToolbar(driverAndroid, "parcheggio");
		myPage.get();
		myPage.clickToElementInToolbar(driverAndroid, dataLayout.getTargetOperation());
		// myPage.get();
		myPage.clickOnCardSpecific(driverAndroid, dataLayout.getTitoloMessaggio(), dataBigliettoUrbano,
				importoBigliettoUrbano);

	}

	@Then("^Utente verifica il messaggio dell acquisto del biglietto urbano$")
	public void utente_verifica_il_messaggio_dell_acquisto_del_biglietto_urbano(List<LayoutDataBean> dataDable)
			throws Throwable {
		LayoutDataBean dataLayout = dataDable.get(0);
		ExtraAndurbanoAndExtraMieiAcquisti dettaglioPage = (ExtraAndurbanoAndExtraMieiAcquisti) PageRepo.get()
				.get(ExtraAndurbanoAndExtraMieiAcquisti.NAME);

		double discrepanza = StringAndNumberOperationTools.convertStringToDouble(dataLayout.getDiscrepanza());

		dettaglioPage.verifyLayoutPage(driverAndroid, dataLayout.getTargetOperation(),
				dataLayout.getTitoloDettaglioBiglietto(), dataLayout.getCompagnia(), "", "", discrepanza);

		WaitManager.get().waitMediumTime();
	}

	/*
	 * Carburante
	 */

	@When("^Utente visualizza la lista delle stazioni di rifornimento$")
	public void utente_visualizza_la_lista_delle_stazioni_di_rifornimento() throws Throwable {
		// inserire click permessi
		try {
			GenericErrorPopUp permission = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);
			permission.isPermissionPositionSamsung();
		} catch (Exception e) {
			// TODO: handle exception
		}

		FuelPurchasePageStep1 list = (FuelPurchasePageStep1) PageRepo.get().get(FuelPurchasePageStep1.NAME);

		list.verifyHeaderpage(TestEnvironment.get().getLanguage("header").getProperty("FuelPurchasePageStep1"));
		list.clickOnFuelCard();
	}

	/*
	 * 
	 * Acquisto carburante
	 * 
	 */
	@Then("^Utente clicca su PROFILO$")
	public void utente_clicca_su_PROFILO() throws Throwable {
		DateFormat df = new SimpleDateFormat("dd_MM_yyyy_HH-mm-ss");
		Date today = Calendar.getInstance().getTime();
		String reportDate = df.format(today);
		System.out.println(reportDate);

		HamburgerMenuHomePage page = (HamburgerMenuHomePage) PageRepo.get().get(HamburgerMenuHomePage.NAME);

		page.clickOnMyProfile();
	}

	@Then("^Utente clicca su Il tuo pieno$")
	public void utente_clicca_su_Il_tuo_pieno() throws Throwable {
		SettingProfile settingProfile = (SettingProfile) PageRepo.get().get(SettingProfile.NAME);

		settingProfile.verifyHeaderpage(TestEnvironment.get().getLanguage("header").getProperty("SettingProfile"));
		settingProfile.verifyLayoutPage();
		settingProfile.clickOnIlTuoPieno();
	}

	@When("^Utente clicca su Il tuo importo preferito$")
	public void utente_clicca_su_Il_tuo_importo_preferito() throws Throwable {
		SettingProfile settingProfile = (SettingProfile) PageRepo.get().get(SettingProfile.NAME);

		settingProfile.clickOnIlTuoRifornimentoPreferito();
	}

	@When("^Utente seleziona l importo di default del \"([^\"]*)\" e salva$")
	public void utente_seleziona_l_importo_di_default_del_e_salva(String arg, List<UserDataCredential> dataDable)
			throws Throwable {
		UserDataCredential table = dataDable.get(0);
		SettingProfileCarburante page = (SettingProfileCarburante) PageRepo.get().get(SettingProfileCarburante.NAME);

		page.verifyHeaderpage(arg);
		page.verifyLayoutPage(arg, driverAndroid);
		// page.checkIncrementoDecremento();
		page.checkAddSubBtn();

		if (arg.equals("pieno")) {
			page.setAmount(table.getImportoPieno());

		} else if (arg.equals("rifornimento")) {
			page.setAmount(table.getImportoPreferito());
		}
	}

	@When("^Utente esce dalla sezione profilo per tornare nella homepage$")
	public void utente_esce_dalla_sezione_profilo_per_tornare_nella_homepage() throws Throwable {
		SettingProfile settingProfile = (SettingProfile) PageRepo.get().get(SettingProfile.NAME);

		settingProfile.clickOnCHIUDI();
		//settingProfile.clickOnCHIUDIRight();
	}

	@When("^Utente verifica il layout della pagina Acquisto di carburante$")
	public void utente_verifica_il_layout_della_pagina_Acquisto_di_carburante(List<UserDataCredential> dataDable)
			throws Throwable {
		UserDataCredential table = dataDable.get(0);

		CarburanteStep1 page = (CarburanteStep1) PageRepo.get().get(CarburanteStep1.NAME);

		page.verifyHeaderpage();
		page.verifyLayoutPage();
		page.isSettedImportoPreferito(table.getImportoPreferito());
	}

	@When("^Utente verifica il corretto funzionamento delle card di prezzo fisse$")
	public void utente_verifica_il_corretto_funzionamento_delle_card_di_prezzo_fisse() throws Throwable {
		CarburanteStep1 page = (CarburanteStep1) PageRepo.get().get(CarburanteStep1.NAME);

		page.moveToOtherAmount();
	}

	@When("^Utente verifica il corretto funzionamento del tasto PIENO$")
	public void utente_verifica_il_corretto_funzionamento_del_tasto_PIENO(List<UserDataCredential> dataDable)
			throws Throwable {
		UserDataCredential table = dataDable.get(0);

		CarburanteStep1 page = (CarburanteStep1) PageRepo.get().get(CarburanteStep1.NAME);

		page.isSettedPieno(table.getImportoPieno());
	}

	@When("^Utente sceglie cinque euro di carburante da acquistare e clicca su prosegui$")
	public void utente_sceglie_cinque_euro_di_carburante_da_acquistare_e_clicca_su_prosegui() throws Throwable {
		CarburanteStep1 page = (CarburanteStep1) PageRepo.get().get(CarburanteStep1.NAME);

		page.moveTo5();
	}

	@When("^Utente seleziona il primo distributore per l erogazione del carburante e clicca su paga$")
	public void utente_seleziona_il_primo_distributore_per_l_erogazione_del_carburante_e_clicca_su_paga()
			throws Throwable {
		CarburanteStep2 page = (CarburanteStep2) PageRepo.get().get(CarburanteStep2.NAME);
		page.verifyHeaderpage();
		page.verifyLayoutPage();
		page.clickOnErogatore();
		page.clickOnPaga();
	}

	@Then("^Utente verifica il riepilogo dell acquito del carburante e clicca su paga$")
	public void utente_verifica_il_riepilogo_dell_acquito_del_carburante_e_clicca_su_paga(
			List<UserDataCredential> dataDable) throws Throwable {
		UserDataCredential table = dataDable.get(0);

		CarburanteStep3 page = (CarburanteStep3) PageRepo.get().get(CarburanteStep3.NAME);

		page.verifyHeaderpage();
		page.verifyLayoutPage();
		page.verifyDateOf();
		page.verifyPayWithCard(table.getCardNumber());
		page.clickOnPay();
	}

	@Then("^Utente atterra sui (\\d+) tutorials per l acquisto di carburante e ne verifica il layout$")
	public void utente_atterra_sui_tutorials_per_l_acquisto_di_carburante_e_ne_verifica_il_layout(int arg1,
			List<LayoutDataBean> dataDable) throws Throwable {
		LayoutDataBean dataLayout = dataDable.get(0);
		double discrepanza = StringAndNumberOperationTools.convertStringToDouble(dataLayout.getDiscrepanza());

		String configuration = TestEnvironment.get().getAppConfiguration().getProperty("app");

		//		if (dataLayout.getCarburantePage() != null && dataLayout.getCarburantePage().equals("true")
		//				&& configuration != null && configuration.equals("BS")) {
		//			return;
		//		}

		WaitManager.get().waitLongTime();
		CarburanteTYP page = (CarburanteTYP) PageRepo.get().get(CarburanteTYP.NAME);

		page.verifyLayoutPage(driverAndroid, 1, discrepanza);
		page.swipeTo(driverAndroid, 1, "RIGHT");
		page.verifyLayoutPage(driverAndroid, 2, discrepanza);
		page.swipeTo(driverAndroid, 2, "RIGHT");
		page.verifyLayoutPage(driverAndroid, 3, discrepanza);
		page.swipeTo(driverAndroid, 3, "LEFT");

		page.clickOnX();

	}

	@Then("^Utente verifica che la somma spesa sia trattenuta per (\\d+) minuti e poi restituita$")
	public void utente_verifica_che_la_somma_spesa_sia_trattenuta_per_minuti_e_poi_restituita(int arg1, DataTable arg2)
			throws Throwable {
		// TODO

	}

	/*
	 * 
	 * Salvadanaio
	 */

	@When("^Utente sulla homepage esegue uno scroll fino alla card del Salvadanaio e clicca su INIZIA$")
	public void utente_sulla_homepage_esegue_uno_scroll_fino_alla_card_del_Salvadanaio_e_clicca_su_INIZIA(
			List<LayoutDataBean> dataTable) throws Throwable {
		LayoutDataBean data = dataTable.get(0);
		// double discrepanza =
		// StringAndNumberOperationTools.convertStringToDouble(data.getDiscrepanza());
		// driverAndroid = new CommonStepImpl().getAndroidDriver();

		HomePage home = (HomePage) PageRepo.get().get(HomePage.NAME);
		GenericErrorPopUp popup=(GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);
		popup.clickOnApplePayPopupCloseBtn();

		// home.scrollToDawn("IN EVIDENZA");
		UIUtils.mobile().swipe((MobileDriver<?>) driverAndroid, SCROLL_DIRECTION.DOWN, 200);
		UIUtils.mobile().swipe((MobileDriver<?>) driverAndroid, SCROLL_DIRECTION.DOWN, 200);
		home.swithCardInEvdenza("Salvadanaio");
		//		home.scrollToSalvadanaio(driverAndroid, discrepanza);
	}

	@Then("^Utente clicca su i miei costi$")
	public void utente_clicca_su_i_miei_costi() throws Throwable {
		HamburgerMenuHomePage menu = (HamburgerMenuHomePage) PageRepo.get().get(HamburgerMenuHomePage.NAME);

		menu.clickOnMyPurchase();

	}

	@Then("^Utente verifica la presenza del biglietto$")
	public void utente_verifica_la_presenza_del_biglietto() throws Throwable {
		MieiAcquistiPage myPage = (MieiAcquistiPage) PageRepo.get().get(MieiAcquistiPage.NAME);

		myPage.verifyHeaderPage(TestEnvironment.get().getLanguage("header").getProperty("MieiAcquistiPage"));
		myPage.verifyLayoutPage(driverAndroid);
		myPage.clickToElementInToolbar(driverAndroid, "parcheggio");
		myPage.get();
		myPage.clickToElementInToolbar(driverAndroid, "urbano");
		// myPage.get();
		myPage.clickOnCardSpecific(driverAndroid, "TRASPORTO", "12/05/2020", "€ 0,90");

		ExtraAndurbanoAndExtraMieiAcquisti dettaglioPage = (ExtraAndurbanoAndExtraMieiAcquisti) PageRepo.get()
				.get(ExtraAndurbanoAndExtraMieiAcquisti.NAME);
		double discrepanza = StringAndNumberOperationTools.convertStringToDouble("0,75");

		dettaglioPage.verifyLayoutPage(driverAndroid, "urbano", "Biglietto Corsa Semplice - Comune di Lanuvio",
				"AGO UNO Srl", "", "", discrepanza);

	}

	@When("^Utente clicca sull obiettivo di nome \"([^\"]*)\" e lo apre$")
	public void utente_clicca_sull_obiettivo_di_nome_e_lo_apre(String arg1, List<LayoutDataBean> dataDable)
			throws Throwable {
		LayoutDataBean table = dataDable.get(0);
		double discrepanza = StringAndNumberOperationTools.convertStringToDouble(table.getDiscrepanza());

		SalvadanaioPage page = (SalvadanaioPage) PageRepo.get().get(SalvadanaioPage.NAME);
		System.out.println(driverAndroid.getPageSource());		
		page.verifyHeaderpage(TestEnvironment.get().getLanguage("header").getProperty("SalvadanaioPage"));
		// page.controlloLista(driverAndroid);
		page.verifyLayoutPage(driverAndroid, table.getNomeObiettivo(), discrepanza);

		page.clickOnCard(driverAndroid, table.getNomeObiettivo());
	}

	double importoPrimaVeramento;

	@When("^Utente clicca su \"([^\"]*)\" per accedere alla schermata Versa sull Obiettivo$")
	public void utente_clicca_su_per_accedere_alla_schermata_Versa_sull_Obiettivo(String nomeObiettivo,
			List<LayoutDataBean> dataDable) throws Throwable {
		LayoutDataBean table = dataDable.get(0);
		double discrepanza = StringAndNumberOperationTools.convertStringToDouble(table.getDiscrepanza());

		SalvadanaioAccantonamentoDettaglio page = (SalvadanaioAccantonamentoDettaglio) PageRepo.get()
				.get(SalvadanaioAccantonamentoDettaglio.NAME);
		page.verifyHeaderpage(
				TestEnvironment.get().getLanguage("header").getProperty("SalvadanaioAccantonamentoDettaglio"));
		//		page.verifyLayoutPage(driverAndroid, table.getNomeObiettivo(), table.getTypeObiettivo(), discrepanza);

		importoPrimaVeramento = page.getImportoVersatoSuObiettivo();

		page.clickOnVersaConPostePay();
	}

	@When("^Utente seleziona la tipologia di versamento$")
	public void utente_seleziona_la_tipologia_di_versamento() throws Throwable {
		SalvadanaioSelezionaTipologiaPage page = (SalvadanaioSelezionaTipologiaPage) PageRepo.get()
				.get(SalvadanaioSelezionaTipologiaPage.NAME);
		if (this.isIOS) {
			page.verifyLayoutPage();
			page.clickOnRicorrente();
		} else {
			page.verifyHeaderpage(
					TestEnvironment.get().getLanguage("header").getProperty("SalvadanaioSelezionaTipologia"));
			page.verifyLayoutPage();
			page.clickOnRicorrente();
		}
	}

	@When("^Utente verifica il layout della schermata Versa sull obiettivo prima di abilitare il versamento ricorrente$")
	public void utente_verifica_il_layout_della_schermata_Versa_sull_obiettivo_prima_di_abilitare_il_versamento_ricorrente(
			List<LayoutDataBean> dataDable) throws Throwable {
		LayoutDataBean table = dataDable.get(0);
		double discrepanza = StringAndNumberOperationTools.convertStringToDouble(table.getDiscrepanza());

		SalvadanaioVersaSuObiettivo page = (SalvadanaioVersaSuObiettivo) PageRepo.get()
				.get(SalvadanaioVersaSuObiettivo.NAME);
		if (this.isIOS) {
			page.verifyLayoutPage(driverAndroid, table.getNomeObiettivo(), table.getTypeObiettivo(),
					table.getIsRicorrente(), discrepanza);
		} else {
			page.verifyHeaderpage(
					TestEnvironment.get().getLanguage("header").getProperty("SalvadanaioVersaSuObiettivo"));
			page.verifyLayoutPage(driverAndroid, table.getNomeObiettivo(), table.getTypeObiettivo(),
					table.getIsRicorrente(), discrepanza);
		}
	}

	@When("^Utente abilita il toggle della ricorrenza e ne verifica il layout$")
	public void utente_inserisce_l_importo_da_versare_e_abilita_il_toggle_della_ricorrenza_e_ne_verifica_il_layout(
			List<LayoutDataBean> dataDable) throws Throwable {
		LayoutDataBean table = dataDable.get(0);
		double discrepanza = StringAndNumberOperationTools.convertStringToDouble(table.getDiscrepanza());

		SalvadanaioVersaSuObiettivo page = (SalvadanaioVersaSuObiettivo) PageRepo.get()
				.get(SalvadanaioVersaSuObiettivo.NAME);

		page.clickOnVersamentoRicorrente();

		SalvadanaioVersaSuObiettivoRicorrente pageRicorrente = (SalvadanaioVersaSuObiettivoRicorrente) PageRepo.get()
				.get(SalvadanaioVersaSuObiettivoRicorrente.NAME);
		if (this.isIOS) {
			pageRicorrente.verifyLayoutPageDefault(driverAndroid, table.getNomeObiettivo(), discrepanza);
		} else {
			pageRicorrente.verifyHeaderpage(
					TestEnvironment.get().getLanguage("header").getProperty("SalvadanaioVersaSuObiettivoRicorrente"));
			pageRicorrente.verifyLayoutPageDefault(driverAndroid, table.getNomeObiettivo(), discrepanza);
		}
		// pageRicorrente.sendAmountValue(table.getImporto());
	}

	@When("^Utente aumenta e diminuisce l importo con le icon piu e meno e verifica le modifiche di layout al raggiungimento$")
	public void utente_verifica_aumenta_e_diminuisce_l_importo_con_le_icon_piu_e_meno_e_verifica_le_modifiche_di_layout_al_raggiungimento(
			List<LayoutDataBean> dataDable) throws Throwable {
		LayoutDataBean table = dataDable.get(0);
		double discrepanza = StringAndNumberOperationTools.convertStringToDouble(table.getDiscrepanza());

		SalvadanaioVersaSuObiettivoRicorrente pageRicorrente = (SalvadanaioVersaSuObiettivoRicorrente) PageRepo.get()
				.get(SalvadanaioVersaSuObiettivoRicorrente.NAME);

		pageRicorrente.verifyLayoutDinamicoAlRaggiungimento(driverAndroid, "2settimane", table.getImporto(),
				discrepanza);
		pageRicorrente.verifyLayoutDinamicoAlRaggiungimento(driverAndroid, "1giorno", table.getImporto(), discrepanza);
	}

	@When("^Utente inserisce l importo effettivo da versare$")
	public void utente_inserisce_l_importo_effettivo_da_versare(List<LayoutDataBean> dataDable) throws Throwable {
		LayoutDataBean table = dataDable.get(0);
		double discrepanza = StringAndNumberOperationTools.convertStringToDouble(table.getDiscrepanza());

		SalvadanaioVersaSuObiettivoRicorrente pageRicorrente = (SalvadanaioVersaSuObiettivoRicorrente) PageRepo.get()
				.get(SalvadanaioVersaSuObiettivoRicorrente.NAME);

		// pageRicorrente.verifyLayoutDinamicoAlRaggiungimento(driverAndroid, "oltre",
		// table.getImporto(), discrepanza);
		pageRicorrente.clickOnPlus(1);
		pageRicorrente.clickOnMinus(1);
		pageRicorrente.sendAmountValue(table.getImporto());
		//		pageRicorrente.clickOnCONTINUA();
	}

	@When("^Utente verifica che le opzioni di frequenza per il versamento ricorrente siano presenti e ne seleziona una$")
	public void utente_verifica_che_le_opzioni_di_frequenza_per_il_versamento_ricorrente_siano_presenti_e_ne_seleziona_una(
			List<LayoutDataBean> dataDable) throws Throwable {
		LayoutDataBean table = dataDable.get(0);

		SalvadanaioVersaSuObiettivoRicorrente pageRicorrente = (SalvadanaioVersaSuObiettivoRicorrente) PageRepo.get()
				.get(SalvadanaioVersaSuObiettivoRicorrente.NAME);

		pageRicorrente.clickOnFrequenza();

		SalvadanaioOpzioniSettaggio picker = (SalvadanaioOpzioniSettaggio) PageRepo.get()
				.get(SalvadanaioOpzioniSettaggio.NAME);

		picker.verifyLayoutOfFrequenza();
		picker.selectFrequenza(table.getFrequenza());
	}

	@When("^Utente clicca sul campo A partire da e seleziona una data di partenza per il versamento$")
	public void utente_clicca_sul_campo_A_partire_da_e_seleziona_una_data_di_partenza_per_il_versamento(
			List<LayoutDataBean> dataDable) throws Throwable {
		LayoutDataBean table = dataDable.get(0);

		SalvadanaioVersaSuObiettivoRicorrente pageRicorrente = (SalvadanaioVersaSuObiettivoRicorrente) PageRepo.get()
				.get(SalvadanaioVersaSuObiettivoRicorrente.NAME);

		pageRicorrente.clickOnApartireDa();

		SalvadanaioPicherCalender picker = (SalvadanaioPicherCalender) PageRepo.get()
				.get(SalvadanaioPicherCalender.NAME);

		picker.verifyLayout();
		picker.setADefaultDay(driverAndroid);
		picker.clickOnCANCEL();
	}

	String frequenzaSettata;
	String aPartireDaSettata;
	String termineSettato = "Al raggiungimento";

	@When("^Utente con termine \"([^\"]*)\" e clicca su CONTINUA$")
	public void utente_con_termine_e_clicca_su_CONTINUA(String arg1) throws Throwable {
		SalvadanaioVersaSuObiettivoRicorrente pageRicorrente = (SalvadanaioVersaSuObiettivoRicorrente) PageRepo.get()
				.get(SalvadanaioVersaSuObiettivoRicorrente.NAME);

		frequenzaSettata = pageRicorrente.getFrequenzaText();
		aPartireDaSettata = pageRicorrente.getAPartireDaText();

		pageRicorrente.clickOnCONTINUA();
	}

	@When("^Utente seleziona il metodo di pagamento desiderato se possiede più di una Postepay Evolution$")
	public void utente_seleziona_il_metodo_di_pagamento_desiderato_se_possiede_più_di_una_Postepay_Evolution(
			List<LayoutDataBean> dataDable) throws Throwable {
		LayoutDataBean table = dataDable.get(0);
		SendP2PPageStep1 howPay = (SendP2PPageStep1) PageRepo.get().get(SendP2PPageStep1.NAME);
		switch (table.getUserNumberCards()) {
		case "1":
			System.out.println("Skip --> Utente con una sola carta");
			break;
		case "2":
			System.out.println(" --> Utente con più carte");
			howPay.clickOnCard(driverAndroid, table.getCardNumber());
			break;
		default:
			break;
		}
	}

	@When("^Utente atterra sulla schermata di riepilogo e ne verifica i campi con termine \"([^\"]*)\"$")
	public void utente_atterra_sulla_schermata_di_riepilogo_e_ne_verifica_i_campi_con_termine(String termineType,
			List<LayoutDataBean> dataDable) throws Throwable {
		LayoutDataBean table = dataDable.get(0);

		SalvadanaioObiettivoSummary summary = (SalvadanaioObiettivoSummary) PageRepo.get()
				.get(SalvadanaioObiettivoSummary.NAME);

		summary.verifyHeaderpage(
				TestEnvironment.get().getLanguage("header").getProperty("SalvadanaioVersaSuObiettivo"));
		try {
			summary.clickOnCONTINUA();
		} catch (Exception e) {

		}

		// Verifica dei dati nel summary - Dati utente base (senza ricorrenza)
		summary.verifyDataUser(table.getCardNumber(), table.getNomeCognome(), table.getImporto());

		summary.verifyLayoutNonRicorrente();

		if (table.getIsRicorrente().equals("true")) {
			summary.verifyLayoutRicorrente();
			// Verifica dei dati nel summary - Dati utente aggiuntivi (con ricorrenza)
			summary.verifySpecificData(frequenzaSettata, aPartireDaSettata, termineSettato);
		}
	}

	@When("^Utente dalla schermata di riepilogo clicca su MODIFICA e atterra sulla pagina di Versa sull Obiettivo$")
	public void utente_dalla_schermata_di_riepilogo_clicca_su_MODIFICA_e_atterra_sulla_pagina_di_Versa_sull_Obiettivo()
			throws Throwable {
		SalvadanaioObiettivoSummary summary = (SalvadanaioObiettivoSummary) PageRepo.get()
				.get(SalvadanaioObiettivoSummary.NAME);

		summary.clickOnMODIFICA();
	}

	@When("^Utente modifica il termine da \"([^\"]*)\" a \"([^\"]*)\"$")
	public void utente_modifica_il_termine_da_a(String arg1, String arg2) throws Throwable {
		SalvadanaioVersaSuObiettivoRicorrente pageRicorrente = (SalvadanaioVersaSuObiettivoRicorrente) PageRepo.get()
				.get(SalvadanaioVersaSuObiettivoRicorrente.NAME);

		pageRicorrente.clickOnDataSpecifica();

	}

	@When("^Utente verifica il layout e inserisce una data specifica come termine e poi clicca su CONTINUA$")
	public void utente_verifica_il_layout_e_inserisce_una_data_specifica_come_termine_e_poi_clicca_su_CONTINUA()
			throws Throwable {
		SalvadanaioVersaSuObiettivoRicorrente pageRicorrente = (SalvadanaioVersaSuObiettivoRicorrente) PageRepo.get()
				.get(SalvadanaioVersaSuObiettivoRicorrente.NAME);

		pageRicorrente.varifyLayoutDinamicoDataSpecifica();
		pageRicorrente.clickOnSelectDataSpecifica();

		SalvadanaioPicherCalender picker = (SalvadanaioPicherCalender) PageRepo.get()
				.get(SalvadanaioPicherCalender.NAME);

		picker.verifyLayout();
		picker.setADefaultDay(driverAndroid);
		picker.clickOnOK();

		try {
			pageRicorrente.scrollToElementDOWNDinamic(driverAndroid, 1200);
		} catch (Exception e) {
			// TODO: handle exception
		}
		// Riportiamo la data del termine per confrontarla nel summary
		termineSettato = pageRicorrente.getDataTermine();

		pageRicorrente.clickOnCONTINUA();
	}

	@When("^Utente clicca su AUTORIZZA$")
	public void utente_clicca_su_AUTORIZZA() throws Throwable {
		SalvadanaioObiettivoSummary summary = (SalvadanaioObiettivoSummary) PageRepo.get()
				.get(SalvadanaioObiettivoSummary.NAME);

		summary.clickOnAUTORIZZA();
	}

	@Then("^Utente atterra sulla TYP del salvadanaio e clicca su CHIUDI$")
	public void utente_atterra_sulla_TYP_del_salvadanaio_e_clicca_su_CHIUDI(List<UserDataCredential> dataTable)
			throws Throwable {
		UserDataCredential tableData = dataTable.get(0);
		double discrepanza = StringAndNumberOperationTools.convertStringToDouble(tableData.getDiscrepanza());

		SalvadanaioTYP typ = (SalvadanaioTYP) PageRepo.get().get(SalvadanaioTYP.NAME);

		typ.verifyLayout(driverAndroid, tableData.getTypeTYP(), discrepanza);
		typ.clickOnClose();
		try {
			GenericErrorPopUp errorPopUp = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);
			errorPopUp.isRatingPopUp();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Then("^Utente torna alla schermata di Salvadanaio$")
	public void utente_torna_alla_schermata_di_Salvadanaio() throws Throwable {

		((PressesKey) driverAndroid).pressKey(new KeyEvent(AndroidKey.BACK));
		WaitManager.get().waitMediumTime();
	}

	@Then("^Utente verifica che siano presenti le icona del CRONOMETRO e dei TRE Puntini  nella schermata di Salvadanaio e nel dettaglio dell obiettivo$")
	public void utente_verifica_che_siano_presenti_le_icona_del_CRONOMETRO_e_dei_TRE_Puntini_nella_schermata_di_Salvadanaio_e_nel_dettaglio_dell_obiettivo(
			List<LayoutDataBean> dataDable) throws Throwable {
		LayoutDataBean table = dataDable.get(0);
		double discrepanza = StringAndNumberOperationTools.convertStringToDouble(table.getDiscrepanza());

		SalvadanaioPage page = (SalvadanaioPage) PageRepo.get().get(SalvadanaioPage.NAME);
		page.verifyAutomatismoIcon(driverAndroid, discrepanza);
		page.clickOnCard(driverAndroid, table.getNomeObiettivo());

		SalvadanaioAccantonamentoDettaglio pageDettaglio = (SalvadanaioAccantonamentoDettaglio) PageRepo.get()
				.get(SalvadanaioAccantonamentoDettaglio.NAME);

		pageDettaglio.verifyAutomatismoIcon(driverAndroid, discrepanza);

		double importoDopoVeramento = pageDettaglio.getImportoVersatoSuObiettivo();

		pageDettaglio.isImportoUpdated(table.getImporto(), importoPrimaVeramento, importoDopoVeramento);

		((PressesKey) driverAndroid).pressKey(new KeyEvent(AndroidKey.BACK));
		WaitManager.get().waitMediumTime();
		((PressesKey) driverAndroid).pressKey(new KeyEvent(AndroidKey.BACK));
		WaitManager.get().waitFor(TimeUnit.MINUTES, 1);

		HomePage home = (HomePage) PageRepo.get().get(HomePage.NAME);
		home.clickAllMessageNumber();

		BachecaPage bacheca = (BachecaPage) PageRepo.get().get(BachecaPage.NAME);
		bacheca.clickOnMESSAGGItab();
	}

	@Then("^Utente accede alla sezione Bacheca poi Messaggi e verifica la presenza del messaggio \"([^\"]*)\"$")
	public void utente_accede_alla_sezione_Bacheca_poi_Messaggi_e_verifica_la_presenza_del_messaggio(
			String nomeMessaggio) throws Throwable {

		BachecaPage bacheca = (BachecaPage) PageRepo.get().get(BachecaPage.NAME);
		// Vaiabili
		// Data di oggi nel formato della bacheca
		StringAndNumberOperationTools tool = new StringAndNumberOperationTools();
		String data = tool.stringDATEforBachecaMessageTab("toDay");
		// Label del tipo di messaggio
		String typeMsg = "Servizi finanziari";

		// Click sul Messaggio in base al nome specifico
		bacheca.clickOnSpecificMessage(driverAndroid, typeMsg, nomeMessaggio, data);

		// Verifica con il messaggio aperto
		DettailsMessageConfirmRemovingAutomaticRecharge msgDettaglio = (DettailsMessageConfirmRemovingAutomaticRecharge) PageRepo
				.get().get(DettailsMessageConfirmRemovingAutomaticRecharge.NAME);
		msgDettaglio.verifyHeaderPage();
		msgDettaglio.verifyLayoutMessage(driverAndroid, typeMsg, data, nomeMessaggio);

		((PressesKey) driverAndroid).pressKey(new KeyEvent(AndroidKey.BACK));
		WaitManager.get().waitLongTime();
	}

	@When("^Utente clicca sul Tab Mappe$")
	public void utente_clicca_sul_Tab_Mappe() throws Throwable {
		HomePage home = (HomePage) PageRepo.get().get(HomePage.NAME);
		GenericErrorPopUp popup=(GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);
		popup.clickOnApplePayPopupCloseBtn();
		Properties devName = UIUtils.ui().getCurrentProperties();
		AdbCommandPrompt adbCmd = new AdbCommandPrompt(adbExe, devName.getProperty("ios.udid"));
		//		adbCmd.disableGps();
		home.clickOnMapsTabIcon();

		try {
			if (this.isIOS) {
				GenericErrorPopUp popupAuth = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);
				popupAuth.verifyLayoutOfPopup(driverAndroid, null,
						TestEnvironment.get().getLanguage("generic-popup").getProperty("GpsPopUpIOS"), isIOS);
				String permissionTextButton = "Consenti una volta";
				System.out.println(popupAuth);
				popupAuth.isPermissionRequiredSamsung(permissionTextButton);
			} else {
				GenericErrorPopUp popupAuth = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);
				popupAuth.verifyLayoutOfPopup(driverAndroid, null,
						TestEnvironment.get().getLanguage("generic-popup").getProperty("GpsPopUp"), isIOS);
				String permissionTextButton = "OK";
				popupAuth.isPermissionRequiredSamsung(permissionTextButton);
			}

		} catch (Exception e) {
			System.out.println("Non ho cliccato sul popup");
		}

		try {
			System.out.println("PopUp di permessi Rubrica - Allow");
			GenericErrorPopUp popupAuth = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);

			String permissionTextButton = "Allow";
			popupAuth.isPermissionRequiredSamsung(permissionTextButton);
		} catch (Exception e) {
			// TODO: handle exception
		}

		try {
			System.out.println("PopUp di permessi Rubrica - ALLOW");
			GenericErrorPopUp popupAuth = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);

			String permissionTextButton = "ALLOW";
			popupAuth.isPermissionRequiredSamsung(permissionTextButton);
			//			popupAuth.isPermissionGeneric();

		} catch (Exception e) {
			// TODO: handle exception
		}

		try {
			System.out.println("PopUp di permessi Rubrica - Consenti");
			GenericErrorPopUp popupAuth = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);

			String permissionTextButton = "Consenti";
			popupAuth.isPermissionRequiredSamsung(permissionTextButton);
			//			popupAuth.isPermissionGeneric();

		} catch (Exception e) {
			// TODO: handle exception
		}

		//		try {
		//			System.out.println("PopUp di permessi Rubrica - NON ORA");
		//			GenericErrorPopUp popupAuth = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);
		//			String permissionTextButton = "NOT NOW";
		//			popupAuth.isPermissionRequiredSamsung(permissionTextButton);
		//			
		//		} catch (Exception e) {
		//			// TODO: handle exception
		//		}

		//		try {
		//			System.out.println("PopUp di permessi Rubrica - ANNULLA");
		//			GenericErrorPopUp popupAuth = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);
		//			String permissionTextButton = "ANNULLA";
		//			popupAuth.isPermissionRequiredSamsung(permissionTextButton);
		//			
		//		} catch (Exception e) {
		//			// TODO: handle exception
		//		}
	}

	@When("^Utente clicca sul bottone Apri Dettaglio$")
	public void utente_clicca_sul_bottone_Apri_Dettaglio() throws Throwable {
		MapsGenericScreenFromHomePage mappePage = (MapsGenericScreenFromHomePage) PageRepo.get()
				.get(MapsGenericScreenFromHomePage.NAME);
		mappePage.clickOnApriDettaglio();
	}

	@Then("^Utente atterra nella pagina di ScontiPoste$")
	public void utente_atterra_nella_pagina_di_ScontiPoste() throws Throwable {
		MapsGenericScreenFromHomePage mappePage = (MapsGenericScreenFromHomePage) PageRepo.get()
				.get(MapsGenericScreenFromHomePage.NAME);
		mappePage.verifyScontiPostePage();
	}

	@When("^Utente clicca su Codice QR e visualizza i merchant$")
	public void utente_clicca_su_Codice_QR_e_visualizza_i_merchant(List<UserDataCredential> dataTable)
			throws Throwable {
		UserDataCredential table = dataTable.get(0);

		MapsGenericScreenFromHomePage mappePage = (MapsGenericScreenFromHomePage) PageRepo.get()
				.get(MapsGenericScreenFromHomePage.NAME);

		String authText = "ALLOW";
		if (table.getDriver().equals("GalaxyS10Luca")) {
			// Samsung S10
			authText = "Allow only while using the app";
		}
		mappePage.isGeoAuthorized(authText);
		mappePage.clickOnCodiceQR();
	}

	String merchantCategory;
	String merchantName;
	String merchantAddress;
	String merchantAmount;

	@When("^Utente clicca sul Paga del primo merchant in lista e si visualizza la pagina di inserimento importo$")
	public void utente_clicca_sul_Paga_del_primo_merchant_in_lista_e_si_visualizza_la_pagina_di_inserimento_importo()
			throws Throwable {
		MapsGenericScreenFromHomePage mappePage = (MapsGenericScreenFromHomePage) PageRepo.get()
				.get(MapsGenericScreenFromHomePage.NAME);

		merchantCategory = mappePage.getQRCategory();
		merchantName = mappePage.getQRName();
		merchantAddress = mappePage.getQRAddress();

		mappePage.clickOnIntantPayFirstMerchant();

		System.out.println("merchantCategory " + merchantCategory);
		System.out.println("merchantName " + merchantName);
		System.out.println("merchantAddress " + merchantAddress);
		System.out.println("merchantAmount " + merchantAmount);

		MapsPayWithQRCode mappeAmountPage = (MapsPayWithQRCode) PageRepo.get().get(MapsPayWithQRCode.NAME);
		try {
			((HidesKeyboard) driverAndroid).hideKeyboard();
		} catch (Exception e) {
			// TODO: handle exception
		}
		mappeAmountPage.verifyLayout();

	}

	@When("^Utente clicca sul cta X e viene riportato alla schermata precedente$")
	public void utente_clicca_sul_cta_X_e_viene_riportato_alla_schermata_precedente() throws Throwable {
		MapsPayWithQRCode mappeAmountPage = (MapsPayWithQRCode) PageRepo.get().get(MapsPayWithQRCode.NAME);
		mappeAmountPage.clickOnX();

		GenericErrorPopUp errorPopUp = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);

		errorPopUp.verifyLayoutOfPopup(driverAndroid, "Avviso", "Vuoi uscire dall'operazione in corso?", this.isIOS);
		errorPopUp.clickOnOKPopUp();

		WaitManager.get().waitShortTime();
		try {
			//			GenericErrorPopUp atacPopUp = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);
			errorPopUp.closeAtacPopup();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@When("^Utente inserisce l importo e clicca su paga il merchant$")
	public void utente_inserisce_l_importo_e_clicca_su_paga_il_merchant(List<UserDataCredential> dataTable)
			throws Throwable {
		UserDataCredential table = dataTable.get(0);
		MapsPayWithQRCode mappeAmountPage = (MapsPayWithQRCode) PageRepo.get().get(MapsPayWithQRCode.NAME);
		merchantAmount = table.getImporto();
		System.out.println("merchantAmount " + merchantAmount);
		mappeAmountPage.sendAmount(table.getImporto());

		mappeAmountPage.clickOnContinua();
	}

	@When("^Utente verifica il riepilogo del pagamento del merchant$")
	public void utente_verifica_il_riepilogo_del_pagamento_del_merchant(List<UserDataCredential> dataTable)
			throws Throwable {
		UserDataCredential table = dataTable.get(0);

		MapsPayWithQRCode mappeAmountPage = (MapsPayWithQRCode) PageRepo.get().get(MapsPayWithQRCode.NAME);

		mappeAmountPage.verifyLayoutSummary();
		mappeAmountPage.verifySummaryData(table.getCardNumber(), merchantAddress, merchantAmount);
	}

	@When("^Utente clicca sul bottone <$")
	public void utente_clicca_sul_bottone() throws Throwable {
		MapsPayWithQRCode mappeAmountPage = (MapsPayWithQRCode) PageRepo.get().get(MapsPayWithQRCode.NAME);

		mappeAmountPage.clickOnBackArrow();

		WaitManager.get().waitShortTime();
		try {
			GenericErrorPopUp errorPopUp = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);
			errorPopUp.closeAtacPopup();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@When("^Utente verifica che l icona della matita non sia presente nel campo paga$")
	public void utente_verifica_che_l_icona_della_matita_non_sia_presente_nel_campo_paga(
			List<UserDataCredential> dataTable) throws Throwable {
		UserDataCredential table = dataTable.get(0);
		MapsPayWithQRCode mappeAmountPage = (MapsPayWithQRCode) PageRepo.get().get(MapsPayWithQRCode.NAME);

		mappeAmountPage.verifyChangeCard(table.getUserNumberCards());
	}

	@When("^Utente clicca su Annulla$")
	public void utente_clicca_su_Annulla() throws Throwable {
		MapsPayWithQRCode mappeAmountPage = (MapsPayWithQRCode) PageRepo.get().get(MapsPayWithQRCode.NAME);

		mappeAmountPage.clickOnAnnulla();

		GenericErrorPopUp errorPopUp = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);

		errorPopUp.verifyLayoutOfPopup(driverAndroid, "Avviso", "Vuoi uscire dal pagamento?", this.isIOS);
		errorPopUp.clickOnOKPopUp();
	}

	@Then("^Utente clicca su PAGA il merchant$")
	public void utente_clicca_su_PAGA_il_merchant() throws Throwable {
		MapsPayWithQRCode mappeAmountPage = (MapsPayWithQRCode) PageRepo.get().get(MapsPayWithQRCode.NAME);

		mappeAmountPage.clickOnPaga();
	}

	@Then("^Utente clicca su < dalla scermata di inserimento PosteId$")
	public void utente_clicca_su_dalla_scermata_di_inserimento_PosteId(List<UserDataCredential> dataTable)
			throws Throwable {
		UserDataCredential table = dataTable.get(0);
		RechargeMyPostepayPageStep5 step3 = (RechargeMyPostepayPageStep5) PageRepo.get()
				.get(RechargeMyPostepayPageStep5.NAME);

		((HidesKeyboard) driverAndroid).hideKeyboard();
		step3.verifyHeaderPage("Paga con Codice QR");
		step3.insertPosteId(table.getPosteid());
		((HidesKeyboard) driverAndroid).hideKeyboard();
		step3.clickOnBackArrow();

	}

	@When("^Utente clicca sull icona a campanello delle bacheca$")
	public void utente_clicca_sull_icona_a_campanello_delle_bacheca() throws Throwable {
		HomePage homePage = (HomePage) PageRepo.get().get(HomePage.NAME);
		GenericErrorPopUp popup=(GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);
		popup.clickOnApplePayPopupCloseBtn();
		homePage.clickOnBachecaIcon();
	}

	@When("^Utente seleziona il tab Notifiche e Autorizzazione$")
	public void Utente_seleziona_il_tab_Notifiche_e_Autorizzazione() throws Throwable {
		BachecaPage rechargePage = (BachecaPage) PageRepo.get().get(BachecaPage.NAME);
		rechargePage.clickOnNOTIFICHEtab();
	}

	@When("^Utente inserisce la parola da cercare nel campo cerca$")
	public void utente_inserisce_la_parola_da_cercare_nel_campo_cerca(List<UserDataCredential> dataTable)
			throws Throwable {
		UserDataCredential table = dataTable.get(0);

		BachecaPage rechargePage = (BachecaPage) PageRepo.get().get(BachecaPage.NAME);
		rechargePage.verifyHeaderPage();
		rechargePage.searchOperation(table.getComment(), isIOS);
		if (isIOS) {
			// TODO
		} else {
			((PressesKey) driverAndroid).pressKey(new KeyEvent(AndroidKey.ENTER));
		}
		//		Properties devName = UIUtils.ui().getCurrentProperties();
		//		AdbCommandPrompt cmd=new AdbCommandPrompt(adbExe, devName.getProperty("ios.udid"));
		//		cmd.tap(300, 300);

		WaitManager.get().waitMediumTime();
	}

	@Then("^Utente verifica che la ricerca funzioni correttamente$")
	public void utente_verifica_che_la_ricerca_funzioni_correttamente(List<UserDataCredential> dataTable)
			throws Throwable {
		UserDataCredential table = dataTable.get(0);
		BachecaPage rechargePage = (BachecaPage) PageRepo.get().get(BachecaPage.NAME);
		rechargePage.verifyResultList(table.getComment());
	}

	@When("^Utente clicca su bollettino$")
	public void utente_clicca_su_bollettino() throws Throwable {
		PaymentPaymentPage payPage = (PaymentPaymentPage) PageRepo.get().get(PaymentPaymentPage.NAME);
		payPage.clickOnPayBullettin();
		try {
			GenericErrorPopUp popup=(GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);
			popup.isPermissionRequiredSamsung("OK");
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	@When("^Utente seleziona la tipologia di bollettino$")
	public void utente_seleziona_la_tipologia_di_bollettino(List<UserDataCredential> dataTable) throws Throwable {
		UserDataCredential table = dataTable.get(0);
		WaitManager.get().waitMediumTime();
		try {
			PagaBollettinoT133 bollPage = (PagaBollettinoT133) PageRepo.get().get(PagaBollettinoT133.NAME);
			bollPage.onClickBollettinoManuale();
		} catch (Exception e) {
			// TODO: handle exception
		}

		SelezionaBollettinoT134 selctBol = (SelezionaBollettinoT134) PageRepo.get().get(SelezionaBollettinoT134.NAME);
		selctBol.onClickBollettino(table.getComment());

	}

	@When("^Utente compila i campi per il pagamento del bollettino e clicca su conferma$")
	public void utente_compila_i_campi_per_il_pagamento_del_bollettino_e_clicca_su_conferma(
			List<UserDataCredential> dataTable) throws Throwable {
		UserDataCredential table = dataTable.get(0);

		PagaBollettinoBiancoT135 fillBollPage = (PagaBollettinoBiancoT135) PageRepo.get()
				.get(PagaBollettinoBiancoT135.NAME);
		fillBollPage.verifyHeaderPage(TestEnvironment.get().getLanguage("header").getProperty("PagaBollettinoT133"));

		fillBollPage.fillBeneficiarioField(table.getContoCorrente(), table.getTargetUser(), table.getImporto(),
				table.getCausale());
		fillBollPage.scrollDown();

		// String nome = "";
		// String cognome = "";
		// String indirizzo = "";
		// String citta = "";
		// String cap = "";
		// String provincia = "";
		String nome = table.getFortunatoNome();
		System.out.println("Nome: " + nome);
		String cognome = table.getFortunatoCognome();
		System.out.println("cognome: " + cognome);
		String indirizzo = table.getFortunatoIndirizzo();
		System.out.println("indirizzo: " + indirizzo);
		String citta = table.getFortunatoCitta();
		System.out.println("citta: " + citta);
		String cap = table.getFortunatoCap();
		System.out.println("cap: " + cap);
		String provincia = table.getFortunatoProvincia();
		System.out.println("provincia: " + provincia);

		// if (table.getName().equals("Musella")) {
		// String nome =
		// TestEnvironment.get().getLanguage("dati-user").getProperty("FortunatoNome");
		// System.out.println(nome);
		// String cognome =
		// TestEnvironment.get().getLanguage("dati-user").getProperty("FortunatoCognome");
		// String indirizzo =
		// TestEnvironment.get().getLanguage("dati-user").getProperty("FortunatoIndirizzo");
		// String citta =
		// TestEnvironment.get().getLanguage("dati-user").getProperty("FortunatoCitta");
		// String cap =
		// TestEnvironment.get().getLanguage("dati-user").getProperty("FortunatoCap");
		// String provincia =
		// TestEnvironment.get().getLanguage("dati-user").getProperty("FortunatoProvincia");
		// }
		fillBollPage.verifyChiPagaSection(nome, cognome, indirizzo, citta, cap, provincia);
		fillBollPage.clickOnContinua();
	}

	@When("^Utente verifica le informazioni nel summary e clicca su paga$")
	public void utente_verifica_le_informazioni_nel_summary_e_clicca_su_paga(List<UserDataCredential> dataTable)
			throws Throwable {
		UserDataCredential table = dataTable.get(0);

		String nome = table.getFortunatoNome();
		System.out.println("Nome: " + nome);
		String cognome = table.getFortunatoCognome();
		System.out.println("cognome: " + cognome);
		String indirizzo = table.getFortunatoIndirizzo();
		System.out.println("indirizzo: " + indirizzo);
		String citta = table.getFortunatoCitta();
		System.out.println("citta: " + citta);
		String cap = table.getFortunatoCap();
		System.out.println("cap: " + cap);
		String provincia = table.getFortunatoProvincia();
		System.out.println("provincia: " + provincia);

		RiepilogoPagamentoSummaryT136 summaryPage = (RiepilogoPagamentoSummaryT136) PageRepo.get()
				.get(RiepilogoPagamentoSummaryT136.NAME);
		summaryPage.verifyHeaderPage(TestEnvironment.get().getLanguage("header").getProperty("PagaBollettinoT133"));
		summaryPage.verifyFilledData(table.getContoCorrente(), table.getTargetUser(), table.getImporto(),
				table.getCausale(), nome, cognome, indirizzo, cap, citta, provincia, table.getCardNumber());
		summaryPage.clickOnPaga();
	}

	@When("^Utente clicca su Sconti da Accreditare$")
	public void utente_clicca_su_Sconti_da_Accreditare() throws Throwable {
		MapsGenericScreenFromHomePage mappePage = (MapsGenericScreenFromHomePage) PageRepo.get()
				.get(MapsGenericScreenFromHomePage.NAME);
		mappePage.clickOnScontiDaAccreditareCard();
	}

	@When("^Utente clicca sul tasto back in alto a sinistra$")
	public void utente_clicca_sul_tasto_back_in_alto_a_sinistra() throws Throwable {
		RechargeMyPostepayPageStep3 page = (RechargeMyPostepayPageStep3) PageRepo.get()
				.get(RechargeMyPostepayPageStep3.NAME);
		page.clickOnBackArrow();
	}

	@When("^Utente clicca su icona Assistenza e torna indietro$")
	public void utente_clicca_su_icona_Assistenza_e_torna_indietro() throws Throwable {
		System.out.println("Da implementare step su assistenza");
	}

	String preferredCardOld = "";

	@Then("^Utente cambia la carta preferita per l accredito del cash back$")
	public void utente_cambia_la_carta_preferita_per_l_accredito_del_cash_back(List<UserDataCredential> dataTable)
			throws Throwable {
		System.out.println("Utenti con più carte");
		UserDataCredential table = dataTable.get(0);
		RechargeMyPostepayPageStep3 step3 = (RechargeMyPostepayPageStep3) PageRepo.get()
				.get(RechargeMyPostepayPageStep3.NAME);

		step3.verifyHeaderpage(TestEnvironment.get().getLanguage("header").getProperty("AccreditoScontiPoste"));
		step3.verifyLayoutScontiPoste(driverAndroid, "Scegli altra carta", "CONNECT", table.getCardNumber());

		String preferredCardActual = step3.whoIsPreferredCard();
		String notPreferredCardActual = step3.whoIsNotPreferredCard();

		System.out.println("preferredCardActual: " + preferredCardActual);
		System.out.println("notPreferredCardActual: " + notPreferredCardActual);
		System.out.println("preferredCardOld: " + preferredCardOld);

		if (preferredCardOld.equals("")) {
			System.out.println("Cliccco sulla carta non preferita " + notPreferredCardActual);
			step3.selectAsPreferredOtherCard();
			preferredCardOld = notPreferredCardActual;
		} else {
			System.out.println("Verifico che la carta preferita attuale combaci con quella selezionata in precedenza");
			assertTrue(preferredCardOld.equals(preferredCardActual));
			System.out.println(
					"Settata prima: " + preferredCardOld + "\tSettata in questo momento: " + preferredCardActual);

			step3.selectAsPreferredOtherCard();
			preferredCardOld = notPreferredCardActual;
		}
	}

	@Then("^Utente atterra sulla TYP di conferma cambio carta preferita e clicca chiudi$")
	public void utente_atterra_sulla_TYP_di_conferma_cambio_carta_preferita_e_clicca_chiudi() throws Throwable {
		BankTranfertSEPAPageStep5 tyPage = (BankTranfertSEPAPageStep5) PageRepo.get()
				.get(BankTranfertSEPAPageStep5.NAME);

		tyPage.verifyLayoutDinamic(TestEnvironment.get().getLanguage("typ-text").getProperty("titoloScontiPoste"),
				TestEnvironment.get().getLanguage("typ-text").getProperty("descrizioneScontiPoste"),
				TestEnvironment.get().getLanguage("typ-text").getProperty("btnChiudi"));
		tyPage.clickOnClose();
	}

	@When("^Utente clicca su Bollo Auto e Moto$")
	public void utente_clicca_su_Bollo_Auto_e_Moto() throws Throwable {

		//		WaitManager.get().waitMediumTime();
		try {
			GenericErrorPopUp errorPopUp = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);
			errorPopUp.closeAtacPopup();
			errorPopUp.isPermissionRequiredSamsung("Annulla");
		} catch (Exception e) {
			// TODO: handle exception
		}
		PaymentPaymentPage payPage = (PaymentPaymentPage) PageRepo.get().get(PaymentPaymentPage.NAME);
		payPage.clickOnGenericOperation(TestEnvironment.get().getLanguage("typ-text").getProperty("bolloAuto"));
	}

	@When("^Utente verifica il layout della pagina Bollo Auto e Moto e compila i campi$")
	public void utente_verifica_il_layout_della_pagina_Bollo_Auto_e_Moto_e_compila_i_campi(
			List<UserDataCredential> dataTable) throws Throwable {
		BolloAutoMotoT139 bolloAuto = (BolloAutoMotoT139) PageRepo.get().get(BolloAutoMotoT139.NAME);
		bolloAuto.verifyHeaderTitle(TestEnvironment.get().getLanguage("header").getProperty("bolloAutoTitle"));
		bolloAuto.verifyLayoutCalcolaM262(TestEnvironment.get().getLanguage("typ-text").getProperty("copyM262"),
				TestEnvironment.get().getLanguage("typ-text").getProperty("annualitaM262"),
				TestEnvironment.get().getLanguage("typ-text").getProperty("btnCalcolM262"),
				TestEnvironment.get().getLanguage("typ-text").getProperty("regioneM262"),
				TestEnvironment.get().getLanguage("typ-text").getProperty("targaM262"),
				TestEnvironment.get().getLanguage("typ-text").getProperty("veicoloM262"));
		UserDataCredential table = dataTable.get(0);
		bolloAuto.inserisciRegione(table.getRegione());
		bolloAuto.inserisciTarga(table.getTarga());
		bolloAuto.inserisciVeicolo(table.getVeicolo());
	}

	@When("^Utente CALCOLA limporto del Bollo Auto$")
	public void utente_CALCOLA_limporto_del_Bollo_Auto() throws Throwable {
		BolloAutoMotoT139 bolloAuto = (BolloAutoMotoT139) PageRepo.get().get(BolloAutoMotoT139.NAME);
		bolloAuto.clickBtnCalcolaM262();
	}

	String importoBolloAutoCalcolato;

	@When("^Utente visualizza l importo da pagare e controlla che i campi siano corretti$")
	public void utente_visualizza_l_importo_da_pagare_e_controlla_che_i_campi_siano_corretti(
			List<UserDataCredential> dataTable) throws Throwable {
		BolloAutoMotoT139 bolloAuto = (BolloAutoMotoT139) PageRepo.get().get(BolloAutoMotoT139.NAME);
		UserDataCredential table = dataTable.get(0);
		bolloAuto.verifyLayoutCalcolaM263(table.getTarga(), table.getVeicolo(),
				TestEnvironment.get().getLanguage("typ-text").getProperty("titleM263"),
				TestEnvironment.get().getLanguage("typ-text").getProperty("btnContinuaM263"), table.getCardNumber());
		importoBolloAutoCalcolato = bolloAuto.getImportoM263();
	}

	@When("^Utente clicca su CONTINUA per procedere al riepilogo di pagamento$")
	public void utente_clicca_su_CONTINUA_per_procedere_al_riepilogo_di_pagamento() throws Throwable {
		BolloAutoMotoT139 bolloAuto = (BolloAutoMotoT139) PageRepo.get().get(BolloAutoMotoT139.NAME);
		bolloAuto.clickBtnContinuaM263();
	}

	@When("^Utente visualizza il riepilogo di pagamento del Bollo e verifica che i dati siano corretti$")
	public void utente_visualizza_il_riepilogo_di_pagamento_del_Bollo_e_verifica_che_i_dati_siano_corretti(
			List<UserDataCredential> dataTable) throws Throwable {
		BolloAutoMotoT139 bolloAuto = (BolloAutoMotoT139) PageRepo.get().get(BolloAutoMotoT139.NAME);
		UserDataCredential table = dataTable.get(0);
		bolloAuto.verifyHeaderTitle(TestEnvironment.get().getLanguage("header").getProperty("bolloAutoTitle"));
		bolloAuto.verifyLayoutSummaryM264(TestEnvironment.get().getLanguage("typ-text").getProperty("titleM264"),
				table.getRegione(), table.getTarga(), importoBolloAutoCalcolato, "€ 1,00", table.getCardNumber(),
				TestEnvironment.get().getLanguage("typ-text").getProperty("btnAnnullaM264"));
	}

	@When("^utente clicca su ANNULLA pagamento del bollo auto$")
	public void utente_clicca_su_ANNULLA_pagamento_del_bollo_auto() throws Throwable {
		BolloAutoMotoT139 bolloAuto = (BolloAutoMotoT139) PageRepo.get().get(BolloAutoMotoT139.NAME);
		bolloAuto.clickBtnAnnullaM264();
		GenericErrorPopUp errorPopUp = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);
		errorPopUp.verifyLayoutOfPopup(driverAndroid, "Avviso", "Vuoi uscire dall'operazione in corso?", this.isIOS);
		errorPopUp.clickOnOKPopUp();

		WaitManager.get().waitShortTime();
		try {
			//			GenericErrorPopUp errorPopUp = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);
			errorPopUp.closeAtacPopup();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@When("^Utente clicca su PAGA Bollo Auto e Moto$")
	public void utente_clicca_su_PAGA_Bollo_Auto_e_Moto() throws Throwable {

		BolloAutoMotoT139 bolloAuto = (BolloAutoMotoT139) PageRepo.get().get(BolloAutoMotoT139.NAME);
		bolloAuto.clickBtnPagaM264();
	}

	@When("^Utente clicca su Notifiche$")
	public void utente_clicca_su_Notifiche() throws Throwable {
		SettingPage setting = (SettingPage) PageRepo.get().get(SettingPage.NAME);
		setting.verifyHeaderPage(driverAndroid, TestEnvironment.get().getLanguage("header").getProperty("SettingPage"));

		setting.clickOnNotification();
	}

	@When("^Utente verifica che il device sia il preferito e torna in HomePage$")
	public void utente_verifica_che_il_device_sia_il_preferito_e_torna_in_HomePage(List<UserDataCredential> dataTable)
			throws Throwable {
		NotificheSettingPage notificheSetting = (NotificheSettingPage) PageRepo.get().get(NotificheSettingPage.NAME);
		notificheSetting.verifyHeaderpage(TestEnvironment.get().getLanguage("header").getProperty("notificheTitle"));
		UserDataCredential table = dataTable.get(0);
		if (notificheSetting.isPreferito()) {
			notificheSetting.verifyLayoutNonPreferito();
			notificheSetting.clickOnImpostaComePreferito();

			WaitManager.get().waitMediumTime();
			GenericErrorPopUp errorPopUp = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);
			String titlePopup = "Avviso";
			if(this.isIOS) {
				titlePopup = "AVVISO";
			} else {
				titlePopup = "Avviso";
			}
			errorPopUp.verifyLayoutOfPopup(driverAndroid,
					titlePopup,
					"Vuoi che questo dispositivo diventi il tuo device preferito, dove riceverai tutte le notifiche di accesso e di autorizzazione dei pagamenti?",this.isIOS);

			//			errorPopUp.verifyLayoutOfPopup(driverAndroid, "Avviso",
			//					"Vuoi che questo dispositivo diventi il tuo device preferito, dove riceverai tutte le notifiche di accesso e di autorizzazione dei pagamenti?",
			//					this.isIOS);

			errorPopUp.clickOnOKPopUp();
			BankTranfertSEPAPageStep4 step4 = (BankTranfertSEPAPageStep4) PageRepo.get()
					.get(BankTranfertSEPAPageStep4.NAME);
			step4.verifyHeaderpage(TestEnvironment.get().getLanguage("header").getProperty("notifichePreferito"));
			step4.insertPosteId(table.getPosteid());
			step4.clickOnConfirmButton();
		}
		notificheSetting.verifyLayoutPreferito();
		WaitManager.get().waitShortTime();
		if (this.isIOS) {
			NotificheSettingPage notifiche = (NotificheSettingPage) PageRepo.get().get(NotificheSettingPage.NAME);
			notifiche.clickOnback();
			WaitManager.get().waitMediumTime();
			notifiche.clickOnChiudi();
			WaitManager.get().waitMediumTime();
		} else {
			((PressesKey) driverAndroid).pressKey(new KeyEvent(AndroidKey.BACK));
			WaitManager.get().waitMediumTime();
			((PressesKey) driverAndroid).pressKey(new KeyEvent(AndroidKey.BACK));
			WaitManager.get().waitMediumTime();
		}
	}

	public void configureMobileChrome() throws MalformedURLException {
		//		DesiredCapabilities k = new DesiredCapabilities();
		//		
		//		k.setCapability(MobileCapabilityType.UDID, "RF8M424HR0L");
		//		k.setCapability(CapabilityType.BROWSER_NAME, "chrome");
		//		k.setCapability(CapabilityType.VERSION, "10");
		//		
		//		WebDriver driverWeb = new RemoteWebDriver(new URL("https://bancoposta.test.poste.it/bpol/public/homeBanking.htmlhttps://bancoposta.test.poste.it/bpol/public/homeBanking.html"),
		//				k);
		//		driverWeb.manage().window().maximize();
		//		WaitManager.get().waitMediumTime();



		//		DesiredCapabilities k = new DesiredCapabilities();
		////		
		//		k.setCapability(MobileCapabilityType.UDID, "RF8M424HR0L");
		//		k.setCapability(CapabilityType.VERSION, "10");
		//		k.setCapability(MobileCapabilityType.AUTOMATION_NAME, "UiAutomator2");
		//		k.setCapability(MobileCapabilityType.FULL_RESET, "false");
		//		k.setCapability(MobileCapabilityType.NO_RESET, true);

		//		AndroidDriver driverDue = new AndroidDriver(k);
		((StartsActivity) driverAndroid).startActivity(new Activity(
				"com.android.chrome", 
				"com.google.android.apps.chrome.Main "));
	}

	@When("^Utente accede al portale BPOL e inserisce le credenziali di accesso$")
	public void utente_accede_al_portale_BPOL_e_inserisce_le_credenziali_di_accesso(List<UserDataCredential> dataTable)
			throws Throwable {
		UserDataCredential table = dataTable.get(0);

		configureMobileChrome();
		//		this.driverBPOL = UIUtils.ui().driver("default_ita");
		//		driverBPOL.manage().window().maximize();
		//		WaitManager.get().waitMediumTime();
		//
		BPOLPage page = (BPOLPage) PageRepo.get().get(BPOLPage.NAME);
		//		page.setDriver(driverBPOL);
		//		page.utenteCliccaProfilo();
		////		WaitManager.get().waitHighTime();
		//		WaitManager.get().waitMediumTime();
		//		page.inserisciCredenziali(table.getUsername(), table.getPassword(), table.getCardNumber());

		page.openMobileChrome();
		page.loginMobile(table.getUsername(), table.getPassword(), table.getCardNumber());
	}

	@When("^Utente accede all area personale della carta PostePay$")
	public void utente_accede_all_area_personale_della_carta_PostePay(List<UserDataCredential> dataTable)
			throws Throwable {

		BPOLPage page = (BPOLPage) PageRepo.get().get(BPOLPage.NAME);
		UserDataCredential table = dataTable.get(0);
		//		page.utenteAccedeAPostepay(table.getCardNumber());
		page.utenteInviaLautorizzativa(table.getCardNumber());

		page.goToPosteApp();


	}

	@When("^Utente verifica la sua identita tramite app PostePay$")
	public void utente_verifica_la_sua_identita_tramite_app_PostePay() throws Throwable {
		BPOLPage page = (BPOLPage) PageRepo.get().get(BPOLPage.NAME);
		page.invioAutorizzazionePostepay();
	}

	@When("^Utente clicca sul messaggio di autorizzazione$")
	public void utente_clicca_sul_messaggio_di_autorizzazione() throws Throwable {
		BachecaPage rechargePage = (BachecaPage) PageRepo.get().get(BachecaPage.NAME);
		rechargePage.clickPrimoElemento("AUTORIZZAZIONE DA WEB");
	}

	@Then("^Utente atterra sulla pagina di autorizzazione web$")
	public void utente_atterra_sulla_pagina_di_autorizzazione_web() throws Throwable {
		NotificheAutorizzativePage notificaPage = (NotificheAutorizzativePage) PageRepo.get()
				.get(NotificheAutorizzativePage.NAME);
		notificaPage
		.verifyHeaderPage(TestEnvironment.get().getLanguage("header").getProperty("AutorizzazioneNotifiche"));

		notificaPage.verifyLayout(
				TestEnvironment.get().getLanguage("typ-text").getProperty("copyM265"),
				TestEnvironment.get().getLanguage("typ-text").getProperty("copyM265Bis"), 
				TestEnvironment.get().getLanguage("typ-text").getProperty("btnAutorizzaM265"), 
				TestEnvironment.get().getLanguage("typ-text").getProperty("btnNegaM265")
				);
	}

	@Then("^Utente autorizza l accesso al portale BPOL per PostePay$")
	public void utente_autorizza_l_accesso_al_portale_BPOL_per_PostePay() throws Throwable {
		NotificheAutorizzativePage notificaPage = (NotificheAutorizzativePage) PageRepo.get()
				.get(NotificheAutorizzativePage.NAME);
		notificaPage.clickOnAutorizza();
	}

	@Then("^Utente verifica l accesso sul portale BPOL$")
	public void utente_verifica_l_accesso_sul_portale_BPOL(List<UserDataCredential> dataTable) throws Throwable {
		UserDataCredential table = dataTable.get(0);
		BPOLPage page = (BPOLPage) PageRepo.get().get(BPOLPage.NAME);
		WaitManager.get().waitMediumTime();
		page.layoutAreaPersonalePostePay(table.getCardNumber());
	}

	@When("^Utente clicca notifica push$")
	public void utente_clicca_notifica_push() throws Throwable {
		DeviceNativoPage devicePage = (DeviceNativoPage) PageRepo.get().get(DeviceNativoPage.NAME);
		//		boolean find= false;
		((AndroidDriver<?>) driverAndroid).openNotifications();
		//		if(!find)
		//			((AndroidDriver<?>) driverAndroid).pressKey(new KeyEvent(AndroidKey.BACK));
		WaitManager.get().waitMediumTime();
		devicePage.verifyLayout("Postepay", "Internet Ban..", "Richiesta di accesso");
		devicePage.clickOnNotification("PostePay online - Internet Ban..");
	}

	@When("^Utente seleziona la tipologia di versamento nel salvadanaio$")
	public void utente_seleziona_la_tipologia_di_versamento_nel_salvadanaio(DataTable arg1) throws Throwable {
		SalvadanaioAccantonamentoDettaglio page = (SalvadanaioAccantonamentoDettaglio) PageRepo.get()
				.get(SalvadanaioAccantonamentoDettaglio.NAME);
		page.clickOnVersamentoSingolo();
		WaitManager.get().waitShortTime();
	}

	@When("^Utente conferma l operazione di versamento cliccando su Continua$")
	public void utente_conferma_l_operazione_di_versamento_cliccando_su_Continua() throws Throwable {
		SalvadanaioVersaSuObiettivoRicorrente pageRicorrente = (SalvadanaioVersaSuObiettivoRicorrente) PageRepo.get()
				.get(SalvadanaioVersaSuObiettivoRicorrente.NAME);
		pageRicorrente.clickOnCONTINUA();
		WaitManager.get().waitShortTime();
	}

	@Then("^Utente clicca su PRODOTTI$")
	public void utente_clicca_su_PRODOTTI() throws Throwable {
		HomePage home = (HomePage) PageRepo.get().get(HomePage.NAME);
		GenericErrorPopUp popup=(GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);
		popup.clickOnApplePayPopupCloseBtn();
		home.clickOnProductsTabIcon();
		WaitManager.get().waitShortTime();
	}

	@Then("^Utente verifica che il versamento e presente nella LISTA MOVIMENTI$")
	public void utente_verifica_che_il_versamento_e_presente_nella_LISTA_MOVIMENTI(List<UserDataCredential> dataTable)
			throws Throwable {
		UserDataCredential table = dataTable.get(0);
		ProductsStandardPage page = (ProductsStandardPage) PageRepo.get().get(ProductsStandardPage.NAME);
		page.clickPrimoMovimento("ADDEBITO PER SALVADANAIO", table.getImporto());
		WaitManager.get().waitShortTime();
	}

	@When("^Utente visualizza il dettaglio del versamento singolo$")
	public void utente_visualizza_il_dettaglio_del_versamento_singolo(List<UserDataCredential> dataTable)
			throws Throwable {
		UserDataCredential table = dataTable.get(0);
		ProductsStandardPage page = (ProductsStandardPage) PageRepo.get().get(ProductsStandardPage.NAME);
		String data = LocalDate.now().format(DateTimeFormatter.ofPattern("dd MMM yyyy"));
		page.verifyDettaglioMovimenti("VERSAMENTO SUL LIBRETTO " + table.getLibretto() + " PER SALVADANAIO",
				table.getImporto(), data.toUpperCase(), data.toUpperCase(), table.getCardNumber(),
				"Addebitato su Postepay");
	}

	@When("^Utente dalla pagina di IMPOSTAZIONI CARTA esegue uno scroll fino a \"([^\"]*)\"$")
	public void utente_dalla_pagina_di_IMPOSTAZIONI_CARTA_esegue_uno_scroll_fino_a(String target) throws Throwable {
		ProductsConnectCardDetailsPage impostazioniCartaPage = (ProductsConnectCardDetailsPage) PageRepo.get()
				.get(ProductsConnectCardDetailsPage.NAME);

		impostazioniCartaPage.scrollTo(target);

	}

	@When("^Utente attiva Google Pay$")
	public void utente_attiva_Google_Pay() throws Throwable {
		ProductsConnectCardDetailsPage impostazioniCartaPage = (ProductsConnectCardDetailsPage) PageRepo.get()
				.get(ProductsConnectCardDetailsPage.NAME);

		impostazioniCartaPage.enableGpatToggle();

	}

	@When("^Utente accetta il CONSENSO PRIVACY$")
	public void utente_accetta_il_CONSENSO_PRIVACY() throws Throwable {
		GenericErrorPopUp errorPopUp = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);
		errorPopUp.verifyLayoutOfPopup(driverAndroid, "Consenso privacy",
				TestEnvironment.get().getLanguage("generic-popup").getProperty("GpayConsensoPrivacy"), this.isIOS);
		errorPopUp.clickOnOKPopUp();
	}

	@When("^Utente procede con l attivazione di Google Pay$")
	public void utente_procede_con_l_attivazione_di_Google_Pay() throws Throwable {
		// Il funnel di attivazione di GPAY è gestito attraverso una serie di click
		// dinamici e basati sul testo per generalizzarli a tutti i dispositivi
		GenericErrorPopUp errorPopUp = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);
		errorPopUp.GPayFunnel();
	}

	@Then("^Il toggle di Google Pay risulta attivo$")
	public void il_toggle_di_Google_Pay_risulta_attivo() throws Throwable {
		ProductsConnectCardDetailsPage impostazioniCartaPage = (ProductsConnectCardDetailsPage) PageRepo.get()
				.get(ProductsConnectCardDetailsPage.NAME);

		impostazioniCartaPage.isGpatToggle();
	}

	@Then("^Utente disattiva Google Pay$")
	public void utente_disattiva_Google_Pay() throws Throwable {
		ProductsConnectCardDetailsPage impostazioniCartaPage = (ProductsConnectCardDetailsPage) PageRepo.get()
				.get(ProductsConnectCardDetailsPage.NAME);

		impostazioniCartaPage.disableGpayToggle();
	}

	@Then("^Il toggle di Google Pay risulta disattivato correttamente$")
	public void il_toggle_di_Google_Pay_risulta_disattivato_correttamente() throws Throwable {
		ProductsConnectCardDetailsPage impostazioniCartaPage = (ProductsConnectCardDetailsPage) PageRepo.get()
				.get(ProductsConnectCardDetailsPage.NAME);

		impostazioniCartaPage.isNotGpatToggle();
	}

	@When("^Viene mostrato il popup di abilita GPS$")
	public void viene_mostrato_il_popup_di_abilita_GPS() throws Throwable {
		GenericErrorPopUp popupAuth = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);
		try {
			if (this.isIOS) {
				popupAuth.verifyLayoutOfPopup(driverAndroid, null,
						TestEnvironment.get().getLanguage("generic-popup").getProperty("GpsPopUpIOS"), isIOS);
				try {
					String permissionTextButton = "Consenti una volta";
					popupAuth.isPermissionRequiredSamsung(permissionTextButton);
				} catch(Exception e)
				{
					//TODO
				}

				try {
					String permissionTextButton = "OK";
					popupAuth.isPermissionRequiredSamsung(permissionTextButton);
				} catch(Exception e)
				{
					//TODO
				}

			} else {
				popupAuth.verifyLayoutOfPopup(driverAndroid, null,
						TestEnvironment.get().getLanguage("generic-popup").getProperty("GpsPopUp"), isIOS);
				String permissionTextButton = "OK";
				popupAuth.isPermissionRequiredSamsung(permissionTextButton);
			}

		} catch (Exception e) {
			System.out.println("Non ho cliccato sul popup");
		}
	}

	@When("^Utente attiva il GPS del device dalla pagina del device$")
	public void utente_attiva_il_GPS_del_device() throws Throwable {
		if (this.isIOS) {

		} else {
			Properties devName = UIUtils.ui().getCurrentProperties();
			AdbCommandPrompt adbCmd = new AdbCommandPrompt(adbExe, devName.getProperty("ios.udid"));
			// assertTrue(adbCmd.getCurrentActivity().getAppPackage().equals("com.android.settings"));
			GenericErrorPopUp popupAuth = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);
			//			if (popupAuth.isLocationVisible()) {
			//				((PressesKey) driverAndroid).pressKey(new KeyEvent(AndroidKey.BACK));
			//			}
			adbCmd.enableGps();
			WaitManager.get().waitMediumTime();
			//			((PressesKey) driverAndroid).pressKey(new KeyEvent(AndroidKey.BACK));
		}

		if(this.isIOS) {

			//			((HasSettings) driverAndroid).setSetting(Setting.WAIT_FOR_SELECTOR_TIMEOUT, 600000);
			//			CommandExecutionHelper.execute((ExecutesMethod) driverAndroid, setSettingsCommand("customSnapshotTimeout", 15f));
			CommandExecutionHelper.execute((ExecutesMethod) driverAndroid, setSettingsCommand("snapshotMaxDepth", 100));
		}
	}

	@When("^Utente seleziona la voce Mappa e la visualizza per ogni sezione$")
	public void utente_seleziona_la_voce_Mappa_e_la_visualizza_per_ogni_sezione() throws Throwable {
		if(this.isIOS) {

			((HasSettings) driverAndroid).setSetting(Setting.WAIT_FOR_SELECTOR_TIMEOUT, 600000);
			//			CommandExecutionHelper.execute((ExecutesMethod) driverAndroid, setSettingsCommand("customSnapshotTimeout", 15f));
			CommandExecutionHelper.execute((ExecutesMethod) driverAndroid, setSettingsCommand("snapshotMaxDepth", 100));
		}

		MapsGenericScreenFromHomePage maps = (MapsGenericScreenFromHomePage) PageRepo.get()
				.get(MapsGenericScreenFromHomePage.NAME);
		WaitManager.get().waitLongTime();

		System.out.println(driverAndroid.getPageSource());

		Parser.parsePageSourceToUix(driverAndroid, "RICCHIONE");

		((LocationContext) driverAndroid).setLocation(new Location(41.62905623988042, 12.476961175726242, 60)); // Pomezia
		// SCONTI
		WaitManager.get().waitLongTime();
		try {
			GenericErrorPopUp popupAuth = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);
			if (this.isIOS) {
				popupAuth.verifyLayoutOfGpsPopupIos(driverAndroid,
						TestEnvironment.get().getLanguage("generic-popup").getProperty("GpsPopupIOSTitle"),
						TestEnvironment.get().getLanguage("generic-popup").getProperty("GpsPopUpIOS"), isIOS);
				String permissionTextButton = "Consenti una volta";
				popupAuth.isPermissionRequiredSamsung(permissionTextButton);
			} else {
				popupAuth.verifyLayoutOfPopup(driverAndroid, null,
						TestEnvironment.get().getLanguage("generic-popup").getProperty("GpsPopUp"), isIOS);
				String permissionTextButton = "OK";
				popupAuth.isPermissionRequiredSamsung(permissionTextButton);
			}
		} catch (Exception e) {
			System.out.println("Ho cliccato il popup");
		}
		//		maps.clickOnTabMappa();
		WaitManager.get().waitShortTime();

		maps.clickOnCashback();
		maps.verifyPin();
		maps.clickOnPin(0);
		maps.verifyCategorie("CASHBACK");
		maps.clickOnCashback();

		//((LocationContext) driverAndroid).setLocation(new Location(41.72799403921825, 12.381603895064076, 108)); // Pomezia
		// QR
		// CODE

		WaitManager.get().waitShortTime();

		maps.clickOnCodiceQR();
		maps.verifyPin();
		maps.clickOnPin(0);
		maps.verifyCategorie("CODICE QR");
		maps.clickOnCodiceQR();

		//((LocationContext) driverAndroid).setLocation(new Location(41.733765445001815, 12.375850556654655, 108)); // UFFICIO

		WaitManager.get().waitShortTime();

		((LocationContext) driverAndroid).setLocation(new Location(41.9023383763801, 12.493247863827003, 180));

		WaitManager.get().waitMediumTime();

		maps.clickOnUfficiPostali();
		maps.verifyPin();
		maps.clickOnPin(0);
		maps.verifyCategorie("UFFICI POSTALI");
		maps.clickOnUfficiPostali();

		//		((LocationContext) driverAndroid).setLocation(new Location(41.73835121641824, 12.375643114191572, 180)); // ATM

		((LocationContext) driverAndroid).setLocation(new Location(41.74667585080436, 12.370632796358477, 180)); // ATM

		WaitManager.get().waitShortTime();

		maps.scrollChipLabel();
		maps.clickOnATM();
		maps.verifyPin();
		maps.clickOnPin(0);
		maps.verifyCategorie("ATM");
		maps.clickOnATM();
		maps.clickOnUfficiPostali();
		WaitManager.get().waitShortTime();

		maps.scrollChipLabel();
		maps.scrollChipLabel();
		maps.scrollChipLabel();
		maps.scrollChipLabel();
		maps.scrollChipLabel();
		WaitManager.get().waitShortTime();

		((LocationContext) driverAndroid).setLocation(new Location(41.86338412837023, 12.548369469374853, 120)); // A domicilio
		WaitManager.get().waitMediumTime();
		maps.clickOnSosta();
		maps.verifyPin();
		maps.clickOnPin(0);
		maps.verifyCategorie("A DOMICILIO");
		maps.clickOnSosta();
		WaitManager.get().waitShortTime();

		((LocationContext) driverAndroid).setLocation(new Location(41.86338412837023, 12.548369469374853, 120)); // Ritiro in negozio
		maps.clickOnCarburante();
		maps.verifyPin();
		maps.clickOnPin(0);
		maps.verifyCategorie("RITIRO IN NEGOZIO");
		maps.clickOnCarburante();
		WaitManager.get().waitShortTime();
	}

	@When("^Utente verifica il layout della videata sconti in formato lista$")
	public void utente_verifica_il_layout_della_videata_sconti_in_formato_lista() throws Throwable {
		MapsGenericScreenFromHomePage maps = (MapsGenericScreenFromHomePage) PageRepo.get()
				.get(MapsGenericScreenFromHomePage.NAME);
		maps.verifyHeaderpage(TestEnvironment.get().getLanguage("header").getProperty("mappaTitle"));
		maps.verifyGenericLayout();
	}

	@When("^Utente clicca sul primo parcheggio disponibile$")
	public void utente_clicca_sul_primo_parcheggio_disponibile() throws Throwable {
		// Localizzato in parcheggio specifico
		WaitManager.get().waitShortTime();
		((LocationContext) driverAndroid).setLocation(new Location(41.91646330737915, 12.54403326552285, 69)); // Roma - Zona01 -7001
		WaitManager.get().waitHighTime();

		ParkingPurchasePageStep1 park = (ParkingPurchasePageStep1) PageRepo.get().get(ParkingPurchasePageStep1.NAME);
		park.verifyHeaderpage(TestEnvironment.get().getLanguage("header").getProperty("ParkingPurchasePageStep1"));
		park.verifyLayout();
		park.clickOnParking();
	}

	@When("^Utente clicca sul PAGA Parcheggio$")
	public void utente_clicca_sul_PAGA_Parcheggio() throws Throwable {
		ParkingPurchasePageStep2 park2 = (ParkingPurchasePageStep2) PageRepo.get().get(ParkingPurchasePageStep2.NAME);
		park2.verifyHeaderpage(TestEnvironment.get().getLanguage("header").getProperty("ParkingPurchasePageStep2"));
		WaitManager.get().waitShortTime();
		park2.verifyLayout();
		WaitManager.get().waitShortTime();
		park2.clickOnPaga();
		WaitManager.get().waitShortTime();
	}

	@Then("^Utente verifica il riepilogo dell acquisto del parcheggio e clicca su Annulla$")
	public void utente_verifica_il_riepilogo_dell_acquisto_del_parcheggio_e_clicca_su_Annulla(
			List<UserDataCredential> dataTable) throws Throwable {
		UserDataCredential table = dataTable.get(0);
		ParkingPurchasePageStep3 park3 = (ParkingPurchasePageStep3) PageRepo.get().get(ParkingPurchasePageStep3.NAME);
		WaitManager.get().waitShortTime();
		park3.verifyHeaderpage(TestEnvironment.get().getLanguage("header").getProperty("ParkingPurchasePageStep3"));
		park3.verifyLayout(table.getCardNumber());
		park3.clickOnAnnullaSummary();
	}

	@Then("^Utente verifica il riepilogo dell acquisto del parcheggio e clicca su paga$")
	public void utente_verifica_il_riepilogo_dell_acquisto_del_parcheggio_e_clicca_su_paga(
			List<UserDataCredential> dataTable) throws Throwable {
		UserDataCredential table = dataTable.get(0);
		ParkingPurchasePageStep3 park3 = (ParkingPurchasePageStep3) PageRepo.get().get(ParkingPurchasePageStep3.NAME);
		park3.verifyHeaderpage(TestEnvironment.get().getLanguage("header").getProperty("ParkingPurchasePageStep3"));
		park3.verifyLayout(table.getCardNumber());
		park3.clickOnPagaSummary();
	}

	@Then("^Utente atterra sulla TYP del parcehggio e clicca sul CTA Vai alla Ricevuta$")
	public void utente_atterra_sulla_TYP_del_parcehggio_e_clicca_sul_CTA_Vai_alla_Ricevuta(
			List<LayoutDataBean> dataDable) throws Throwable {
		LayoutDataBean dataLayout = dataDable.get(0);
		double discrepanza = StringAndNumberOperationTools.convertStringToDouble(dataLayout.getDiscrepanza());

		ServiziTYP page = (ServiziTYP) PageRepo.get().get(ServiziTYP.NAME);

		page.verifyLayoutPageGeneric(driverAndroid, dataLayout.getTargetOperation(), discrepanza);
		page.clickOnIlTuoBiglietto();
		page.verificaTalloncino();
	}

	//Visualizzo il biglietto e lo interrompo per eliminarlo dalla lista dei biglietti attivi
	@Then("^Utente interrompe il biglietto acquistato$")
	public void utente_interrompe_il_biglietto_acquistato() {

		ServiziTYP page = (ServiziTYP) PageRepo.get().get(ServiziTYP.NAME);
		page.interrompiTalloncino();
	}

	@Then("^Utente controlla se è presente il biglietto del parcheggio$")
	public void utente_controlla_se_è_presente_il_biglietto_del_parcheggio() throws Throwable {
		//		AcquistoParcheggioPage  dettaglio = (AcquistoParcheggioPage) PageRepo.get().get(AcquistoParcheggioPage.NAME);
		//		dettaglio.get();

		// Per implementare questo comando è richiesto il settaggio ad una posizione  
		// specivica fatta allo step "Utente clicca sul primo parcheggio disponibile"
		// I campi sono troppo dinamici per effettuare una verifica puntuale
		//		dettaglio.verifyData(titolo, tipo, luogo, indirizzo, msgStatico, msgDinamico);

	}

	String dataSelezionata;

	@When("^Utente clicca su filtri e inserisce le date$")
	public void utente_clicca_su_filtri_e_inserisce_le_date() throws Throwable {
		BachecaPage bacheca = (BachecaPage) PageRepo.get().get(BachecaPage.NAME);
		SelezionaFiltriBachecaPage filtri = (SelezionaFiltriBachecaPage) PageRepo.get()
				.get(SelezionaFiltriBachecaPage.NAME);

		bacheca.clickOnFilterIcon();
		filtri.verifyHeaderpage(TestEnvironment.get().getLanguage("header").getProperty("SelezionaFiltriBacheca"));
		filtri.verifyLayout();
		WaitManager.get().waitShortTime();
		filtri.inserisciData();
		dataSelezionata = filtri.getDataSelezionata();
		filtri.controlloFiltro();
		WaitManager.get().waitShortTime();
		filtri.clickOnSalva();
		WaitManager.get().waitMediumTime();
	}

	@Then("^Utente controlla che il filtro funziona correttamente$")
	public void utente_controlla_che_il_filtro_funziona_correttamente() throws Throwable {
		BachecaPage bacheca = (BachecaPage) PageRepo.get().get(BachecaPage.NAME);
		bacheca.controlloFiltroData(dataSelezionata.toUpperCase());
		bacheca.clickOnFilterIcon();

		SelezionaFiltriBachecaPage filtri = (SelezionaFiltriBachecaPage) PageRepo.get()
				.get(SelezionaFiltriBachecaPage.NAME);
		filtri.resettaFiltro();
		bacheca.clickOnFilterIcon();
	}

	@Then("^Utente inserisce date sbagliate e verifica il corretto popUp$")
	public void utente_inserisce_date_sbagliate_e_verifica_il_corretto_popUp() throws Throwable {
		SelezionaFiltriBachecaPage filtri = (SelezionaFiltriBachecaPage) PageRepo.get()
				.get(SelezionaFiltriBachecaPage.NAME);
		filtri.inserisciDateDifferenti();
		GenericErrorPopUp errorPopUp = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);
		errorPopUp.verifyLayoutOfPopup(driverAndroid, "Avviso", "Data selezionata non valida", this.isIOS);
		errorPopUp.clickOnOKPopUp();
	}

	@Then("^Utente verifica i movimenti in uscita relativi alla ricarica \\(importo e commissione\\)$")
	public void utente_verifica_i_movimenti_in_uscita_relativi_alla_ricarica_importo_e_commissione(
			List<UserDataCredential> dataTable) throws Throwable {
		UserDataCredential table = dataTable.get(0);
		ProductsStandardPage page = (ProductsStandardPage) PageRepo.get().get(ProductsStandardPage.NAME);
		String data = LocalDate.now().format(DateTimeFormatter.ofPattern("dd MMM yyyy"));
		page.verifyLayoutPage();
		page.clickPrimoMovimento("COMMISSIONE RICARICA POSTEPAY", "€1,00");
		page.verifyDettaglioMovimenti("OPERAZIONE DA CANALE MOBILE", "€1,00", data.toUpperCase(), data.toUpperCase(),
				table.getCardNumber(), "Addebitato su Postepay");

		((PressesKey) driverAndroid).pressKey(new KeyEvent(AndroidKey.BACK));

		page.clickPrimoMovimento("RICARICA POSTEPAY", table.getImporto());
		page.verifyDettaglioMovimenti("RICARICA POSTEPAY", table.getImporto(), data.toUpperCase(), data.toUpperCase(),
				table.getCardNumber(), "Addebitato su Postepay");
	}

	@When("^Utente clicca su Ministero dei Trasporti$")
	public void utente_clicca_su_Ministero_dei_Trasporti() throws Throwable {

		if(this.isIOS) {			
			CommandExecutionHelper.execute((ExecutesMethod) driverAndroid, setSettingsCommand("snapshotMaxDepth", 100));
		}

		PaymentPaymentPage payPage = (PaymentPaymentPage) PageRepo.get().get(PaymentPaymentPage.NAME);
		payPage.clickOnMinisteroTrasporti();
		WaitManager.get().waitShortTime();
	}

	@When("^Utente inserisce i dati della pratica MIT$")
	public void utente_inserisce_i_dati_della_pratica_MIT() throws Throwable {
		MinisteroTrasportiPage ministeroPage = (MinisteroTrasportiPage) PageRepo.get().get(MinisteroTrasportiPage.NAME);
		ministeroPage
		.verifyHeaderpage(TestEnvironment.get().getLanguage("header").getProperty("MinisteroDeiTrasportiPage"));
		ministeroPage.verifyLayout();
		WaitManager.get().waitShortTime();

		ministeroPage.inserisciTipologiaPratica();
		WaitManager.get().waitShortTime();

		ministeroPage.inserisciPratica();
		WaitManager.get().waitShortTime();

	}

	@Then("^Utente clicca su CALCOLA della pratica MIT$")
	public void utente_clicca_su_CALCOLA_della_pratica_MIT() throws Throwable {
		MinisteroTrasportiPage ministeroPage = (MinisteroTrasportiPage) PageRepo.get().get(MinisteroTrasportiPage.NAME);
		ministeroPage.clickOnCalcola();
		WaitManager.get().waitShortTime();
	}

	@Then("^Utente verifica i dati di riepilogo MIT e clicca su PAGA$")
	public void utente_verifica_i_dati_di_riepilogo_MIT_e_clicca_su_PAGA(List<UserDataCredential> dataTable)
			throws Throwable {
		MinisteroTrasportiSummaryPage ministeroSummary = (MinisteroTrasportiSummaryPage) PageRepo.get()
				.get(MinisteroTrasportiSummaryPage.NAME);
		ministeroSummary
		.verifyHeaderpage(TestEnvironment.get().getLanguage("header").getProperty("MinisteroDeiTrasportiPage"));
		UserDataCredential table = dataTable.get(0);
		ministeroSummary.verifyLayoutMITSummary(table.getCardNumber(), table.getCodiceFiscale());
		UIUtils.mobile().swipe((MobileDriver<?>) driverAndroid, SCROLL_DIRECTION.DOWN, 200);
		WaitManager.get().waitShortTime();
		ministeroSummary.clickOnPagaMinistero();
	}

	@When("^Utente clicca su icona della rubrica e inserisce i dati$")
	public void utente_clicca_su_icona_della_rubrica_e_inserisce_i_dati(List<UserDataCredential> dataTable)
			throws Throwable {
		UserDataCredential table = dataTable.get(0);
		PagaBollettinoBiancoT135 pagaBollettino = (PagaBollettinoBiancoT135) PageRepo.get()
				.get(PagaBollettinoBiancoT135.NAME);
		pagaBollettino.clickOnRubrica();
		WaitManager.get().waitShortTime();

		RubricaBollettinoPage rubrica = (RubricaBollettinoPage) PageRepo.get().get(RubricaBollettinoPage.NAME);
		rubrica.verifyHeaderpage(TestEnvironment.get().getLanguage("header").getProperty("RubricaBollettino"));
		rubrica.scegliBeneficiario(table.getTargetUser(), table.getContoCorrente(), table.getImporto(),
				table.getCausale());

		String nome = table.getFortunatoNome();
		System.out.println("Nome: " + nome);
		String cognome = table.getFortunatoCognome();
		System.out.println("cognome: " + cognome);
		String indirizzo = table.getFortunatoIndirizzo();
		System.out.println("indirizzo: " + indirizzo);
		String citta = table.getFortunatoCitta();
		System.out.println("citta: " + citta);
		String cap = table.getFortunatoCap();
		System.out.println("cap: " + cap);
		String provincia = table.getFortunatoProvincia();
		System.out.println("provincia: " + provincia);
		pagaBollettino.verifyChiPagaSection(nome, cognome, indirizzo, citta, cap, provincia);
		WaitManager.get().waitShortTime();

		UIUtils.mobile().swipe((MobileDriver<?>) driverAndroid, SCROLL_DIRECTION.DOWN, 200);
		pagaBollettino.clickOnContinua();
	}

	@When("^Utente clicca su Trento$")
	public void utente_clicca_su_Trento() throws Throwable {
		MinisteroTrasportiPage ministeroPage = (MinisteroTrasportiPage) PageRepo.get().get(MinisteroTrasportiPage.NAME);
		ministeroPage.clickOnTrento();
	}

	@When("^Utente aspetta che scade la sessione$")
	public void utente_aspetta_che_scade_la_sessione() throws Throwable {
		try {
			for (int i = 0; i < 15; i++) {
				try {
					Thread.sleep(TimeUnit.MINUTES.toMillis(1));
				} catch (Exception err) {
				}
				System.out.println("sono trascorsi " + (i + 1) + " minuti...");
			}
		} catch (Exception err) {

		}
	}

	@When("^Utente non compila il campo \"([^\"]*)\" e compila tutti gli altri campi dal Bonifico SEPA$")
	public void utente_non_compila_il_campo_e_compila_tutti_gli_altri_campi_dal_Bonifico_SEPA(String emptyField,
			List<UserDataCredential> dataTable) throws Throwable {

		if(isIOS) {
			CommandExecutionHelper.execute((ExecutesMethod) driverAndroid, setSettingsCommand("customSnapshotTimeout", 15f));
			CommandExecutionHelper.execute((ExecutesMethod) driverAndroid, setSettingsCommand("snapshotMaxDepth", 100));
			CommandExecutionHelper.execute((ExecutesMethod) driverAndroid, setSettingsCommand("waitForIdleTimeout", 10));
			CommandExecutionHelper.execute((ExecutesMethod) driverAndroid, setSettingsCommand("waitForIdleTimeout", true));
		}

		UserDataCredential table = dataTable.get(0);
		BankTranfertSEPAPageStep2 step2 = (BankTranfertSEPAPageStep2) PageRepo.get()
				.get(BankTranfertSEPAPageStep2.NAME);

		step2.verifyHeaderpage(TestEnvironment.get().getLanguage("header").getProperty("BankTranfertSEPAPageStep1"));
		step2.compileAllExceptOne(emptyField, table.getIban(), table.getTargetUser(), table.getImporto(),
				table.getComment());

	}

	@When("^Utente visualizza una popup di errore per il campo \"([^\"]*)\"$")
	public void utente_visualizza_una_popup_di_errore_per_il_campo(String emptyField) throws Throwable {
		GenericErrorPopUp errorPopUp = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);
		errorPopUp.verifyEmptyFieldError(emptyField);

	}

	@When("^Utente clicca su PROCEDI della Bonifico SEPA$")
	public void utente_clicca_su_PROCEDI_della_Bonifico_SEPA(DataTable arg1) throws Throwable {

		BankTranfertSEPAPageStep2 step2 = (BankTranfertSEPAPageStep2) PageRepo.get()
				.get(BankTranfertSEPAPageStep2.NAME);

		step2.clickOnProceed();

	}

	@Then("^Utente controlla la pagina di ScontiPoste$")
	public void utente_controlla_la_pagina_di_ScontiPoste() throws Throwable {
		ScontiPosteDettaglioT137 sconti = (ScontiPosteDettaglioT137) PageRepo.get().get(ScontiPosteDettaglioT137.NAME);
		sconti.verifyLayout();

	}

	@Then("^Utente controlla che la pagina Mappe sia corretta$")
	public void utente_controlla_che_la_pagina_Mappe_sia_corretta() throws Throwable {
		MapsGenericScreenFromHomePage mappe = (MapsGenericScreenFromHomePage) PageRepo.get()
				.get(MapsGenericScreenFromHomePage.NAME);
		mappe.checkLayout();
	}

	@When("^Utente clicca sul toggle \"([^\"]*)\"$")
	public void utente_clicca_sul_toggle(String arg1) throws Throwable {
		MapsGenericScreenFromHomePage tabMappe = (MapsGenericScreenFromHomePage) PageRepo.get()
				.get(MapsGenericScreenFromHomePage.NAME);
		if (arg1.equals("MAPPE")) {
			tabMappe.clickOnTabMappa();
		} else {
			tabMappe.clickOnTabLista();
		}

	}

	@When("^Utente effettua una ricerca su piu parole random e verifica i risultati$")
	public void utente_effettua_una_ricerca_su_piu_parole_random_e_verifica_i_risultati(List<UserDataCredential> dataTable) throws Throwable {
		UserDataCredential table = dataTable.get(0);

		String[] targetList = getTargetList(table.getListaTarget());

		int resScore = 0; // somma dei risultati
		int totSearch = targetList.length; // lunghezza della lista

		BachecaPage bPage = (BachecaPage) PageRepo.get().get(BachecaPage.NAME);
		bPage.verifyHeaderPage();

		for (int i = 0; i < targetList.length; i++) {
			String array_element = targetList[i];
			System.out.println(array_element);

			int res = bPage.sendAndVerify(array_element, isIOS);
			System.out.println("I risultati della ricerca per la parola: " + array_element + " sono: " + res);
			resScore = resScore + res;
		}

		double counterControl = new Double(resScore)/new Double(totSearch-1); // Percentuale di ricerca positiva
		System.out.println("Il risultato finale della ricerca su " + (totSearch-1) + " chiavi di ricerca è stato: " + counterControl + " ovvero " + resScore + " ricerche andate a buon fine");
		assertTrue("La ricerca non ha superato la percentuale del 75%", counterControl > 0.75);
	}

	private String[] getTargetList(String listaTarget) {
		System.out.println("getTargetList");
		String list = TestEnvironment.get().getLanguage("liste").getProperty(listaTarget);
		System.out.println(list);
		String[] targetList = list.split(";");
		System.out.println(Arrays.toString(targetList));
		return targetList;
	}

	@Then("^Una Popup di errore avvisa che il CC o l IBAN è errato$")
	public void una_Popup_di_errore_avvisa_che_il_CC_o_l_IBAN_è_errato(List<UserDataCredential> dataTable) throws Throwable {
		UserDataCredential table = dataTable.get(0);

		String popupMsg = TestEnvironment.get().getLanguage("generic-popup").getProperty("IbanError");
		if(table.getComment().equals("CCErrato")) {
			popupMsg = TestEnvironment.get().getLanguage("generic-popup").getProperty("CCErrato");
		}

		WaitManager.get().waitMediumTime();
		GenericErrorPopUp errorPopup = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);

		errorPopup.verifyLayoutOfPopup(driverAndroid, 
				"Avviso", 
				popupMsg, 
				isIOS);
		errorPopup.clickOnOKPopUp();
	}

	@Then("^Utente seleziona il Vettore \"([^\"]*)\" e controlla che la lista di risultati si aggiorni$")
	public void utente_seleziona_il_Vettore_e_controlla_che_la_lista_di_risultati_si_aggiorni(String vettore) throws Throwable {
		ExtraurbanPurchasePageStep2 page = (ExtraurbanPurchasePageStep2) PageRepo.get()
				.get(ExtraurbanPurchasePageStep2.NAME);

		WaitManager.get().waitMediumTime();
		page.verifyHeaderpage(TestEnvironment.get().getLanguage("header").getProperty("ExtraurbanPurchasePageStep2"));
		page.verifyLayoutPage();
		page.selectVector(vettore);
	}

	@Then("^Utente seleziona come ulteriore filtro il parametro \"([^\"]*)\" e controlla che la lista di risultati si aggiorni$")
	public void utente_seleziona_come_ulteriore_filtro_il_parametro_e_controlla_che_la_lista_di_risultati_si_aggiorni(String filtro) throws Throwable {
		ExtraurbanPurchasePageStep2 page = (ExtraurbanPurchasePageStep2) PageRepo.get()
				.get(ExtraurbanPurchasePageStep2.NAME);

		WaitManager.get().waitMediumTime();
		page.verifyHeaderpage(TestEnvironment.get().getLanguage("header").getProperty("ExtraurbanPurchasePageStep2"));

		page.selectFilter(filtro);

		page.verifyListOrderByPREZZO();
	}

	@When("^Utente clicca su PagoPA$")
	public void utente_clicca_su_PagoPA() throws Throwable {
		PaymentPaymentPage payPage = (PaymentPaymentPage) PageRepo.get().get(PaymentPaymentPage.NAME);
		payPage.clickOnPagaPA();
		WaitManager.get().waitShortTime();
		try {
			GenericErrorPopUp popup=(GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);
			popup.isPermissionRequiredSamsung("OK");
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@When("^Utente clicca sul tab \"([^\"]*)\"$")
	public void utente_clicca_sul_tab(String tab) throws Throwable {
		PagoPaPage paPage = (PagoPaPage) PageRepo.get().get(PagoPaPage.NAME);

		System.out.println("Click su compila manualmente");
		WaitManager.get().waitShortTime();
		System.out.println(driverAndroid.getPageSource());
		//		paPage.click();

		//		paPage.clickOnManuale();

		if(tab.equals("BANCHE")) {
			paPage.clickOnTabBanche();

		} else {
			paPage.clickOnTabPostale();
		}

	}

	@When("^Utente verifica il layout della pagina PagoPA per Banche$")
	public void utente_verifica_il_layout_della_pagina_PagoPA_per_Banche() throws Throwable {
		PagoPaPage paPage = (PagoPaPage) PageRepo.get().get(PagoPaPage.NAME);

		paPage.verifyLayoutBanche();
	}

	String msgTitolo;
	String msgDescrizione;
	String msgData;

	@Then("^Utente fa clic sul primo elemento della scheda messaggio di ricerca$")
	public void utente_fa_clic_sul_primo_elemento_della_scheda_messaggio_di_ricerca(List<UserDataCredential> dataTable) throws Throwable {
		UserDataCredential table = dataTable.get(0);
		BachecaPage bPage = (BachecaPage) PageRepo.get().get(BachecaPage.NAME);
		bPage.reserch(table.getComment());
		if(!isIOS) {
			((HidesKeyboard) driverAndroid).hideKeyboard();
			WaitManager.get().waitMediumTime();
		}
		List<String> res = bPage.verifyAndOpenTarget(table.getComment());

		msgTitolo = res.get(0);
		msgDescrizione = res.get(1);
		msgData = res.get(2);

		System.out.println("1: " + msgTitolo + " 2: " + msgDescrizione + " 3: " + msgData);

	}

	@Then("^Utente controlla elemento di ricerca nei dettagli del messaggio$")
	public void utente_controlla_elemento_di_ricerca_nei_dettagli_del_messaggio() throws Throwable {
		DettailsMessageConfirmRemovingAutomaticRecharge msgPage = (DettailsMessageConfirmRemovingAutomaticRecharge) PageRepo
				.get().get(DettailsMessageConfirmRemovingAutomaticRecharge.NAME);

		msgPage.verifyHeaderPage();
		msgPage.verifyMessage(msgTitolo, msgData, msgDescrizione);

		WaitManager.get().waitLongTime();

	}

	@Then("^Utente seleziona la carta con cui pagare$")
	public void utente_seleziona_la_carta_con_cui_pagare(List<UserDataCredential> dataTable) throws Throwable {
		if(this.isIOS)	{
			System.out.println(driverAndroid.getPageSource());
			UserDataCredential table = dataTable.get(0);
			double discrepanza = StringAndNumberOperationTools.convertStringToDouble(table.getDiscrepanza());
			RechargeMyPostepayPageStep3 step3 = (RechargeMyPostepayPageStep3) PageRepo.get().get(RechargeMyPostepayPageStep3.NAME);
			step3.verifyHeaderpage(
					TestEnvironment.get().getLanguage("header").getProperty("MinisteroDeiTrasportiPage"));
			step3.verifyLayoutPage(driverAndroid, "Come vuoi pagare?", "DIGITAL", table.getCardNumber(),discrepanza);
			step3.selectCardToRecharge(driverAndroid, table.getCardNumber());
		}
		else {
			System.out.println("Lo step serve solo per lo sviluppo IOS");
		}
	}

	@When("^Utente verifica il layout della sezione VICINO A TE$")
	public void utente_verifica_il_layout_della_sezione_VICINO_A_TE() throws Throwable {
		//		WaitManager.get().waitHighTime();


		MapsGenericScreenFromHomePage maps = (MapsGenericScreenFromHomePage) PageRepo.get()
				.get(MapsGenericScreenFromHomePage.NAME);
		WaitManager.get().waitLongTime();

		//		try {
		//			GenericErrorPopUp popupAuth = (GenericErrorPopUp) PageRepo.get().get(GenericErrorPopUp.NAME);
		//			if (this.isIOS) {
		//				popupAuth.verifyLayoutOfGpsPopupIos(driverAndroid,
		//						TestEnvironment.get().getLanguage("generic-popup").getProperty("GpsPopupIOSTitle"),
		//						TestEnvironment.get().getLanguage("generic-popup").getProperty("GpsPopUpIOS"), isIOS);
		//				String permissionTextButton = "Consenti una volta";
		//				popupAuth.isPermissionRequiredSamsung(permissionTextButton);
		//			} else {
		//				popupAuth.verifyLayoutOfPopup(driverAndroid, null,
		//						TestEnvironment.get().getLanguage("generic-popup").getProperty("GpsPopUp"), isIOS);
		//				String permissionTextButton = "OK";
		//				popupAuth.isPermissionRequiredSamsung(permissionTextButton);
		//			}
		//		} catch (Exception e) {
		//			System.out.println("Ho cliccato il popup");
		//		}

		maps.verifyLayoutNewPageMaps(isIOS);

		//		maps.clickOnCashback();
		//		maps.verifyPin();
		//		maps.clickOnPin(0);
		//		maps.verifyCategorie("CASHBACK");
		//		maps.clickOnCashback();
	}

	@When("^Utente verifica il corretto funzionamento della mappa della sezione VICINO A TE$")
	public void utente_verifica_il_corretto_funzionamento_della_mappa_della_sezione_VICINO_A_TE() throws Throwable {
		MapsGenericScreenFromHomePage maps = (MapsGenericScreenFromHomePage) PageRepo.get()
				.get(MapsGenericScreenFromHomePage.NAME);

		maps.clickOnCerca();
	}

	@When("^Utente clicca sul tab homePage$")
	public void utente_clicca_sul_tab_homePage() throws Throwable {
		HomePage home = (HomePage) PageRepo.get().get(HomePage.NAME);
		home.clickOnMenuTabIcon();
		WaitManager.get().waitShortTime();
	}
}
