package com.postepay.acceptance.test.ui;

import static io.appium.java_client.touch.offset.PointOption.point;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.postepay.acceptance.test.ui.TestEnvironment;

import cucumber.api.Scenario;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import test.automation.core.UIUtils;
import test.automation.core.UIUtils.SCROLL_DIRECTION;

public class VideoManagerTool {
	
	String deviceUsed;
	String testId;
	String user;
	String language;
	
	public VideoManagerTool() {
		// TODO Auto-generated constructor stub
	}
	
	public void updateStringForFile(String deviceUsedInput, String testIdInput, String userInput, String languageInput) {
		this.deviceUsed=deviceUsedInput;
		this.testId=testIdInput;
		this.user=userInput;
		this.language=languageInput;
	}
	
	public void openNotificationCenter(AndroidDriver<?> driver) {
		
		System.out.println("Prova ad aprire la notifica");
		
		driver.openNotifications();
		
		System.out.println("Notifica aperta");
		
		try {Thread.sleep(2000);} catch (Exception err )  {}
	}
	
	public void scrollDown(AndroidDriver<?> driver) {

		System.out.println("Prova ad aprire la notifica");
		
		try {
			UIUtils.mobile().swipe((MobileDriver<?>) driver, SCROLL_DIRECTION.DOWN, 500);
		} catch (Exception err )  {}
		
		
		try {Thread.sleep(2000);} catch (Exception err )  {}
	}
	
	public void scrollDownOnIconStatusMenu(AndroidDriver<?> driver, String deviceUsed) {
		
		if (deviceUsed.equals("GalaxyS8")) {
			
			System.out.println("Prova ad aprire la notifica");
			
			String nuovoLocator="//*[@resource-id='com.android.systemui:id/brightness']";
			
			WebElement elem1=driver.findElement(By.xpath(nuovoLocator));

			int coordinateX; int coordinateY; int coordinateYnew;

			coordinateX=elem1.getLocation().getX();
			coordinateY=elem1.getLocation().getY();
			coordinateYnew=coordinateY+800;
			
			// TouchAction e imposto il driver
			TouchAction touchAction = new TouchAction((MobileDriver<?>) driver);
			touchAction.longPress(point(coordinateX,coordinateY)).moveTo(point(coordinateX,coordinateYnew)).release().perform();
			
			try {Thread.sleep(2000);} catch (Exception err )  {}
		}
	}
	
	public void clickOnIconOfRechargeVideo(AndroidDriver<?> driver, String deviceUsed) {

		if (deviceUsed.equals("GalaxyS8")) {
		
			String nuovoLocator="//*[@resource-id='com.android.systemui:id/tile_label' and @text='Registra schermo']";
			
			WebElement elem=driver.findElement(By.xpath(nuovoLocator));
	
			elem.click();
			
			try {Thread.sleep(5000);} catch (Exception err )  {}
		}
	}
	
	public void saveFileVideoPerforming(AndroidDriver<?> driver) {
		
		String nameFolderTarget= this.testId +"_"+ this.deviceUsed +"_"+ this.user +"_"+ this.language;
		System.out.println(nameFolderTarget);
		
		if (deviceUsed.equals("GalaxyS8")) {
			
			try {
				String firstAction="adb pull \"/sdcard/Pictures/Screenshots/\" ";
//				System.out.println(firstAction);
				String seconfAction="\"C:\\Users\\Utente\\Documents\\Automation\\Repository\\POSTE ITALIANE\\PP_package\\v1_0\\com.postepay.acceptance.test.v2\\com.postepay.acceptance.test.ui\\target\\embedded-videos\\";
//				System.out.println(seconfAction);
				String suffixDestination=nameFolderTarget+"\"";
//				System.out.println(suffixDestination);
				
				String command= firstAction + seconfAction + suffixDestination;
//				System.out.println(command);
				
				Process commandRun=Runtime.getRuntime().exec(command);
				int exitCode = commandRun.waitFor();
				
				Runtime.getRuntime().exec("adb shell rm -rf /sdcard/Pictures/Screenshots/");
			
			} catch (IOException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			try {Thread.sleep(5000);} catch (Exception err )  {}
		}
	}

	public void clickOnRECbuttonSTART(AndroidDriver<?> driver, String deviceUsedInput, String testIdInput, String userInput, String languageInput) {
		
		if (deviceUsed.equals("GalaxyS8")) {
			
			updateStringForFile(deviceUsedInput, testIdInput, userInput, languageInput);
			
			openNotificationCenter(driver);
			scrollDownOnIconStatusMenu(driver, deviceUsed);
			clickOnIconOfRechargeVideo(driver, deviceUsed);
		}
		else {
			TestEnvironment.get().startVideoRecording(deviceUsed);
		}
	}
	
	public void clickOnRECbuttonSTOP(AndroidDriver<?> driver, String deviceUsed) {
		
		if (deviceUsed.equals("GalaxyS8")) {
			openNotificationCenter(driver);
			scrollDownOnIconStatusMenu(driver, deviceUsed);
			clickOnIconOfRechargeVideo(driver, deviceUsed);
			
			// Salva il file nella directori di progetto
			saveFileVideoPerforming(driver);
		}
		else {
//			TestEnvironment.get().stopVideoRecording(scenario);
			
			}
		}
	
	public void stopVideoRecorder() {
		
		// Logica per stoppare il video recorder      /////////////////////////////////////////////////////////////////////////////////////////
//		EnvironmentSettings setting=dataTable.get(0);

		AndroidDriver<?> driver=(AndroidDriver<?>) TestEnvironment.get().getDriver(this.deviceUsed);

		VideoManagerTool tool=new VideoManagerTool();
		tool.clickOnRECbuttonSTOP(driver, deviceUsed);
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	}

}
	
