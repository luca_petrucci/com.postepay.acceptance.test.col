package com.postepay.acceptance.test.ui;

import java.io.File;
import java.net.URI;
import java.nio.file.Paths;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import ui.core.support.uiobject.Particle;

public class TestUtils 
{
	public static File[] getFilesFromFolder(URI uri)
	{
		File folder;
		try {
			folder = Paths.get(uri).toFile();
			File[] listOfFiles = folder.listFiles();
			
			return listOfFiles;
		}
		catch(Exception err)
		{
			err.printStackTrace();
			return null;
		}
	}
}

