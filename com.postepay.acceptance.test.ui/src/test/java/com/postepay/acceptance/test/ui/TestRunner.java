package com.postepay.acceptance.test.ui;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import test.automation.core.XCucumber;
import test.automation.core.annotation.Ui;

//   Classe di running

@RunWith(XCucumber.class)
@CucumberOptions(
features = {"src/test/resources/features/Login_v2.feature",
			"src/test/resources/features/Bollettino.feature",
		    "src/test/resources/features/G2G_v2.feature",
		    "src/test/resources/features/BonificoSEPA_v2.feature",
		    "src/test/resources/features/PosteID.feature",
		    "src/test/resources/features/P2P_v3.feature",
		    "src/test/resources/features/RicaricaAltraPostepay_v2.feature",
		    "src/test/resources/features/Salvadanaio.feature",
		    "src/test/resources/features/Servizi_v1.feature",
		    "src/test/resources/features/Impostazioni.feature",
		    "src/test/resources/features/RicaricaTelefonica.feature",
		    "src/test/resources/features/Bacheca.feature",
		    "src/test/resources/features/Sconti.feature",
		    "src/test/resources/features/Carte.feature",
		    "src/test/resources/features/BolloAuto.feature",
		    "src/test/resources/features/Notifiche.feature",
		    "src/test/resources/features/Pratica MIT.feature",
		    "src/test/resources/features/Mappe.feature",
		    "src/test/resources/features/PagoPA.feature"},
tags = {"@PPINPROAND-3415_fiorella"},
glue= {"classpath:com/postepay/acceptance/test/ui/features"}
)

// @PPINPROAND-3076_musella
//plugin = { "json:target/reports/LoginTest.json" },
@Ui
public class TestRunner {
	
	public TestRunner() {
		// TODO Auto-generated constructor stub
	}

}
