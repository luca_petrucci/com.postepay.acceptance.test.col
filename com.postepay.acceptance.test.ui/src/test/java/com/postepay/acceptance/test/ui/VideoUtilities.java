package com.postepay.acceptance.test.ui;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class VideoUtilities {
	
	Process processRecorder;
	
//	String deviceUsed;
//	String testId;
//	String user;
//	String language;
	
	private static String nameFolderTarget;
	
	public VideoUtilities() {
		// TODO Auto-generated constructor stub
	}
	
	public String getToDayString() {
		Date d = new Date();
		DateFormat formatoData = DateFormat.getDateInstance(DateFormat.LONG, Locale.ITALY);
		
		String s = formatoData.format(d);
		System.out.println(s);
		String date=s.replace(" ", "_");
		
		System.out.println(date);
		
		return date;
			
	}
	
	public String getToDayString2() {
		Date d = new Date();
		DateFormat formatoData = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

		String s = formatoData.format(d);
		System.out.println(s);
		String date=s.replace(" ", "_");
		String date2=date.replace(":", "-");
		
		System.out.println(date2);
		
		return date2;
			
	}
	public String updateStringForFile(String deviceUsedInput, String testIdInput, String userInput, String languageInput) {
//		this.deviceUsed=deviceUsedInput;
//		this.testId=testIdInput;
//		this.user=userInput;
//		this.language=languageInput;
		
		String toDayDate=getToDayString2();
		
		String nameFolderTarget= deviceUsedInput +"_"+ testIdInput +"_"+ userInput +"_"+ languageInput;
		String nameFolderTarget2=nameFolderTarget.replace(".", "");
		String nameFolderTarget3=nameFolderTarget2+"_"+toDayDate+".mp4\"";
		
		return VideoUtilities.nameFolderTarget=nameFolderTarget3;
	}

	public void startVideoRecorder(String deviceUsedInput, String testIdInput, String userInput, String languageInput) {
		
		updateStringForFile(deviceUsedInput, testIdInput, userInput, languageInput);
		System.out.println(deviceUsedInput);
		System.out.println(testIdInput);
		System.out.println(userInput);
		System.out.println(languageInput);
		System.out.println(VideoUtilities.nameFolderTarget);
		System.out.println("Parte il video");
		
		try {
			this.processRecorder=Runtime.getRuntime().exec("adb -s emulator-5554 shell screenrecord --size 1920x1080 /sdcard/Download/test.mp4");
			System.out.println("Il video e partito");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void stopVideoRecorder() {
		System.out.println("In stop video recorder " + VideoUtilities.nameFolderTarget);
		
		try {
			Runtime.getRuntime().exec("adb -s emulator-5554 shell pkill -2 screenrecord").waitFor();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
//		try {
//			System.out.println("Prova a chiudere il video recorder");
//			this.processRecorder.destroy();
//			if (this.processRecorder.isAlive()) {
//				
//				System.out.println("is Alive");
//				this.processRecorder.destroyForcibly();
//				System.out.println("Usa la forced Destroy");
//			}
//			
//			try {Thread.sleep(5000);} catch (Exception err )  {}
//			
//		}catch (Exception e) {
//			e.printStackTrace();
//		}
	}
	
	public void pullFileVideoRecorder() {
		
//		String nameFolderTarget= this.testId +"_"+ this.deviceUsed +"_"+ this.user +"_"+ this.language+".mp4";
		System.out.println(VideoUtilities.nameFolderTarget);
		
		String firstAction="adb -s emulator-5554 pull /sdcard/Download/test.mp4 ";
//		String seconfAction="\"C:\\Users\\Utente\\Documents\\Automation\\Repository\\POSTE ITALIANE\\PP_package\\v1_0\\com.postepay.acceptance.test.v2\\com.postepay.acceptance.test.ui\\target\\embedded-videos\\";
		String seconfAction="\"C:/Users/Utente/Documents/Automation/Repository/POSTE ITALIANE/PP_package/v1_0/com.postepay.acceptance.test.v2/com.postepay.acceptance.test.ui/target/embedded-videos/";
//		String seconfAction="C:/Users/Utente/Downloads/NuovaCartella/";
		
//		String commandProcess=firstAction + seconfAction + this.nameFolderTarget +".mp4\"";
		String commandProcess=firstAction + seconfAction + VideoUtilities.nameFolderTarget;
		
		System.out.println(commandProcess);
		
		try {
			Runtime.getRuntime().exec(commandProcess);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {Thread.sleep(3000);} catch (Exception err )  {}
		try {
			Runtime.getRuntime().exec("adb -s emulator-5554 shell rm -rf /sdcard/Download/test.mp4");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
