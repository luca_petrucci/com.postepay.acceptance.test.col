package com.postepay.acceptance.test.ui;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.postepay.automation.core.ui.verifytools.LayoutTools;

import cucumber.api.java.en.And;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.connection.ConnectionState;
import io.appium.java_client.android.connection.ConnectionStateBuilder;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import test.automation.core.cmd.adb.AdbCommandPrompt;
import test.automation.core.cmd.adb.AdbKeyEvent;
import ui.core.support.waitutil.WaitManager;

public class ConnectionsEmulatorTools {
	
	public ConnectionsEmulatorTools() {
		// TODO Auto-generated constructor stub
	}
	
	public void enableAirModeEmulator(AndroidDriver<?> driver) {
		driver.openNotifications();
		WaitManager.get().waitMediumTime();
		
		String expandArrow = "//*[@resource-id='com.android.systemui:id/expand_indicator']";
		driver.findElement(By.xpath(expandArrow)).click();
		WaitManager.get().waitShortTime();
		
		String airModeLocator = "//*[@content-desc='Airplane mode']";
		driver.findElement(By.xpath(airModeLocator)).click();
		WaitManager.get().waitShortTime();
		assertTrue(driver.findElement(By.xpath(airModeLocator)).getAttribute("checked").equals("true"));
		
		driver.pressKey(new KeyEvent(AndroidKey.BACK));
		WaitManager.get().waitShortTime();
		driver.pressKey(new KeyEvent(AndroidKey.BACK));
		WaitManager.get().waitLongTime();
	}

	public void disableAirModeEmulator(AndroidDriver<?> driver) {
		driver.openNotifications();
		WaitManager.get().waitMediumTime();
		
		String expandArrow = "//*[@resource-id='com.android.systemui:id/expand_indicator']";
		driver.findElement(By.xpath(expandArrow)).click();
		WaitManager.get().waitShortTime();
		
		String airModeLocator = "//*[@content-desc='Airplane mode']";
		driver.findElement(By.xpath(airModeLocator)).click();
		WaitManager.get().waitShortTime();
		assertTrue(driver.findElement(By.xpath(airModeLocator)).getAttribute("checked").equals("false"));
		
		driver.pressKey(new KeyEvent(AndroidKey.BACK));
		WaitManager.get().waitShortTime();
		driver.pressKey(new KeyEvent(AndroidKey.BACK));
		WaitManager.get().waitLongTime();
	}
	
	
	public static void disableCustomWifi(String adbExe, String emuName) {
		AdbCommandPrompt cmd = new AdbCommandPrompt(adbExe);
		cmd.setDeviceName(emuName);
		
		cmd.startActivity("com.android.settings/.wifi.WifiSettings");
		WaitManager.get().waitFor(TimeUnit.SECONDS, 6);
		
		if (cmd.isWifiEnable()) {
			cmd.keyEvent(AdbKeyEvent.KEYCODE_DPAD_CENTER);
			WaitManager.get().waitShortTime();
			
			}
		assertTrue(cmd.isWifiEnable()==false);
		cmd.forceStop("com.android.settings");
	}
	
	public static void enableCustomWifi(String adbExe, String emuName) {
		AdbCommandPrompt cmd = new AdbCommandPrompt(adbExe);
		cmd.setDeviceName(emuName);
		
		cmd.startActivity("com.android.settings/.wifi.WifiSettings");
//		cmd.startActivity("com.android.settings.AIRPLANE_MODE_SETTINGS");
		
		WaitManager.get().waitFor(TimeUnit.SECONDS, 6);
		
		if (cmd.isWifiEnable()==false) {
			cmd.keyEvent(AdbKeyEvent.KEYCODE_DPAD_CENTER);
			WaitManager.get().waitShortTime();
			
			}
		assertTrue(cmd.isWifiEnable()==true);
		cmd.forceStop("com.android.settings");
	}
	
	public static void disableWiFiEmulatorNotifiche(AndroidDriver<?> driver) {
		driver.openNotifications();
		WaitManager.get().waitMediumTime();
		
		String expandArrow = "//*[@resource-id='com.android.systemui:id/expand_indicator']";
		driver.findElement(By.xpath(expandArrow)).click();
		WaitManager.get().waitShortTime();
		
		String wifiLocator = "//*[@content-desc='Wi-Fi,Wifi signal full.,AndroidWifi']";
		WebElement wifiElem = driver.findElement(By.xpath(wifiLocator));
		wifiElem.click();
		WaitManager.get().waitMediumTime();

		String toggleLocator = "//*[@resource-id='android:id/toggle' and @class='android.widget.Switch']";
		WebElement toggleElem = driver.findElement(By.xpath(toggleLocator));
		if (toggleElem.getText().equals("ON")) {
			toggleElem.click();
		}
		WaitManager.get().waitMediumTime();

		assertTrue(driver.findElement(By.xpath(toggleLocator)).getAttribute("checked").equals("false"));
		
		driver.pressKey(new KeyEvent(AndroidKey.BACK));
		WaitManager.get().waitShortTime();
		driver.pressKey(new KeyEvent(AndroidKey.BACK));
		WaitManager.get().waitShortTime();
		driver.pressKey(new KeyEvent(AndroidKey.BACK));
		WaitManager.get().waitLongTime();
	}
	
	public static void enableWiFiEmulatorNotifiche(AndroidDriver<?> driver) {
		driver.openNotifications();
		WaitManager.get().waitMediumTime();
		
		String expandArrow = "//*[@resource-id='com.android.systemui:id/expand_indicator']";
		driver.findElement(By.xpath(expandArrow)).click();
		WaitManager.get().waitShortTime();
		
		WebElement wifiElem = null;
		String wifiLocator = "//*[@content-desc='Wi-Fi,Wifi signal full.,AndroidWifi']";
		try {
			wifiElem = driver.findElement(By.xpath(wifiLocator));
		} catch (Exception e) {
			// TODO: handle exception
		}
		if (wifiElem == null) {
			String wifiLocatorNew = "//*[@content-desc='Wi-Fi,']";
			wifiElem = driver.findElement(By.xpath(wifiLocatorNew));
		}
		
		wifiElem.click();
		WaitManager.get().waitMediumTime();
		
		String toggleLocator = "//*[@resource-id='android:id/toggle' and @class='android.widget.Switch']";
		WebElement toggleElem = driver.findElement(By.xpath(toggleLocator));
		
		if (toggleElem.getText().equals("OFF")) {
			toggleElem.click();
		}
		WaitManager.get().waitMediumTime();
		
		assertTrue(driver.findElement(By.xpath(toggleLocator)).getAttribute("checked").equals("true"));
		
		driver.pressKey(new KeyEvent(AndroidKey.BACK));
		WaitManager.get().waitShortTime();
		driver.pressKey(new KeyEvent(AndroidKey.BACK));
		WaitManager.get().waitShortTime();
		driver.pressKey(new KeyEvent(AndroidKey.BACK));
		WaitManager.get().waitLongTime();
	}
	
	
//	public void disableWiFi(WebDriver driver) {
//		ConnectionState state=driver.setConnection(new ConnectionStateBuilder()
//				.withWiFiDisabled()
//				.build());
//		assertFalse(state.isWiFiEnabled());
//		
//		WaitManager.get().waitLongTime();
//	}
//	
//	public void disableData(WebDriver driver) {
//		ConnectionState state=driver.setConnection(new ConnectionStateBuilder()
//				.withDataDisabled()
//				.build());
//		assertFalse(state.isDataEnabled());
//		
//		WaitManager.get().waitLongTime();
//	}
//	
//	public void disableAirMode(WebDriver driver) {
//		ConnectionState state=driver.setConnection(new ConnectionStateBuilder()
//				.withAirplaneModeDisabled()
//				.build());
//		assertFalse(state.isAirplaneModeEnabled());
//		
//		WaitManager.get().waitLongTime();
//	}
//	
//	public void enableWiFi(WebDriver driver) {
//		ConnectionState state=driver.setConnection(new ConnectionStateBuilder()
//				.withWiFiEnabled()
//				.build());
//		assertTrue(state.isWiFiEnabled());
//
//		WaitManager.get().waitLongTime();
//	}
//	
//	public void enableData(WebDriver driver) {
//		ConnectionState state=driver.setConnection(new ConnectionStateBuilder()
//				//				.withWiFiEnabled()
//				.withDataEnabled()
//				.build());
//		assertTrue(state.isDataEnabled());
//		
//		WaitManager.get().waitLongTime();
//	}
//	
//	public void enableAirMode(WebDriver driver) {
//		ConnectionState state=driver.setConnection(new ConnectionStateBuilder()
//				//				.withWiFiEnabled()
//				.withAirplaneModeEnabled()
//				.build());
//		assertTrue(state.isAirplaneModeEnabled());
//		WaitManager.get().waitLongTime();
//	}
//	
//	public void disableWiFiADB(String emuName) {
//		try {
//			Runtime.getRuntime().exec("adb -s "+emuName+" shell svc wifi disable");
//			WaitManager.get().waitMediumTime();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
//		WaitManager.get().waitLongTime();
//	}
//	
//	public void disableDataADB(String emuName) {
//		try {
//			Runtime.getRuntime().exec("adb -s "+emuName+" shell svc data disable");
//			WaitManager.get().waitMediumTime();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
//		WaitManager.get().waitLongTime();
//	}
//	
//	public void disableAirModeADB(String emuName) {
//		try {
//			Runtime.getRuntime().exec("adb -s "+emuName+" shell settings put global airplane_mode_on 0");
//			WaitManager.get().waitMediumTime();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
//		WaitManager.get().waitLongTime();
//	}
//	
//	public void enableWiFiADB(String emuName) {
//		try {
//			Runtime.getRuntime().exec("adb -s "+emuName+" shell svc wifi enable");
//			WaitManager.get().waitMediumTime();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
//		WaitManager.get().waitLongTime();
//	}
//	
//	public void enableDataADB(String emuName) {
//		try {
//			Runtime.getRuntime().exec("adb -s "+emuName+" shell svc data enable");
//			WaitManager.get().waitMediumTime();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
//		WaitManager.get().waitLongTime();
//	}
//	
//	public void enableAirModeADB(String emuName) {
//		try {
//			Runtime.getRuntime().exec("adb -s "+emuName+" shell settings put global airplane_mode_on 1");
//			WaitManager.get().waitMediumTime();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
//		WaitManager.get().waitLongTime();
//	}
	
	
	/*
	 *  ESEMPI VARI
	 */
//	AppiumDriver<?> d = (AppiumDriver<?>) TestEnvironment.get().getDriver(setting.getDriver());
	
	
//	NetworkConnection mobileDriver = (NetworkConnection) driver;
//	 if (mobileDriver.getNetworkConnection() != ConnectionType.AIRPLANE_MODE) {
//	   // enabling Airplane mode
//	   mobileDriver.setNetworkConnection(ConnectionType.AIRPLANE_MODE);
//	 }
//	ConnectionState state=driver.setConnection(new ConnectionStateBuilder().withWiFiDisabled().build());
	
}
