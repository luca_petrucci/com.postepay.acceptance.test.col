package com.postepay.acceptance.test.ui;

public class EnvironmentSettings 
{
	private String testIdJira;
	private String driver;
	private String site;
	private String language;
	private String scenarioId;
	private String testIdCheck;
	private String username;
	private String chromeDriver;
	
	public String getChromeDriver() {
		return chromeDriver;
	}
	public void setChromeDriver(String chromeDriver) {
		this.chromeDriver = chromeDriver;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getSite() {
		return site;
	}
	public void setSite(String site) {
		this.site = site;
	}
	public String getTestIdJira() {
		return testIdJira;
	}
	public void setTestIdJira(String testIdJira) {
		this.testIdJira = testIdJira;
	}
	public String getDriver() {
		return driver;
	}
	public void setDriver(String driver) {
		this.driver = driver;
	}
	public String getScenarioId() {
		return scenarioId;
	}
	public void setScenarioId(String scenarioId) {
		this.scenarioId = scenarioId;
	}
	public String getTestIdCheck() {
		return testIdCheck;
	}
	public void setTestIdCheck(String value) {
		this.testIdCheck = value;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String value) {
		this.username = value;
	}
	
}

