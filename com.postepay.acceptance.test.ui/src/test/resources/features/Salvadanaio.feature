Feature: Salvadanaio

  @PP20AND-809 @PPINPROAND-610 @PPINPROIOS-620
  Scenario Outline: Salvadanaio automatismi, impostare un accantonamento ricorrente
    Utente con un obiettivo non scaduto e senza automatismo, eseguire un accantonamento ricorrente partendo dalla schermata Versa sull'Obiettivo, l'inizio del funnel accantonamento

    # Obiettivi utilizzabili
    # Nome: Viaggio Amsterdam; Tipo: Viaggi; Scadenza 3 mesi, 50€, Ricorrente
    # Nome: Viaggio Andra tutto bene; Tipo: Salute e Benessere; Scadenza 3 mesi, 50€, Non Ricorrente
    Given App PP viene avviata
      | testIdJira    | driver   | site   | language   |
      | <testIdCheck> | <driver> | <site> | <language> |
    And Utente inserisce le credenziali nella login page
      | username   | password   | device   | testIdCheck   | discrepanza   | name   |
      | <username> | <password> | <device> | <testIdCheck> | <discrepanza> | <name> |
    And Utente clicca su accedi
    And Utente inserisce il posteId
      | username   | password   | device   | testIdCheck   | posteId   | name   |
      | <username> | <password> | <device> | <testIdCheck> | <posteId> | <name> |
    And Utente clicca sul bottone conferma
    When Utente sulla homepage esegue uno scroll fino alla card del Salvadanaio e clicca su INIZIA
      | discrepanza   |
      | <discrepanza> |
    And Utente clicca sull obiettivo di nome "Nome Obiettivo" e lo apre
      | nomeObiettivo   | discrepanza   |
      | <nomeObiettivo> | <discrepanza> |
    And Utente clicca su "Versa con PostePay Evolution" per accedere alla schermata Versa sull Obiettivo
      | isRicorrente   | nomeObiettivo   | discrepanza   | typeObiettivo   |
      | <isRicorrente> | <nomeObiettivo> | <discrepanza> | <typeObiettivo> |
    And Utente seleziona la tipologia di versamento
    And Utente verifica il layout della schermata Versa sull obiettivo prima di abilitare il versamento ricorrente
      | isRicorrente   | nomeObiettivo   | discrepanza   | typeObiettivo   |
      | <isRicorrente> | <nomeObiettivo> | <discrepanza> | <typeObiettivo> |
    And Utente abilita il toggle della ricorrenza e ne verifica il layout
      | nomeObiettivo   | discrepanza   |
      | <nomeObiettivo> | <discrepanza> |
    And Utente aumenta e diminuisce l importo con le icon piu e meno e verifica le modifiche di layout al raggiungimento
      | discrepanza   | importo   |
      | <discrepanza> | <importo> |
    And Utente inserisce l importo effettivo da versare
      | discrepanza   | importo   |
      | <discrepanza> | <importo> |
    And Utente verifica che le opzioni di frequenza per il versamento ricorrente siano presenti e ne seleziona una
      | frequenza   |
      | <frequenza> |
    And Utente clicca sul campo A partire da e seleziona una data di partenza per il versamento
      | startTime   |
      | <startTime> |
    And Utente con termine "Al raggiungimento" e clicca su CONTINUA
    And Utente seleziona il metodo di pagamento desiderato se possiede più di una Postepay Evolution
      | userNumberCards   | cardNumber   |
      | <userNumberCards> | <cardNumber> |
    And Utente atterra sulla schermata di riepilogo e ne verifica i campi con termine "Al raggiungimento"
      | nomeCognome   | cardNumber   | importo   | isRicorrente   |
      | <nomeCognome> | <cardNumber> | <importo> | <isRicorrente> |
    And Utente dalla schermata di riepilogo clicca su MODIFICA e atterra sulla pagina di Versa sull Obiettivo
    And Utente modifica il termine da "Al raggiungimento" a "Data specifica"
    And Utente verifica il layout e inserisce una data specifica come termine e poi clicca su CONTINUA
    And Utente atterra sulla schermata di riepilogo e ne verifica i campi con termine "Data specifica"
      | nomeCognome   | cardNumber   | importo   | isRicorrente   |
      | <nomeCognome> | <cardNumber> | <importo> | <isRicorrente> |
    And Utente clicca su AUTORIZZA
    Then Utente inserisce il posteId nel nuovo format e clicca su conferma
      | posteId   |
      | <posteId> |
    And Utente atterra sulla TYP del salvadanaio e clicca su CHIUDI
      | discrepanza   | typeTYP   |
      | <discrepanza> | <typeTYP> |
    And Utente torna alla schermata di Salvadanaio
    And Utente verifica che siano presenti le icona del CRONOMETRO e dei TRE Puntini  nella schermata di Salvadanaio e nel dettaglio dell obiettivo
      | importo   | discrepanza   | nomeObiettivo   |
      | <importo> | <discrepanza> | <nomeObiettivo> |
    And Utente accede alla sezione Bacheca poi Messaggi e verifica la presenza del messaggio "Conferma attivazione versamento ricorrente su Salvadanaio"
    And Utente accede alla sezione Bacheca poi Messaggi e verifica la presenza del messaggio "Conferma versamento ricorrente su Salvadanaio"
    And Utente chiude App PP

    @PPINPROAND-610_musella
    Examples: 
      | username             | password  | driver        | posteId | testIdCheck    | nomeCognome       | cardNumber | importo | frequenza   | startTime | startDate | endDateRaggiungimento | endDate    | userNumberCards | discrepanza | nomeObiettivo | isRicorrente | typeObiettivo | name      | typeTYP   |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | PPINPROAND-610 | Salvatore Musella | **** 7177  |    0,10 | ogni giorno | Non usato | Oggi      | Al raggiungimento     | 10/05/2020 |               2 |         0,0 | non toccare   | false        | viaggi        | Salvatore | creazione |

    @PPINPROIOS-620_fortunato
    Examples: 
      | username                | password        | driver               | posteId | testIdCheck    | nomeCognome        | cardNumber | importo | frequenza   | startTime | startDate | endDateRaggiungimento | endDate    | userNumberCards | discrepanza | nomeObiettivo | isRicorrente | typeObiettivo | name     | typeTYP   |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PPINPROIOS-620 | Vincenzo Fortunato | **** 4679  |    0,10 | ogni giorno | Non usato | Oggi      | Al raggiungimento     | 10/05/2020 |               2 |        0,00 | non toccare   | false        | viaggi        | Vincenzo | creazione |

    @PPINPROIOS-620_musella
    Examples: 
      | username             | password  | driver               | posteId | testIdCheck    | nomeCognome       | cardNumber | importo | frequenza   | startTime | startDate | endDateRaggiungimento | endDate    | userNumberCards | discrepanza | nomeObiettivo | isRicorrente | typeObiettivo | name      | typeTYP   |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | PPINPROIOS-620 | Salvatore Musella | **** 7177  |    0,10 | ogni giorno | Non usato | Oggi      | Al raggiungimento     | 10/05/2020 |               1 |         0,0 | non toccare   | false        | viaggi        | Salvatore | creazione |

  @PPINPROAND-549
  Scenario Outline: Utente actual con un obiettivo eseguire un accantonamento manuale da Versa sull obiettivo
    Utente actual con un obiettivo eseguire un accantonamento partendo dalla schermata Versa sull obiettivo  l inizio del funnel accantonamento Successivamente salvare  l accantonamento come operazione veloce

    # Obiettivi utilizzabili
    # Nome: Non toccare; Tipo: Viaggi; Scadenza 1 anno, 50€, Vercamento
    Given App PP viene avviata
      | testIdJira    | driver   | site   | language   |
      | <testIdCheck> | <driver> | <site> | <language> |
    When Utente inserisce le credenziali nella login page
      | username   | password   | device   | testIdCheck   | name   | discrepanza   |
      | <username> | <password> | <device> | <testIdCheck> | <name> | <discrepanza> |
    And Utente esegue la login in App
      | username   | password   | device   | testIdCheck   | name   | discrepanza   | posteId   |
      | <username> | <password> | <device> | <testIdCheck> | <name> | <discrepanza> | <posteId> |
    When Utente sulla homepage esegue uno scroll fino alla card del Salvadanaio e clicca su INIZIA
      | discrepanza   |
      | <discrepanza> |
    And Utente clicca sull obiettivo di nome "Nome Obiettivo" e lo apre
      | nomeObiettivo   | discrepanza   |
      | <nomeObiettivo> | <discrepanza> |
    And Utente clicca su "Versa con PostePay Evolution" per accedere alla schermata Versa sull Obiettivo
      | isRicorrente   | nomeObiettivo   | discrepanza   | typeObiettivo   |
      | <isRicorrente> | <nomeObiettivo> | <discrepanza> | <typeObiettivo> |
    And Utente seleziona la tipologia di versamento nel salvadanaio
      | tipoVersamento   |
      | <tipoVersamento> |
    And Utente inserisce l importo effettivo da versare
      | discrepanza   | importo   |
      | <discrepanza> | <importo> |
    And Utente conferma l operazione di versamento cliccando su Continua
    And Utente seleziona il metodo di pagamento desiderato se possiede più di una Postepay Evolution
      | userNumberCards   | cardNumber   |
      | <userNumberCards> | <cardNumber> |
    And Utente atterra sulla schermata di riepilogo e ne verifica i campi con termine "Nessuno"
      | nomeCognome   | cardNumber   | importo   | isRicorrente   |
      | <nomeCognome> | <cardNumber> | <importo> | <isRicorrente> |
    And Utente dalla schermata di riepilogo clicca su MODIFICA e atterra sulla pagina di Versa sull Obiettivo
    And Utente conferma l operazione di versamento cliccando su Continua
    And Utente seleziona il metodo di pagamento desiderato se possiede più di una Postepay Evolution
      | userNumberCards   | cardNumber   |
      | <userNumberCards> | <cardNumber> |
    And Utente atterra sulla schermata di riepilogo e ne verifica i campi con termine "Nessuno"
      | nomeCognome   | cardNumber   | importo   | isRicorrente   |
      | <nomeCognome> | <cardNumber> | <importo> | <isRicorrente> |
    And Utente clicca su AUTORIZZA
    Then Utente inserisce il posteId nel nuovo format e clicca su conferma
      | posteId   |
      | <posteId> |
    And Utente atterra sulla TYP del salvadanaio e clicca su CHIUDI
      | discrepanza   | typeTYP   |
      | <discrepanza> | <typeTYP> |
    And Utente clicca sul bottone <
    And Utente clicca sul bottone <
    And Utente clicca su PRODOTTI
    And Utente verifica che il versamento e presente nella LISTA MOVIMENTI
      | importo   |
      | <importo> |
    And Utente visualizza il dettaglio del versamento singolo
      | importo   | cardNumber   | libretto   |
      | <importo> | <cardNumber> | <libretto> |
    And Utente chiude App PP

    @PPINPROAND-549_musella
    Examples: 
      | username             | password  | driver               | posteId | testIdCheck    | nomeCognome       | cardNumber | importo | frequenza   | startTime | startDate | endDateRaggiungimento | endDate    | userNumberCards | discrepanza | nomeObiettivo | isRicorrente | typeObiettivo | name      | tipoVersamento | libretto | typeTYP     |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | PPINPROAND-549 | Salvatore Musella | **** 7177  |    0,10 | ogni giorno | Non usato | Oggi      | Al raggiungimento     | 10/05/2020 |               2 |         0,0 | non toccare   | false        | viaggi        | Salvatore | singolo        | *216     | lavorazione |

    @PPINPROAND-549_fortunato
    Examples: 
      | username                | password        | driver        | posteId | testIdCheck    | nomeCognome        | cardNumber | importo | frequenza   | startTime | startDate | endDateRaggiungimento | endDate    | userNumberCards | discrepanza | nomeObiettivo | isRicorrente | typeObiettivo | name     | tipoVersamento | libretto | typeTYP     |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PPINPROAND-549 | Vincenzo Fortunato | **** 4679  |    0,10 | ogni giorno | Non usato | Oggi      | Al raggiungimento     | 10/05/2020 |               2 |         0,0 | non toccare   | false        | viaggi        | Vincenzo | singolo        | *610     | lavorazione |
