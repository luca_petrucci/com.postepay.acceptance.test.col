@bacheca
Feature: Bacheca

  @PPINPROAND-388
  Scenario Outline: Corretta ricerca dei messaggi in Bacheca Messaggi
  Corretta ricerca dei messaggi in Bacheca Messaggi
  
    Given App PP viene avviata
    | testIdJira    | driver   | site   | language   |
    | <testIdCheck> | <driver> | <site> | <language> |
     When Utente inserisce le credenziali nella login page
    | username   | password   | device   | testIdCheck   | name   | discrepanza   |
    | <username> | <password> | <device> | <testIdCheck> | <name> | <discrepanza> |
    And Utente esegue la login in App
    | username   | password   | device   | testIdCheck   | name   | discrepanza   | posteId   | 
    | <username> | <password> | <device> | <testIdCheck> | <name> | <discrepanza> | <posteId> |
    And Utente clicca sull icona a campanello delle bacheca
    And Utente effettua una ricerca su piu parole random e verifica i risultati
    | listaTarget   |
    | <listaTarget> |
    Then Utente chiude App PP
        
    @PPINPROAND-388_fortunato
    Examples: 
    | username                | password        | driver        | posteId | testIdCheck    | language | site | name     | discrepanza | listaTarget           |
    | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PPINPROAND-388 | test     | site | Vincenzo |        0,0  | Messaggi |
       
    @PPINPROAND-388_musella
    Examples: 
    | username             | password  | driver              | posteId | testIdCheck    | language | site | name      | discrepanza | listaTarget         |
    | musellasal@gmail.com | Musabp67! | GalaxyS10Luca| prisma  | PPINPROAND-388 | test     | site | Salvatore |        0,0  | Messaggi |
        
        
   @PPINPROAND-2298
  Scenario Outline: Filtri per data in bacheca messaggi
  
    Given App PP viene avviata
    | testIdJira    | driver   | site   | language   |
    | <testIdCheck> | <driver> | <site> | <language> |
     When Utente inserisce le credenziali nella login page
    | username   | password   | device   | testIdCheck   | name   | discrepanza   |
    | <username> | <password> | <device> | <testIdCheck> | <name> | <discrepanza> |
    And Utente esegue la login in App
    | username   | password   | device   | testIdCheck   | name   | discrepanza   | posteId   | 
    | <username> | <password> | <device> | <testIdCheck> | <name> | <discrepanza> | <posteId> |
    And Utente clicca sull icona a campanello delle bacheca
    And Utente clicca su filtri e inserisce le date 
    Then Utente controlla che il filtro funziona correttamente
		And Utente inserisce date sbagliate e verifica il corretto popUp
    And Utente chiude App PP
        
    @PPINPROAND-2298_fortunato
    Examples: 
    | username                | password        | driver        | posteId | testIdCheck    | language | site | name     | discrepanza |
    | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PPINPROAND- | test     | site | Vincenzo |        0,0  | 
       
    @PPINPROAND-2298_musella
    Examples: 
    | username             | password  | driver              | posteId | testIdCheck    | language | site | name      | discrepanza |
    | musellasal@gmail.com | Musabp67! | GalaxyS10Luca| prisma  | PPINPROAND-388 | test     | site | Salvatore |        0,0  | 
    
    
    @PPINPROAND-3719
  	Scenario Outline: Messaggi in bacheca, ricerca per ricarica automatica (esito positivo)
    Nella sezione dedicata ai messaggi in bacheca dovrà essere prevista la possibilità di ricercare le ricariche ricorrenti (esito positivo)
    
    Given App PP viene avviata
      | testIdJira    | driver   | site   | language   |
      | <testIdCheck> | <driver> | <site> | <language> |
    When Utente inserisce le credenziali nella login page
      | username   | password   | device   | testIdCheck   | name   | discrepanza   |
      | <username> | <password> | <device> | <testIdCheck> | <name> | <discrepanza> |
    And Utente esegue la login in App
      | username   | password   | device   | testIdCheck   | name   | discrepanza   | posteId   |
      | <username> | <password> | <device> | <testIdCheck> | <name> | <discrepanza> | <posteId> |
    And Utente clicca sull icona a campanello delle bacheca
    And Utente inserisce la parola da cercare nel campo cerca
      | comment   |
      | <comment> |
    And Utente fa clic sul primo elemento della scheda messaggio di ricerca 
   	  | comment   |
      | <comment> |
    And Utente controlla elemento di ricerca nei dettagli del messaggio
    And Utente chiude App PP

    @PPINPROAND-3719_fortunato
    Examples: 
      | username                | password        | driver        | posteId | testIdCheck     | language | site | name     | discrepanza | comment           |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PPINPROAND-3719 | test     | site | Vincenzo |         0,0 | Conferma Sottoscrizione Ricarica Automatica |

    @PPINPROAND-3719_musella
    Examples: 
      | username             | password  | driver               | posteId | testIdCheck     | language | site | name      | discrepanza | comment           |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | PPINPROAND-3719 | test     | site | Salvatore |         0,0 | Conferma Sottoscrizione Ricarica Automatica |
      