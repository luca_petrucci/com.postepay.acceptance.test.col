Feature: Impostazioni


  Scenario Outline: PP20AND-51: Impostazioni - Gestisci il tuo codice PosteID - Cancella Codice PosteID


   Given App PP viene avviata
   | testIdJira   | driver   | site   | language   |
   | <testIdCheck> | <driver> | <site> | <language> |
  
   When  Utente inserisce le credenziali nella login page  
   | username | password | device | testIdCheck |
   | <username> | <password>  | <device> |<testIdCheck>|
   And  Utente clicca su accedi
   And  Utente inserisce il posteId
   | username | password | device | testIdCheck | posteId|
   | <username> | <password>  | <device> |<testIdCheck>|<posteId>|
   And  Utente clicca sul bottone conferma
   And  Utente sulla homepage apre hamburger menu
   When Utente clicca su impostazioni da hamburger menu
   And  Utente clicca gestisci il tuo codice PosteID
   And  Utente clicca su cancella codice PosteID 
   And  Utente clicca sul tasto prosegui
   And  Utente inserisce il codice ricevuto per sms e clicca su avanti
   
   Then  Utente atterra sulla "TnkP2p"
   |landingPage|
   |<landingPage>|
   And  Utente chiude App PP
  
   
   @PP20AND-51 @PPINPROAND-392

   Examples:
   | username               | password       | driver      | posteId | testIdCheck | cardNumber |targetUser  | importo | landingPage | language | site|
   | luca.petrucci-abmo		| NLWbnsx6#st0	 | GalaxyS8   | prisma  | PP20AND-51  | **** 4679   | vincenzo fortunato | 0.01       | TnkP2p | test | site | 
#   | vincenzo.fortunato-4869| D@niela22Lili10| GalaxyS8   | prisma  | PP20AND-51  | **** 4679  |Daniele Ioviero| 1       | TnkP2p | test | site | 
   
   
   