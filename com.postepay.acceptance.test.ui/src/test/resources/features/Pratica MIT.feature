@Pratica_MIT
Feature: Pratica MIT

  @BDDTEST-TAA-1079 @Functional @PPINPROAND-2915 @PPINPROIOS-704 @Pagamento @Pratica_MIT
  Scenario Outline: Pagamento Pratica MIT - Tariffario Nazionale - Utente con una sola carta
    Pagamento Pratica MIT - Tariffario Nazionale - Utente con una sola carta

    Given App PP viene avviata
      | testIdJira    | driver   | site   | language   |
      | <testIdCheck> | <driver> | <site> | <language> |
    When Utente inserisce le credenziali nella login page
      | username   | password   | device   | testIdCheck   | name   | discrepanza   |
      | <username> | <password> | <device> | <testIdCheck> | <name> | <discrepanza> |
    And Utente esegue la login in App
      | username   | password   | device   | testIdCheck   | name   | discrepanza   | posteId   |
      | <username> | <password> | <device> | <testIdCheck> | <name> | <discrepanza> | <posteId> |
    And Utente sulla homepage clicca sul tab PAGA
    And Utente clicca su PAGAMENTI
    And Utente clicca su Ministero dei Trasporti
    And Utente inserisce i dati della pratica MIT
    Then Utente clicca su CALCOLA della pratica MIT
    And Utente seleziona la carta da utilizzare per il pagamento su "targetOperation"
      | targetOperation   | cardNumber   | discrepanza   | userNumberCards   | name |
      | <targetOperation> | <cardNumber> | <discrepanza> | <userNumberCards> | <name> |
    And Utente verifica i dati di riepilogo MIT e clicca su PAGA
      | codiceFiscale   | cardNumber   |
      | <codiceFiscale> | <cardNumber> |
    And Utente inserisce il posteId durante la "targetOperation" e clicca conferma
      | posteId   | targetOperation   |
      | <posteId> | <targetOperation> |
    Then Utente atterra sulla "TnkMIT"
      | landingPage   |
      | <landingPage> |
    And Utente chiude App PP

    @PPINPROAND-2915_fortunato
    Examples: 
      | username                | password        | driver        | posteId | testIdCheck     | language | site | name     | discrepanza | userNumberCards | targetOperation | cardNumber | codiceFiscale    |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PPINPROAND-2915 | test     | site | Vincenzo |         0,0 |               2 | Ministero       | **** 4679  | FRTVCN95H10E205U |

    @PPINPROAND-2915_musella
    Examples: 
      | username             | password  | driver               | posteId | testIdCheck     | language | site | name      | discrepanza | userNumberCards | targetOperation | cardNumber | codiceFiscale    |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | PPINPROAND-2915 | test     | site | Salvatore |         0,0 |               1 | Ministero       | **** 7177  | MSLSVT59P03F839L |

 		@PPINPROAND-2915_sarno
    Examples: 
      | username             | password  | driver        | posteId | testIdCheck     | language | site | name    | discrepanza | userNumberCards | targetOperation | cardNumber | codiceFiscale    |
      | testACN_4@poste.it   | Password1!| GalaxyS10Luca | 123123  | PPINPROAND-2915 | test     | site | Fabiano |         0,0 |               1 | Ministero       | **** 1525  | SRNFBN02S05A020V |

  	@PPINPROAND-2915_romani
    Examples: 
      | username           | password  | driver        | posteId | testIdCheck     | language | site | name         | discrepanza | userNumberCards | targetOperation | cardNumber | codiceFiscale    |landingPage|
      | prisma20@poste.it  | Password2!| GalaxyS10Luca | 123123  | PPINPROAND-2915 | test     | site | Maria Grazia |         0,0 |               2 | Ministero       | **** 9007  | RMNMGR67T55H501C | TnkMIT |

    @PPINPROIOS-704_fortunato
    Examples: 
      | username                | password        | driver               | posteId | testIdCheck     | language | site | name     | discrepanza | userNumberCards | targetOperation | cardNumber | codiceFiscale    |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PPINPROIOS-704 | test     | site | Vincenzo |         0,0 |               2 | Ministero       | **** 4679  | FRTVCN95H10E205U |

    @PPINPROIOS-704_musella
    Examples: 
      | username             | password  | driver               | posteId | testIdCheck     | language | site | name      | discrepanza | userNumberCards | targetOperation | cardNumber | codiceFiscale    |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | PPINPROIOS-704  | test     | site | Salvatore |         0,0 |               1 | Ministero       | **** 7177  | MSLSVT59P03F839L |

 		@PPINPROIOS-704_rimato
    Examples: 
      | username            | password   | driver        | posteId | testIdCheck    | landingPage | language | site | name     | discrepanza | userNumberCards | targetOperation | cardNumber | codiceFiscale    | 
      | testacn_13@poste.it | Password1! | GalaxyS10Luca | 123123  | PPINPROIOS-367 | Homepage    | test     | site | Giuseppe |         0,0 |   1             | Ministero       | **** 1566  | MSLSVT59P03F839L |
      
    @PPINPROIOS-704_sarno
    Examples: 
      | username             | password  | driver        | posteId | testIdCheck     | language | site | name    | discrepanza | userNumberCards | targetOperation | cardNumber | codiceFiscale    |
      | testACN_4@poste.it   | Password1!| GalaxyS10Luca | 123123  | PPINPROIOS-704  | test     | site | Fabiano |         0,0 |               1 | Ministero       | **** 1525  | SRNFBN02S05A020V |
 
 		@PPINPROIOS-704_mancini
    Examples: 
      | username             | password  | driver        | posteId | testIdCheck     | language | site | name    | discrepanza | userNumberCards | targetOperation | cardNumber | codiceFiscale    | landingPage|
      | prisma21@poste.it    | Password1!| GalaxyS10Luca | 123123  | PPINPROIOS-704  | test     | site | Antonio |         0,0 |               2 | Ministero       | **** 9015  | MNCNTN61S08L407E | TnkMIT |
  
  @BDDTEST-TAA-1120 @Functional @PPINPROAND-2924 @PPINPROIOS-3264 @Ricerca @Pratica_MIT
  Scenario Outline: Verifica il funzionamento del campo Cerca Pratica - Tariffario Trento
    Verifica il funzionamento del campo Cerca Pratica - Tariffario Trento

    Given App PP viene avviata
      | testIdJira    | driver   | site   | language   |
      | <testIdCheck> | <driver> | <site> | <language> |
    When Utente inserisce le credenziali nella login page
      | username   | password   | device   | testIdCheck   | name   | discrepanza   |
      | <username> | <password> | <device> | <testIdCheck> | <name> | <discrepanza> |
    And Utente esegue la login in App
      | username   | password   | device   | testIdCheck   | name   | discrepanza   | posteId   |
      | <username> | <password> | <device> | <testIdCheck> | <name> | <discrepanza> | <posteId> |
    And Utente sulla homepage clicca sul tab PAGA
    And Utente clicca su PAGAMENTI
    And Utente clicca su Ministero dei Trasporti
    And Utente clicca su Trento
    And Utente inserisce i dati della pratica MIT
    Then Utente clicca su CALCOLA della pratica MIT
    And Utente seleziona la carta da utilizzare per il pagamento su "targetOperation"
      | targetOperation   | cardNumber   | discrepanza   | userNumberCards   | name |
      | <targetOperation> | <cardNumber> | <discrepanza> | <userNumberCards> | <name> |
    And Utente verifica i dati di riepilogo MIT e clicca su PAGA
      | codiceFiscale   | cardNumber   |
      | <codiceFiscale> | <cardNumber> |
    And Utente inserisce il posteId durante la "targetOperation" e clicca conferma
      | posteId | targetOperation   |
      | <posteId> | <targetOperation> |
    Then Utente atterra sulla "TnkMIT"
      | landingPage   |
      | <landingPage> |
    And Utente chiude App PP

    @PPINPROAND-2924_fortunato
    Examples: 
      | username                | password        | driver        | posteId | testIdCheck     | language | site | name     | discrepanza | userNumberCards | targetOperation | cardNumber | codiceFiscale    |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PPINPROAND-2924 | test     | site | Vincenzo |         0,0 |               2 | Ministero       | **** 4679  | FRTVCN95H10E205U |

    @PPINPROIOS-3264_fortunato
    Examples: 
      | username                | password        | driver               | posteId | testIdCheck     | language | site | name     | discrepanza | userNumberCards | targetOperation | cardNumber | codiceFiscale    |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PPINPROAND-3264 | test     | site | Vincenzo |         0,0 |               2 | Ministero       | **** 4679  | FRTVCN95H10E205U |

    @PPINPROAND-2924_musella
    Examples: 
      | username             | password  | driver               | posteId | testIdCheck     | language | site | name      | discrepanza | userNumberCards | targetOperation | cardNumber | codiceFiscale    |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | PPINPROAND-2924 | test     | site | Salvatore |         0,0 |               2 | Ministero       | **** 7177  | MSLSVT59P03F839L |

    @PPINPROAND-2924_sarno
    Examples: 
      | username             | password  | driver        | posteId | testIdCheck     | language | site | name    | discrepanza | userNumberCards | targetOperation | cardNumber | codiceFiscale    |
      | testACN_4@poste.it   | Password1!| GalaxyS10Luca | 123123  | PPINPROAND-2924 | test     | site | Fabiano |         0,0 |               1 | Ministero       | **** 1525  | SRNFBN02S05A020V |
    
    @PPINPROAND-2924_romani
    Examples: 
      | username           | password  | driver        | posteId | testIdCheck     | language | site | name         | discrepanza | userNumberCards | targetOperation | cardNumber | codiceFiscale    |landingPage|
      | prisma20@poste.it  | Password2!| GalaxyS10Luca | 123123  | PPINPROAND-2924 | test     | site | Maria Grazia |         0,0 |               2 | Ministero       | **** 9007  | RMNMGR67T55H501C | TnkMIT |
    
    @PPINPROIOS-3264_musella
    Examples: 
      | username             | password  | driver               | posteId | testIdCheck     | language | site | name      | discrepanza | userNumberCards | targetOperation | cardNumber | codiceFiscale    |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | PPINPROAND-3264 | test     | site | Salvatore |         0,0 |               2 | Ministero       | **** 7177  | MSLSVT59P03F839L |

 		@PPINPROIOS-3264_sarno
    Examples: 
      | username             | password  | driver        | posteId | testIdCheck      | language | site | name    | discrepanza | userNumberCards | targetOperation | cardNumber | codiceFiscale    |
      | testACN_4@poste.it   | Password1!| GalaxyS10Luca | 123123  | PPINPROIOS-3264  | test     | site | Fabiano |         0,0 |               1 | Ministero       | **** 1525  | SRNFBN02S05A020V |
 
 		@PPINPROIOS-3264_mancini
    Examples: 
      | username             | password  | driver        | posteId | testIdCheck      | language | site | name    | discrepanza | userNumberCards | targetOperation | cardNumber | codiceFiscale    | landingPage|
      | prisma21@poste.it    | Password1!| GalaxyS10Luca | 123123  | PPINPROIOS-3264  | test     | site | Antonio |         0,0 |               2 | Ministero       | **** 9015  | MNCNTN61S08L407E | TnkMIT |
      
  @BDDTEST-TAA-1121 @Functional @PPINPROAND-3299 @Pratica_MIT @PPINPROIOS-931
  Scenario Outline: Navigazione in app con sessione scaduta
    Navigazione in app con sessione scaduta

    Given App PP viene avviata
      | testIdJira    | driver   | site   | language   |
      | <testIdCheck> | <driver> | <site> | <language> |
    When Utente inserisce le credenziali nella login page
      | username   | password   | device   | testIdCheck   | name   | discrepanza   |
      | <username> | <password> | <device> | <testIdCheck> | <name> | <discrepanza> |
    And Utente esegue la login in App
      | username   | password   | device   | testIdCheck   | name   | discrepanza   | posteId   |
      | <username> | <password> | <device> | <testIdCheck> | <name> | <discrepanza> | <posteId> |
    And Utente sulla homepage clicca sul tab PAGA
    And Utente clicca su PAGAMENTI
    And Utente clicca su Ministero dei Trasporti
    And Utente aspetta che scade la sessione
    And Utente clicca su Trento
    And Utente inserisce i dati della pratica MIT
    Then Utente clicca su CALCOLA della pratica MIT
    And Utente seleziona la carta con cui pagare 
      | cardNumber   | discrepanza   | 
      | <cardNumber> | <discrepanza> | 
    Then Utente atterra sulla pagina del posteId dopo il resume
      | discrepanza   |
      | <discrepanza> |
    And Utente chiude App PP

    @PPINPROAND-3299_fortunato
    Examples: 
      | username                | password        | driver        | posteId | testIdCheck     | language | site | name     | discrepanza | userNumberCards | targetOperation | cardNumber | codiceFiscale    |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PPINPROAND-3299 | test     | site | Vincenzo |         0,0 |               2 | Ministero       | **** 4679  | FRTVCN95H10E205U |

    @PPINPROAND-3299_musella
    Examples: 
      | username             | password  | driver               | posteId | testIdCheck     | language | site | name      | discrepanza | userNumberCards | targetOperation | cardNumber | codiceFiscale    |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | PPINPROAND-3299 | test     | site | Salvatore |         0,0 |               2 | Ministero       | **** 7177  | MSLSVT59P03F839L |

    @PPINPROAND-3299_sarno
    Examples: 
      | username             | password  | driver        | posteId | testIdCheck     | language | site | name    | discrepanza | userNumberCards | targetOperation | cardNumber | codiceFiscale    |
      | testACN_4@poste.it   | Password1!| GalaxyS10Luca | 123123  | PPINPROAND-3299 | test     | site | Fabiano |         0,0 |               1 | Ministero       | **** 1525  | SRNFBN02S05A020V |
   
   @PPINPROAND-3299_romani
    Examples: 
      | username            | password  | driver        | posteId | testIdCheck     | language | site | name         | discrepanza | userNumberCards | targetOperation | cardNumber | codiceFiscale    |
      | prisma20@poste.it   | Password2!| GalaxyS10Luca | 123123  | PPINPROAND-3299 | test     | site | Maria Grazia |         0,0 |               1 | Ministero       | **** 9007  | RMNMGR67T55H501C |
   
   @PPINPROIOS-931_fortunato
    Examples: 
      | username                | password        | driver               | posteId | testIdCheck    | language | site | name     | discrepanza | userNumberCards | targetOperation | cardNumber | codiceFiscale    |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PPINPROIOS-931 | test     | site | Vincenzo |         0,0 |               2 | Ministero       | **** 4679  | FRTVCN95H10E205U |

    @PPINPROIOS-931_musella
    Examples: 
      | username             | password  | driver               | posteId | testIdCheck    | language | site | name      | discrepanza | userNumberCards | targetOperation | cardNumber | codiceFiscale    |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | PPINPROIOS-931 | test     | site | Salvatore |         0,0 |               2 | Ministero       | **** 7177  | MSLSVT59P03F839L |

    @PPINPROIOS-931_sarno
    Examples: 
      | username             | password  | driver        | posteId | testIdCheck     | language | site | name    | discrepanza | userNumberCards | targetOperation | cardNumber | codiceFiscale    |
      | testACN_4@poste.it   | Password1!| GalaxyS10Luca | 123123  | PPINPROIOS-931  | test     | site | Fabiano |         0,0 |               1 | Ministero       | **** 1525  | SRNFBN02S05A020V |
 
 		@PPINPROIOS-931_mancini
    Examples: 
      | username             | password  | driver        | posteId | testIdCheck     | language | site | name    | discrepanza | userNumberCards | targetOperation | cardNumber | codiceFiscale    |
      | prisma21@poste.it    | Password1!| GalaxyS10Luca | 123123  | PPINPROIOS-931  | test     | site | Antonio |         0,0 |               2 | Ministero       | **** 9015  | MNCNTN61S08L407E |