@rechargePostePay
Feature: Ricarica Altra Postepay

  @PP20AND-3 @PPINPROAND-354 @PPINPROIOS-365
  Scenario Outline: Ricarica Altra Postepay
    L'utente inserisce i dati della Postepay da ricaricare o da rubrica o manualmente ed i relativi dati obbligatori.

    Given App PP viene avviata
      | testIdJira    | driver   | site   | language   |
      | <testIdCheck> | <driver> | <site> | <language> |
    When Utente inserisce le credenziali nella login page
      | username   | password   | device   | testIdCheck   | discrepanza   | name   |
      | <username> | <password> | <device> | <testIdCheck> | <discrepanza> | <name> |
    And Utente clicca su accedi
    And Utente inserisce il posteId
      | username   | password   | device   | testIdCheck   | posteId   | name   |
      | <username> | <password> | <device> | <testIdCheck> | <posteId> | <name> |
    And Utente clicca sul bottone conferma
    And Utente sulla homepage clicca sul tab PAGA
    And Utente clicca sulla Card Ricarica Postepay
    And Utente clicca su Ricarica Altra Postepay
    And Utente seleziona la carta da utilizzare per il pagamento su "targetOperation"
      | targetOperation   | cardNumber   | discrepanza   | username   | name |
      | <targetOperation> | <cardNumber> | <discrepanza> | <username> | <name> |
    And Utente compila il form della dispositiva
      | cardNumberDestinatario   | targetUser   | importo   | comment   | discrepanza   | cardNumber   | name |
      | <cardNumberDestinatario> | <targetUser> | <importo> | <comment> | <discrepanza> | <cardNumber> |<name>|
    And Utente clicca su PROCEDI della "targetOperation"
      | targetOperation   |
      | <targetOperation> |
    #And Utente clicca su PROCEDI della "targetOperation"
     # | targetOperation   |
      #| <targetOperation> |
    And Utente verifica il riepilogo del "targetOperation"
      | targetOperation   | targetUser   | importo   | comment   | cardNumber   | cardNumberDestinatario   |
      | <targetOperation> | <targetUser> | <importo> | <comment> | <cardNumber> | <cardNumberDestinatario> |
    And Utente clicca su PAGA del "targetOperation"
      | targetOperation   |
      | <targetOperation> |
    And Utente inserisce il posteId durante la "targetOperation" e clicca conferma
      | posteId   | targetOperation   |
      | <posteId> | <targetOperation> |
    Then Utente atterra sulla "TnkRecharge"
      | landingPage   |
      | <landingPage> |
    And Utente chiude App PP

    @PPINPROAND-354_fortunato
    Examples: 
      | username                | password        | driver        | posteId | testIdCheck | cardNumber | targetUser        | importo | cardNumberDestinatario | comment                 | landingPage | site | language | targetOperation | discrepanza | name     |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PP20AND-354 | **** 4679  | Salvatore Musella |    0.01 |       5333171077147177 | ricaricaAltraPpStandard | TnkRecharge | test | site     | OtherPP         |         0,0 | Vincenzo |

    @PPINPROAND-354_musella
    Examples: 
      | username             | password  | driver               | posteId | testIdCheck | cardNumber | targetUser         | importo | cardNumberDestinatario | comment                 | landingPage | site | language | targetOperation | discrepanza | name      |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | PP20AND-3   | **** 7177  | Vincenzo Fortunato |    0.01 |       5333171077144679 | ricaricaAltraPpStandard | TnkRecharge | test | site     | OtherPP         |         0,0 | Salvatore |

    @PPINPROAND-354_muto
    Examples: 
      | username       | password    | driver        | posteId | testIdCheck    | cardNumber | targetUser         | importo | cardNumberDestinatario | comment                 | landingPage | site | language | targetOperation | discrepanza | name      |
      | francesco.muto | Prisma@2019 | GalaxyS10Luca | prisma  | PPINPROAND-354 | **** 4950  | Vincenzo Fortunato |    0.01 |       5333171077144679 | ricaricaAltraPpStandard | TnkRecharge | test | site     | OtherPP         |         0,0 | Francesco |

    @PPINPROAND-354_sarno
    Examples: 
      | username           | password   | driver        | posteId | cardNumber | cardNumberDestinatario | targetUser      | importo | comment                 | testIdCheck    | landingPage | language | site | name    | discrepanza | targetOperation | userNumberCards |
      | testACN_4@poste.it | Password1! | GalaxyS10Luca |  123123 | **** 1525  |       5333171000671566 | Giuseppe Rimato |    0.01 | ricaricaAltraPpStandard | PPINPROAND-354 | TnkRecharge | test     | site | Fabiano |         0,0 | OtherPP         |               1 |

    @PPINPROAND-354_romani
    Examples: 
      | username          | password   | driver        | posteId | cardNumber | cardNumberDestinatario | targetUser      | importo | comment                 | testIdCheck    | landingPage | language | site | name         | discrepanza | targetOperation | userNumberCards |
      | prisma20@poste.it | Password2! | GalaxyS10Luca |  123123 | **** 9007  |       5333171000679015 | Antonio Mancini |    1.02 | ricaricaAltraPpStandard | PPINPROAND-354 | TnkRecharge | test     | site | Maria Grazia |         0,0 | OtherPP         |               1 |

    @PPINPROIOS-365_fortunato
    Examples: 
      | username                | password        | driver               | posteId | testIdCheck    | cardNumber | targetUser        | importo | cardNumberDestinatario | comment                 | landingPage | site | language | targetOperation | discrepanza | name     |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PPINPROIOS-365 | **** 4679  | Salvatore Musella |    0.01 |       5333171077147177 | ricaricaAltraPpStandard | TnkRecharge | test | site     | OtherPP         |         0,0 | Vincenzo |

    @PPINPROIOS-365_sarno
    Examples: 
      | username           | password   | driver        | posteId | cardNumber | cardNumberDestinatario | targetUser      | importo | comment                 | testIdCheck    | landingPage | language | site | name    | discrepanza | targetOperation | userNumberCards |
      | testACN_4@poste.it | Password1! | GalaxyS10Luca |  123123 | **** 1525  |       5333171000671566 | Giuseppe Rimato |    0.01 | ricaricaAltraPpStandard | PPINPROIOS-365 | TnkRecharge | test     | site | Fabiano |         0,0 | OtherPP         |               1 |

    @PPINPROIOS-365_mancini
    Examples: 
      | username          | password   | driver        | posteId | cardNumber | cardNumberDestinatario | targetUser          | importo | comment                 | testIdCheck    | landingPage | language | site | name    | discrepanza | targetOperation | userNumberCards |
      | prisma21@poste.it | Password1! | GalaxyS10Luca |  123123 | **** 9015  |       5333171000679007 | Maria Grazia Romani |    0.01 | ricaricaAltraPpStandard | PPINPROIOS-365 | TnkRecharge | test     | site | Antonio |         0,0 | OtherPP         |               2 |

    @PPINPROAND-354_grecia
    Examples: 
      | username          | password   | driver        | posteId | cardNumber | cardNumberDestinatario | targetUser      | importo | comment                 | testIdCheck    | landingPage | language | site | name         | discrepanza | targetOperation | userNumberCards |
      | prisma66@poste.it | Password1! | GalaxyS10Luca |  123123 | **** 1341  |       5333171000679015 | Antonio Mancini |    1.02 | ricaricaAltraPpStandard | PPINPROAND-354 | TnkRecharge | test     | site | Agata        |         0,0 | OtherPP         |               2 |
  
    @PPINPROAND-354_marinello
    Examples: 
      | username          | password   | driver        | posteId | cardNumber | cardNumberDestinatario | targetUser      | importo | comment                 | testIdCheck    | landingPage | language | site | name         | discrepanza | targetOperation | userNumberCards |
      | prisma65@poste.it | Password1! | GalaxyS10Luca |  123123 | **** 8701  |       5333171000679015 | Antonio Mancini |    1.02 | ricaricaAltraPpStandard | PPINPROAND-354 | TnkRecharge | test     | site | Fiorella     |         0,0 | OtherPP         |               2 |
  
  @PP20AND-17 @PPINPROAND-368 @PPINPROIOS-379
  Scenario Outline: Ricarica la mia Postepay
    L'utente inserisce i dati della Postepay da ricaricare o da rubrica o manualmente ed i relativi dati obbligatori.

    Given App PP viene avviata
      | testIdJira    | driver   | site   | language   |
      | <testIdCheck> | <driver> | <site> | <language> |
    When Utente inserisce le credenziali nella login page
      | username   | password   | device   | testIdCheck   | discrepanza   | name   |
      | <username> | <password> | <device> | <testIdCheck> | <discrepanza> | <name> |
    And Utente clicca su accedi
    And Utente inserisce il posteId
      | username   | password   | device   | testIdCheck   | posteId   | name   |
      | <username> | <password> | <device> | <testIdCheck> | <posteId> | <name> |
    And Utente clicca sul bottone conferma
    And Utente sulla homepage clicca sul tab PAGA
    And Utente clicca sulla Card Ricarica Postepay
    And Utente clicca su Ricarica La mia Postepay
    And Utente seleziona la propria carta da ricaricare
      | targetOperation   | cardNumberDestinatario   | discrepanza   |
      | <targetOperation> | <cardNumberDestinatario> | <discrepanza> |
    And Utente compila il form della ricarica mia postepay
      | importo   | comment   |
      | <importo> | <comment> |
    And Utente clicca su PROCEDI della "targetOperation"
      | targetOperation   |
      | <targetOperation> |
    And Utente seleziona la carta da utilizzare per il pagamento su "targetOperation"
      | targetOperation   | cardNumber   | discrepanza   |  name |
      | <targetOperation> | <cardNumber> | <discrepanza> | <name> |
    And Utente verifica il riepilogo del "targetOperation"
      | targetOperation   | targetUser   | importo   | comment   | cardNumber   | cardNumberDestinatario   |
      | <targetOperation> | <targetUser> | <importo> | <comment> | <cardNumber> | <cardNumberDestinatario> |
    And Utente clicca su PAGA del "targetOperation"
      | targetOperation   |
      | <targetOperation> |
    And Utente inserisce il posteId durante la "targetOperation" e clicca conferma
      | posteId   | targetOperation   |
      | <posteId> | <targetOperation> |
    Then Utente atterra sulla "TnkRecharge"
      | landingPage   |
      | <landingPage> |
    And Utente chiude App PP

    @PPINPROAND-368_fortunato
    Examples: 
      | username                | password        | driver        | posteId | testIdCheck    | cardNumber | targetUser         | importo | cardNumberDestinatario | comment                | landingPage | site | language | targetOperation | discrepanza | name     |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PPINPROAND-368 | **** 4679  | Vincenzo Fortunato |    0.01 | **** 0395              | ricaricaLaMiaPpConnect | TnkRecharge | test | site     | MyPP            |         0,0 | Vincenzo |

	@PPINPROAND-368_romani
    Examples: 
      | username          | password  | driver        | posteId | testIdCheck    | cardNumber | targetUser          | importo | cardNumberDestinatario | comment                | landingPage | site | language | targetOperation | discrepanza | name     |
      | prisma20@poste.it | Password2! | GalaxyS10Luca | 123123  | PPINPROAND-368 | **** 9007  | Maria Grazia Romani |    2.51 | **** 8115              | ricaricaLaMiaPpConnect | TnkRecharge | test | site     | MyPP            |         0,0 | Maria Grazia |

    @PPINPROAND-368_muto
    Examples: 
      | username       | password    | driver        | posteId | testIdCheck    | cardNumber | targetUser         | importo | cardNumberDestinatario | comment                 | landingPage | site | language | targetOperation | discrepanza | name      |
      | francesco.muto | Prisma@2019 | GalaxyS10Luca | prisma  | PPINPROAND-368 | **** 4950  | Vincenzo Fortunato |    0.01 |       5333171077144679 | ricaricaAltraPpStandard | TnkRecharge | test | site     | OtherPP         |         0,0 | Francesco |

    @PPINPROIOS-379_fortunato
    Examples: 
      | username                | password        | driver               | posteId | testIdCheck    | cardNumber | targetUser         | importo | cardNumberDestinatario | comment                | landingPage | site | language | targetOperation | discrepanza | name     |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PPINPROIOS-379 | **** 4679  | Fortunato Vincenzo |    0.01 | **** 0395              | ricaricaLaMiaPpConnect | TnkRecharge | test | site     | MyPP            |         0,0 | Vincenzo |

    @PPINPROIOS-379_mancini
    Examples: 
      | username          | password   | driver        | posteId | cardNumber | cardNumberDestinatario | targetUser      | importo | comment                 | testIdCheck    | landingPage | language | site | name    | discrepanza | targetOperation | name    |
      | prisma21@poste.it | Password1! | GalaxyS10Luca |  123123 | **** 6226  | **** 9015              | MANCINI ANTONIO |    0.01 | ricaricaLaMiaPpStandard | PPINPROIOS-379 | TnkRecharge | test     | site | Antonio |         0,0 | MyPP            | Antonio |

  @PP20AND-73 @PPINPROAND-402
  Scenario Outline: Verifica che sia possibile cancellare le coordinate inserite da rubrica
    Verifica che sia possibile cancellare le coordinate

    Given App PP viene avviata
      | testIdJira    | driver   | site   | language   |
      | <testIdCheck> | <driver> | <site> | <language> |
    When Utente inserisce le credenziali nella login page
      | username   | password   | device   | testIdCheck   | discrepanza   | name   |
      | <username> | <password> | <device> | <testIdCheck> | <discrepanza> | <name> |
    And Utente clicca su accedi
    And Utente inserisce il posteId
      | username   | password   | device   | testIdCheck   | posteId   | name   |
      | <username> | <password> | <device> | <testIdCheck> | <posteId> | <name> |
    And Utente clicca sul bottone conferma
    And Utente sulla homepage clicca sul tab PAGA
    And Utente clicca su Ricarica Altra Postepay
    And Utente clicca sull icona RUBRICA
    And Utente seleziona il contatto per inviare il pagamento e salva
      | targetOperation   | targetUser   | cardNumberDestinatario   |
      | <targetOperation> | <targetUser> | <cardNumberDestinatario> |
    And Utente verifica che i campi NUMERO CARTA e INTESTATARIO si siano "popolati"
      | targetOperation   | targetUser   | cardNumberDestinatario   |
      | <targetOperation> | <targetUser> | <cardNumberDestinatario> |
    And Utente clicca sull icona RUBRICA
    And Utente verifica che i campi NUMERO CARTA e INTESTATARIO si siano "vuoti"
      | targetOperation   | targetUser   | cardNumberDestinatario   |
      | <targetOperation> | <targetUser> | <cardNumberDestinatario> |
    Then Utente chiude App PP

    @PPINPROAND-402_fortunato
    Examples: 
      | username                | password        | driver        | posteId | testIdCheck | cardNumber | targetUser    | importo | cardNumberDestinatario | comment                          | landingPage | site | language | targetOperation | name     | discrepanza |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PP20AND-73  | **** 4679  | Luca Petrucci |    0.01 |       4023600948770922 | ricaricaAltraPpCancelFromRubrica | TnkRecharge | test | site     | OtherPP         | Vincenzo |        0,75 |

    @PPINPROAND-402_musella
    Examples: 
      | username             | password  | driver               | posteId | testIdCheck | cardNumber | targetUser         | importo | cardNumberDestinatario | comment                          | landingPage | site | language | targetOperation | name      | discrepanza |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | PP20AND-73  | **** 7177  | Vincenzo Fortunato |    0.01 |       5333171077144679 | ricaricaAltraPpCancelFromRubrica | TnkRecharge | test | site     | OtherPP         | Salvatore |         0,0 |

  #   | vincenzo.fortunato-4869| D@niela22Lili10| GalaxyS8   | prisma  | PP20AND-73  |**** 4679   |Luca Petrucci| 0.01       | 4023600948770922  |  ricaricaAltraPpCancelFromRubrica   | TnkRecharge| test | site| OtherPP | Vincenzo | 0,75 |
  @PP20AND-18 @PPINPROAND-369
  Scenario Outline: Ricarica ricorrente per tempo da PP evo a PP evo di altro utente
    L'utente inserisce i dati della Postepay da ricaricare o da rubrica o manualmente ed i relativi dati obbligatori. L'utente effettua una ricarica ricorrente.

    Given App PP viene avviata
      | testIdJira    | driver   | site   | language   |
      | <testIdCheck> | <driver> | <site> | <language> |
    When Utente inserisce le credenziali nella login page
      | username   | password   | device   | testIdCheck   | discrepanza   | name   |
      | <username> | <password> | <device> | <testIdCheck> | <discrepanza> | <name< |
    And Utente clicca su accedi
    And Utente inserisce il posteId
      | username   | password   | device   | testIdCheck   | posteId   | name   |
      | <username> | <password> | <device> | <testIdCheck> | <posteId> | <name> |
    And Utente clicca sul bottone conferma
    And Utente sulla homepage clicca sul tab PAGA
    And Utente clicca sulla Card Ricarica Postepay
    And Utente clicca su Ricarica Postepay intestata ad altri
    And Utente compila il form della dispositiva
      | cardNumberDestinatario   | targetUser   | importo   | comment   | discrepanza   | cardNumber   | name |
      | <cardNumberDestinatario> | <targetUser> | <importo> | <comment> | <discrepanza> | <cardNumber> |<name>|
    And Utente clicca sul toggle Ricarica automatica
    And Utente compila il form ricarica automatica
      | targetOperation   | recurrencyRecharge   | recurrencyRechargeDate   | recurrencyRechargeName   | importo   |
      | <targetOperation> | <recurrencyRecharge> | <recurrencyRechargeDate> | <recurrencyRechargeName> | <importo> |
    And Utente clicca su PROCEDI della "targetOperation"
      | targetOperation   |
      | <targetOperation> |
    And Utente seleziona la carta da utilizzare per il pagamento su "targetOperation"
      | username   | targetOperation   | cardNumber   | discrepanza   | name |
      | <username> | <targetOperation> | <cardNumber> | <discrepanza> | <name> |
    And Utente verifica il riepilogo del "targetOperation"
      | targetOperation   | targetUser   | importo   | comment   | cardNumber   | cardNumberDestinatario   | recurrencyRechargeName   |
      | <targetOperation> | <targetUser> | <importo> | <comment> | <cardNumber> | <cardNumberDestinatario> | <recurrencyRechargeName> |
    And Utente clicca su PAGA del "targetOperation"
      | targetOperation   |
      | <targetOperation> |
    And Utente inserisce il posteId durante la "targetOperation" e clicca conferma
      | posteId   | targetOperation   |
      | <posteId> | <targetOperation> |
    Then Utente atterra sulla "TnkRechargeAutomatic"
      | landingPage   |
      | <landingPage> |
    And Utente chiude App PP

    @PPINPROAND-369_fortunato
    Examples: 
      | username                | password        | driver        | posteId | testIdCheck    | cardNumber | targetUser      | importo | cardNumberDestinatario | comment               | landingPage          | site | language | targetOperation  | recurrencyRecharge | recurrencyRechargeDate | recurrencyRechargeName | discrepanza | name     |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PPINPROAND-369 | **** 4679  | Antonio Nannola |    0.01 |       5333171077146740 | ricaricaAltraRicPpEvo | TnkRechargeAutomatic | test | site     | AutomaRechargePP | ogni settimana     | 11/08/2021             | CausaleEveryWeek       |         0,0 | Vincenzo |

    #   | gennaro.rega-1784     | Captur04$      | GalaxyS10Luca   | prisma  | PP20AND-18  | **** 2759 | Antonio Nannola | 0.01       | 5333171077146740  |  ricaricaAltraRicPpEvo   | TnkRechargeAutomatic| test | site| AutomaRechargePP | ogni settimana | 11/08/2020 | CausaleEveryWeek | 0,75 |
    #   | vincenzo.fortunato-4869| D@niela22Lili10| GalaxyS10Luca   | prisma  | PP20AND-18  |**** 4679   | Antonio Nannola | 0.01       | 5333171077146740  |  ricaricaAltraRicPpEvo   | TnkRechargeAutomatic| test | site| AutomaRechargePP | ogni settimana | 11/08/2020 | CausaleEveryWeek | 0,75 | Gennaro |
    #      | gennaro.rega-1784 | Captur04$ | GalaxyS10Luca | prisma  | PP20AND-18  | **** 2759  | Antonio Nannola |    0.01 |       5333171077146740 | ricaricaAltraRicPpEvo | TnkRechargeAutomatic | test | site     | AutomaRechargePP | ogni settimana     | 11/08/2020             | CausaleEveryWeek       |        0,75 | Gennaro |
    @PPINPROAND-369_musella
    Examples: 
      | username             | password  | driver               | posteId | testIdCheck    | cardNumber | targetUser      | importo | cardNumberDestinatario | comment               | landingPage          | site | language | targetOperation  | recurrencyRecharge | recurrencyRechargeDate | recurrencyRechargeName | discrepanza | name      |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | PPINPROAND-369 | **** 7177  | Antonio Nannola |    0.01 |       5333171077146740 | ricaricaAltraRicPpEvo | TnkRechargeAutomatic | test | site     | AutomaRechargePP | ogni settimana     | 11/08/2021             | CausaleEveryWeek       |         0,0 | Salvatore |

    @PPINPROAND-369_muto
    Examples: 
      | username       | password    | driver        | posteId | testIdCheck    | cardNumber | targetUser      | importo | cardNumberDestinatario | comment               | landingPage          | site | language | targetOperation  | recurrencyRecharge | recurrencyRechargeDate | recurrencyRechargeName | discrepanza | name      |
      | francesco.muto | Prisma@2019 | GalaxyS10Luca | prisma  | PPINPROAND-369 | **** 4950  | Antonio Nannola |    0.01 |       5333171077146740 | ricaricaAltraRicPpEvo | TnkRechargeAutomatic | test | site     | AutomaRechargePP | ogni settimana     | 11/08/2021             | CausaleEveryWeek       |         0,0 | Francesco |

  @PP20AND-19 @PPINPROAND-370
  Scenario Outline: Verifica la revoca di una ricarica ricorrente
    Verifica che sia possibile revocare una ricarica ricorrente.

    Given App PP viene avviata
      | testIdJira    | driver   | site   | language   |
      | <testIdCheck> | <driver> | <site> | <language> |
    When Utente inserisce le credenziali nella login page
      | username   | password   | device   | testIdCheck   | discrepanza   | name   |
      | <username> | <password> | <device> | <testIdCheck> | <discrepanza> | <name> |
    And Utente clicca su accedi
    And Utente inserisce il posteId
      | username   | password   | device   | testIdCheck   | posteId   | name   |
      | <username> | <password> | <device> | <testIdCheck> | <posteId> | <name> |
    And Utente clicca sul bottone conferma
    And Utente sulla homepage clicca sul tab PAGA
    And Utente clicca sulla card RICARICA IN AUTOMATICO
    And Utente effettua lo swipe sul movimento della ricarica ricorrente
      | cardNumber   | recurrencyRechargeName   |
      | <cardNumber> | <recurrencyRechargeName> |
    And Utente clicca su Cestino
    And Utente verifica la conformita del popup di "ConfirmDeleteAutoRecharge" e clicca su "OK"
    And Utente inserisce il posteId durante la "targetOperation" e clicca conferma
      | posteId   | targetOperation   |
      | <posteId> | <targetOperation> |
    Then Utente atterra sulla "firtRechargeAutomatic"
      | landingPage   |
      | <landingPage> |
    And Utente clicca sul tab HOME dalla schermata "PAGA"
    And Utente clicca sull icona dei messaggi
    And Utente cliccca sul tab dei messaggi e cerca "tipeMessage" e "dateOfOperation"
      | tipeMessage   | dateOfOperation   |
      | <tipeMessage> | <dateOfOperation> |
    Then E presente il messaggio di revoca della ricarica ricorrente precedentemente eliminata
    And Utente chiude App PP

    @PPINPROAND-370_fortunato
    Examples: 
      | username                | password        | driver        | posteId | testIdCheck    | cardNumber | targetUser      | importo | cardNumberDestinatario | comment                 | landingPage           | site | language | targetOperation        | recurrencyRecharge | recurrencyRechargeDate | recurrencyRechargeName | tipeMessage                         | dateOfOperation | name     | discrepanza |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PPINPROAND-370 | **** 4679  | Daniele Ioviero |    0.01 |       5333171089861187 | ricaricaAltraPpStandard | firtRechargeAutomatic | test | site     | DeleteAutomaRechargePP | ogni settimana     | 11/08/2021             | CausaleEveryWeek       | Conferma revoca Ricarica Automatica | toDay           | Vincenzo |         0,0 |

    #      | gennaro.rega-1784 | Captur04$ | GalaxyS10Luca | prisma  | PP20AND-19  | **** 2759  | Daniele Ioviero |    0.01 |       5333171089861187 | ricaricaAltraPpStandard | firtRechargeAutomatic | test | site     | DeleteAutomaRechargePP | ogni settimana     | 11/08/2020             | CausaleEveryWeek       | Conferma revoca Ricarica Automatica | toDay           | Gennaro |        0,75 |
    @PPINPROAND-370_musella
    Examples: 
      | username             | password  | driver               | posteId | testIdCheck | cardNumber | targetUser      | importo | cardNumberDestinatario | comment                 | landingPage           | site | language | targetOperation        | recurrencyRecharge | recurrencyRechargeDate | recurrencyRechargeName | tipeMessage                         | dateOfOperation | name      | discrepanza |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | PP20AND-19  | **** 7177  | Daniele Ioviero |    0.01 |       5333171089861187 | ricaricaAltraPpStandard | firtRechargeAutomatic | test | site     | DeleteAutomaRechargePP | ogni settimana     | 11/08/2020             | CausaleEveryWeek       | Conferma revoca Ricarica Automatica | toDay           | Salvatore |         0,0 |

  #   | vincenzo.fortunato-4869| D@niela22Lili10| GalaxyS8Plus   | prisma  | PP20AND-18  |**** 4679  |Daniele Ioviero| 0.01    | 5333171089861187     | ricaricaAltraPpStandard   | firtRechargeAutomatic| test | site     | DeleteAutomaRechargePP | ogni settimana | 11/04/2020 | CausaleEveryWeek | Conferma revoca Ricarica Automatica| toDay | Vincenzo | 0,75 |
  @L-1
  Scenario Outline: Cerca una ricarica ricorrente cancellata qualche giorni prima
    Given App PP viene avviata
      | testIdJira    | driver   | site   | language   |
      | <testIdCheck> | <driver> | <site> | <language> |
    When Utente inserisce le credenziali nella login page
      | username   | password   | device   | testIdCheck   | discrepanza   | name   |
      | <username> | <password> | <device> | <testIdCheck> | <discrepanza> | <name> |
    And Utente clicca su accedi
    And Utente inserisce il posteId
      | username   | password   | device   | testIdCheck   | posteId   | name   |
      | <username> | <password> | <device> | <testIdCheck> | <posteId> | <name> |
    And Utente clicca sul bottone conferma
    And Utente clicca sull icona dei messaggi
    And Utente cliccca sul tab dei messaggi e cerca "tipeMessage" e "dateOfOperation"
      | tipeMessage   | dateOfOperation   |
      | <tipeMessage> | <dateOfOperation> |
    Then E presente il messaggio di revoca della ricarica ricorrente precedentemente eliminata
    And Utente chiude App PP

    Examples: 
      | username                | password        | driver               | posteId | testIdCheck | cardNumber | targetUser      | importo | cardNumberDestinatario | comment                 | landingPage           | site | language | targetOperation  | recurrencyRecharge | recurrencyRechargeDate | recurrencyRechargeName | tipeMessage                         | dateOfOperation | name     | discrepanza |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | L-1         | **** 4679  | Daniele Ioviero |    0.01 |       5333171089861187 | ricaricaAltraPpStandard | firtRechargeAutomatic | test | site     | AutomaRechargePP | ogni settimana     | 11/04/2020             | CausaleEveryWeek       | Conferma revoca Ricarica Automatica | 01-apr-2020     | Vincenzo |        0,75 |

  @PP20AND-431 @PPINPROAND-542
  Scenario Outline: Verificare in Ricarica PP il prodotto Conto Bancoposta porti alApp BP - App BP installata
    Verificare che quando si effettua una ricarica PP si visualizza la voce Conto Bancoposta nella sezione Paga Con.

    Given App PP viene avviata
      | testIdJira    | driver   | site   | language   |
      | <testIdCheck> | <driver> | <site> | <language> |
    When Utente inserisce le credenziali nella login page
      | username   | password   | device   | testIdCheck   | discrepanza   | name   |
      | <username> | <password> | <device> | <testIdCheck> | <discrepanza> | <name> |
    And Utente clicca su accedi
    And Utente inserisce il posteId
      | username   | password   | device   | testIdCheck   | posteId   | name   |
      | <username> | <password> | <device> | <testIdCheck> | <posteId> | <name> |
    And Utente clicca sul bottone conferma
    And Utente sulla homepage clicca sul tab PAGA
    And Utente clicca su Ricarica Altra Postepay
    And Utente compila il form della dispositiva
      | cardNumberDestinatario   | targetUser   | importo   | comment   | discrepanza   | cardNumber   | name |
      | <cardNumberDestinatario> | <targetUser> | <importo> | <comment> | <discrepanza> | <cardNumber> |<name>|
    And Utente clicca su PROCEDI della "targetOperation"
      | targetOperation   |
      | <targetOperation> |
    And Utente clicca sul link BANCOPOSTA
    Then Utente chiude App PP

    @PPINPROAND-542_fortunato
    Examples: 
      | username                | password        | driver               | posteId | testIdCheck | cardNumber | targetUser    | importo | cardNumberDestinatario | comment                 | landingPage | site | language | targetOperation | name     | discrepanza |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PP20AND-431 | **** 4679  | Luca Petrucci |    0.01 |       4023600948770922 | ricaricaAltraPpStandard | TnkRecharge | test | site     | OtherPP         | Vincenzo |        0,75 |

    @PPINPROAND-542_musella
    Examples: 
      | username             | password  | driver               | posteId | testIdCheck | cardNumber | targetUser         | importo | cardNumberDestinatario | comment                 | landingPage | site | language | targetOperation | name      | discrepanza |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | PP20AND-431 | **** 7177  | Vincenzo Fortunato |    0.01 |       5333171077144679 | ricaricaAltraPpStandard | TnkRecharge | test | site     | OtherPP         | Salvatore |         0,0 |

  @PPINPROAND-594
  Scenario Outline: Revoca ricarica automatica per soglia
     Revoca ricarica automatica per soglia

    Given App PP viene avviata
      | testIdJira    | driver   | site   | language   |
      | <testIdCheck> | <driver> | <site> | <language> |
    When Utente inserisce le credenziali nella login page
      | username   | password   | device   | testIdCheck   | discrepanza   | name   |
      | <username> | <password> | <device> | <testIdCheck> | <discrepanza> | <name< |
    And Utente clicca su accedi
    And Utente inserisce il posteId
      | username   | password   | device   | testIdCheck   | posteId   | name   |
      | <username> | <password> | <device> | <testIdCheck> | <posteId> | <name> |
    And Utente clicca sul bottone conferma
    And Utente sulla homepage clicca sul tab PAGA
    And Utente clicca sulla Card Ricarica Postepay
    And Utente clicca su Ricarica Postepay intestata ad altri
    And Utente clicca sull icona RUBRICA
    And Utente seleziona il contatto per inviare il pagamento e salva
      | targetOperation   | targetUser   | cardNumberDestinatario   |
      | <targetOperation> | <targetUser> | <cardNumberDestinatario> |
    And Utente completa il form della dispositiva inserendo importo e causale
      | cardNumberDestinatario   | targetUser   | importo   | comment   |
      | <cardNumberDestinatario> | <targetUser> | <importo> | <comment> |
    And Utente clicca sul toggle Ricarica automatica
    And Utente compila il form ricarica automatica
      | targetOperation   | recurrencyRecharge   | recurrencyRechargeDate   | recurrencyRechargeName   | importo   |
      | <targetOperation> | <recurrencyRecharge> | <recurrencyRechargeDate> | <recurrencyRechargeName> | <importo> |
    And Utente clicca su PROCEDI della "targetOperation"
      | targetOperation   |
      | <targetOperation> |
    And Utente seleziona la carta da utilizzare per il pagamento su "targetOperation"
      | username   | targetOperation   | cardNumber   | discrepanza   | name |
      | <username> | <targetOperation> | <cardNumber> | <discrepanza> | <name> |
    And Utente verifica il riepilogo del "targetOperation"
      | targetOperation   | targetUser   | importo   | comment   | cardNumber   | cardNumberDestinatario   | recurrencyRechargeName   |
      | <targetOperation> | <targetUser> | <importo> | <comment> | <cardNumber> | <cardNumberDestinatario> | <recurrencyRechargeName> |
    And Utente clicca su PAGA del "targetOperation"
      | targetOperation   |
      | <targetOperation> |
    And Utente inserisce il posteId durante la "targetOperation" e clicca conferma
      | posteId   | targetOperation   |
      | <posteId> | <targetOperation> |
    And Utente atterra sulla "TnkRechargeAutomatic"
      | landingPage   |
      | <landingPage> |
    Then Utente clicca sulla card RICARICA IN AUTOMATICO
    And Utente effettua lo swipe sul movimento della ricarica ricorrente
      | cardNumber   | recurrencyRechargeName   |
      | <cardNumber> | <recurrencyRechargeName> |
    And Utente clicca su Cestino
    And Utente verifica la conformita del popup di "ConfirmDeleteAutoRecharge" e clicca su "OK"
    And Utente inserisce il posteId durante la "targetOperation" e clicca conferma
      | posteId   | targetOperation   |
      | <posteId> | <targetOperation> |
    And Utente atterra sulla "firtRechargeAutomatic"
      | landingPage   |
      | <landingPage> |
    And Utente chiude App PP

    @PPINPROAND-594_fortunato
    Examples: 
      | username                | password        | driver        | posteId | testIdCheck    | cardNumber | targetUser         | importo | cardNumberDestinatario | comment                  | landingPage           | site | language | targetOperation      | recurrencyRecharge | recurrencyRechargeDate | recurrencyRechargeName | tipeMessage                         | dateOfOperation | name     | discrepanza | rechargeType |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PPINPROAND-594 | **** 4679  | Vincenzo Fortunato |    0.01 | **** 0395              | RicaricaAutomaticaSoglia | firtRechargeAutomatic | test | site     | AutomaRechargeSoglia | ogni settimana     | 11/08/2021             | CausaleSoglia          | Conferma revoca Ricarica Automatica | toDay           | Vincenzo |         0,0 | soglia       |

  #      | gennaro.rega-1784 | Captur04$ | GalaxyS10Luca | prisma  | PP20AND-19  | **** 2759  | Daniele Ioviero |    0.01 |       5333171089861187 | ricaricaAltraPpStandard | firtRechargeAutomatic | test | site     | DeleteAutomaRechargePP | ogni settimana     | 11/08/2020             | CausaleEveryWeek       | Conferma revoca Ricarica Automatica | toDay           | Gennaro |        0,75 |
  @PPINPROAND-593
  Scenario Outline: Ricarica automatica a soglia da pp evolution a pp evolution mia
     Ricarica automatica a soglia da pp evolution a pp evolution mia

    Given App PP viene avviata
      | testIdJira    | driver   | site   | language   |
      | <testIdCheck> | <driver> | <site> | <language> |
    When Utente inserisce le credenziali nella login page
      | username   | password   | device   | testIdCheck   | discrepanza   | name   |
      | <username> | <password> | <device> | <testIdCheck> | <discrepanza> | <name< |
    And Utente clicca su accedi
    And Utente inserisce il posteId
      | username   | password   | device   | testIdCheck   | posteId   | name   |
      | <username> | <password> | <device> | <testIdCheck> | <posteId> | <name> |
    And Utente clicca sul bottone conferma
    And Utente sulla homepage clicca sul tab PAGA
    And Utente clicca sulla Card Ricarica Postepay
    And Utente clicca su Ricarica Postepay intestata ad altri
    And Utente clicca sull icona RUBRICA
    And Utente seleziona il contatto per inviare il pagamento e salva
      | targetOperation   | targetUser   | cardNumberDestinatario   |
      | <targetOperation> | <targetUser> | <cardNumberDestinatario> |
    And Utente completa il form della dispositiva inserendo importo e causale
      | cardNumberDestinatario   | targetUser   | importo   | comment   |
      | <cardNumberDestinatario> | <targetUser> | <importo> | <comment> |
    And Utente clicca sul toggle Ricarica automatica
    And Utente compila il form ricarica automatica
      | targetOperation   | recurrencyRecharge   | recurrencyRechargeDate   | recurrencyRechargeName   | importo   |
      | <targetOperation> | <recurrencyRecharge> | <recurrencyRechargeDate> | <recurrencyRechargeName> | <importo> |
    And Utente clicca su PROCEDI della "targetOperation"
      | targetOperation   |
      | <targetOperation> |
    And Utente seleziona la carta da utilizzare per il pagamento su "targetOperation"
      | username   | targetOperation   | cardNumber   | discrepanza   | name |
      | <username> | <targetOperation> | <cardNumber> | <discrepanza> | <name> |
    And Utente verifica il riepilogo del "targetOperation"
      | targetOperation   | targetUser   | importo   | comment   | cardNumber   | cardNumberDestinatario   | recurrencyRechargeName   |
      | <targetOperation> | <targetUser> | <importo> | <comment> | <cardNumber> | <cardNumberDestinatario> | <recurrencyRechargeName> |
    And Utente clicca su PAGA del "targetOperation"
      | targetOperation   |
      | <targetOperation> |
    And Utente inserisce il posteId durante la "targetOperation" e clicca conferma
      | posteId   | targetOperation   |
      | <posteId> | <targetOperation> |
    And Utente atterra sulla "TnkRechargeAutomatic"
      | landingPage   |
      | <landingPage> |
    And Utente chiude App PP

    @PPINPROAND-593_fortunato
    Examples: 
      | username                | password        | driver        | posteId | testIdCheck    | cardNumber | targetUser         | importo | cardNumberDestinatario | comment                  | landingPage           | site | language | targetOperation      | recurrencyRecharge | recurrencyRechargeDate | recurrencyRechargeName | tipeMessage                         | dateOfOperation | name     | discrepanza | rechargeType |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PPINPROAND-593 | **** 4679  | Vincenzo Fortunato |    0.01 | **** 0395              | RicaricaAutomaticaSoglia | firtRechargeAutomatic | test | site     | AutomaRechargeSoglia | ogni settimana     | 11/08/2021             | CausaleSoglia          | Conferma revoca Ricarica Automatica | toDay           | Vincenzo |         0,0 | soglia       |

  @PPINPROAND-4216
  Scenario Outline: Ricarica Altra Postepay - Inserimento manuale
    Ricarica altra Postepay, inserimento manuale

    Given App PP viene avviata
      | testIdJira    | driver   | site   | language   |
      | <testIdCheck> | <driver> | <site> | <language> |
    When Utente inserisce le credenziali nella login page
      | username   | password   | device   | testIdCheck   | discrepanza   | name   |
      | <username> | <password> | <device> | <testIdCheck> | <discrepanza> | <name> |
    And Utente clicca su accedi
    And Utente inserisce il posteId
      | username   | password   | device   | testIdCheck   | posteId   | name   |
      | <username> | <password> | <device> | <testIdCheck> | <posteId> | <name> |
    And Utente clicca sul bottone conferma
    And Utente sulla homepage clicca sul tab PAGA
    And Utente clicca sulla Card Ricarica Postepay
    And Utente clicca su Ricarica Altra Postepay
      And Utente seleziona la carta da utilizzare per il pagamento su "targetOperation"
      | targetOperation   | cardNumber   | discrepanza   | username   | name |
      | <targetOperation> | <cardNumber> | <discrepanza> | <username> | <name> |
    And Utente compila il form della dispositiva
      | cardNumberDestinatario   | targetUser   | importo   | comment   | discrepanza   | cardNumber   | name |
      | <cardNumberDestinatario> | <targetUser> | <importo> | <comment> | <discrepanza> | <cardNumber> |<name>|
    And Utente clicca su PROCEDI della "targetOperation"
      | targetOperation   |
      | <targetOperation> |
    #And Utente clicca su PROCEDI della "targetOperation"
      #| targetOperation   |
      #| <targetOperation> |
    And Utente verifica il riepilogo del "targetOperation"
      | targetOperation   | targetUser   | importo   | comment   | cardNumber   | cardNumberDestinatario   |
      | <targetOperation> | <targetUser> | <importo> | <comment> | <cardNumber> | <cardNumberDestinatario> |
    And Utente clicca su PAGA del "targetOperation"
      | targetOperation   |
      | <targetOperation> |
    And Utente inserisce il posteId durante la "targetOperation" e clicca conferma
      | posteId   | targetOperation   |
      | <posteId> | <targetOperation> |
    Then Utente atterra sulla "TnkRecharge"
      | landingPage   |
      | <landingPage> |
    And Utente clicca sul tab HOME dalla schermata "PAGA"
    #    And Utente clicca sull icona dei messaggi
    #    And Utente cliccca sul tab dei messaggi e cerca "tipeMessage" e "dateOfOperation"
    #     | tipeMessage   | dateOfOperation   |
    #     | <tipeMessage> | <dateOfOperation> |
    #    And Utente clicca sul bottone <
    #    And Utente sulla homepage clicca sulla card della Carta
    #    Then Utente verifica i movimenti in uscita relativi alla ricarica (importo e commissione)
    #      | importo   | cardNumber   |
    #      | <importo> | <cardNumber> |
    And Utente chiude App PP

    @PPINPROAND-4216_fortunato
    Examples: 
      | username                | password        | driver        | posteId | testIdCheck     | cardNumber | targetUser        | importo | cardNumberDestinatario | comment                 | landingPage | site | language | targetOperation | discrepanza | name     | tipeMessage                    | dateOfOperation |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PPINPROAND-4216 | **** 4679  | Salvatore Musella |    0,01 |       5333171077147177 | ricaricaAltraPpStandard | TnkRecharge | test | site     | OtherPP         |         0,0 | Vincenzo | Ricarica Carta Postepay da App | toDay           |

    @PPINPROAND-4216_musella
    Examples: 
      | username             | password  | driver               | posteId | testIdCheck     | cardNumber | targetUser         | importo | cardNumberDestinatario | comment                 | landingPage | site | language | targetOperation | discrepanza | name      | tipeMessage                    | dateOfOperation |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | PPINPROAND-4216 | **** 7177  | Vincenzo Fortunato |    0,01 |       5333171077144679 | ricaricaAltraPpStandard | TnkRecharge | test | site     | OtherPP         |         0,0 | Salvatore | Ricarica Carta Postepay da App | toDay           |

    @PPINPROAND-4216_sarno
    Examples: 
      | username           | password   | driver        | posteId | testIdCheck     | cardNumber | targetUser      | importo | cardNumberDestinatario | comment                 | landingPage | site | language | targetOperation | discrepanza | name    | tipeMessage                    | dateOfOperation |
      | testACN_4@poste.it | Password1! | GalaxyS10Luca |  123123 | PPINPROAND-4216 | **** 1525  | Giuseppe Rimato |    0,01 |       5333171000671566 | ricaricaAltraPpStandard | TnkRecharge | test | site     | OtherPP         |         0,0 | Fabiano | Ricarica Carta Postepay da App | toDay           |

    @PPINPROAND-4216_romani
    Examples: 
      | username          | password   | driver        | posteId | testIdCheck     | cardNumber | targetUser      | importo | cardNumberDestinatario | comment                 | landingPage | site | language | targetOperation | discrepanza | name         | tipeMessage                    | dateOfOperation |
      | prisma20@poste.it | Password2! | GalaxyS10Luca |  123123 | PPINPROAND-4216 | **** 9007  | Antonio Mancini |    2,03 |       5333171000679015 | ricaricaAltraPpStandard | TnkRecharge | test | site     | OtherPP         |         0,0 | Maria Grazia | Ricarica Carta Postepay da App | toDay           |
   
    @PPINPROAND-4216_grecia
    Examples: 
      | username          | password   | driver        | posteId | testIdCheck     | cardNumber | targetUser      | importo | cardNumberDestinatario | comment                 | landingPage | site | language | targetOperation | discrepanza | name         | tipeMessage                    | dateOfOperation |
      | prisma66@poste.it | Password1! | GalaxyS10Luca |  123123 | PPINPROAND-4216 | **** 1341  | Agata Grecia    |    2,03 |       5333171000679015 | ricaricaAltraPpStandard | TnkRecharge | test | site     | OtherPP         |         0,0 | Agata        | Ricarica Carta Postepay da App | toDay           |
      