@Carte
Feature: Carte

  @PPINPROAND-568
  Scenario Outline: Verifica l'attivazione di Gpay tramite il toggle - indirizzo inserito
    L'utente attiva GPAY da dettaglio carta con indirizzo inserito precedentemente

    Given App PP viene avviata
      | testIdJira    | driver   | site   | language   |
      | <testIdCheck> | <driver> | <site> | <language> |
    When Utente inserisce le credenziali nella login page
      | username   | password   | device   | testIdCheck   | name   | discrepanza   |
      | <username> | <password> | <device> | <testIdCheck> | <name> | <discrepanza> |
    And Utente esegue la login in App
      | username   | password   | device   | testIdCheck   | name   | discrepanza   | posteId   |
      | <username> | <password> | <device> | <testIdCheck> | <name> | <discrepanza> | <posteId> |
    And Utente sulla homepage clicca sulla card della Carta
    And Utente clicca sulla card della Carta dalla sezione PRODOTTI
      | username   |
      | <username> |
    And Utente dalla pagina di IMPOSTAZIONI CARTA esegue uno scroll fino a "GOOGLEPAY"
    And Utente attiva Google Pay
    And Utente accetta il CONSENSO PRIVACY
    And Utente inserisce il posteId durante la "targetOperation" e clicca conferma
      | posteId   | targetOperation   |
      | <posteId> | <targetOperation> |
    And Utente procede con l attivazione di Google Pay
    Then Il toggle di Google Pay risulta attivo
    And Utente disattiva Google Pay
    And Utente inserisce il posteId durante la "targetOperation" e clicca conferma
      | posteId   | targetOperation   |
      | <posteId> | <targetOperation> |
    And Il toggle di Google Pay risulta disattivato correttamente
    And Utente chiude App PP

    @PPINPROAND-568_fortunato
    Examples: 
      | username                | password        | driver        | posteId | testIdCheck    | language | site | name     | discrepanza | targetOperation |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PPINPROAND-568 | test     | site | Vincenzo |         0,0 | GPay            |

    @PPINPROAND-568_musella
    Examples: 
      | username             | password  | driver        | posteId | testIdCheck    | language | site | name     | discrepanza | targetOperation |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | PPINPROAND-568 | test     | site | Vincenzo |         0,0 | GPay            |
