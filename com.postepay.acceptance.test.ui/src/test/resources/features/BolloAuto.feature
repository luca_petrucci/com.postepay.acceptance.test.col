@BolloAuto
Feature: Bollo Auto

  @PPINPROAND-602
  Scenario Outline: Pagamento Bollo Auto - Bollo auto da pagare
    Eseguire tutte le operazioni di preparazione al pagamento del bollo auto

    Given App PP viene avviata
      | testIdJira    | driver   | site   | language   |
      | <testIdCheck> | <driver> | <site> | <language> |
    When Utente inserisce le credenziali nella login page
      | username   | password   | device   | testIdCheck   | name   | discrepanza   |
      | <username> | <password> | <device> | <testIdCheck> | <name> | <discrepanza> |
    And Utente esegue la login in App
      | username   | password   | device   | testIdCheck   | name   | discrepanza   | posteId   |
      | <username> | <password> | <device> | <testIdCheck> | <name> | <discrepanza> | <posteId> |
    And Utente sulla homepage clicca sul tab PAGA
    And Utente clicca su PAGAMENTI
    And Utente clicca su Bollo Auto e Moto
    And Utente seleziona la carta da utilizzare per il pagamento su "targetOperation"
      | targetOperation   | cardNumber   | discrepanza   | userNumberCards   |  name |
      | <targetOperation> | <cardNumber> | <discrepanza> | <userNumberCards> | <name> |
    And Utente verifica il layout della pagina Bollo Auto e Moto e compila i campi
      | regione   | targa   | veicolo   |
      | <regione> | <targa> | <veicolo> |
    And Utente clicca sul bottone <
    And Utente clicca su Bollo Auto e Moto
    And Utente seleziona la carta da utilizzare per il pagamento su "targetOperation"
      | targetOperation   | cardNumber   | discrepanza   | userNumberCards   | name |
      | <targetOperation> | <cardNumber> | <discrepanza> | <userNumberCards> | <name> |
    And Utente clicca sul cta X e viene riportato alla schermata precedente
    And Utente clicca su Bollo Auto e Moto
    And Utente seleziona la carta da utilizzare per il pagamento su "targetOperation"
      | targetOperation   | cardNumber   | discrepanza   | userNumberCards   |  name |
      | <targetOperation> | <cardNumber> | <discrepanza> | <userNumberCards> | <name> |
    And Utente verifica il layout della pagina Bollo Auto e Moto e compila i campi
      | regione   | targa   | veicolo   |
      | <regione> | <targa> | <veicolo> |
    And Utente CALCOLA limporto del Bollo Auto
    And Utente visualizza l importo da pagare e controlla che i campi siano corretti
      | regione   | targa   | veicolo   | cardNumber   |
      | <regione> | <targa> | <veicolo> | <cardNumber> |
    And Utente clicca sul bottone <
    And Utente CALCOLA limporto del Bollo Auto
    And Utente clicca sul cta X e viene riportato alla schermata precedente
    And Utente clicca su Bollo Auto e Moto
    And Utente seleziona la carta da utilizzare per il pagamento su "targetOperation"
      | targetOperation   | cardNumber   | discrepanza   | userNumberCards   |  name |
      | <targetOperation> | <cardNumber> | <discrepanza> | <userNumberCards> | <name> |
    And Utente verifica il layout della pagina Bollo Auto e Moto e compila i campi
      | regione   | targa   | veicolo   |
      | <regione> | <targa> | <veicolo> |
    And Utente CALCOLA limporto del Bollo Auto
    And Utente visualizza l importo da pagare e controlla che i campi siano corretti
      | regione   | targa   | veicolo   | cardNumber   |
      | <regione> | <targa> | <veicolo> | <cardNumber> |
    And Utente clicca su CONTINUA per procedere al riepilogo di pagamento
    And Utente visualizza il riepilogo di pagamento del Bollo e verifica che i dati siano corretti
      | regione   | targa   | veicolo   | cardNumber   |
      | <regione> | <targa> | <veicolo> | <cardNumber> |
    And utente clicca su ANNULLA pagamento del bollo auto
    And Utente clicca su Bollo Auto e Moto
    And Utente seleziona la carta da utilizzare per il pagamento su "targetOperation"
      | targetOperation   | cardNumber   | discrepanza   | userNumberCards   | name |
      | <targetOperation> | <cardNumber> | <discrepanza> | <userNumberCards> | <name> |
    And Utente verifica il layout della pagina Bollo Auto e Moto e compila i campi
      | regione   | targa   | veicolo   |
      | <regione> | <targa> | <veicolo> |
    And Utente CALCOLA limporto del Bollo Auto
    And Utente visualizza l importo da pagare e controlla che i campi siano corretti
      | regione   | targa   | veicolo   | cardNumber   |
      | <regione> | <targa> | <veicolo> | <cardNumber> |
    And Utente clicca su CONTINUA per procedere al riepilogo di pagamento
    And Utente visualizza il riepilogo di pagamento del Bollo e verifica che i dati siano corretti
      | regione   | targa   | veicolo   | cardNumber   |
      | <regione> | <targa> | <veicolo> | <cardNumber> |
    And Utente clicca su PAGA Bollo Auto e Moto
    And Utente inserisce il posteId durante la "targetOperation" e clicca conferma
      | posteId   | targetOperation   |
      | <posteId> | <targetOperation> |
    Then Utente atterra sulla "TnkBolloAuto"
      | landingPage   |
      | <landingPage> |
    And Utente chiude App PP

    @PPINPROAND-602_fortunato
    Examples: 
      | username                | password        | driver        | posteId | testIdCheck    | language | site | name     | discrepanza | userNumberCards | targetOperation | cardNumber | importo | regione  | targa   | veicolo     |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PPINPROAND-602 | test     | site | Vincenzo |         0,0 |               2 | BolloAuto       | **** 4679  |    2.00 | CAMPANIA | AA000BB | Autoveicolo |

    @PPINPROAND-602_musella
    Examples: 
      | username             | password  | driver        | posteId | testIdCheck    | language | site | name      | discrepanza | userNumberCards | targetOperation | cardNumber | importo | regione  | targa   | veicolo     |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | PPINPROAND-602 | test     | site | Salvatore |         0,0 |               2 | BolloAuto       | **** 7177  |    2.00 | CAMPANIA | AA000BB | Autoveicolo |

    @PPINPROAND-602_romani
    Examples: 
      | username          | password   | driver        | posteId | testIdCheck    | language | site | name         | discrepanza | userNumberCards | targetOperation | cardNumber | importo | regione | targa   | veicolo             | landingPage  |
      | prisma20@poste.it | Password2! | GalaxyS10Luca |  123123 | PPINPROAND-602 | test     | site | Maria Grazia |         0,0 |               2 | BolloAuto       | **** 9007  |    2.00 | ABRUZZO | AG435DF | Quadriciclo leggero | TnkBolloAuto |

    @PPINPROAND-602_muto
    Examples: 
      | username       | password    | driver        | posteId | testIdCheck    | language | site | name     | discrepanza | userNumberCards | targetOperation | cardNumber | importo | regione  | targa   | veicolo     |
      | francesco.muto | Prisma@2019 | GalaxyS10Luca | prisma  | PPINPROAND-602 | test     | site | Vincenzo |         0,0 |               1 | BolloAuto       | **** 4950  |    2.00 | CAMPANIA | AA000BB | Autoveicolo |
