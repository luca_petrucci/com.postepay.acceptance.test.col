@Sconti
Feature: Sconti

  @PPINPROAND-751
  Scenario Outline: Storico sconti accumulati - Verifica che è possibile modificare il metodo di accredito
    L’utente avrà a disposizione due funzionalità che gli permetteranno rispettivamente di selezionare il metodo di accredito del cash back (cliccando su “Accreditato su Conto*/Postepay*”),

    Given App PP viene avviata
      | testIdJira    | driver   | site   | language   |
      | <testIdCheck> | <driver> | <site> | <language> |
    When Utente inserisce le credenziali nella login page
      | username   | password   | device   | testIdCheck   | name   | discrepanza   |
      | <username> | <password> | <device> | <testIdCheck> | <name> | <discrepanza> |
    And Utente esegue la login in App
      | username   | password   | device   | testIdCheck   | name   | discrepanza   | posteId   |
      | <username> | <password> | <device> | <testIdCheck> | <name> | <discrepanza> | <posteId> |
    And Utente clicca sul Tab Mappe
    And Viene mostrato il popup di abilita GPS
    And Utente attiva il GPS del device dalla pagina del device
    And Utente clicca sul bottone Apri Dettaglio
    And Utente atterra nella pagina di ScontiPoste
    And Utente clicca su Sconti da Accreditare
    And Utente clicca sul tasto back in alto a sinistra
    And Utente clicca su Sconti da Accreditare
    And Utente clicca su icona Assistenza e torna indietro
    Then Utente cambia la carta preferita per l accredito del cash back
      | cardNumber   |
      | <cardNumber> |
    And Utente atterra sulla TYP di conferma cambio carta preferita e clicca chiudi
    And Utente clicca su Sconti da Accreditare
    And Utente cambia la carta preferita per l accredito del cash back
      | cardNumber   |
      | <cardNumber> |
    And Utente atterra sulla TYP di conferma cambio carta preferita e clicca chiudi
    And Utente chiude App PP

    @PPINPROAND-751_fortunato
    Examples: 
      | username                | password        | driver        | posteId | testIdCheck    | language | site | name     | discrepanza | cardNumber |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PPINPROAND-751 | test     | site | Vincenzo |         0,0 | **** 4679  |

#    @PPINPROAND-751_musella
#    Examples: 
#      | username             | password  | driver               | posteId | testIdCheck     | language | site | name      | discrepanza | cardNumber |
#      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | PPINPROAND-751  | test     | site | Salvatore |         0,0 | **** 7177  |
 
  @PPINPROAND-3191
  Scenario Outline: Videata Sconti, Layout visualizzazione Lista
    Verificare il layout della videata Sconti con la visualizzazione in formato Lista

    Given App PP viene avviata
      | testIdJira    | driver   | site   | language   |
      | <testIdCheck> | <driver> | <site> | <language> |
    When Utente inserisce le credenziali nella login page
      | username   | password   | device   | testIdCheck   | name   | discrepanza   |
      | <username> | <password> | <device> | <testIdCheck> | <name> | <discrepanza> |
    And Utente esegue la login in App
      | username   | password   | device   | testIdCheck   | name   | discrepanza   | posteId   |
      | <username> | <password> | <device> | <testIdCheck> | <name> | <discrepanza> | <posteId> |
    And Utente clicca sul Tab Mappe
    And Viene mostrato il popup di abilita GPS
    And Utente attiva il GPS del device dalla pagina del device
    And Utente verifica il layout della videata sconti in formato lista
    And Utente chiude App PP

    @PPINPROAND-3191_fortunato
    Examples: 
      | username                | password        | driver        | posteId | testIdCheck     | language | site | name     | discrepanza |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PPINPROAND-3191 | test     | site | Vincenzo |         0,0 |

    @PPINPROAND-3191_musella
    Examples: 
      | username             | password  | driver               | posteId | testIdCheck     | language | site | name      | discrepanza |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | PPINPROAND-3191 | test     | site | Salvatore |         0,0 |
