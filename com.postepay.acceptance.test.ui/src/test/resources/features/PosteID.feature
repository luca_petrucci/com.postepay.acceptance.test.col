@posteID
Feature: PosteID

  @PP20AND-12 @PPINPROAND-363

  Scenario Outline: Onbording tramite sync
    Si effettua la SYNC tra due device, il numero 1 ovvero il preferito ed il 2 che si deve sincronizzare

    Given Vengono avviati i due devices
      | testIdJira    | driver              | site   | language   |
      | <testIdCheck> | <driver1>,<driver2> | <site> | <language> |
    And Viene effettuata la login sul device preferito
      | username   | password   | device    | testIdCheck   | posteId   | name   | discrepanza   |
      | <username> | <password> | <driver1> | <testIdCheck> | <posteId> | <name> | <discrepanza> |
    And Utente verifica che il "Device1" e il preferito altrimenti lo imposta come tale
      | device    | posteId   |
      | <driver1> | <posteId> |
    And Viene effettuata la login sul device non onboardato
      | username   | password   | device    | testIdCheck   | posteId   |
      | <username> | <password> | <driver2> | <testIdCheck> | <posteId> |
    And Utente inizia il funnel di SYNC sul secondo device e clicca su INIZIA
      | device    |
      | <driver2> |
    And Utente atterra sulla schermata Abilita in App e sceglie la sync da dispositivo preferito
      | device    |
      | <driver2> |
    Then Utente atterra sulla schermata di QR Code Reader
      | device    |
      | <driver2> |
    And Sul "Device 1" viene ricevuta la notifica di SYNC
      | device    |
      | <driver1> |
    And Utente autorizza la SYNC
      | device    | discrepanza   | posteId   |
      | <driver1> | <discrepanza> | <posteId> |
    Then Viene generato il codice QR per autorizzare la SYNC
      | device    |
      | <driver1> |
    And Il "Device 2" legge il QR Code
      | device    |
      | <driver2> |
    Then Viene mostrata la pagina di creazione del posteID
      | device    | posteId   |
      | <driver2> | <posteId> |
    And Inizia il countdown e alla fine l utente atterra sulla TYP
      | device    | discrepanza   |
      | <driver2> | <discrepanza> |
    And Utente naviga nell App
      | device    |
      | <driver2> |
    And Viene eseguita la cancellazione del posteID
      | device    |
      | <driver2> |
    And Vengono chiuse entrambe le App
      | device              |
      | <driver1>,<driver2> |

    Examples: 
      | username                | password        | driver1              | driver2            | posteId | testIdCheck | landingPage | language | site | discrepanza | name     |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | GalaxyS8PlusCamera | prisma  | PP20AND-659 | Homepage    | test     | site |        0,75 | Vincenzo |
