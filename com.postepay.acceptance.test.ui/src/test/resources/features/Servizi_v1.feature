Feature: Servizi

  @PP20AND-891 @PPINPROAND-631 @PPINPROIOS-640
  Scenario Outline: Servizi: Acquisto Biglietto extraurbano
    Utente ha onboardato almeno una carta PP ed effettua l'acquisto di un biglietto extraurbano. L'utente acquista un biglietto per un solo passeggero.

    Given App PP viene avviata
      | testIdJira    | driver   | site   | language   |
      | <testIdCheck> | <driver> | <site> | <language> |
    When Utente inserisce le credenziali nella login page
      | username   | password   | device   | testIdCheck   | discrepanza   | name   |
      | <username> | <password> | <device> | <testIdCheck> | <discrepanza> | <name< |
    And Utente clicca su accedi
    And Utente inserisce il posteId
      | username   | password   | device   | testIdCheck   | posteId   | name   |
      | <username> | <password> | <device> | <testIdCheck> | <posteId> | <name> |
    And Utente clicca sul bottone conferma
    And Utente sulla homepage clicca sul tab PAGA
    And Utente clicca su SERVIZI
    When Utente clicca sul servizio "Biglietto Extraurbano"
      | serviceType   |
      | <serviceType> |
    And Utente seleziona il luogo di "partenza"
      | fromCity   | toCity   |
      | <fromCity> | <toCity> |
    And Utente seleziona il luogo di "destinazione"
      | fromCity   | toCity   |
      | <fromCity> | <toCity> |
    And Utente inserisce le informazioni aggiuntive del viaggio
      | moreInfo   | fromCity   | toCity   | andataRitorno   | startDate   | startTime   | passengers   | kids   |
      | <moreInfo> | <fromCity> | <toCity> | <andataRitorno> | <startDate> | <startTime> | <passengers> | <kids> |
    And Utente clicca su CERCA BIGLIETTO
    And Utente seleziona il biglietto tra quelli disponibili e clicca su SCEGLI
    And Utente clicca sceglie l opzione di biglietto "ORDINARIO"
    And Utente inserisce i dati dei passeggeri
      | namePassenger   | surnamePassenger   | cardPassenger   | emailPassenger   | phonePassenger   |
      | <namePassenger> | <surnamePassenger> | <cardPassenger> | <emailPassenger> | <phonePassenger> |
    And Utente accetta le condizioni del "Biglietto Extraurbano" e clicca sul PAGA
      | serviceType   | namePassenger   | surnamePassenger   | fromCity   | toCity   |
      | <serviceType> | <namePassenger> | <surnamePassenger> | <fromCity> | <toCity> |
    And Utente verifica il riepilogo dell acquito del biglietto e clicca su ANNULLA
      | cardNumber   |
      | <cardNumber> |
    And Utente verifica il popup di conferma cancellazione e clicca su OK
    And Utente ricomincia il funnel dalla schermata di riepilogo del biglietto extraurbano
      | serviceType   | namePassenger   | surnamePassenger   | fromCity   | toCity   |
      | <serviceType> | <namePassenger> | <surnamePassenger> | <fromCity> | <toCity> |
    And Utente verifica il riepilogo dell acquito del biglietto e clicca su paga
      | cardNumber   |
      | <cardNumber> |
    Then Utente inserisce il posteId nel nuovo format e clicca su conferma
      | posteId   |
      | <posteId> |
    And Utente atterra sulla TYP del Biglietto Extraurbano
      | targetOperation   | discrepanza   |
      | <targetOperation> | <discrepanza> |
    And Utente chiude App PP

    @PPINPROAND-631_fortunato
    Examples: 
      | username                | password        | driver        | posteId | testIdCheck    | serviceType           | cardNumber | moreInfo | fromCity        | toCity | namePassenger | surnamePassenger | cardPassenger | emailPassenger           | phonePassenger | andataRitorno | startDate | startTime | passengers | kids | targetOperation | discrepanza | name     |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PPINPROAND-631 | Biglietto Extraurbano | **** 4679  | no       | Napoli Centrale | Sarno  | Vincenzo      | Fortunato        |     168794577 | d.pica@prismaprogetti.it |     3333333333 | no            | startDate | startTime | passengers | kids | extraurbano     |         0,0 | Vincenzo |

    #      | luca.petrucci-abmo | NLWbnsx6#st0 | GalaxyS10Luca | prisma  | PP20AND-891 | Biglietto Extraurbano | **** 0922  | no       | Napoli Centrale | Portici-Ercolano | Luca          | Petrucci         |     168794577 | l.petrucci@prismaprogetti.it |     3338228273 | no            | startDate | startTime | passengers | kids | extraurbano     |        0,75 | Luca |
    #	    | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PP20AND-891 | Biglietto Extraurbano | **** 4679  |si		  | Napoli Centrale | Portici-Ercolano | Luca 		   | Petrucci         | 168794577	  | l.petrucci@prismaprogetti.it | 3338228273     | no			 | startDate | startTime | passengers | kids | extraurbano  | 0,75   |
    @PPINPROAND-631_musella
    Examples: 
      | username             | password  | driver        | posteId | testIdCheck | serviceType           | cardNumber | moreInfo | fromCity        | toCity | namePassenger | surnamePassenger | cardPassenger | emailPassenger                | phonePassenger | andataRitorno | startDate | startTime | passengers | kids | targetOperation | discrepanza | name      |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | PP20AND-891 | Biglietto Extraurbano | **** 7177  | no       | Napoli Centrale | Sarno  | Salvatore     | Musella          |     168794577 | testfactory@prismaprogetti.it |     3333333333 | no            | startDate | startTime | passengers | kids | extraurbano     |         0,0 | Salvatore |

    @PPINPROAND-631_sarno
    Examples: 
      | username           | password   | driver        | posteId | testIdCheck    | serviceType           | cardNumber | moreInfo | fromCity        | toCity | namePassenger | surnamePassenger | cardPassenger | emailPassenger                | phonePassenger | andataRitorno | startDate | startTime | passengers | kids | targetOperation | discrepanza | name    |
      | testACN_4@poste.it | Password1! | GalaxyS10Luca |  123123 | PPINPROAND-631 | Biglietto Extraurbano | **** 1525  | no       | Napoli Centrale | Sarno  | Salvatore     | Musella          |     168794577 | testfactory@prismaprogetti.it |     3333333333 | no            | startDate | startTime | passengers | kids | extraurbano     |         0,0 | Fabiano |

    @PPINPROAND-631_romani
    Examples: 
      | username          | password   | driver        | posteId | testIdCheck    | serviceType           | cardNumber | moreInfo | fromCity        | toCity | namePassenger | surnamePassenger | cardPassenger | emailPassenger                | phonePassenger | andataRitorno | startDate | startTime | passengers | kids | targetOperation | discrepanza | name         |
      | prisma20@poste.it | Password2! | GalaxyS10Luca |  123123 | PPINPROAND-631 | Biglietto Extraurbano | **** 9007  | no       | Napoli Centrale | Sarno  | Salvatore     | Musella          |     168794577 | testfactory@prismaprogetti.it |     3333333333 | no            | startDate | startTime | passengers | kids | extraurbano     |         0,0 | Maria Grazia |

    @PPINPROIOS-640_musella
    Examples: 
      | username             | password  | driver        | posteId | testIdCheck | serviceType           | cardNumber | moreInfo | fromCity        | toCity | namePassenger | surnamePassenger | cardPassenger | emailPassenger                | phonePassenger | andataRitorno | startDate | startTime | passengers | kids | targetOperation | discrepanza | name      |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | PP20AND-891 | Biglietto Extraurbano | **** 7177  | no       | Napoli Centrale | Sarno  | Salvatore     | Musella          |     168794577 | testfactory@prismaprogetti.it |     3333333333 | no            | startDate | startTime | passengers | kids | extraurbano     |         0,0 | Salvatore |

    @PPINPROIOS-640_sarno
    Examples: 
      | username           | password   | driver        | posteId | testIdCheck    | serviceType           | cardNumber | moreInfo | fromCity        | toCity | namePassenger | surnamePassenger | cardPassenger | emailPassenger                | phonePassenger | andataRitorno | startDate | startTime | passengers | kids | targetOperation | discrepanza | name    |
      | testACN_4@poste.it | Password1! | GalaxyS10Luca |  123123 | PPINPROIOS-640 | Biglietto Extraurbano | **** 1525  | no       | Napoli Centrale | Sarno  | Salvatore     | Musella          |     168794577 | testfactory@prismaprogetti.it |     3333333333 | no            | startDate | startTime | passengers | kids | extraurbano     |         0,0 | Fabiano |

    @PPINPROIOS-640_mancini
    Examples: 
      | username          | password   | driver        | posteId | testIdCheck    | serviceType           | cardNumber | moreInfo | fromCity        | toCity   | namePassenger | surnamePassenger | cardPassenger | emailPassenger                | phonePassenger | andataRitorno | startDate | startTime | passengers | kids | targetOperation | discrepanza | name    |
      | prisma21@poste.it | Password1! | GalaxyS10Luca |  123123 | PPINPROIOS-640 | Biglietto Extraurbano | **** 9015  | no       | Napoli Centrale | Albanova | Antonio       | Mancini          |     168794577 | testfactory@prismaprogetti.it |     3333333333 | no            | startDate | startTime | passengers | kids | extraurbano     |         0,0 | Antonio |

    @PPINPROAND-631_marinello
    Examples: 
      | username          | password   | driver        | posteId | testIdCheck    | serviceType           | cardNumber | moreInfo | fromCity        | toCity | namePassenger | surnamePassenger | cardPassenger | emailPassenger                | phonePassenger | andataRitorno | startDate | startTime | passengers | kids | targetOperation | discrepanza | name         |
      | prisma65@poste.it | Password1! | GalaxyS10Luca |  123123 | PPINPROAND-631 | Biglietto Extraurbano | **** 2194  | no       | Napoli Centrale | Albanova  | Salvatore     | Musella          |     168794577 | testfactory@prismaprogetti.it |     3333333333 | no            | startDate | startTime | passengers | kids | extraurbano     |         0,0 | Fiorella     |

  @PP20AND-918 @PPINPROAND-644
  Scenario Outline: Servizi: Acquisto Biglietto urbano
    Acquisto Biglietto urbano da Servizi con impostazioni di default.

    Given App PP viene avviata
      | testIdJira    | driver   | site   | language   |
      | <testIdCheck> | <driver> | <site> | <language> |
    When Utente inserisce le credenziali nella login page
      | username   | password   | device   | testIdCheck   | discrepanza   | name   |
      | <username> | <password> | <device> | <testIdCheck> | <discrepanza> | <name> |
    And Utente clicca su accedi
    And Utente inserisce il posteId
      | username   | password   | device   | testIdCheck   | posteId   | name   |
      | <username> | <password> | <device> | <testIdCheck> | <posteId> | <name> |
    And Utente clicca sul bottone conferma
    And Utente sulla homepage clicca sul tab PAGA
    And Utente clicca su SERVIZI
    When Utente clicca sul servizio "Biglietto Urbano"
      | serviceType   |
      | <serviceType> |
    And Utente cerca e seleziona la "citta" in cui utilizzare il biglietto
      | searchBy   | toCity   |
      | <searchBy> | <toCity> |
    And Utente verifica il riepilogo del biglietto urbano
      | moreInfo   | qty   |
      | <moreInfo> | <qty> |
    And Utente verifica il riepilogo dell acquito del biglietto urbano e clicca su ANNULLA
      | serviceType   | cardNumber   |
      | <serviceType> | <cardNumber> |
    And Utente verifica il popup di conferma cancellazione e clicca su OK
    And Utente verifica il riepilogo del biglietto urbano
      | moreInfo   | qty   |
      | <moreInfo> | <qty> |
    And Utente verifica il riepilogo dell acquito del biglietto e clicca su paga
      | serviceType   | cardNumber   |
      | <serviceType> | <cardNumber> |
    Then Utente inserisce il posteId nel nuovo format e clicca su conferma
      | posteId   |
      | <posteId> |
    And Utente atterra sulla TYP del biglietto urbano e clicca sul CTA Vai alla Ricevuta
      | targetOperation   | discrepanza   |
      | <targetOperation> | <discrepanza> |
    And Utente verifica il layout della schermata Ricevuta di pagnamento e clicca su Vai ai miei Acquisti
      | cardNumber   | discrepanza   |
      | <cardNumber> | <discrepanza> |
    And Utente atterra nella sezione degli acquisti e apre il messaggio di conferma acquisto TRASPORTO
      | targetOperation   | discrepanza   | compagnia   | titoloMessaggio   | titoloDettaglioBiglietto   |
      | <targetOperation> | <discrepanza> | <compagnia> | <titoloMessaggio> | <titoloDettaglioBiglietto> |
    And Utente verifica il messaggio dell acquisto del biglietto urbano
      | targetOperation   | discrepanza   | compagnia   | titoloDettaglioBiglietto   |
      | <targetOperation> | <discrepanza> | <compagnia> | <titoloDettaglioBiglietto> |
    And Utente chiude App PP

    @PPINPROAND-644_fortunato
    Examples: 
      | username                | password        | driver        | posteId | testIdCheck    | serviceType      | searchBy | moreInfo | cardNumber | fromCity | toCity  | posteId | qty | payWith | targetOperation | discrepanza | compagnia   | titoloMessaggio | titoloDettaglioBiglietto                     | name     |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PPINPROAND-644 | Biglietto Urbano | citta    | no       | **** 4769  | fromCity | Lanuvio | prisma  | qty | payWith | urbano          |        0,00 | AGO UNO Srl | TRASPORTO       | Biglietto Corsa Semplice - Comune di Lanuvio | Vincenzo |

    @PPINPROAND-644_musella
    Examples: 
      | username             | password  | driver        | posteId | testIdCheck    | serviceType      | searchBy | moreInfo | cardNumber | fromCity | toCity  | posteId | qty | payWith | targetOperation | discrepanza | compagnia   | titoloMessaggio | titoloDettaglioBiglietto                     | name      |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | PPINPROAND-644 | Biglietto Urbano | citta    | no       | **** 7177  | fromCity | Lanuvio | prisma  | qty | payWith | urbano          |         0,0 | AGO UNO Srl | TRASPORTO       | Biglietto Corsa Semplice - Comune di Lanuvio | Salvatore |

    @PPINPROAND-644_sarno
    Examples: 
      | username           | password   | driver        | posteId | testIdCheck    | serviceType      | searchBy | moreInfo | cardNumber | fromCity | toCity  | posteId | qty | payWith | targetOperation | discrepanza | compagnia   | titoloMessaggio | titoloDettaglioBiglietto                     | name    |
      | testACN_4@poste.it | Password1! | GalaxyS10Luca |  123123 | PPINPROAND-644 | Biglietto Urbano | citta    | no       | **** 1525  | fromCity | Lanuvio | prisma  | qty | payWith | urbano          |         0,0 | AGO UNO Srl | TRASPORTO       | Biglietto Corsa Semplice - Comune di Lanuvio | Fabiano |

    @PPINPROAND-644_romani
    Examples: 
      | username          | password   | driver        | posteId | testIdCheck    | serviceType      | searchBy | moreInfo | cardNumber | fromCity | toCity  | posteId | qty | payWith | targetOperation | discrepanza | compagnia   | titoloMessaggio | titoloDettaglioBiglietto                     | name         |
      | prisma20@poste.it | Password2! | GalaxyS10Luca |  123123 | PPINPROAND-644 | Biglietto Urbano | citta    | no       | **** 9007  | fromCity | Lanuvio | prisma  | qty | payWith | urbano          |         0,0 | AGO UNO Srl | TRASPORTO       | Biglietto Corsa Semplice - Comune di Lanuvio | Maria Grazia |

  @PP20AND-922 @PPINPROAND-646 @PPINPROIOS-643
  Scenario Outline: Servizi: Acquisto Carburante
    Utente ha onboardato almeno una carta PP ed esegue l'acquisto di carburante di 5 euro, si effettuano controlli anche sui settaggi nel profilo

    Given App PP viene avviata
      | testIdJira    | driver   | site   | language   |
      | <testIdCheck> | <driver> | <site> | <language> |
    And Utente inserisce le credenziali nella login page
      | username   | password   | device   | testIdCheck   | discrepanza   | name   |
      | <username> | <password> | <device> | <testIdCheck> | <discrepanza> | <name> |
    And Utente clicca su accedi
    And Utente inserisce il posteId
      | username   | password   | device   | testIdCheck   | posteId   | name   |
      | <username> | <password> | <device> | <testIdCheck> | <posteId> | <name> |
    And Utente clicca sul bottone conferma
    When Utente atterra sulla home page e clicca sul hamburger menu
    And Utente clicca su PROFILO
    And Utente clicca su Il tuo pieno
    And Utente seleziona l importo di default del "pieno" e salva
      | importoPieno   |
      | <importoPieno> |
    And Utente clicca su Il tuo importo preferito
    And Utente seleziona l importo di default del "rifornimento" e salva
      | importoPreferito   |
      | <importoPreferito> |
    And Utente esce dalla sezione profilo per tornare nella homepage
    When Utente sulla homepage clicca sul tab PAGA
    And Utente clicca su SERVIZI
    And Utente clicca sul servizio "Carburante"
      | serviceType   |
      | <serviceType> |
    And Utente visualizza la lista delle stazioni di rifornimento
    And Utente verifica il layout della pagina Acquisto di carburante
      | importoPreferito   |
      | <importoPreferito> |
    And Utente verifica il corretto funzionamento del tasto PIENO
      | importoPieno   |
      | <importoPieno> |
    And Utente verifica il corretto funzionamento delle card di prezzo fisse
    And Utente sceglie cinque euro di carburante da acquistare e clicca su prosegui
    And Utente seleziona il primo distributore per l erogazione del carburante e clicca su paga
    Then Utente verifica il riepilogo dell acquito del carburante e clicca su paga
      | cardNumber   |
      | <cardNumber> |
    And Utente inserisce il posteId nel nuovo format e clicca su conferma
      | posteId   | carburantePage |
      | <posteId> | true           |
    And Utente atterra sui 3 tutorials per l acquisto di carburante e ne verifica il layout
      | discrepanza   | carburantePage |
      | <discrepanza> | true           |
    And Utente chiude App PP

    @PPINPROAND-646_fortunato
    Examples: 
      | username                | password        | driver        | posteId | testIdCheck    | serviceType | listMap | importoPieno | importoPreferito | cardNumber | importo | discrepanza | name     |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PPINPROAND-646 | Carburante  | listMap |           10 |               10 | **** 4679  |    5,00 |         0,0 | Vincenzo |

    #      | luca.petrucci-abmo | NLWbnsx6#st0 | GalaxyS10Luca | prisma  | PP20AND-922 | Carburante  | listMap |           80 |               10 | **** 0922  |    5,00 |        0,75 | Luca |
    @PPINPROAND-646_musella
    Examples: 
      | username             | password  | driver        | posteId | testIdCheck | serviceType | listMap | importoPieno | importoPreferito | cardNumber | importo | discrepanza | name      |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | PP20AND-922 | Carburante  | listMap |            5 |               10 | **** 7177  |    5,00 |         0,0 | Salvatore |

    @PPINPROAND-646_sarno
    Examples: 
      | username           | password   | driver        | posteId | testIdCheck    | serviceType | listMap | importoPieno | importoPreferito | cardNumber | importo | discrepanza | name    |
      | testACN_4@poste.it | Password1! | GalaxyS10Luca |  123123 | PPINPROAND-646 | Carburante  | listMap |            5 |               10 | **** 1525  |    5,00 |         0,0 | Fabiano |

    @PPINPROAND-646_romani
    Examples: 
      | username          | password   | driver        | posteId | testIdCheck    | serviceType | listMap | importoPieno | importoPreferito | cardNumber | importo | discrepanza | name         |
      | prisma20@poste.it | Password2! | GalaxyS10Luca |  123123 | PPINPROAND-646 | Carburante  | listMap |            5 |               10 | **** 9007  |    5,00 |         0,0 | Maria Grazia |

    @PPINPROIOS-643_mancini
    Examples: 
      | username          | password   | driver        | posteId | testIdCheck    | serviceType | listMap | importoPieno | importoPreferito | cardNumber | importo | discrepanza | name    |
      | prisma21@poste.it | Password1! | GalaxyS10Luca |  123123 | PPINPROIOS-643 | Carburante  | listMap |            5 |               10 | **** 9015  |    5,00 |         0,0 | Antonio |

    @PPINPROAND-643_marinello
    Examples: 
      | username          | password   | driver        | posteId | testIdCheck    | serviceType | listMap | importoPieno | importoPreferito | cardNumber | importo | discrepanza | name    |
      | prisma65@poste.it | Password1! | GalaxyS10Luca |  123123 | PPINPROIOS-643 | Carburante  | listMap |            5 |               10 | **** 2194  |    5,00 |         0,0 | Fiorella|


  @PPINPROAND-645 @PPINPROIOS-642
  Scenario Outline: Servizi-Acquisto Parcheggio
    Test sulla funzionalita di acquisto parcheggio

    Given App PP viene avviata
      | testIdJira    | driver   | site   | language   |
      | <testIdCheck> | <driver> | <site> | <language> |
    When Utente inserisce le credenziali nella login page
      | username   | password   | device   | testIdCheck   | name   | discrepanza   |
      | <username> | <password> | <device> | <testIdCheck> | <name> | <discrepanza> |
    And Utente esegue la login in App
      | username   | password   | device   | testIdCheck   | name   | discrepanza   | posteId   |
      | <username> | <password> | <device> | <testIdCheck> | <name> | <discrepanza> | <posteId> |
    And Utente sulla homepage clicca sul tab PAGA
    And Utente clicca su SERVIZI
    When Utente clicca sul servizio "Parcheggio"
      | serviceType   |
      | <serviceType> |
    And Viene mostrato il popup di abilita GPS
    And Utente clicca sul primo parcheggio disponibile
    And Utente clicca sul PAGA Parcheggio
    Then Utente verifica il riepilogo dell acquisto del parcheggio e clicca su Annulla
      | serviceType   | cardNumber   |
      | <serviceType> | <cardNumber> |
    And Utente clicca sul PAGA Parcheggio
    And Utente verifica il riepilogo dell acquisto del parcheggio e clicca su paga
      | serviceType   | cardNumber   |
      | <serviceType> | <cardNumber> |
    Then Utente inserisce il posteId nel nuovo format e clicca su conferma
      | posteId   |
      | <posteId> |
    And Utente atterra sulla TYP del parcehggio e clicca sul CTA Vai alla Ricevuta
      | targetOperation   | discrepanza   |
      | <targetOperation> | <discrepanza> |
    Then Utente controlla se è presente il biglietto del parcheggio
    Then Utente interrompe il biglietto acquistato
    And Utente chiude App PP

    @PPINPROAND-645_fortunato
    Examples: 
      | username                | password        | driver        | posteId | testIdCheck    | language | site | name     | discrepanza | serviceType | targetOperation | cardNumber |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PPINPROAND-645 | test     | site | Vincenzo |         0,0 | Parcheggio  | parcheggio      | **** 4679  |

    @PPINPROAND-645_musella
    Examples: 
      | username             | password  | driver        | posteId | testIdCheck    | language | site | name      | discrepanza | serviceType | targetOperation | cardNumber |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | PPINPROAND-645 | test     | site | Salvatore |         0,0 | Parcheggio  | parcheggio      | **** 7177  |

    @PPINPROAND-645_sarno
    Examples: 
      | username           | password   | driver        | posteId | testIdCheck    | language | site | name    | discrepanza | serviceType | targetOperation | cardNumber |
      | testACN_4@poste.it | Password1! | GalaxyS10Luca |  123123 | PPINPROAND-645 | test     | site | Fabiano |         0,0 | Parcheggio  | parcheggio      | **** 1525  |

    @PPINPROAND-645_romani
    Examples: 
      | username          | password   | driver        | posteId | testIdCheck    | language | site | name         | discrepanza | serviceType | targetOperation | cardNumber |
      | prisma20@poste.it | Password2! | GalaxyS10Luca |  123123 | PPINPROAND-645 | test     | site | Maria Grazia |         0,0 | Parcheggio  | parcheggio      | **** 9007  |

    @PPINPROIOS-642_mancini
    Examples: 
      | username          | password   | driver        | posteId | testIdCheck    | serviceType | cardNumber | discrepanza | name    | targetOperation | site | language |
      | prisma21@poste.it | Password1! | GalaxyS10Luca |  123123 | PPINPROIOS-642 | Parcheggio  | **** 9015  |         0,0 | Antonio | parcheggio      | site | test     |

    @PPINPROAND-645_mancini
    Examples: 
      | username          | password   | driver        | posteId | testIdCheck    | serviceType | cardNumber | discrepanza | name    | targetOperation | site | language |
      | prisma21@poste.it | Password1! | GalaxyS10Luca |  123123 | PPINPROAND-645 | Parcheggio  | **** 9015  |         0,0 | Antonio | parcheggio      | site | test     |

    @PPINPROAND-645_agata
    Examples: 
      | username          | password   | driver        | posteId | testIdCheck    | serviceType | cardNumber | discrepanza | name    | targetOperation | site | language |
      | prisma66@poste.it | Password1! | GalaxyS10Luca |  123123 | PPINPROAND-645 | Parcheggio  | **** 1341  |         0,0 | Agata | parcheggio      | site | test     |

  @PPINPROAND-3451
  Scenario Outline: Ordinamento delle opzioni di viaggio su base prezzo
    Ordinamento delle opzioni di viaggio su base prezzo di TRENITALIA

    Given App PP viene avviata
      | testIdJira    | driver   | site   | language   |
      | <testIdCheck> | <driver> | <site> | <language> |
    When Utente inserisce le credenziali nella login page
      | username   | password   | device   | testIdCheck   | discrepanza   | name   |
      | <username> | <password> | <device> | <testIdCheck> | <discrepanza> | <name< |
    And Utente clicca su accedi
    And Utente inserisce il posteId
      | username   | password   | device   | testIdCheck   | posteId   | name   |
      | <username> | <password> | <device> | <testIdCheck> | <posteId> | <name> |
    And Utente clicca sul bottone conferma
    And Utente sulla homepage clicca sul tab PAGA
    And Utente clicca su SERVIZI
    When Utente clicca sul servizio "Biglietto Extraurbano"
      | serviceType   |
      | <serviceType> |
    And Utente seleziona il luogo di "partenza"
      | fromCity   | toCity   |
      | <fromCity> | <toCity> |
    And Utente seleziona il luogo di "destinazione"
      | fromCity   | toCity   |
      | <fromCity> | <toCity> |
    And Utente inserisce le informazioni aggiuntive del viaggio
      | moreInfo   | fromCity   | toCity   | andataRitorno   | startDate   | startTime   | passengers   | kids   |
      | <moreInfo> | <fromCity> | <toCity> | <andataRitorno> | <startDate> | <startTime> | <passengers> | <kids> |
    And Utente clicca su CERCA BIGLIETTO
    Then Utente seleziona il Vettore "TRENITALIA" e controlla che la lista di risultati si aggiorni
    And Utente seleziona come ulteriore filtro il parametro "PREZZO" e controlla che la lista di risultati si aggiorni
    And Utente chiude App PP

    @PPINPROAND-3451_fortunato
    Examples: 
      | username                | password        | driver        | posteId | testIdCheck     | serviceType           | cardNumber | moreInfo | fromCity        | toCity | namePassenger | surnamePassenger | cardPassenger | emailPassenger           | phonePassenger | andataRitorno | startDate | startTime | passengers | kids | targetOperation | discrepanza | name     |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PPINPROAND-3451 | Biglietto Extraurbano | **** 4679  | no       | Napoli Centrale | Sarno  | Vincenzo      | Fortunato        |     168794577 | d.pica@prismaprogetti.it |     3333333333 | no            | startDate | startTime | passengers | kids | extraurbano     |         0,0 | Vincenzo |

    @PPINPROAND-3451_musella
    Examples: 
      | username             | password  | driver        | posteId | testIdCheck     | serviceType           | cardNumber | moreInfo | fromCity        | toCity | namePassenger | surnamePassenger | cardPassenger | emailPassenger                | phonePassenger | andataRitorno | startDate | startTime | passengers | kids | targetOperation | discrepanza | name      |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | PPINPROAND-3451 | Biglietto Extraurbano | **** 7177  | no       | Napoli Centrale | Sarno  | Salvatore     | Musella          |     168794577 | testfactory@prismaprogetti.it |     3333333333 | no            | startDate | startTime | passengers | kids | extraurbano     |         0,0 | Salvatore |
