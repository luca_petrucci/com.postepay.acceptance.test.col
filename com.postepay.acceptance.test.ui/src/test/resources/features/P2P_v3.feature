@P2P
Feature: P2P

  @PP20AND-20 @PPINPROAND-371 @PPINPROIOS-382
  Scenario Outline: Invio P2P - Contatto P2P - Utente con una PPAY
    Invio di denaro tramite la funzionalità P2P ad un contatto già P2P. Se l’utente possiede una sola PP si attiva il funnel di invio P2P.

    Given App PP viene avviata
      | testIdJira    | driver   | site   | language   |
      | <testIdCheck> | <driver> | <site> | <language> |
    When Utente inserisce le credenziali nella login page
      | username   | password   | device   | testIdCheck   | name   | discrepanza   |
      | <username> | <password> | <device> | <testIdCheck> | <name> | <discrepanza> |
    And Utente clicca su accedi
    And Utente inserisce il posteId
      | username   | password   | device   | testIdCheck   | posteId   | name   |
      | <username> | <password> | <device> | <testIdCheck> | <posteId> | <name> |
    And Utente clicca sul bottone conferma
    And Utente sulla homepage clicca sul tab community
    And Utente clicca su P2P invia denaro
    And Utente seleziona il destinatario abilitato a ricevere il P2P
      | targetUser   | cardNumber   | username   | userNumberCards   | driver   | name   |
      | <targetUser> | <cardNumber> | <username> | <userNumberCards> | <driver> | <name> |
    And Utente inserisce importo da inviare e clicca su conferma
      | importo   |
      | <importo> |
    And Utente verifica i dati del riepilogo del P2P e lo invia
    And Utente inserisce il posteId nel nuovo format e clicca su conferma
      | posteId   |
      | <posteId> |
    Then Utente atterra sulla "TnkP2p"
      | landingPage   |
      | <landingPage> |
    And Utente chiude App PP

    @PPINPROAND-371_fortunato
    Examples: 
      | username                | password        | driver        | posteId | testIdCheck    | cardNumber | targetUser | importo | landingPage | language | site | userNumberCards | name     | discrepanza |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PPINPROAND-371 | **** 4679  | Salvatore  |    0.01 | TnkP2p      | test     | site |               2 | Vincenzo |        0,75 |

    @PPINPROAND-371_musella
    Examples: 
      | username             | password  | driver        | posteId | testIdCheck    | cardNumber | targetUser         | importo | landingPage | language | site | userNumberCards | name      | discrepanza |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | PPINPROAND-371 | **** 7177  | Vincenzo Fortunato |    0.01 | TnkP2p      | test     | site |               2 | Salvatore |         0,0 |

    @PPINPROAND-371_muto
    Examples: 
      | username       | password    | driver        | posteId | testIdCheck    | cardNumber | targetUser         | importo | cardNumberDestinatario | comment                 | landingPage | site | language | targetOperation | discrepanza | name      | userNumberCards |
      | francesco.muto | Prisma@2019 | GalaxyS10Luca | prisma  | PPINPROAND-371 | **** 4950  | Vincenzo Fortunato |    0.01 |       5333171077144679 | ricaricaAltraPpStandard | TnkRecharge | test | site     | OtherPP         |         0,0 | Francesco |               1 |

    @PPINPROAND-371_romani
    Examples: 
      | username          | password   | driver        | posteId | testIdCheck    | cardNumber | targetUser      | importo | cardNumberDestinatario | comment                 | landingPage | site | language | targetOperation | discrepanza | name         | userNumberCards |
      | prisma20@poste.it | Password2! | GalaxyS10Luca |  123123 | PPINPROAND-371 | **** 9007  | Antonio Mancini |    1.01 |       5333171000679015 | ricaricaAltraPpStandard | TnkRecharge | test | site     | OtherPP         |         0,0 | Maria Grazia |               2 |

    @PPINPROIOS-382_fortunato
    Examples: 
      | username                | password        | driver        | posteId | testIdCheck    | cardNumber | targetUser | importo | landingPage | language | site | userNumberCards | name     | discrepanza |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PPINPROIOS-382 | **** 4679  | Nannola    |    0,01 | TnkP2p      | test     | site |               2 | Vincenzo |        0,75 |

    @PPINPROIOS-382_musella
    Examples: 
      | username             | password  | driver        | posteId | testIdCheck    | cardNumber | targetUser         | importo | landingPage | language | site | userNumberCards | name      | discrepanza |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | PPINPROIOS-382 | **** 7177  | Vincenzo Fortunato |    0,01 | TnkP2p      | test     | site |               2 | Salvatore |         0,0 |

    @PPINPROIOS-382_muto
    Examples: 
      | username       | password    | driver        | posteId | testIdCheck    | cardNumber | targetUser         | importo | cardNumberDestinatario | comment                 | landingPage | site | language | targetOperation | discrepanza | name      | userNumberCards |
      | francesco.muto | Prisma@2019 | GalaxyS10Luca | prisma  | PPINPROIOS-382 | **** 4950  | Vincenzo Fortunato |    0,01 |       5333171077144679 | ricaricaAltraPpStandard | TnkRecharge | test | site     | OtherPP         |         0,0 | Francesco |               1 |

    @PPINPROAND-371_grecia
    Examples: 
      | username          | password   | driver        | posteId | testIdCheck    | cardNumber | targetUser      | importo | cardNumberDestinatario | comment                 | landingPage | site | language | targetOperation | discrepanza | name         | userNumberCards |
      | prisma65@poste.it | Password1! | GalaxyS10Luca |  123123 | PPINPROAND-371 | **** 8701  | Antonio Mancini |    1.01 |       5333171000679015 | ricaricaAltraPpStandard | TnkRecharge | test | site     | OtherPP         |         0,0 | Fiorella     |               2 |

  @PP20AND-90 @PPINPROAND-417 @PPINPROIOS-429
  Scenario Outline: Operazione veloce - P2P
    Richiesta di denaro tramite la funzionalità P2P ad un contatto già P2P. Da operazione veloce nella Homepage.

    Given App PP viene avviata
      | testIdJira    | driver   | site   | language   |
      | <testIdCheck> | <driver> | <site> | <language> |
    When Utente inserisce le credenziali nella login page
      | username   | password   | device   | testIdCheck   | name   | discrepanza   |
      | <username> | <password> | <device> | <testIdCheck> | <name> | <discrepanza> |
    And Utente clicca su accedi
    And Utente inserisce il posteId
      | username   | password   | device   | testIdCheck   | posteId   | name   |
      | <username> | <password> | <device> | <testIdCheck> | <posteId> | <name> |
    And Utente clicca sul bottone conferma
    And Utente sulla homepage clicca sul tab prodotti
    And Utente clicca sul tab homePage
    And Utente e sulla homepage e clicca sulla card invia P2P operazione veloce
      | userNumberCards   | cardNumber   | name   |
      | <userNumberCards> | <cardNumber> | <name> |
    And Utente seleziona il destinatario del P2P tal tab CONTATTI P2P
      | targetUser   | cardNumber   | username   |
      | <targetUser> | <cardNumber> | <username> |
    And Utente inserisce importo da inviare e clicca su conferma
      | importo   |
      | <importo> |
    And Utente verifica i dati del riepilogo del P2P e lo invia
    And Utente inserisce il posteId nel nuovo format e clicca su conferma
      | posteId   |
      | <posteId> |
    Then Utente atterra sulla "TnkP2pSpeed"
      | landingPage   |
      | <landingPage> |
    And Utente chiude App PP

    @PPINPROAND-417_fortunato
    Examples: 
      | username                | password        | driver        | posteId | testIdCheck    | cardNumber | targetUser | importo | landingPage | language | site | userNumberCards | name     | discrepanza |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PPINPROAND-417 | **** 4679  | Antonio    |    0.01 | TnkP2pSpeed | test     | site |               2 | Vincenzo |         0,0 |

    @PPINPROAND-417_musella
    Examples: 
      | username             | password  | driver        | posteId | testIdCheck    | cardNumber | targetUser         | importo | landingPage | language | site | userNumberCards | name      | discrepanza |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | PPINPROAND-417 | **** 7177  | Vincenzo Fortunato |    0.01 | TnkP2p      | test     | site |               2 | Salvatore |         0,0 |

    @PPINPROAND-417_romani
    Examples: 
      | username          | password   | driver        | posteId | testIdCheck    | cardNumber | targetUser      | importo | landingPage | language | site | userNumberCards | name         | discrepanza |
      | prisma20@poste.it | Password2! | GalaxyS10Luca |  123123 | PPINPROAND-417 | **** 9007  | Antonio Mancini |    2.41 | TnkP2p      | test     | site |               2 | Maria Grazia |         0,0 |

    @PPINPROIOS-429_fortunato
    Examples: 
      | username                | password        | driver        | posteId | testIdCheck    | cardNumber | targetUser | importo | landingPage | language | site | userNumberCards | name     | discrepanza |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PPINPROIOS-429 | **** 4679  | Antonio    |    0,01 | TnkP2pSpeed | test     | site |               2 | Vincenzo |         0,0 |

    @PPINPROIOS-429_musella
    Examples: 
      | username             | password  | driver        | posteId | testIdCheck    | cardNumber | targetUser         | importo | landingPage | language | site | userNumberCards | name      | discrepanza |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | PPINPROIOS-429 | **** 7177  | Vincenzo Fortunato |    0,01 | TnkP2p      | test     | site |               2 | Salvatore |         0,0 |

    @PPINPROIOS-429_mancini
    Examples: 
      | username          | password   | driver        | posteId | testIdCheck    | landingPage | language | site | name    | discrepanza | targetUser          | importo | userNumberCards | cardNumber |
      | prisma21@poste.it | Password1! | GalaxyS10Luca |  123123 | PPINPROIOS-429 | TnkP2p      | test     | site | Antonio |         0,0 | Maria Grazia Romani |    0,01 |               2 | **** 9015  |

    @PPINPROAND-417_mancini
    Examples: 
      | username          | password   | driver        | posteId | testIdCheck    | landingPage | language | site | name    | discrepanza | targetUser          | importo | userNumberCards | cardNumber |
      | prisma21@poste.it | Password1! | GalaxyS10Luca |  123123 | PPINPROAND-417 | TnkP2p      | test     | site | Antonio |         0,0 | Maria Grazia Romani |    0,01 |               2 | **** 9015  |

  @PP20AND-146 @PPINPROAND-459 @PPINPROIOS-471
  Scenario Outline: Richiedi P2P - Contatto P2P - Utente con PPAY
    Richiesta di denaro tramite la funzionalità P2P ad un contatto già P2P. Al tap su «Richiedi Denaro» se l’Utente possiede almeno una PP si attiva il funnel di richiedi P2P.

    Given App PP viene avviata
      | testIdJira    | driver   | site   | language   |
      | <testIdCheck> | <driver> | <site> | <language> |
    When Utente inserisce le credenziali nella login page
      | username   | password   | device   | testIdCheck   | name   | discrepanza   |
      | <username> | <password> | <device> | <testIdCheck> | <name> | <discrepanza> |
    And Utente clicca su accedi
    And Utente inserisce il posteId
      | username   | password   | device   | testIdCheck   | posteId   | name   |
      | <username> | <password> | <device> | <testIdCheck> | <posteId> | <name> |
    And Utente clicca sul bottone conferma
    And Utente sulla homepage clicca sul tab community
    And Utente clicca sul tab P2P richiedi denaro
    And Utente seleziona il contatto per richiedere il pagamento
      | targetUser   |
      | <targetUser> |
    And Utente inserisce importo da richiedere e clicca su conferma
      | importo   |
      | <importo> |
    And Utente verifica i dati del riepilogo del P2P e clicca su conferma
    Then Utente atterra sulla "TnkP2pRequest"
      | landingPage   |
      | <landingPage> |
    And Utente chiude App PP

    @PPINPROAND-459_fortunato
    Examples: 
      | username                | password        | driver        | posteId | testIdCheck    | cardNumber | targetUser | importo | landingPage   | language | site | name     | discrepanza |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PPINPROAND-459 | **** 4679  | Antonio    |    0.01 | TnkP2pRequest | test     | site | Vincenzo |         0,0 |

    @PPINPROAND-459_musella
    Examples: 
      | username             | password  | driver        | posteId | testIdCheck    | cardNumber | targetUser         | importo | landingPage   | language | site | name      | discrepanza |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | PPINPROAND-459 | **** 7177  | Vincenzo Fortunato |    0.01 | TnkP2pRequest | test     | site | Salvatore |         0,0 |

    @PPINPROAND-459_sarno
    Examples: 
      | username           | password   | driver        | posteId | testIdCheck    | landingPage   | language | site | name    | discrepanza | targetUser         | comment     | importo |
      | testACN_4@poste.it | Password1! | GalaxyS10Luca |  123123 | PPINPROAND-459 | TnkP2pRequest | test     | site | Fabiano |         0,0 | Vincenzo Fortunato | testRequest |    0,01 |

    @PPINPROAND-459_romani
    Examples: 
      | username          | password   | driver        | posteId | testIdCheck    | landingPage   | language | site | name         | discrepanza | targetUser      | comment     | importo |
      | prisma20@poste.it | Password2! | GalaxyS10Luca |  123123 | PPINPROAND-459 | TnkP2pRequest | test     | site | Maria Grazia |         0,0 | Antonio Mancini | testRequest |    1,01 |

    @PPINPROIOS-471_fortunato
    Examples: 
      | username                | password        | driver        | posteId | testIdCheck    | cardNumber | targetUser | importo | landingPage   | language | site | name     | discrepanza |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PPINPROIOS-471 | **** 4679  | Nannola    |    0,01 | TnkP2pRequest | test     | site | Vincenzo |         0,0 |

    @PPPINPROIOS-471_musella
    Examples: 
      | username             | password  | driver        | posteId | testIdCheck    | cardNumber | targetUser         | importo | landingPage   | language | site | name      | discrepanza |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | PPINPROIOS-471 | **** 7177  | Vincenzo Fortunato |    0,01 | TnkP2pRequest | test     | site | Salvatore |         0,0 |

    @PPINPROIOS-471_sarno
    Examples: 
      | username           | password   | driver        | posteId | testIdCheck    | landingPage   | language | site | name    | discrepanza | targetUser         | comment     | importo |
      | testACN_4@poste.it | Password1! | GalaxyS10Luca |  123123 | PPINPROIOS-471 | TnkP2pRequest | test     | site | Fabiano |         0,0 | Vincenzo Fortunato | testRequest |    0,01 |

    @PPINPROIOS-471_mancini
    Examples: 
      | username          | password   | driver        | posteId | testIdCheck    | landingPage   | language | site | name    | discrepanza | targetUser          | comment     | importo |
      | prisma21@poste.it | Password1! | GalaxyS10Luca |  123123 | PPINPROIOS-471 | TnkP2pRequest | test     | site | Antonio |         0,0 | Maria Grazia Romani | testRequest |    0,01 |

		@PPINPROIOS-471_fanucci
    Examples: 
      | username          | password   | driver        | posteId | testIdCheck    | landingPage   | language | site | name     | discrepanza | targetUser          | comment     | importo |
      | prisma33@poste.it | Password1! | GalaxyS10Luca |  123123 | PPINPROIOS-471 | TnkP2pRequest | test     | site | Vincenza |         0,0 | Maria Grazia Romani | testRequest |    0,01 |
      
  @PP20AND-147 @PPINPROAND-460 @PPINPROIOS-472
  Scenario Outline: Richiedi P2P - Aggiungi commento
    E' possibile aggiungere un commento al P2P da ricevere

    Given App PP viene avviata
      | testIdJira    | driver   | site   | language   |
      | <testIdCheck> | <driver> | <site> | <language> |
    When Utente inserisce le credenziali nella login page
      | username   | password   | device   | testIdCheck   | name   | discrepanza   |
      | <username> | <password> | <device> | <testIdCheck> | <name> | <discrepanza> |
    And Utente clicca su accedi
    And Utente inserisce il posteId
      | username   | password   | device   | testIdCheck   | posteId   | name   |
      | <username> | <password> | <device> | <testIdCheck> | <posteId> | <name> |
    And Utente clicca sul bottone conferma
    And Utente sulla homepage clicca sul tab community
    And Utente clicca sul tab P2P richiedi denaro
    And Utente seleziona il contatto per richiedere il pagamento
      | targetUser   |
      | <targetUser> |
    And Utente inserisce importo da richiedere e clicca su conferma
      | importo   |
      | <importo> |
    And Utente sul riepiologo del P2P aggiunge un commento e clicca su conferma
      | comment   |
      | <comment> |
    Then Utente atterra sulla "TnkP2pRequest"
      | landingPage   |
      | <landingPage> |
    And Utente chiude App PP

    @PPINPROAND-460_fortunato
    Examples: 
      | username                | password        | driver        | posteId | testIdCheck    | cardNumber | targetUser | importo | landingPage   | language | site | comment     | name     | discrepanza |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PPINPROAND-460 | **** 4679  | Antonio    |    0.01 | TnkP2pRequest | test     | site | testRequest | Vincenzo |         0,0 |

    @PPINPROAND-460_musella
    Examples: 
      | username             | password  | driver        | posteId | testIdCheck    | cardNumber | targetUser         | importo | landingPage   | language | site | comment     | name      | discrepanza |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | PPINPROAND-460 | **** 7177  | Vincenzo Fortunato |    0.01 | TnkP2pRequest | test     | site | testRequest | Salvatore |         0,0 |

    @PPINPROAND-460_sarno
    Examples: 
      | username           | password   | driver        | posteId | testIdCheck    | landingPage   | language | site | name    | discrepanza | targetUser         | comment     | importo |
      | testACN_4@poste.it | Password1! | GalaxyS10Luca |  123123 | PPINPROAND-460 | TnkP2pRequest | test     | site | Fabiano |         0,0 | Vincenzo Fortunato | testRequest |    0,01 |

    @PPINPROAND-460_romani
    Examples: 
      | username          | password   | driver        | posteId | testIdCheck    | landingPage   | language | site | name         | discrepanza | targetUser      | comment     | importo |
      | prisma20@poste.it | Password2! | GalaxyS10Luca |  123123 | PPINPROAND-460 | TnkP2pRequest | test     | site | Maria Grazia |         0,0 | Antonio Mancini | testRequest |    1,01 |

    @PPINPROIOS-472_fortunato
    Examples: 
      | username                | password        | driver        | posteId | testIdCheck    | cardNumber | targetUser | importo | landingPage   | language | site | comment     | name     | discrepanza |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PPINPROIOS-472 | **** 4679  | Nannola    |    0,01 | TnkP2pRequest | test     | site | testRequest | Vincenzo |         0,0 |

    @PPINPROIOS-472_musella
    Examples: 
      | username             | password  | driver        | posteId | testIdCheck    | cardNumber | targetUser         | importo | landingPage   | language | site | comment     | name      | discrepanza |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | PPINPROIOS-472 | **** 7177  | Vincenzo Fortunato |    0,01 | TnkP2pRequest | test     | site | testRequest | Salvatore |         0,0 |

    @PPINPROIOS-472_sarno
    Examples: 
      | username           | password   | driver        | posteId | testIdCheck    | landingPage   | language | site | name    | discrepanza | targetUser         | comment     | importo |
      | testACN_4@poste.it | Password1! | GalaxyS10Luca |  123123 | PPINPROIOS-472 | TnkP2pRequest | test     | site | Fabiano |         0,0 | Vincenzo Fortunato | testRequest |    0,01 |

    @PPINPROIOS-472_mancini
    Examples: 
      | username          | password   | driver        | posteId | testIdCheck    | landingPage   | language | site | name    | discrepanza | targetUser          | comment     | importo |
      | prisma21@poste.it | Password1! | GalaxyS10Luca |  123123 | PPINPROIOS-472 | TnkP2pRequest | test     | site | Antonio |         0,0 | Maria Grazia Romani | testRequest |    0,01 |

  @PP20AND-389 @PPINPROAND-537
  Scenario Outline: Presenza card Gioca a Goal&Win e Postepay Sound in Community
    Visualizzazione card Gioca a Goal&Win e Postepay Sound in Community

    Given App PP viene avviata
      | testIdJira    | driver   | site   | language   |
      | <testIdCheck> | <driver> | <site> | <language> |
    When Utente inserisce le credenziali nella login page
      | username   | password   | device   | testIdCheck   | name   | discrepanza   |
      | <username> | <password> | <device> | <testIdCheck> | <name> | <discrepanza> |
    And Utente clicca su accedi
    And Utente inserisce il posteId
      | username   | password   | device   | testIdCheck   | posteId   | name   |
      | <username> | <password> | <device> | <testIdCheck> | <posteId> | <name> |
    And Utente clicca sul bottone conferma
    And Utente sulla homepage clicca sul tab community
    Then Utente verifica che il tab Goal&Win e presente
    And Utente chiude App PP

    Examples: 
      | username           | password     | driver        | posteId | testIdCheck | cardNumber | targetUser         | importo | landingPage | language | site | comment     | name | discrepanza |
      | luca.petrucci-abmo | NLWbnsx6#st0 | GalaxyS10Luca | prisma  | PP20AND-389 | **** 4679  | vincenzo fortunato |    0.01 | TnkP2p      | test     | site | testRequest | Luca |        0,75 |

    @PPINPROAND-537_musella
    Examples: 
      | username             | password  | driver        | posteId | testIdCheck | cardNumber | targetUser         | importo | landingPage | language | site | comment     | name      | discrepanza |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | PP20AND-389 | **** 7177  | vincenzo fortunato |    0.01 | TnkP2p      | test     | site | testRequest | Salvatore |         0,0 |

  #   | vincenzo.fortunato-4869| D@niela22Lili10| GalaxyS8   | prisma  | PP20AND-147  | **** 4679  |Daniele Ioviero| 1       | TnkP2p | test | site | testRequest | Vincenzo | 0,75 |
  @PP20AND-149 @PPINPROAND-462 @PPINPROIOS-474
  Scenario Outline: Richiedi P2P - Contatto non P2P
    Richiesta di denaro tramite la funzionalità P2P ad un contatto non P2P.

    Given App PP viene avviata
      | testIdJira    | driver   | site   | language   |
      | <testIdCheck> | <driver> | <site> | <language> |
    When Utente inserisce le credenziali nella login page
      | username   | password   | device   | testIdCheck   | name   | discrepanza   |
      | <username> | <password> | <device> | <testIdCheck> | <name> | <discrepanza> |
    And Utente clicca su accedi
    And Utente inserisce il posteId
      | username   | password   | device   | testIdCheck   | posteId   | name   |
      | <username> | <password> | <device> | <testIdCheck> | <posteId> | <name> |
    And Utente clicca sul bottone conferma
    And Utente sulla homepage clicca sul tab community
    And Utente clicca sul tab P2P richiedi denaro
    And Utente seleziona il contatto per richiedere il pagamento da no poste
      | targetUser   | driver   |
      | <targetUser> | <driver> |
    Then Utente atterra sulla "TnkNoP2pUser"
      | landingPage   |
      | <landingPage> |
    And Utente chiude App PP

    # Richiede un contato salvato nella rubrica del telefono con nome: Pippo e numero a piacere
    @PPINPROAND-462_fortunato
    Examples: 
      | username                | password        | driver        | posteId | testIdCheck    | cardNumber | targetUser | importo | landingPage  | language | site | name     | discrepanza |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PPINPROAND-462 | **** 4679  | pippo      |    0.01 | TnkNoP2pUser | test     | site | Vincenzo |         0,0 |

    @PPINPROAND-462_musella
    Examples: 
      | username             | password  | driver        | posteId | testIdCheck | cardNumber | targetUser | importo | landingPage  | language | site | name      | discrepanza |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | PP20AND-149 | **** 7177  | pippo      |    0.01 | TnkNoP2pUser | test     | site | Salvatore |         0,0 |

    @PPINPROAND-462_sarno
    Examples: 
      | username           | password   | driver        | posteId | testIdCheck    | landingPage | language | site | name    | discrepanza | targetUser |
      | testACN_4@poste.it | Password1! | GalaxyS10Luca |  123123 | PPINPROAND-462 | Homepage    | test     | site | Fabiano |         0,0 | pippo      |

    @PPINPROAND-462_romani
    Examples: 
      | username          | password   | driver        | posteId | testIdCheck    | landingPage | language | site | name         | discrepanza | targetUser |
      | prisma20@poste.it | Password2! | GalaxyS10Luca |  123123 | PPINPROAND-462 | Homepage    | test     | site | Maria Grazia |         0,0 | Pippo      |

    @PPINPROIOS-474_musella
    Examples: 
      | username             | password  | driver        | posteId | testIdCheck    | cardNumber | targetUser | importo | landingPage  | language | site | name      | discrepanza |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | PPINPROIOS-474 | **** 7177  | Pippo      |    0.01 | TnkNoP2pUser | test     | site | Salvatore |         0,0 |

    @PPINPROIOS-474_fortunato
    Examples: 
      | username                | password        | driver        | posteId | testIdCheck    | cardNumber | targetUser | importo | landingPage  | language | site | name     | discrepanza |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PPINPROIOS-474 | **** 4679  | pippo      |    0.01 | TnkNoP2pUser | test     | site | Vincenzo |         0,0 |

    @PPINPROIOS-474_sarno
    Examples: 
      | username           | password   | driver        | posteId | testIdCheck    | landingPage | language | site | name    | discrepanza | targetUser |
      | testACN_4@poste.it | Password1! | GalaxyS10Luca |  123123 | PPINPROIOS-474 | Homepage    | test     | site | Fabiano |         0,0 | pippo      |

    @PPINPROIOS-474_mancini
    Examples: 
      | username          | password   | driver        | posteId | testIdCheck    | landingPage   | language | site | name    | discrepanza | targetUser |
      | prisma21@poste.it | Password1! | GalaxyS10Luca |  123123 | PPINPROIOS-474 | TnkP2pRequest | test     | site | Antonio |         0,0 | pippo      |
