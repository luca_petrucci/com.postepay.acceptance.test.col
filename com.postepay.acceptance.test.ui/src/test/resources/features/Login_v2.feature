@login
Feature: Login-Logout

  #  @L-2
  #  Scenario Outline: Test esempio e prove
  #
  @L-2
  Scenario Outline: Login con Fingerprint
    Given App PP viene avviata
      | testIdJira    | driver   | site   | language   |
      | <testIdCheck> | <driver> | <site> | <language> |
    And Utente mette in modalità aereo il device per quindici minuti
      | driver   |
      | <driver> |

    #	When Utente ottiene css
    #    When  Utente inserisce le credenziali nella login page
    #   | username | password | device | testIdCheck |
    #   | <username> | <password>  | <device> |<testIdCheck>|
    #   And  Utente clicca su accedi
    #   And  Utente inserisce il posteId
    #   | username | password | device | testIdCheck | posteId|
    #   | <username> | <password>  | <device> |<testIdCheck>|<posteId>|
    #   And  Utente clicca sul bottone conferma
    #   Then Utente atterra sulla home page e clicca sul hamburger menu
    #   And Utente clicca su i miei costi
    #   And Utente verifica la presenza del biglietto
    #
    Examples: 
      | username                | password        | driver               | posteId | testIdCheck | language | site | fingerprintID | landingPage |
      #   | vincenzo.fortunato-4869 | D@niela22Lili10| Honor6X | prisma  | PP20AND-1 | test | site | 1 |  Homepage|
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PP20AND-1   | test     | site |             1 | Homepage    |

  ############################################################################################
  # Sugligli Examples ci sono le precondizioni del dispositivo, se richieste
  # Aggiungi le tue righe... NON cancellare le altre
  # Dasta copiare e incollare una stringa di settaggio già presente
  # Successivamente modifica i campi sulle tue esigenze (es. driver)
  # Infine commenta il vecchio settaggio in verde e lascia il tuo specifico
  #
  # Esempio
  #   | luca.petrucci-abmo| NLWbnsx6#st0| GalaxyS8   | prisma | PP20AND-5  | Homepage | test | site |
  @PP20AND-5 @PPINPROAND-356 @PPINPROIOS-367
  Scenario Outline: Login Accesso con credenziali poste.it
    L’utente è già registrato al sito Poste.it ed effettua l’accesso con le proprie credenziali

    Given App PP viene avviata
      | testIdJira    | driver   | site   | language   |
      | <testIdCheck> | <driver> | <site> | <language> |
    When Utente inserisce le credenziali nella login page
      | username   | password   | device   | testIdCheck   | name   | discrepanza   |
      | <username> | <password> | <device> | <testIdCheck> | <name> | <discrepanza> |
    And Utente clicca su accedi
    And Utente inserisce il posteId
      | username   | password   | device   | testIdCheck   | posteId   | name   |
      | <username> | <password> | <device> | <testIdCheck> | <posteId> | <name> |
    And Utente clicca sul bottone conferma
    Then Utente atterra sulla "Homepage"
      | landingPage   |
      | <landingPage> |
    And Utente chiude App PP

    @PPINPROAND-356_musella
    Examples: 
      | username             | password  | driver        | posteId | testIdCheck    | landingPage | language | site | name      | discrepanza |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | PPINPROAND-356 | Homepage    | test     | site | Salvatore |         0,0 |

    @PPINPROAND-356_fortunato
    Examples: 
      | username                | password        | driver        | posteId | testIdCheck    | landingPage | language | site | name     | discrepanza |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PPINPROAND-356 | Homepage    | test     | site | Vincenzo |         0,0 |

    @PPINPROAND-356_sarno
    Examples: 
      | username           | password   | driver        | posteId | testIdCheck    | landingPage | language | site | name    | discrepanza |
      | testACN_4@poste.it | Password1! | GalaxyS10Luca |  123123 | PPINPROAND-356 | Homepage    | test     | site | Fabiano |         0,0 |

    @PPINPROAND-356_romani
    Examples: 
      | username          | password   | driver        | posteId | testIdCheck    | landingPage | language | site | name         | discrepanza |
      | prisma20@poste.it | Password2! | GalaxyS10Luca |  123123 | PPINPROAND-356 | Homepage    | test     | site | Maria Grazia |         0,0 |

    @PPINPROIOS-367_musella
    Examples: 
      | username             | password  | driver               | posteId | testIdCheck    | landingPage | language | site | name      | discrepanza |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | PPINPROIOS-367 | Homepage    | test     | site | Salvatore |         0,0 |

    @PPINPROIOS-367_fortunato
    Examples: 
      | username                | password        | driver        | posteId | testIdCheck    | landingPage | language | site | name     | discrepanza |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PPINPROIOS-367 | Homepage    | test     | site | Vincenzo |         0,0 |

    @PPINPROIOS-367_rimato
    Examples: 
      | username            | password   | driver        | posteId | testIdCheck    | landingPage | language | site | name     | discrepanza |
      | testacn_13@poste.it | Password1! | GalaxyS10Luca |  123123 | PPINPROIOS-367 | Homepage    | test     | site | Giuseppe |         0,0 |

    @PPINPROIOS-367_sarno
    Examples: 
      | username           | password   | driver               | posteId | testIdCheck    | landingPage | language | site | name    | discrepanza |
      | testACN_4@poste.it | Password1! | GalaxyS10Luca |  123123 | PPINPROIOS-367 | Homepage    | test     | site | Fabiano |         0,0 |

  	@PPINPROIOS-367_mancini
    Examples: 
      | username           | password   | driver               | posteId | testIdCheck    | landingPage | language | site | name    | discrepanza |
      | prisma21@poste.it  | Password1! | GalaxyS10Luca |  123123 | PPINPROIOS-367 | Homepage    | test     | site | Antonio |         0,0 |
  
    @PPINPROIOS-367_romani
    Examples: 
      | username          | password   | driver        | posteId | testIdCheck    | landingPage | language | site | name         | discrepanza |
      | prisma20@poste.it | Password2! | GalaxyS10Luca |  123123 | PPINPROAND-356 | Homepage    | test     | site | Maria Grazia |         0,0 |
      
  @PP20AND-449 @PPINPROAND-546 @PPINPROIOS-569
  Scenario Outline: Login senza connessione
    Accedere all'APP senza nessuna connessione di rete attiva e verificare il corretto messaggio di errore.

    Given App PP viene avviata
      | testIdJira    | driver   | site   | language   |
      | <testIdCheck> | <driver> | <site> | <language> |
    When Utente inserisce le credenziali nella login page
      | username   | password   | device   | testIdCheck   | name   | discrepanza   |
      | <username> | <password> | <device> | <testIdCheck> | <name> | <discrepanza> |
    And Utente mette offline il device
      | driver   |
      | <driver> |
    And Utente clicca su accedi da offline
    Then Compare il messaggio di errore per la connessione assente
    And Utente mette online il device
      | driver   |
      | <driver> |
    And Utente chiude App PP

    @PPINPROAND-546_fortunato
    Examples: 
      | username                | password        | driver        | posteId | testIdCheck    | language | site | name     | discrepanza |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PPINPROAND-546 | test     | site | Vincenzo |         0,0 |

    @PPINPROAND-546_musella
    Examples: 
      | username             | password  | driver               | posteId | testIdCheck    | language | site | name      | discrepanza |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | PPINPROAND-546 | test     | site | Salvatore |         0,0 |

    @PPINPROAND-546_sarno
    Examples: 
      | username           | password   | driver        | posteId | testIdCheck    | language | site | name    | discrepanza |
      | testACN_4@poste.it | Password1! | GalaxyS10Luca |  123123 | PPINPROAND-546 | test     | site | Fabiano |         0,0 |

    @PPINPROAND-546_romani
    Examples: 
      | username          | password   | driver        | posteId | testIdCheck    | language | site | name         | discrepanza |
      | prisma20@poste.it | Password2! | GalaxyS10Luca |  123123 | PPINPROAND-546 | test     | site | Maria Grazia |         0,0 |

    @PPINPROAND-546_muto
    Examples: 
      | username       | password    | driver        | posteId | testIdCheck    | language | site | name      | discrepanza |
      | francesco.muto | Prisma@2019 | GalaxyS10Luca | prisma  | PPINPROAND-546 | test     | site | Salvatore |         0,0 |

    @PPINPROIOS-569_fortunato
    Examples: 
      | username                | password        | driver               | posteId | testIdCheck    | language | site | name     | discrepanza |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PPINPROIOS-569 | test     | site | Vincenzo |         0,0 |

    @PPINPROIOS-569_musella
    Examples: 
      | username             | password  | driver               | posteId | testIdCheck    | language | site | name      | discrepanza |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | PPINPROIOS-569 | test     | site | Salvatore |         0,0 |

    @PPINPROIOS-569_muto
    Examples: 
      | username       | password    | driver        | posteId | testIdCheck    | language | site | name      | discrepanza |
      | francesco.muto | Prisma@2019 | GalaxyS10Luca | prisma  | PPINPROIOS-569 | test     | site | Salvatore |         0,0 |

    @PPINPROIOS-569_rimato
    Examples: 
      | username            | password   | driver        | posteId | testIdCheck    | landingPage | language | site | name     | discrepanza |
      | testacn_13@poste.it | Password1! | GalaxyS10Luca |  123123 | PPINPROIOS-569 | Homepage    | test     | site | Giuseppe |         0,0 |

    @PPINPROIOS-569_sarno
    Examples: 
      | username           | password   | driver               | posteId | testIdCheck    | landingPage | language | site | name    | discrepanza |
      | testACN_4@poste.it | Password1! | GalaxyS10Luca |  123123 | PPINPROIOS-569 | Homepage    | test     | site | Fabiano |         0,0 |

 		@PPINPROIOS-569_mancini
    Examples: 
      | username           | password   | driver               | posteId | testIdCheck    | landingPage | language | site | name    | discrepanza |
      | prisma21@poste.it  | Password1! | GalaxyS10Luca |  123123 | PPINPROIOS-569 | Homepage    | test     | site | Antonio |         0,0 |
  
  @PP20AND-310 @PPINPROAND-519 @PPINPROIOS-534
  Scenario Outline: Login con posteID dopo App in background ed in modalita aereo per 15 minuti
    App in background ed in modalità aereo per 15 minuti

    Given App PP viene avviata
      | testIdJira    | driver   | site   | language   |
      | <testIdCheck> | <driver> | <site> | <language> |
    When Utente inserisce le credenziali nella login page
      | username   | password   | device   | testIdCheck   | name   | discrepanza   |
      | <username> | <password> | <device> | <testIdCheck> | <name> | <discrepanza> |
    And Utente clicca su accedi
    And Utente inserisce il posteId
      | username   | password   | device   | testIdCheck   | posteId   | name   |
      | <username> | <password> | <device> | <testIdCheck> | <posteId> | <name> |
    And Utente clicca sul bottone conferma
    Then Utente atterra sulla "Homepage"
      | landingPage   |
      | <landingPage> |
    And Utente mette in modalità aereo il device per quindici minuti
      | driver   |
      | <driver> |
    And Utente mette online il device da modalita aerea
      | driver   |
      | <driver> |
    And Utente esegue il resume del App
      | driver   |
      | <driver> |
    Then Utente atterra sulla pagina del posteId dopo il resume
      | discrepanza   |
      | <discrepanza> |
    And Utente chiude App PP

    @PPINPROAND-519_fortunato
    Examples: 
      | username                | password        | driver        | posteId | testIdCheck    | language | site | name     | discrepanza |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PPINPROAND-519 | test     | site | Vincenzo |         0,0 |

    @PPINPROAND-519_musella
    Examples: 
      | username             | password  | driver               | posteId | testIdCheck | language | site | name      | discrepanza |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | PP20AND-310 | test     | site | Salvatore |         0,0 |

    @PPINPROIOS-534_fortunato
    Examples: 
      | username                | password        | driver        | posteId | testIdCheck    | language | site | name     | discrepanza |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PPINPROIOS-534 | test     | site | Vincenzo |         0,0 |

    @PPINPROIOS-534_musella
    Examples: 
      | username             | password  | driver               | posteId | testIdCheck    | language | site | name      | discrepanza |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | PPINPROIOS-534 | test     | site | Salvatore |         0,0 |

  #   | vincenzo.fortunato-4869 | D@niela22Lili10| GalaxyS10Luca   | prisma  | PP20AND-310 | test     | site |  Vincenzo | 0,75 |
  #| luca.petrucci-abmo | NLWbnsx6#st0 | GalaxyS10Luca | prisma  | PP20AND-310 | test     | site | Luca |        0,75 |
  #   | gennaro.rega-1784 | Captur04$ | Honor6X | prisma  | PP20AND-310 | test | site |   Gennaro | 0,75 |
  #   | luca.petrucci-abmo | NLWbnsx6#st0 | OnePlus | prisma  | PP20AND-310 | test     | site | Luca |        0,75 |
  @PP20AND-308 @PPINPROAND-518 @PPINPROIOS-532
  Scenario Outline: Login con posteID dopo App in background 15 minuti
    lasciare l'app inattiva in background per 15 minuti

    Given App PP viene avviata
      | testIdJira    | driver   | site   | language   |
      | <testIdCheck> | <driver> | <site> | <language> |
    When Utente inserisce le credenziali nella login page
      | username   | password   | device   | testIdCheck   | name   | discrepanza   |
      | <username> | <password> | <device> | <testIdCheck> | <name> | <discrepanza> |
    And Utente clicca su accedi
    And Utente inserisce il posteId
      | posteId   | name   |
      | <posteId> | <name> |
    And Utente clicca sul bottone conferma
    Then Utente atterra sulla "Homepage"
      | landingPage   |
      | <landingPage> |
    And Utente mette in background il device per quindici minuti
      | driver   |
      | <driver> |
    Then Utente atterra sulla pagina del posteId dopo il resume
      | discrepanza   |
      | <discrepanza> |
    And Utente chiude App PP

    @PPINPROAND-518_fortunato
    Examples: 
      | username                | password        | driver        | posteId | testIdCheck    | language | site | name     | discrepanza |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PPINPROAND-518 | test     | site | Vincenzo |         0,0 |

    @PPINPROAND-518_musella
    Examples: 
      | username             | password  | driver               | posteId | testIdCheck    | language | site | name      | discrepanza |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | PPINPROAND-518 | test     | site | Salvatore |         0,0 |

    @PPINPROIOS-532_fortunato
    Examples: 
      | username                | password        | driver        | posteId | testIdCheck    | language | site | name     | discrepanza |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PPINPROIOS-532 | test     | site | Vincenzo |         0,0 |

    @PPINPROIOS-532_musella
    Examples: 
      | username             | password  | driver               | posteId | testIdCheck    | language | site | name      | discrepanza |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | PPINPROIOS-532 | test     | site | Salvatore |         0,0 |

  #   | vincenzo.fortunato-4869| D@niela22Lili10| GalaxyS8   | prisma  | PP20AND-308 |  test 	  | site |  Vincenzo | 0,75 |
  #   | luca.petrucci-abmo		 | NLWbnsx6#st0	  | GalaxyS8   | prisma  | PP20AND-308 |  test    | site | Luca | 0,75 |
  #   | gennaro.rega-1784 | Captur04$ | Honor6X | prisma  | PP20AND-308 |  test    | site | Gennaro | 0,75 |
  #   | luca.petrucci-abmo		 | NLWbnsx6#st0	  | OnePlus   | prisma  | PP20AND-308 |  test    | site | Luca | 0,75 |
  @PP20AND-659 @PPINPROAND-576 @PPINPROIOS-589
  Scenario Outline: Login con PosteID
    Dopo il primo accesso in App per l’utente che ha un Wallet valido viene attivata automaticamente la login attraverso l’uso del PosteID.

    Given App PP viene avviata
      | testIdJira    | driver   | site   | language   |
      | <testIdCheck> | <driver> | <site> | <language> |
    And Utente inserisce le credenziali nella login page
      | username   | password   | device   | testIdCheck   | name   | discrepanza   |
      | <username> | <password> | <device> | <testIdCheck> | <name> | <discrepanza> |
    And Utente esegue la login in App
      | username   | password   | device   | testIdCheck   | name   | discrepanza   | posteId   |
      | <username> | <password> | <device> | <testIdCheck> | <name> | <discrepanza> | <posteId> |
    And Utente effettua il logout dall app
    When App PP viene avviata
      | testIdJira    | driver   | site   | language   |
      | <testIdCheck> | <driver> | <site> | <language> |
    And Utente atterra sulla pagina del posteId
    And Utente inserisce il posteId
      | username   | password   | device   | testIdCheck   | posteId   | name   |
      | <username> | <password> | <device> | <testIdCheck> | <posteId> | <name> |
    And Utente clicca sul bottone conferma
    Then Utente atterra sulla "Homepage"
      | landingPage   |
      | <landingPage> |
    And Utente chiude App PP

    @PPINPROAND-576_fortunato
    Examples: 
      | username                | password        | driver        | posteId | testIdCheck    | landingPage | language | site | name     | discrepanza |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PPINPROAND-576 | Homepage    | test     | site | Vincenzo |         0,0 |

    @PPINPROAND-576_musella
    Examples: 
      | username             | password  | driver               | posteId | testIdCheck | landingPage | language | site | name      | discrepanza |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | PP20AND-659 | Homepage    | test     | site | Salvatore |         0,0 |

    @PPINPROIOS-589_fortunato
    Examples: 
      | username                | password        | driver        | posteId | testIdCheck    | landingPage | language | site | name     | discrepanza |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PPINPROIOS-589 | Homepage    | test     | site | Vincenzo |         0,0 |

    @PPINPROIOS-589_musella
    Examples: 
      | username             | password  | driver               | posteId | testIdCheck    | landingPage | language | site | name      | discrepanza |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | PPINPROIOS-589 | Homepage    | test     | site | Salvatore |         0,0 |

  #   | vincenzo.fortunato-4869| D@niela22Lili10| RedMiNote8   | prisma | PP20AND-659  | Homepage| test | site |  Vincenzo | 0,75 |
  #| luca.petrucci-abmo | NLWbnsx6#st0 | GalaxyS10Luca | prisma  | PP20AND-659 | Homepage    | test     | site | Luca |        0,75 |
  @PP20AND-443 @PPINPROAND-545
  Scenario Outline: Nuova login - Verifica che non e possibile accedere alApp con credenziali SPID
    Verificare che il servizio non supporta più PosteID abilitato a SPID. Per accedere devi utilizzare le credenziali di Poste.

    Given App PP viene avviata
      | testIdJira    | driver   | site   | language   |
      | <testIdCheck> | <driver> | <site> | <language> |
    When Utente inserisce le credenziali nella login page
      | username   | password   | device   | testIdCheck   | name   | discrepanza   |
      | <username> | <password> | <device> | <testIdCheck> | <name> | <discrepanza> |
    And Utente clicca su accedi
    And Compare il pop-up che informa non si può accedere con le credenziali SPID
    And Utente clicca su Ok
    And Utente clicca su accedi
    And Compare il pop-up che informa non si può accedere con le credenziali SPID
    And Utente clicca su Non ho un account Poste
    Then Utente atterra sulla "WebView"
      | landingPage   |
      | <landingPage> |
    And Utente chiude App PP

    @PPINPROAND-545_fortunato
    Examples: 
      | username                | password        | driver        | posteId | testIdCheck    | landingPage | language | site | name | discrepanza |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PPINPROAND-545 | WebView     | test     | site | Luca |         0,0 |

    @PPINPROAND-545_musella
    Examples: 
      | username             | password  | driver               | posteId | testIdCheck    | landingPage | language | site | name | discrepanza |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | PPINPROAND-545 | WebView     | test     | site | Luca |         0,0 |

  #   | petrucci.luca92@gmail.com| NLWbnsx6#st0nlw | RedMiNote8   | prisma |PP20AND-443| WebView | test | site |  Luca | 0,75 |
  @PP20AND-1 @PPINPROAND-353
  Scenario Outline: Login con Fingerprint
    L’utente è già registrato al sito Poste.it ed effettua l’accesso con fingerprint

    Given App PP viene avviata
      | testIdJira    | driver   | site   | language   |
      | <testIdCheck> | <driver> | <site> | <language> |
    When Utente inserisce le credenziali nella login page
      | username   | password   | device   | testIdCheck   | name   | discrepanza   |
      | <username> | <password> | <device> | <testIdCheck> | <name> | <discrepanza> |
    And Utente clicca su accedi
    And Utente inserisce il posteId
      | username   | password   | device   | testIdCheck   | posteId   | name   |
      | <username> | <password> | <device> | <testIdCheck> | <posteId> | <name> |
    And Utente clicca sul bottone conferma
    Then Utente atterra sulla home page e clicca sul hamburger menu
    And Utente clicca su impostazioni
    And Utente accede alla sezione Accessi e Autorizzazioni
    And Utente imposta su "si" accesso con impronta
    And Utente clicca su ABILITA al popup abilita impronta digitale
    And Utente inserisce il posteID
      | posteId   |
      | <posteId> |
    And Utente inserisce il finger print per l'abilitazione
      | fingerprintID   |
      | <fingerprintID> |
    And Utente esce dalle impostazioni
    And Utente effettua la logout
    And Utente effettua l'accesso con fingerprint
      | fingerprintID   |
      | <fingerprintID> |
    And Utente atterra sulla home page e clicca sul hamburger menu
    And Utente clicca su impostazioni
    And Utente accede alla sezione Accessi e Autorizzazioni
    And Utente imposta su "no" accesso con impronta
    And Utente inserisce il posteID
      | posteId   |
      | <posteId> |
    And Utente esce dalle impostazioni
    #   And Utente effettua la logout
    And Utente chiude App PP

    @PPINPROAND-353_fortunato
    Examples: 
      | username                | password        | driver        | posteId | testIdCheck    | language | site | fingerprintID | landingPage | name     | discrepanza |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PPINPROAND-353 | test     | site |             1 | Homepage    | Vincenzo |         0,0 |
