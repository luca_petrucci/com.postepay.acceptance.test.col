@PagoPA
Feature: PagoPA
  Componente PagoPA app PostePay

  @PPINPROAND-3076 @PPINPROIOS-3392
  Scenario Outline: Layout pagina di compilazione Pagamento PagoPA_Banche e altri canali
    Verificare il layout della pagina Pagamento PagoPA con il tab Banche e altri canali abilitato
    
    Given App PP viene avviata
      | testIdJira    | driver   | site   | language   |
      | <testIdCheck> | <driver> | <site> | <language> |
    When Utente inserisce le credenziali nella login page
      | username   | password   | device   | testIdCheck   | name   | discrepanza   |
      | <username> | <password> | <device> | <testIdCheck> | <name> | <discrepanza> |
    And Utente clicca su accedi
    And Utente inserisce il posteId
      | username   | password   | device   | testIdCheck   | posteId   | name   |
      | <username> | <password> | <device> | <testIdCheck> | <posteId> | <name> |
    And Utente clicca sul bottone conferma
    And Utente sulla homepage clicca sul tab PAGA
    And Utente clicca su PAGAMENTI
    And Utente clicca su PagoPA
    And Utente seleziona la carta da utilizzare per il pagamento su "targetOperation"
      | targetOperation   | cardNumber   | discrepanza   | userNumberCards   | name |
      | <targetOperation> | <cardNumber> | <discrepanza> | <userNumberCards> | <name> |
    And Utente seleziona la tipologia di bollettino
      | comment   |
      | <comment> |
    And Utente clicca sul tab "BANCHE"
    And Utente verifica il layout della pagina PagoPA per Banche
    And Utente chiude App PP

    @PPINPROAND-3076_fortunato
    Examples: 
      | username                | password        | driver        | posteId | cardNumber | myIban                      | iban                        | targetUser          | testIdCheck     | site | language | discrepanza | name     | userNumberCards | targetOperation | comment       |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | **** 4679  | IT70M3608105138207391907401 | IT36I0301503200000006018757 | Carmine Di Vincenzo | PPINPROAND-3076 | test | site     |         0,0 | Vincenzo |               2 | PagoPa          | Avvisi pagoPA |

    @PPINPROAND-3076_musella
    Examples: 
      | username             | password  | driver               | posteId | cardNumber | myIban                      | iban                        | targetUser          | testIdCheck     | site | language | discrepanza | name      | userNumberCards | targetOperation | comment       |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | **** 7177  | IT15E3608105138271495971507 | IT36I0301503200000006018757 | Carmine Di Vincenzo | PPINPROAND-3076 | test | site     |         0,0 | Salvatore |               2 | PagoPa          | Avvisi pagoPA |

		@PPINPROAND-3076_sarno
    Examples: 
      | username           | password   | driver        | posteId | cardNumber | myIban                      | iban                        | targetUser      | importo | comment       | testIdCheck     | landingPage | language | site | name    | discrepanza |targetOperation |userNumberCards |
      | testACN_4@poste.it | Password1! | GalaxyS10Luca | 123123  | **** 1525  | IT49T3608105138216577216583 | IT23P3608105138238935238941 | Giuseppe Rimato |    0.01 | Avvisi pagoPA | PPINPROAND-3076 | TnkGeneric  |  test     | site | Fabiano |         0,0 |PagoPa       |              1 |
	
	@PPINPROAND-3076_romani
    Examples: 
      | username          | password   | driver        | posteId | cardNumber | myIban                      | iban                        | targetUser      | importo | comment       | testIdCheck     | landingPage | language | site | name    | discrepanza |targetOperation |userNumberCards |
      | prisma20@poste.it | Password2! | GalaxyS10Luca | 123123  | **** 9007  | IT13G0760103200001009187830 | IT81B0760103200001009187798 | Antonio Mancini |    1.01 | Avvisi pagoPA | PPINPROAND-3076 | TnkGeneric  |  test     | site | Maria Grazia |         0,0 |PagoPa       |              2 |
	
    @PPINPROIOS-3392_fortunato
    Examples: 
      | username                | password        | driver        | posteId | cardNumber | myIban                      | iban                        | targetUser          | testIdCheck     | site | language | discrepanza | name     | userNumberCards | targetOperation | comment       |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | **** 4679  | IT70M3608105138207391907401 | IT36I0301503200000006018757 | Carmine Di Vincenzo | PPINPROIOS-3392 | test | site     |         0,0 | Vincenzo |               2 | PagoPa          | Avvisi pagoPA |


    @PPINPROIOS-3392_musella
    Examples: 
      | username             | password  | driver               | posteId | cardNumber | myIban                      | iban                        | targetUser          | testIdCheck     | site | language | discrepanza | name      | userNumberCards | targetOperation | comment       |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | **** 7177  | IT15E3608105138271495971507 | IT36I0301503200000006018757 | Carmine Di Vincenzo | PPINPROIOS-3392 | test | site     |         0,0 | Salvatore |               2 | PagoPa          | Avvisi pagoPA |

    @PPINPROIOS-3392_rimato
   	Examples: 
      | username            | password   | driver        | posteId | cardNumber | myIban                      | iban                        | targetUser      | importo | comment    | testIdCheck     | landingPage | language  | site | name     | discrepanza | targetOperation |userNumberCards |
      | testacn_13@poste.it | Password1! | GalaxyS10Luca | 123123  | **** 1566  | IT23P3608105138238935238941 | IT49T3608105138216577216583 | Fabiano Sarno   |    0.01 | Avvisi pagoPA | PPINPROIOS-3392 | TnkGeneric  |  test     | site | Giuseppe |         0,0 | PagoPa           |              1 |
      
     @PPINPROIOS-3392_sarno
    Examples: 
      | username           | password   | driver        | posteId | cardNumber | myIban                      | iban                        | targetUser      | importo | comment       | testIdCheck     | landingPage | language | site | name    | discrepanza |targetOperation |userNumberCards |
      | testACN_4@poste.it | Password1! | GalaxyS10Luca | 123123  | **** 1525  | IT49T3608105138216577216583 | IT23P3608105138238935238941 | Giuseppe Rimato |    0.01 | Avvisi pagoPA | PPINPROIOS-3392 | TnkGeneric  |  test     | site | Fabiano |         0,0 |PagoPa       |              1 |
      
     @PPINPROIOS-3392_mancini
    Examples: 
      | username           | password   | driver        | posteId | cardNumber | myIban                      | iban                        | targetUser      | importo | comment       | testIdCheck     | landingPage | language  | site | name    | discrepanza |targetOperation |userNumberCards |
      | prisma21@poste.it  | Password1! | GalaxyS10Luca | 123123  | **** 9015  | IT49T3608105138216577216583 | IT23P3608105138238935238941 | Giuseppe Rimato |    0.01 | Avvisi pagoPA | PPINPROIOS-3392 | TnkGeneric  |  test     | site | Antonio |         0,0 |PagoPa       |              2 |