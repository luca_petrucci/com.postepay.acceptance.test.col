@mappe
Feature: Mappe

  @PPINPROAND-630
  Scenario Outline: Verifica il pulsante Apri dettaglio dalla sezione Sconti
  Verifica che il pulsante Apri dettaglio accede alla sezione SCONTI

    Given App PP viene avviata
    | testIdJira    | driver   | site   | language   |
    | <testIdCheck> | <driver> | <site> | <language> |
    When Utente inserisce le credenziali nella login page
    | username   | password   | device   | testIdCheck   | name   | discrepanza   |
    | <username> | <password> | <device> | <testIdCheck> | <name> | <discrepanza> |
    And Utente esegue la login in App
    | username   | password   | device   | testIdCheck   | name   | discrepanza   | posteId   | 
    | <username> | <password> | <device> | <testIdCheck> | <name> | <discrepanza> | <posteId> |
    And Utente clicca sul Tab Mappe
    And Viene mostrato il popup di abilita GPS
    And Utente attiva il GPS del device dalla pagina del device
    And Utente clicca sul bottone Apri Dettaglio
    Then Utente atterra nella pagina di ScontiPoste
	And Utente chiude App PP
	
   @PPINPROAND-630_fortunato
   Examples: 
   | username                | password        | driver        | posteId | testIdCheck    | language | site | name     | discrepanza |
   | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PPINPROAND-630 | test     | site | Vincenzo |        0,0  |
   
   @PPINPROAND-630_musella
   Examples: 
   | username             | password  | driver              | posteId | testIdCheck    | language | site | name      | discrepanza |
   | musellasal@gmail.com | Musabp67! | GalaxyS10Luca| prisma  | PPINPROAND-630 | test     | site | Salvatore |        0,0  |
   
  
  @PPINPROAND-628
  Scenario Outline: Pagamento con Codice QR dalla sezione Mappe - Utente con una sola PP
  Pagamento con Codice QR dalla sezione Mappe Utente con una sola PP

    Given App PP viene avviata
    | testIdJira    | driver   | site   | language   |
    | <testIdCheck> | <driver> | <site> | <language> |
    When Utente inserisce le credenziali nella login page
    | username   | password   | device   | testIdCheck   | name   | discrepanza   |
    | <username> | <password> | <device> | <testIdCheck> | <name> | <discrepanza> |
    And Utente esegue la login in App
    | username   | password   | device   | testIdCheck   | name   | discrepanza   | posteId   | 
    | <username> | <password> | <device> | <testIdCheck> | <name> | <discrepanza> | <posteId> |
    And Utente clicca sul Tab Mappe
    And Utente clicca su Codice QR e visualizza i merchant
    | driver   |
    | <driver> |
    And Utente clicca sul Paga del primo merchant in lista e si visualizza la pagina di inserimento importo
    And Utente clicca sul cta X e viene riportato alla schermata precedente
    And Utente clicca sul Paga del primo merchant in lista e si visualizza la pagina di inserimento importo
    And Utente inserisce l importo e clicca su paga il merchant
    | importo   |
    | <importo> |
    And Utente verifica il riepilogo del pagamento del merchant
    | cardNumber   |
    | <cardNumber> |
    And Utente clicca sul bottone < 
    And Utente clicca sul cta X e viene riportato alla schermata precedente
    And Utente clicca sul Paga del primo merchant in lista e si visualizza la pagina di inserimento importo
    And Utente inserisce l importo e clicca su paga il merchant
    | importo   |
    | <importo> |
    And Utente verifica il riepilogo del pagamento del merchant
    | cardNumber   |
    | <cardNumber> |
    And Utente verifica che l icona della matita non sia presente nel campo paga
    | userNumberCards   |
    | <userNumberCards> |
    And Utente clicca su Annulla
  	And Utente clicca sul Paga del primo merchant in lista e si visualizza la pagina di inserimento importo
    And Utente inserisce l importo e clicca su paga il merchant
    | importo   |
    | <importo> |
    And Utente verifica il riepilogo del pagamento del merchant 
    | cardNumber   |
    | <cardNumber> |
    Then Utente clicca su PAGA il merchant
    And Utente clicca su < dalla scermata di inserimento PosteId
    | posteId   | 
    | <posteId> | 
    And Utente chiude App PP
    
  @PPINPROAND-628_fortunato
   Examples: 
   | username                | password        | driver        | posteId | testIdCheck    | language | site | name     | discrepanza |  importo | cardNumber | userNumberCards |
   | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PPINPROAND-628 | test     | site | Vincenzo |        0,0  |  1,00    | **** 4679  | 2               |
    
  @PPINPROAND-628_musella
   Examples: 
   | username             | password  | driver               | posteId | testIdCheck    | language | site | name      | discrepanza |  importo | cardNumber | userNumberCards |
   | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | PPINPROAND-628 | test     | site | Salvatore |        0,0  |  1,00    | **** 7177  | 1               |
  
  
    @PPINPROAND-529 @PPINPROIOS-547
    Scenario Outline: Verifica del layout della pagina Mappe
    Utente verivica il layout della sezione TROVA

    Given App PP viene avviata
      | testIdJira    | driver   | site   | language   |
      | <testIdCheck> | <driver> | <site> | <language> |
    When Utente inserisce le credenziali nella login page
      | username   | password   | device   | testIdCheck   | name   | discrepanza   |
      | <username> | <password> | <device> | <testIdCheck> | <name> | <discrepanza> |
    And Utente esegue la login in App
      | username   | password   | device   | testIdCheck   | name   | discrepanza   | posteId   |
      | <username> | <password> | <device> | <testIdCheck> | <name> | <discrepanza> | <posteId> |
    And Utente clicca sul Tab Mappe
    And Viene mostrato il popup di abilita GPS
    And Utente attiva il GPS del device dalla pagina del device
    And Utente verifica il layout della sezione VICINO A TE
    And Utente verifica il corretto funzionamento della mappa della sezione VICINO A TE
#    And Utente seleziona la voce Mappa e la visualizza per ogni sezione
    And Utente chiude App PP

    @PPINPROAND-529_fortunato
    Examples: 
      | username                | password        | driver        | posteId | testIdCheck    | language | site | name     | discrepanza |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PPINPROAND-529 | test     | site | Vincenzo |         0,0 |

    @PPINPROAND-529_musella
    Examples: 
      | username             | password  | driver               | posteId | testIdCheck    | language | site | name      | discrepanza |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | PPINPROAND-529 | test     | site | Salvatore |         0,0 |
  
   @PPINPROIOS-547_fortunato
    Examples: 
      | username                | password        | driver        | posteId | testIdCheck    | language | site | name     | discrepanza |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PPINPROAND-529 | test     | site | Vincenzo |         0,0 |
      
  @PPINPROIOS-547_musella
    Examples: 
      | username             | password  | driver               | posteId | testIdCheck    | language | site | name      | discrepanza |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | PPINPROIOS-547 | test     | site | Salvatore |         0,0 |
  
  
   @PPINPROAND-2892
  Scenario Outline: Mappe - accedere alla schermata ScontiPoste

    Given App PP viene avviata
    | testIdJira    | driver   | site   | language   |
    | <testIdCheck> | <driver> | <site> | <language> |
    When Utente inserisce le credenziali nella login page
    | username   | password   | device   | testIdCheck   | name   | discrepanza   |
    | <username> | <password> | <device> | <testIdCheck> | <name> | <discrepanza> |
    And Utente esegue la login in App
    | username   | password   | device   | testIdCheck   | name   | discrepanza   | posteId   | 
    | <username> | <password> | <device> | <testIdCheck> | <name> | <discrepanza> | <posteId> |
    And Utente clicca sul Tab Mappe
    And Viene mostrato il popup di abilita GPS
    And Utente attiva il GPS del device dalla pagina del device
    And Utente clicca sul toggle "MAPPE"
    And Utente clicca sul toggle "LISTA"
    And Utente clicca sul toggle "MAPPE"
    And Utente clicca sul bottone Apri Dettaglio
    Then Utente atterra nella pagina di ScontiPoste
    And Utente controlla la pagina di ScontiPoste
    And Utente clicca sul bottone <
    And Utente controlla che la pagina Mappe sia corretta 
	  And Utente chiude App PP
	
   @PPINPROAND-2892_fortunato
   Examples: 
   | username                | password        | driver        | posteId | testIdCheck    | language | site | name     | discrepanza |
   | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PPINPROAND-2892 | test     | site | Vincenzo |        0,0  |
   
   @PPINPROAND-2892_musella
   Examples: 
   | username             | password  | driver              | posteId | testIdCheck    | language | site | name      | discrepanza |
   | musellasal@gmail.com | Musabp67! | GalaxyS10Luca| prisma  | PPINPROAND-2892 | test     | site | Salvatore |        0,0  |
   
  