@Notifiche
Feature: Notifiche

  @PPINPROAND-682 @PPINPROIOS-688
  Scenario Outline: Richiesta di autorizzazione dall entry point Notifiche e autorizzazioni - Esito OK
    Richiesta di autorizzazione dall entry point Lista delle autorizzazioni - Esito OK

    Given App PP viene avviata
      | testIdJira    | driver   | site   | language   |
      | <testIdCheck> | <driver> | <site> | <language> |
    When Utente inserisce le credenziali nella login page
      | username   | password   | device   | testIdCheck   | name   | discrepanza   |
      | <username> | <password> | <device> | <testIdCheck> | <name> | <discrepanza> |
    And Utente esegue la login in App
      | username   | password   | device   | testIdCheck   | name   | discrepanza   | posteId   |
      | <username> | <password> | <device> | <testIdCheck> | <name> | <discrepanza> | <posteId> |
    And Utente atterra sulla home page e clicca sul hamburger menu
    When Utente clicca su impostazioni
    And Utente clicca su Notifiche
    And Utente verifica che il device sia il preferito e torna in HomePage
      | posteId   |
      | <posteId> |
    And Utente accede al portale BPOL e inserisce le credenziali di accesso
      | username   | password   | cardNumber   |
      | <username> | <password> | <cardNumber> |
    And Utente accede all area personale della carta PostePay
      | cardNumber   |
      | <cardNumber> |
#    And Utente verifica la sua identita tramite app PostePay
    And Utente clicca sull icona a campanello delle bacheca
    And Utente seleziona il tab Notifiche e Autorizzazione
    And Utente inserisce la parola da cercare nel campo cerca
      | comment   |
      | <comment> |
    And Utente clicca sul messaggio di autorizzazione
    Then Utente atterra sulla pagina di autorizzazione web
    And Utente autorizza l accesso al portale BPOL per PostePay
    And Utente inserisce il posteId durante la "targetOperation" e clicca conferma
      | posteId   | targetOperation   |
      | <posteId> | <targetOperation> |
    And Utente verifica l accesso sul portale BPOL
      | cardNumber   |
      | <cardNumber> |
    And Utente chiude App PP

    @PPINPROAND-682_fortunato
    Examples: 
      | username                | password        | driver        | posteId | testIdCheck    | language | site | name     | discrepanza | userNumberCards | targetOperation    | cardNumber | comment        |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PPINPROAND-682 | test     | site | Vincenzo |         0,0 |               2 | AutorizzazioneBPOL | **** 4679  | Autorizzazione |

    @PPINPROAND-682_musella
    Examples: 
      | username             | password  | driver               | posteId | testIdCheck    | language | site | name      | discrepanza | userNumberCards | targetOperation    | cardNumber | comment        |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | PPINPROAND-682 | test     | site | Salvatore |         0,0 |               2 | AutorizzazioneBPOL | **** 7177  | Autorizzazione |

	@PPINPROAND-682_romani
    Examples: 
      | username          | password   | driver               | posteId | testIdCheck    | language | site | name         | discrepanza | userNumberCards | targetOperation    | cardNumber | comment        |
      | prisma20@poste.it | Password2! | GalaxyS10Luca | 123123  | PPINPROAND-682 | test     | site | Maria Grazia |         0,0 |               2 | AutorizzazioneBPOL | **** 9007  | Autorizzazione |

    @PPINPROAND-682_muto
    Examples: 
      | username       | password    | driver        | posteId | testIdCheck    | language | site | name     | discrepanza | userNumberCards | targetOperation    | cardNumber | comment        |
      | francesco.muto | Prisma@2019 | GalaxyS10Luca | prisma  | PPINPROAND-682 | test     | site | Vincenzo |         0,0 |               1 | AutorizzazioneBPOL | **** 4950  | Autorizzazione |

    @PPINPROIOS-688_musella
    Examples: 
      | username             | password  | driver               | posteId | testIdCheck    | language | site | name      | discrepanza | userNumberCards | targetOperation    | cardNumber | comment        |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | PPINPROIOS-688 | test     | site | Salvatore |         0,0 |               2 | AutorizzazioneBPOL | **** 7177  | Autorizzazione |

  @PPINPROAND-366
  Scenario Outline: Notifica in uscita ricevuta in app PP dopo transazione da carta su PP (wallet solo su PP)
    Given App PP viene avviata
      | testIdJira    | driver   | site   | language   |
      | <testIdCheck> | <driver> | <site> | <language> |
    When Utente inserisce le credenziali nella login page
      | username   | password   | device   | testIdCheck   | name   | discrepanza   |
      | <username> | <password> | <device> | <testIdCheck> | <name> | <discrepanza> |
    And Utente esegue la login in App
      | username   | password   | device   | testIdCheck   | name   | discrepanza   | posteId   |
      | <username> | <password> | <device> | <testIdCheck> | <name> | <discrepanza> | <posteId> |
    And  Utente atterra sulla home page e clicca sul hamburger menu
    When Utente clicca su impostazioni
    And Utente clicca su Notifiche
    And Utente verifica che il device sia il preferito e torna in HomePage
    | posteId |
    |<posteId>|
    And Utente accede al portale BPOL e inserisce le credenziali di accesso
      | username   | password   |
      | <username> | <password> |
    And Utente accede all area personale della carta PostePay
      | cardNumber   |
      | <cardNumber> |
    And Utente verifica la sua identita tramite app PostePay
    And Utente clicca notifica push
    And Utente autorizza l accesso al portale BPOL per PostePay

    @PPINPROAND-366_fortunato
    Examples: 
      | username                | password        | driver        | posteId | testIdCheck    | language | site | name     | discrepanza | userNumberCards | targetOperation    | cardNumber | comment        |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PPINPROAND-366 | test     | site | Vincenzo |         0,0 |               2 | AutorizzazioneBPOL | **** 4679  | Autorizzazione |

    @PPINPROAND-366_musella
    Examples: 
      | username             | password  | driver               | posteId | testIdCheck    | language | site | name      | discrepanza | userNumberCards | targetOperation    | cardNumber | comment        |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | PPINPROAND-366 | test     | site | Salvatore |         0,0 |               2 | AutorizzazioneBPOL | **** 7177  | Autorizzazione |

    @PPINPROAND-366_muto
    Examples: 
      | username       | password    | driver        | posteId | testIdCheck    | language | site | name     | discrepanza | userNumberCards | targetOperation    | cardNumber | comment        |
      | francesco.muto | Prisma@2019 | GalaxyS10Luca | prisma  | PPINPROAND-366 | test     | site | Vincenzo |         0,0 |               1 | AutorizzazioneBPOL | **** 4950  | Autorizzazione |
