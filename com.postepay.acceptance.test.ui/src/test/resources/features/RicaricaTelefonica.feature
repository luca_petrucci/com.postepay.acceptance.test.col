Feature: Ricarica

   @PPINPROAND-358 @PPINPROIOS-369
  Scenario Outline: Ricarica Telefonica
    Effetuare una ricarica telefonica inserendo i dati manualmente

    Given App PP viene avviata
      | testIdJira    | driver   | site   | language   |
      | <testIdCheck> | <driver> | <site> | <language> |
    When Utente inserisce le credenziali nella login page
      | username   | password   | device   | testIdCheck   | name   | discrepanza   |
      | <username> | <password> | <device> | <testIdCheck> | <name> | <discrepanza> |
    And Utente clicca su accedi
    And Utente inserisce il posteId
      | username   | password   | device   | testIdCheck   | posteId   | name   |
      | <username> | <password> | <device> | <testIdCheck> | <posteId> | <name> |
    And Utente clicca sul bottone conferma
    And Utente sulla homepage clicca sul tab PAGA
    And Utente clicca su "gestoreRicarica"
      | gestoreRicarica   | cardNumber   | userNumberCards   |
      | <gestoreRicarica> | <cardNumber> | <userNumberCards> |
    And Utente inserisce il numero di telefono da ricaricare
      | numeroTel   |
      | <numeroTel> |
    And Utente verifica che il campo operatore e valorizzato e seleziona importo e clicca su procedi
      | importo   | gestoreRicarica   |
      | <importo> | <gestoreRicarica> |
    And Utente verifica il riepilogo della ricarica telefonica e clicca su paga
      | importo   | gestoreRicarica   | numeroTel   | cardNumber   |
      | <importo> | <gestoreRicarica> | <numeroTel> | <cardNumber> |
    And Utente inserisce il posteId nel nuovo format e clicca su conferma
      | posteId   |
      | <posteId> |
    Then Utente atterra sulla "TnkTelephoneRecharge"
      | landingPage   |
      | <landingPage> |
    And Utente chiude App PP

    @PPINPROAND-358_fortunato
    Examples: 
      | username                | password        | driver        | posteId | testIdCheck    | numeroTel  | importo | landingPage          | gestoreRicarica | cardNumber | userNumberCards | name     | discrepanza |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PPINPROAND-358 | 3713067834 | 5 €     | TnkTelephoneRecharge | poste           | **** 4679  |               2 | Vincenzo |         0,0 |

    @PPINPROAND-358_musella
    Examples: 
      | username             | password  | driver               | posteId | testIdCheck    | numeroTel  | importo | landingPage          | gestoreRicarica | cardNumber | userNumberCards | name      | discrepanza |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | PPINPROAND-358 | 3713067834 | 5 €     | TnkTelephoneRecharge | poste           | **** 7177  |               2 | Salvatore |         0,0 |

		@PPINPROAND-358_sarno
    Examples: 
      | username            | password   | driver        | posteId | testIdCheck    | numeroTel  | importo | landingPage          | gestoreRicarica | cardNumber | userNumberCards | name    | discrepanza |
      |  testACN_4@poste.it | Password1! | GalaxyS10Luca | 123123  | PPINPROAND-358 | 3331234567 | 5 €    | TnkTelephoneRecharge | vodafone         | **** 1525  |               1 | Fabiano |         0,0 |

	@PPINPROAND-358_romani
    Examples: 
      | username            | password   | driver        | posteId | testIdCheck    | numeroTel  | importo | landingPage          | gestoreRicarica | cardNumber | userNumberCards | name    | discrepanza |
      |  prisma20@poste.it | Password2! | GalaxyS10Luca | 123123  | PPINPROAND-358 | 3331234567 | 10 €    | TnkTelephoneRecharge | vodafone         | **** 9007  |               2 | Maria Grazia |         0,0 |

    @PPINPROIOS-369_fortunato
    Examples: 
      | username                | password        | driver        | posteId | testIdCheck    | numeroTel  | importo | landingPage          | gestoreRicarica | cardNumber | userNumberCards | name     | discrepanza |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PPINPROIOS-369 | 3713067834 | 5 €     | TnkTelephoneRecharge | poste           | **** 4679  |               2 | Vincenzo |         0,0 |

    @PPINPROIOS-369_musella
    Examples: 
      | username             | password  | driver               | posteId | testIdCheck    | numeroTel  | importo | landingPage          | gestoreRicarica | cardNumber | userNumberCards | name      | discrepanza |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | PPINPROIOS-369 | 3713067834 | 5 €     | TnkTelephoneRecharge | poste           | **** 7177  |               2 | Salvatore |         0,0 |

		@PPINPROIOS-369_sarno
    Examples: 
      | username            | password   | driver        | posteId | testIdCheck    | numeroTel  | importo | landingPage          | gestoreRicarica | cardNumber | userNumberCards | name    | discrepanza |
      |  testACN_4@poste.it | Password1! | GalaxyS10Luca | 123123  | PPINPROIOS-369 | 3331234567 | 5 €    | TnkTelephoneRecharge | vodafone         | **** 1525  |               1 | Fabiano |         0,0 |
     
    @PPINPROIOS-369_mancini
    Examples: 
      | username            | password   | driver        | posteId | testIdCheck    | numeroTel  | importo | landingPage          | gestoreRicarica  | cardNumber  | userNumberCards | name    | discrepanza |
      | prisma21@poste.it   | Password1! | GalaxyS10Luca | 123123  | PPINPROIOS-369 | 3386444111 | 10 €    | TnkTelephoneRecharge  | vodafone         | **** 9015   |               2 | Antonio |         0,0 |
  
  @PPINPROAND-358_fiorella
    Examples: 
      | username          | password   | driver        | posteId | testIdCheck    | numeroTel  | importo | landingPage          | gestoreRicarica | cardNumber | userNumberCards | name | discrepanza |
      | prisma65@poste.it | Password1! | GalaxyS10Luca | 123123  | PPINPROAND-358 | 3713315590 | 10 €    | TnkTelephoneRecharge | vodafone        | **** 8701  |               2 | Fiorella    |         0,0 |
  
  @PPINPROAND-389
  Scenario Outline: Verificare layout Ricarica Telefonica
    Verificare layout Ricarica Telefonica sul pagina di riepilogo

    Given App PP viene avviata
      | testIdJira    | driver   | site   | language   |
      | <testIdCheck> | <driver> | <site> | <language> |
    When Utente inserisce le credenziali nella login page
      | username   | password   | device   | testIdCheck   | name   | discrepanza   |
      | <username> | <password> | <device> | <testIdCheck> | <name> | <discrepanza> |
    And Utente clicca su accedi
    And Utente inserisce il posteId
      | username   | password   | device   | testIdCheck   | posteId   | name   |
      | <username> | <password> | <device> | <testIdCheck> | <posteId> | <name> |
    And Utente clicca sul bottone conferma
    And Utente sulla homepage clicca sul tab PAGA
    And Utente clicca su "gestoreRicarica"
      | gestoreRicarica   | cardNumber   | userNumberCards   |
      | <gestoreRicarica> | <cardNumber> | <userNumberCards> |
    And Utente inserisce il numero di telefono da ricaricare
      | numeroTel   |
      | <numeroTel> |
    And Utente verifica che il campo operatore e valorizzato e seleziona importo e clicca su procedi
      | importo   | gestoreRicarica   |
      | <importo> | <gestoreRicarica> |
    And Utente verifica il riepilogo della ricarica telefonica e clicca su paga
      | importo   | gestoreRicarica   | numeroTel   | cardNumber   |
      | <importo> | <gestoreRicarica> | <numeroTel> | <cardNumber> |
    Then Utente chiude App PP

    @PPINPROAND-389_fortunato
    Examples: 
      | username                | password        | driver        | posteId | testIdCheck    | numeroTel  | importo | landingPage          | gestoreRicarica | cardNumber | userNumberCards | name     | discrepanza |
      #      | luca.petrucci-abmo      | NLWbnsx6#st0 | GalaxyS8PlusAnd8 | prisma  | PPINPROAND-389 | 3713067834 |    5,00 | TnkTelephoneRecharge | poste           | **** 0922  |               1 | Luca |        0,75 |7
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PPINPROAND-389 | 3713067834 | 5 €     | TnkTelephoneRecharge | poste           | **** 4679  |               2 | Vincenzo |         0,0 |

    @PPINPROAND-389_musella
    Examples: 
      | username             | password  | driver               | posteId | testIdCheck    | numeroTel  | importo | landingPage          | gestoreRicarica | cardNumber | userNumberCards | name      | discrepanza |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | PPINPROAND-389 | 3713067834 | 5 €     | TnkTelephoneRecharge | poste           | **** 7177  |               2 | Salvatore |         0,0 |

    @PPINPROAND-389_muto
    Examples: 
      | username       | password    | driver        | posteId | testIdCheck    | numeroTel  | importo | landingPage          | gestoreRicarica | cardNumber | userNumberCards | name      | discrepanza |
      | francesco.muto | Prisma@2019 | GalaxyS10Luca | prisma  | PPINPROAND-389 | 3713067834 | 5 €     | TnkTelephoneRecharge | poste           | **** 4950  |               1 | Francesco |         0,0 |

     @PPINPROAND-389_sarno
    Examples: 
      | username            | password   | driver        | posteId | testIdCheck    | numeroTel  | importo | landingPage          | gestoreRicarica | cardNumber | userNumberCards | name    | discrepanza |
      |  testACN_4@poste.it | Password1! | GalaxyS10Luca | 123123  | PPINPROAND-389 | 3331234567 | 5 €    | TnkTelephoneRecharge | vodafone         | **** 1525  |               1 | Fabiano |         0,0 |
      
      @PPINPROAND-389_romani
    Examples: 
      | username            | password   | driver        | posteId | testIdCheck    | numeroTel  | importo | landingPage          | gestoreRicarica | cardNumber | userNumberCards | name    | discrepanza |
      |  prisma20@poste.it | Password2! | GalaxyS10Luca | 123123  | PPINPROAND-389 | 3331234567 | 10 €    | TnkTelephoneRecharge | vodafone         | **** 9007  |               2 | Maria Grazia |         0,0 |
    