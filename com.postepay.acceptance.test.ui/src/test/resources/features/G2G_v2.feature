Feature: G2G

  @PPINPROAND-372 @PPINPROIOS-3327 @PPINPROAND-3007
  Scenario Outline: Invio giga - contatto G2G - utente con 1 PPAY Connect
    Invio di giga tramite la funzionalità G2G ad un contatto già G2G. Se l’utente possiede una sola Connect si attiva il funnel di invio.

    Given App PP viene avviata
      | testIdJira    | driver   | site   | language   |
      | <testIdCheck> | <driver> | <site> | <language> |
    When Utente inserisce le credenziali nella login page
      | username   | password   | device   | testIdCheck   | discrepanza   | name   |
      | <username> | <password> | <device> | <testIdCheck> | <discrepanza> | <name> |
    And Utente clicca su accedi
    And Utente inserisce il posteId
      | username   | password   | device   | testIdCheck   | posteId   | name   |
      | <username> | <password> | <device> | <testIdCheck> | <posteId> | <name> |
    And Utente clicca sul bottone conferma
    And Utente sulla homepage clicca sul tab community
    And Utente clicca sul tab G2G invia Giga
    And Utente verifica il layout della pagina Invio Giga e clicca su Procedi
      | discrepanza   |
      | <discrepanza> |
    And Utente seleziona il destinatario del G2G tal tab CONTATTI G2G
      | targetUser   |
      | <targetUser> |
    And Utente verifica il layout della pagina G2G e clicca su procedi
      | discrepanza   |
      | <discrepanza> |
    And Utente inserisce il posteId nel nuovo format e clicca su conferma
      | posteId   |
      | <posteId> |
    Then Utente atterra sulla "TnkG2G"
      | landingPage   | discrepanza   |
      | <landingPage> | <discrepanza> |
    And Utente chiude App PP

    @PPINPROAND-372_fortunato @PPINPROAND-3007_fortunato
    Examples: 
      | username                | password        | driver        | posteId | testIdCheck    | targetUser      | landingPage | discrepanza | name     |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PPINPROAND-372 | Daniele Ioviero | TnkG2G      |         0,0 | Vincenzo |

    @PPINPROAND-372_musella @PPINPROAND-3007_musella
    Examples: 
      | username             | password  | driver               | posteId | testIdCheck    | targetUser      | landingPage | discrepanza | name      |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | PPINPROAND-372 | Daniele Ioviero | TnkG2G      |         0,0 | Salvatore |

    @PPINPROIOS-3327_musella
    Examples: 
      | username             | password  | driver               | posteId | testIdCheck     | targetUser      | landingPage | discrepanza | name      |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | PPINPROIOS-3327 | Daniele Ioviero | TnkG2G      |         0,0 | Salvatore |

 		@PPINPROIOS-3327_fortunato
    Examples: 
      | username                | password        | driver        | posteId | testIdCheck    | targetUser      | landingPage | discrepanza | name     |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PPINPROIOS-3327 | Daniele Ioviero | TnkG2G      |         0,0 | Vincenzo |
  
  @PP20AND-158 @PPINPROAND-471 @PPINPROIOS-482
  Scenario Outline: Verificare che il saldo disponibile e contabile vengano aggiornati correttamente dopo acquisto giga
    Verificare che vengano aggiornati correttamente i saldi contabili e disponibili dopo l'acquisto di giga

    Given App PP viene avviata
      | testIdJira    | driver   | site   | language   |
      | <testIdCheck> | <driver> | <site> | <language> |
    When Utente inserisce le credenziali nella login page
      | username   | password   | device   | testIdCheck   | discrepanza   | name   |
      | <username> | <password> | <device> | <testIdCheck> | <discrepanza> | <name> |
    And Utente clicca su accedi
    And Utente inserisce il posteId
      | username   | password   | device   | testIdCheck   | posteId   | name   |
      | <username> | <password> | <device> | <testIdCheck> | <posteId> | <name> |
    And Utente clicca sul bottone conferma
    And Utente sulla homepage clicca sulla card della Carta
    And Utente clicca sulla card della Carta dalla sezione PRODOTTI
      | username   |
      | <username> |
    And Utente memorizza importo disponibile ed importo contabile
    And Utente clicca su back
    And Utente clicca su Gestisci SIM e invia Giga
    And Utente clicca su Acquista nella sezione SIM Connect
    And Verifica il riepilogo del acquisto dei Giga e clicca su paga
    And Utente inserisce il posteId nel nuovo format e clicca su conferma
      | posteId   |
      | <posteId> |
    And Utente atterra sulla "TkpG2GPurchase"
      | landingPage   |
      | <landingPage> |
    Then Utente torna nella sezione PRODOTTI
    And Utente verifica che importo disponibile ed importo contabile siano stati decrementati del costo dei giga
    And Utente chiude App PP

    @PPINPROAND-471_fortunato
    Examples: 
      | username                | password        | driver        | posteId | testIdCheck    | landingPage    | payment | name     | discrepanza |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PPINPROAND-471 | TkpG2GPurchase | payment | Vincenzo |         0,0 |

    @PPINPROAND-471_musella
    Examples: 
      | username             | password  | driver               | posteId | testIdCheck    | landingPage    | payment | name      | discrepanza |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | PPINPROAND-471 | TkpG2GPurchase | payment | Salvatore |         0,0 |

    @PPINPROIOS-482_musella
    Examples: 
      | username             | password  | driver               | posteId | testIdCheck    | landingPage    | payment | name      | discrepanza |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | PPINPROIOS-482 | TkpG2GPurchase | payment | Salvatore |         0,0 |

    @PPINPROIOS-482_fortunato
    Examples: 
      | username                | password        | driver        | posteId | testIdCheck    | landingPage    | payment | name     | discrepanza |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PPINPROAND-471 | TkpG2GPurchase | payment | Vincenzo |         0,0 |
      
  @PP20AND-1110 @PPINPROAND-685 @PPINPROIOS-692
  Scenario Outline: Invio giga dalla sezione Prodotti - contatto G2G - utente con 1 PPAY Connect
    Invio di giga tramite la funzionalità G2G ad un contatto già G2G. Se l’utente possiede una sola Connect si attiva il funnel di invio.

    Given App PP viene avviata
      | testIdJira    | driver   | site   | language   |
      | <testIdCheck> | <driver> | <site> | <language> |
    When Utente inserisce le credenziali nella login page
      | username   | password   | device   | testIdCheck   | discrepanza   | name   |
      | <username> | <password> | <device> | <testIdCheck> | <discrepanza> | <name> |
    And Utente clicca su accedi
    And Utente inserisce il posteId
      | username   | password   | device   | testIdCheck   | posteId   | name   |
      | <username> | <password> | <device> | <testIdCheck> | <posteId> | <name> |
    And Utente clicca sul bottone conferma
    And Utente sulla homepage clicca sul tab prodotti
    And Utente clicca su Gestisci SIM e invia Giga
    And Utente clicca su bottone G2G per inviare giga
    And Utente verifica il layout della pagina Invio Giga e clicca su Procedi
      | discrepanza   |
      | <discrepanza> |
    And Utente seleziona il destinatario del G2G tal tab CONTATTI G2G
      | targetUser   |
      | <targetUser> |
    And Utente verifica il layout della pagina G2G e clicca su procedi
      | discrepanza   |
      | <discrepanza> |
    And Utente inserisce il posteId nel nuovo format e clicca su conferma
      | posteId   |
      | <posteId> |
    Then Utente atterra sulla "TnkG2G"
      | landingPage   | discrepanza   |
      | <landingPage> | <discrepanza> |
    And Utente chiude App PP

    @PPINPROAND-685_fortunato
    Examples: 
      | username                | password        | driver        | posteId | testIdCheck    | targetUser      | landingPage | discrepanza | name     |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PPINPROAND-685 | Daniele Ioviero | TnkG2G      |         0,0 | Vincenzo |

    @PPINPROAND-685_musella
    Examples: 
      | username             | password  | driver               | posteId | testIdCheck    | targetUser      | landingPage | discrepanza | name      |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | PPINPROAND-685 | Daniele Ioviero | TnkG2G      |         0,0 | Salvatore |

    @PPINPROIOS-692_musella
    Examples: 
      | username             | password  | driver               | posteId | testIdCheck    | targetUser      | landingPage | discrepanza | name      |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | PPINPROIOS-692 | Daniele Ioviero | TnkG2G      |         0,0 | Salvatore |

  @PPINPROAND-375
  Scenario Outline: Invio giga - contatto G2G - utente senza PPAY Connect
    Invio di giga tramite la funzionalita G2G ad un contatto non G2G. Se l’utente non e possessore di una carta Connet si attiva il Funnel E2E.

    Given App PP viene avviata
      | testIdJira    | driver   | site   | language   |
      | <testIdCheck> | <driver> | <site> | <language> |
    When Utente inserisce le credenziali nella login page
      | username   | password   | device   | testIdCheck   | discrepanza   | name   |
      | <username> | <password> | <device> | <testIdCheck> | <discrepanza> | <name> |
    And Utente clicca su accedi
    And Utente inserisce il posteId
      | username   | password   | device   | testIdCheck   | posteId   | name   |
      | <username> | <password> | <device> | <testIdCheck> | <posteId> | <name> |
    And Utente clicca sul bottone conferma
    And Utente sulla homepage clicca sul tab community
    And Utente clicca sul tab G2G invia Giga
    And Utente verifica il layout della pagina Invio Giga e clicca su Procedi
      | discrepanza   |
      | <discrepanza> |
    And Utente seleziona il destinatario del G2G dal tab CONTATTI generici
      | targetUser   |
      | <targetUser> |
    Then Utente atterra sulla "TnkNoG2gUser"
      | landingPage   |
      | <landingPage> |
    And Utente chiude App PP

    @PPINPROAND-375_fortunato
    Examples: 
      | username                | password        | driver        | posteId | testIdCheck    | targetUser | landingPage | discrepanza | name     | landingPage  |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PPINPROAND-375 | Pippo      | TnkG2G      |         0,0 | Vincenzo | TnkNoG2gUser |

    @PPINPROAND-375_musella
    Examples: 
      | username             | password  | driver               | posteId | testIdCheck    | targetUser     | landingPage | discrepanza | name      | landingPage  |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | PPINPROAND-375 | Angelo Casoria | TnkG2G      |         0,0 | Salvatore | TnkNoG2gUser |

  @PPINPROAND-4217
  Scenario Outline: PP Connect - Acquista GB
    Verificare che e possibile acquistare GB mediante la funzione Acquista

    Given App PP viene avviata
      | testIdJira    | driver   | site   | language   |
      | <testIdCheck> | <driver> | <site> | <language> |
    When Utente inserisce le credenziali nella login page
      | username   | password   | device   | testIdCheck   | name   | discrepanza   |
      | <username> | <password> | <device> | <testIdCheck> | <name> | <discrepanza> |
    And Utente esegue la login in App
      | username   | password   | device   | testIdCheck   | name   | discrepanza   | posteId   |
      | <username> | <password> | <device> | <testIdCheck> | <name> | <discrepanza> | <posteId> |
    And Utente sulla homepage clicca sulla card della Carta
    And Utente clicca su Gestisci SIM e invia Giga
    And Utente clicca sul bottone indietro dalla toolbar
    And Utente clicca su Gestisci SIM e invia Giga
    And Utente clicca su Acquista nella sezione SIM Connect
    And Utente clicca sul bottone <
    And Utente clicca su Acquista nella sezione SIM Connect
    And Utente clicca sul cta X e viene riportato alla schermata precedente
    And Utente clicca su Acquista nella sezione SIM Connect
    And Verifica il riepilogo del acquisto dei Giga e clicca su paga
    And Utente clicca sul bottone <
    And Verifica il riepilogo del acquisto dei Giga e clicca su paga
    And Utente clicca sul cta X e viene riportato alla schermata precedente
    And Utente clicca su Acquista nella sezione SIM Connect
    Then Verifica il riepilogo del acquisto dei Giga e clicca su paga
    And Utente inserisce il posteId nel nuovo format e clicca su conferma
      | posteId   |
      | <posteId> |
    And Utente atterra sulla "TkpG2GPurchase"
      | landingPage   |
      | <landingPage> |
    And Utente chiude App PP

    @PPINPROAND-4217_fortunato
    Examples: 
      | username                | password        | driver        | posteId | testIdCheck    | landingPage    | payment | name     | discrepanza |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PPINPROAND-467 | TkpG2GPurchase | payment | Vincenzo |         0,0 |

    @PPINPROAND-4217_musella
    Examples: 
      | username             | password  | driver               | posteId | testIdCheck    | landingPage    | payment | name      | discrepanza |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | PPINPROAND-467 | TkpG2GPurchase | payment | Salvatore |         0,0 |

     @PPINPROAND-3007
     Scenario Outline: Invio giga - contatto G2G - utente con 1 PPAY Connect
     Invio di giga tramite la funzionalita G2G ad un contatto già G2G. Se l utente possiede una sola Connect si attiva il funnel di invio.
     
     