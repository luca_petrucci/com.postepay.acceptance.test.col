@bonificoSepa
Feature: Bonifico SEPA

  @PP20AND-14 @PPINPROAND-365 @PPINPROIOS-376
  Scenario Outline: Bonifico SEPA da carta su conto NO POSTE
    L'utente inserisce i dati del bonifico, verso conto NO POSTE, da effettuare manualmente.

    Given App PP viene avviata
      | testIdJira    | driver   | site   | language   |
      | <testIdCheck> | <driver> | <site> | <language> |
    When Utente inserisce le credenziali nella login page
      | username   | password   | device   | testIdCheck   | name   | discrepanza   |
      | <username> | <password> | <device> | <testIdCheck> | <name> | <discrepanza> |
    And Utente clicca su accedi
    And Utente inserisce il posteId
      | username   | password   | device   | testIdCheck   | posteId   | name   |
      | <username> | <password> | <device> | <testIdCheck> | <posteId> | <name> |
    And Utente clicca sul bottone conferma
    And Utente sulla homepage clicca sul tab PAGA
    And Utente clicca su PAGAMENTI
    And Utente clicca su Bonifico SEPA
    And Utente seleziona la carta da utilizzare per il pagamento su "targetOperation"
      | targetOperation   | cardNumber   | discrepanza   | userNumberCards   | name |
      | <targetOperation> | <cardNumber> | <discrepanza> | <userNumberCards> | <name> |
    And Utente seleziona il tipo di bonifico o postagiro da effettuare
      | targetOperation   |
      | <targetOperation> |
    And Utente compila i dati per il bonifico
      | iban   | targetUser   | importo   | comment   | cardNumber   | name |
      | <iban> | <targetUser> | <importo> | <comment> | <cardNumber> |<name>|
    And Utente clicca su PROCEDI della "targetOperation"
      | targetOperation   |
      | <targetOperation> |
    And Utente verifica il riepilogo del "targetOperation"
      | iban   | targetUser   | importo   | comment   | myIban   | targetOperation   | cardNumber   |
      | <iban> | <targetUser> | <importo> | <comment> | <myIban> | <targetOperation> | <cardNumber> |
    And Utente clicca su PAGA del "targetOperation"
      | targetOperation   |
      | <targetOperation> |
    And Utente inserisce il posteId durante la "targetOperation" e clicca conferma
      | posteId   | targetOperation   |
      | <posteId> | <targetOperation> |
    Then Utente atterra sulla "TnkGeneric"
      | landingPage   |
      | <landingPage> |
    And Utente chiude App PP


    @PPINPROAND-365_fortunato
    Examples: 
      | username                | password        | driver        | posteId | cardNumber | myIban                      | iban                        | targetUser          | importo | comment           | testIdCheck    | landingPage | site | language | targetOperation | discrepanza | name     | userNumberCards |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | **** 4679  | IT70M3608105138207391907401 | IT36I0301503200000006018757 | Carmine Di Vincenzo |    5.00 | RimborsoUnicredit | PPINPROAND-365 | TnkGeneric  | test | site     | SEPA            |         0,0 | Vincenzo |               2 |

    @PPINPROAND-365_musella
    Examples: 
      | username             | password  | driver        | posteId | cardNumber | myIban                      | iban                        | targetUser          | importo | comment           | testIdCheck    | landingPage | site | language | targetOperation | discrepanza | name      | userNumberCards |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | **** 7177  | IT15E3608105138271495971507 | IT36I0301503200000006018757 | Carmine Di Vincenzo |    5.00 | RimborsoUnicredit | PPINPROAND-365 | TnkGeneric  | test | site     | SEPA            |         0,0 | Salvatore |               1 |

    @PPINPROIOS-376_fortunato
    Examples: 
      | username                | password        | driver               | posteId | cardNumber | myIban                      | iban                        | targetUser    | importo | comment       | testIdCheck    | landingPage | site | language | targetOperation | discrepanza | name     | userNumberCards |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | **** 4679  | IT70M3608105138207391907401 | IT40E3608105138270786970800 | Domenico Pica |    0.01 | BonificoCarta | PPINPROIOS-376 | TnkGeneric  | test | site     | Postagiro       |         0,0 | Vincenzo |               2 |

    @PPINPROIOS-376_musella
    Examples: 
      | username             | password  | driver               | posteId | cardNumber | myIban                      | iban                        | targetUser       | importo | comment           | testIdCheck    | landingPage | site | language | targetOperation | discrepanza | name      | userNumberCards |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | **** 7177  | IT15E3608105138271495971507 | IT11F0200876312000420398325 | Candida Cangiano |    2.00 | RimborsoUnicredit | PPINPROIOS-376 | TnkGeneric  | test | site     | SEPA            |         0,0 | Salvatore |               1 |

  #   | vincenzo.fortunato-4869| D@niela22Lili10| GalaxyS10Luca | prisma  | **** 4679  | IT70M3608105138207391907401 | IT60I0200805236000102962357 |Luca Petrucci  | 2.00   | RimborsoUnicredit |  PP20AND-14 | TnkGeneric |test | site| SEPA | 0,75 | Vincenzo |
  #   | vincenzo.fortunato-4869| D@niela22Lili10| GalaxyS8Plus | prisma  | **** 4679  | IT70M3608105138207391907401 | IT60I0200805236000102962357 |Luca Petrucci  | 2.00   | RimborsoUnicredit |  PP20AND-14 | TnkGeneric |test | site| SEPA | 0,75 | Vincenzo |
  @PP20AND-59 @PPINPROAND-397 @PPINPROIOS-408
  Scenario Outline: Bonifico SEPA - Postagiro da carta a carta
    L'utente dalla sezione Paga effettua un Postagiro da carta a carta.

    Given App PP viene avviata
      | testIdJira    | driver   | site   | language   |
      | <testIdCheck> | <driver> | <site> | <language> |
    When Utente inserisce le credenziali nella login page
      | username   | password   | device   | testIdCheck   | name   | discrepanza   |
      | <username> | <password> | <device> | <testIdCheck> | <name> | <discrepanza> |
    And Utente clicca su accedi
    And Utente inserisce il posteId
      | username   | password   | device   | testIdCheck   | posteId   | name   |
      | <username> | <password> | <device> | <testIdCheck> | <posteId> | <name> |
    And Utente clicca sul bottone conferma
    And Utente sulla homepage clicca sul tab PAGA
    And Utente clicca su PAGAMENTI
    And Utente clicca su Bonifico SEPA
    And Utente seleziona la carta da utilizzare per il pagamento su "targetOperation"
      | targetOperation   | cardNumber   | discrepanza   | userNumberCards   | name   |
      | <targetOperation> | <cardNumber> | <discrepanza> | <userNumberCards> | <name> |
    And Utente seleziona il tipo di bonifico o postagiro da effettuare
      | targetOperation   |
      | <targetOperation> |
    And Utente compila i dati per il bonifico
      | iban   | targetUser   | importo   | comment   | cardNumber   | name |
      | <iban> | <targetUser> | <importo> | <comment> | <cardNumber> |<name>|
    And Utente clicca su PROCEDI della "targetOperation"
      | targetOperation   |
      | <targetOperation> |
    And Utente verifica il riepilogo del "targetOperation"
      | iban   | targetUser   | importo   | comment   | myIban   | targetOperation   | cardNumber   |
      | <iban> | <targetUser> | <importo> | <comment> | <myIban> | <targetOperation> | <cardNumber> |
    And Utente clicca su PAGA del "targetOperation"
      | targetOperation   |
      | <targetOperation> |
    And Utente inserisce il posteId durante la "targetOperation" e clicca conferma
      | posteId   | targetOperation   |
      | <posteId> | <targetOperation> |
    Then Utente atterra sulla "TnkGeneric"
      | landingPage   |
      | <landingPage> |
    And Utente chiude App PP

    @PPINPROAND-397_fortunato
    Examples: 
      | username                | password        | driver        | posteId | cardNumber | myIban                      | iban                        | targetUser        | importo | comment       | testIdCheck    | landingPage | site | language | targetOperation | discrepanza | name     | userNumberCards |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | **** 4679  | IT70M3608105138207391907401 | IT15E3608105138271495971507 | Salvatore Musella |    0.01 | BonificoCarta | PPINPROAND-397 | TnkGeneric  | test | site     | Postagiro       |         0,0 | Vincenzo |               2 |

    @PPINPROAND-397_musella
    Examples: 
      | username             | password  | driver        | posteId | cardNumber | myIban                      | iban                        | targetUser         | importo | comment           | testIdCheck    | landingPage | site | language | targetOperation | discrepanza | name      | userNumberCards |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | **** 7177  | IT15E3608105138271495971507 | IT70M3608105138207391907401 | Vincenzo Fortunato |    0.01 | RimborsoUnicredit | PPINPROAND-397 | TnkGeneric  | test | site     | Postagiro       |         0,0 | Salvatore |               1 |

    @PPINPROAND-397_sarno
    Examples: 
      | username           | password   | driver        | posteId | cardNumber | myIban                      | iban                        | targetUser      | importo | comment           | testIdCheck    | landingPage | language | site | name    | discrepanza | targetOperation | userNumberCards |
      | testACN_4@poste.it | Password1! | GalaxyS10Luca |  123123 | **** 1525  | IT49T3608105138216577216583 | IT23P3608105138238935238941 | Giuseppe Rimato |    0.01 | Bonifico Collaudo | PPINPROAND-397 | TnkGeneric  | test     | site | Fabiano |         0,0 | Postagiro       |               1 |

    @PPINPROAND-397_romani
    Examples: 
      | username          | password   | driver        | posteId | cardNumber | myIban                      | iban                        | targetUser      | importo | comment           | testIdCheck    | landingPage | language | site | name         | discrepanza | targetOperation | userNumberCards |
      | prisma20@poste.it | Password2! | GalaxyS10Luca |  123123 | **** 9007  | IT69Y3608105138226711626718 | IT56V3608105138235847635853 | Antonio Mancini |    0.01 | Bonifico Collaudo | PPINPROAND-397 | TnkGeneric  | test     | site | Maria Grazia |         0,0 | Postagiro       |               1 |

    @PPINPROIOS-408_musella
    Examples: 
      | username             | password  | driver               | posteId | cardNumber | myIban                      | iban                        | targetUser         | importo | comment           | testIdCheck    | landingPage | site | language | targetOperation | discrepanza | name      | userNumberCards |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | **** 7177  | IT15E3608105138271495971507 | IT70M3608105138207391907401 | Vincenzo Fortunato |    0.01 | RimborsoUnicredit | PPINPROIOS-408 | TnkGeneric  | test | site     | Postagiro       |         0,0 | Salvatore |               1 |

    @PPINPROIOS-408_rimato
    Examples: 
      | username            | password   | driver        | posteId | cardNumber | myIban                      | iban                        | targetUser    | importo | comment           | testIdCheck    | landingPage | language | site | name     | discrepanza | targetOperation | userNumberCards |
      | testacn_13@poste.it | Password1! | GalaxyS10Luca |  123123 | **** 1566  | IT23P3608105138238935238941 | IT49T3608105138216577216583 | Fabiano Sarno |    0.01 | Bonifico Collaudo | PPINPROIOS-408 | TnkGeneric  | test     | site | Giuseppe |         0,0 | Postagiro       |               1 |

    @PPINPROIOS-408_sarno
    Examples: 
      | username           | password   | driver               | posteId | cardNumber | myIban                      | iban                        | targetUser      | importo | comment           | testIdCheck    | landingPage | language | site | name    | discrepanza | targetOperation | userNumberCards |
      | testACN_4@poste.it | Password1! | GalaxyS10Luca |  123123 | **** 1525  | IT49T3608105138216577216583 | IT23P3608105138238935238941 | Giuseppe Rimato |    0.01 | Bonifico Collaudo | PPINPROIOS-408 | TnkGeneric  | test     | site | Fabiano |         0,0 | Postagiro       |               1 |

 		@PPINPROIOS-408_mancini
    Examples:  
      | username           | password   | driver        | posteId | cardNumber | myIban                      | iban                        | targetUser          | importo | comment           | testIdCheck    | landingPage | language | site | name    | discrepanza | targetOperation | userNumberCards |
      | prisma21@poste.it  | Password1! | GalaxyS10Luca |  123123 | **** 9015  | IT56V3608105138235847635853 | IT69Y3608105138226711626718 | Maria Grazia Romani |    0.01 | Bonifico Collaudo | PPINPROIOS-408 | TnkGeneric  | test     | site | Antonio |         0,0 | Postagiro       |               1 |
  
  #   | vincenzo.fortunato-4869| D@niela22Lili10| GalaxyS8Plus | prisma  | **** 4679  | IT70M3608105138207391907401 |IT41B3608105138291285391291 |Gennaro Rega | 0.01    | BonificoCarta    |  PP20AND-59 | TnkGeneric |test | site| Postagiro |  0,75 | Vincenzo |
  #   | vincenzo.fortunato-4869| D@niela22Lili10| GalaxyS8   | prisma  | **** 4679  | IT70M3608105138207391907401 |IT41B3608105138291285391291 |Gennaro Rega | 0.01    | BonificoCarta    |  PP20AND-59 | TnkGeneric |test | site| Postagiro |  0,75 | Vincenzo |
  @PP20AND-58 @PPINPROAND-396 @PPINPROIOS-407
  Scenario Outline: Bonifico SEPA - Postagiro da carta a conto
    L'utente dalla sezione Paga effettua un Postagiro da carta a conto.

    Given App PP viene avviata
      | testIdJira    | driver   | site   | language   |
      | <testIdCheck> | <driver> | <site> | <language> |
    When Utente inserisce le credenziali nella login page
      | username   | password   | device   | testIdCheck   | name   | discrepanza   |
      | <username> | <password> | <device> | <testIdCheck> | <name> | <discrepanza> |
    And Utente clicca su accedi
    And Utente inserisce il posteId
      | username   | password   | device   | testIdCheck   | posteId   | name   |
      | <username> | <password> | <device> | <testIdCheck> | <posteId> | <name> |
    And Utente clicca sul bottone conferma
    And Utente sulla homepage clicca sul tab PAGA
    And Utente clicca su PAGAMENTI
    And Utente clicca su Bonifico SEPA
    And Utente seleziona la carta da utilizzare per il pagamento su "targetOperation"
      | targetOperation   | cardNumber   | discrepanza   | userNumberCards   | name |
      | <targetOperation> | <cardNumber> | <discrepanza> | <userNumberCards> | <name> |
    And Utente seleziona il tipo di bonifico o postagiro da effettuare
      | targetOperation   |
      | <targetOperation> |
    And Utente compila i dati per il bonifico
      | iban   | targetUser   | importo   | comment   | cardNumber   | name |
      | <iban> | <targetUser> | <importo> | <comment> | <cardNumber> |<name>|
    And Utente clicca su PROCEDI della "targetOperation"
      | targetOperation   |
      | <targetOperation> |
    And Utente verifica il riepilogo del "targetOperation"
      | iban   | targetUser   | importo   | comment   | myIban   | targetOperation   | cardNumber   |
      | <iban> | <targetUser> | <importo> | <comment> | <myIban> | <targetOperation> | <cardNumber> |
    And Utente clicca su PAGA del "targetOperation"
      | targetOperation   |
      | <targetOperation> |
    And Utente inserisce il posteId durante la "targetOperation" e clicca conferma
      | posteId   | targetOperation   |
      | <posteId> | <targetOperation> |
    Then Utente atterra sulla "TnkGeneric"
      | landingPage   |
      | <landingPage> |
    And Utente chiude App PP

    @PPINPROAND-396_fortunato
    Examples: 
      | username                | password        | driver        | posteId | cardNumber | myIban                      | iban                        | targetUser        | importo | comment       | testIdCheck    | landingPage | site | language | targetOperation | discrepanza | name     | userNumberCards |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | **** 4679  | IT70M3608105138207391907401 | IT06W0760103400001044548509 | Salvatore Musella |    0.01 | BonificoConto | PPINPROAND-396 | TnkGeneric  | test | site     | Postagiro       |         0,0 | Vincenzo |               2 |

    @PPINPROAND-396_musella
    Examples: 
      | username             | password  | driver        | posteId | cardNumber | myIban                      | iban                        | targetUser         | importo | comment           | testIdCheck    | landingPage | site | language | targetOperation | discrepanza | name      | userNumberCards |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | **** 7177  | IT15E3608105138271495971507 | IT15L0760103400001044920229 | Vincenzo Fortunato |    0.01 | RimborsoUnicredit | PPINPROAND-396 | TnkGeneric  | test | site     | Postagiro       |         0,0 | Salvatore |               1 |

    @PPINPROIOS-407_musella
    Examples: 
      | username             | password  | driver               | posteId | cardNumber | myIban                      | iban                        | targetUser         | importo | comment           | testIdCheck    | landingPage | site | language | targetOperation | discrepanza | name      | userNumberCards |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | **** 7177  | IT15E3608105138271495971507 | IT15L0760103400001044920229 | Vincenzo Fortunato |    0.01 | RimborsoUnicredit | PPINPROIOS-407 | TnkGeneric  | test | site     | Postagiro       |         0,0 | Salvatore |               1 |

    @PPINPROIOS-407_fortunato
    Examples: 
      | username                | password        | driver               | posteId | cardNumber | myIban                      | iban                        | targetUser        | importo | comment       | testIdCheck    | landingPage | site | language | targetOperation | discrepanza | name     | userNumberCards |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | **** 4679  | IT70M3608105138207391907401 | IT06W0760103400001044548509 | Salvatore Musella |    0.01 | BonificoConto | PPINPROIOS-407 | TnkGeneric  | test | site     | Postagiro       |         0,0 | Vincenzo |               2 |

  @PPINPROAND-3415 @PPINPROIOS-3590
  Scenario Outline: Verificare obbligatorietà del campo IBAN
    L utente verifica che il campo IBAN è obbligatorio

    Given App PP viene avviata
      | testIdJira    | driver   | site   | language   |
      | <testIdCheck> | <driver> | <site> | <language> |
    When Utente inserisce le credenziali nella login page
      | username   | password   | device   | testIdCheck   | name   | discrepanza   |
      | <username> | <password> | <device> | <testIdCheck> | <name> | <discrepanza> |
    And Utente clicca su accedi
    And Utente inserisce il posteId
      | username   | password   | device   | testIdCheck   | posteId   | name   |
      | <username> | <password> | <device> | <testIdCheck> | <posteId> | <name> |
    And Utente clicca sul bottone conferma
    And Utente sulla homepage clicca sul tab PAGA
    And Utente clicca su PAGAMENTI
    And Utente clicca su Bonifico SEPA
    And Utente seleziona la carta da utilizzare per il pagamento su "targetOperation"
      | targetOperation   | cardNumber   | discrepanza   | userNumberCards   |  name |
      | <targetOperation> | <cardNumber> | <discrepanza> | <userNumberCards> | <name> |
    And Utente seleziona il tipo di bonifico o postagiro da effettuare
      | targetOperation   |
      | <targetOperation> |
    And Utente non compila il campo "iban" e compila tutti gli altri campi dal Bonifico SEPA
      | iban   | targetUser   | importo   | comment   |
      | <iban> | <targetUser> | <importo> | <comment> |
    And Utente clicca su PROCEDI della Bonifico SEPA
      | targetOperation   |
      | <targetOperation> |
    And Utente visualizza una popup di errore per il campo "iban"
    And Utente chiude App PP

    @PPINPROAND-3415_fortunato
    Examples: 
      | username                | password        | driver               | posteId | cardNumber | myIban                      | iban                        | targetUser          | importo | comment    | testIdCheck     | landingPage | site | language | targetOperation | discrepanza | name     | userNumberCards |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | **** 4679  | IT70M3608105138207391907401 | IT36I0301503200000006018757 | Carmine Di Vincenzo |    5.00 | ErroreIban | PPINPROAND-3415 | TnkGeneric  | test | site     | SEPA            |         0,0 | Vincenzo |               2 |

    @PPINPROAND-3415_musella
    Examples: 
      | username             | password  | driver               | posteId | cardNumber | myIban                      | iban                        | targetUser          | importo | comment    | testIdCheck     | landingPage | site | language | targetOperation | discrepanza | name      | userNumberCards |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | **** 7177  | IT15E3608105138271495971507 | IT36I0301503200000006018757 | Carmine Di Vincenzo |    5.00 | ErroreIban | PPINPROAND-3415 | TnkGeneric  | test | site     | SEPA            |         0,0 | Salvatore |               1 |

    @PPINPROAND-3415_sarno
    Examples: 
      | username           | password   | driver        | posteId | cardNumber | myIban                      | iban                        | targetUser      | importo | comment    | testIdCheck     | landingPage | language | site | name    | discrepanza | targetOperation | userNumberCards |
      | testACN_4@poste.it | Password1! | GalaxyS10Luca |  123123 | **** 1525  | IT49T3608105138216577216583 | IT23P3608105138238935238941 | Giuseppe Rimato |    0.01 | ErroreIban | PPINPROAND-3415 | TnkGeneric  | test     | site | Fabiano |         0,0 | SEPA            |               1 |

    @PPINPROAND-3415_romani
    Examples: 
      | username          | password   | driver        | posteId | cardNumber | myIban                      | iban                        | targetUser      | importo | comment    | testIdCheck     | landingPage | language | site | name         | discrepanza | targetOperation | userNumberCards |
      | prisma20@poste.it | Password2! | GalaxyS10Luca |  123123 | **** 9007  | IT13G0760103200001009187830 | IT23P3608105138238935238941 | Giuseppe Rimato |    0.01 | ErroreIban | PPINPROAND-3415 | TnkGeneric  | test     | site | Maria Grazia |         0,0 | SEPA            |               1 |

@PPINPROAND-3415_fiorella
    Examples: 
      | username          | password   | driver        | posteId | cardNumber | myIban                      | iban                        | targetUser      | importo | comment    | testIdCheck     | landingPage | language | site | name         | discrepanza | targetOperation | userNumberCards |
      | prisma65@poste.it | Password1! | GalaxyS10Luca |  123123 | **** 8701  | IT36A3608105138275752475759 | IT23P3608105138238935238941 | Giuseppe Rimato |    0.01 | ErroreIban | PPINPROAND-3415 | TnkGeneric  | test     | site | Fiorella |         0,0 | SEPA            |               1 |

    @PPINPROIOS-3590_fortunato
    Examples: 
      | username                | password        | driver               | posteId | cardNumber | myIban                      | iban                        | targetUser          | importo | comment    | testIdCheck     | landingPage | site | language | targetOperation | discrepanza | name     | userNumberCards |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | **** 4679  | IT70M3608105138207391907401 | IT36I0301503200000006018757 | Carmine Di Vincenzo |    5.00 | ErroreIban | PPINPROIOS-3590 | TnkGeneric  | test | site     | SEPA            |         0,0 | Vincenzo |               2 |

    @PPINPROIOS-3590_musella
    Examples: 
      | username             | password  | driver               | posteId | cardNumber | myIban                      | iban                        | targetUser          | importo | comment    | testIdCheck     | landingPage | site | language | targetOperation | discrepanza | name      | userNumberCards |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | **** 7177  | IT15E3608105138271495971507 | IT36I0301503200000006018757 | Carmine Di Vincenzo |    5.00 | ErroreIban | PPINPROIOS-3590 | TnkGeneric  | test | site     | SEPA            |         0,0 | Salvatore |               1 |

    @PPINPROIOS-3590_rimato
    Examples: 
      | username            | password   | driver        | posteId | cardNumber | myIban                      | iban                        | targetUser    | importo | comment    | testIdCheck     | landingPage | language | site | name     | discrepanza | targetOperation | userNumberCards |
      | testacn_13@poste.it | Password1! | GalaxyS10Luca |  123123 | **** 1566  | IT23P3608105138238935238941 | IT49T3608105138216577216583 | Fabiano Sarno |    0.01 | ErroreIban | PPINPROIOS-3590 | TnkGeneric  | test     | site | Giuseppe |         0,0 | SEPA            |               1 |

    @PPINPROIOS-3590_sarno
    Examples: 
      | username           | password   | driver        | posteId | cardNumber | myIban                      | iban                        | targetUser      | importo | comment    | testIdCheck     | landingPage | language | site | name    | discrepanza | targetOperation | userNumberCards |
      | testACN_4@poste.it | Password1! | GalaxyS10Luca |  123123 | **** 1525  | IT49T3608105138216577216583 | IT23P3608105138238935238941 | Giuseppe Rimato |    0.01 | ErroreIban | PPINPROIOS-3590 | TnkGeneric  | test     | site | Fabiano |         0,0 | SEPA            |               1 |
      
    @PPINPROIOS-3590_mancini
    Examples: 
      | username           | password   | driver        | posteId | cardNumber | myIban                      | iban                        | targetUser          | importo | comment    | testIdCheck     | landingPage | language | site | name    | discrepanza | targetOperation | userNumberCards |
      | prisma21@poste.it  | Password1! | GalaxyS10Luca |  123123 | **** 9015  | IT81B0760103200001009187798 | IT13G0760103200001009187830 | Maria Grazia Romani |    0.01 | ErroreIban | PPINPROIOS-3590 | TnkGeneric  | test     | site | Antonio |         0,0 | SEPA            |               1 |

  @PPINPROAND-3413
  Scenario Outline: Bonifico - Associazione IBAN intestatario
    L utente verifica che l associazione IBAN intestatario sia corretta

    Given App PP viene avviata
      | testIdJira    | driver   | site   | language   |
      | <testIdCheck> | <driver> | <site> | <language> |
    When Utente inserisce le credenziali nella login page
      | username   | password   | device   | testIdCheck   | name   | discrepanza   |
      | <username> | <password> | <device> | <testIdCheck> | <name> | <discrepanza> |
    And Utente clicca su accedi
    And Utente inserisce il posteId
      | username   | password   | device   | testIdCheck   | posteId   | name   |
      | <username> | <password> | <device> | <testIdCheck> | <posteId> | <name> |
    And Utente clicca sul bottone conferma
    And Utente sulla homepage clicca sul tab PAGA
    And Utente clicca su PAGAMENTI
    And Utente clicca su Bonifico SEPA
    And Utente seleziona la carta da utilizzare per il pagamento su "targetOperation"
      | targetOperation   | cardNumber   | discrepanza   | userNumberCards   | name |
      | <targetOperation> | <cardNumber> | <discrepanza> | <userNumberCards> | <name> |
    And Utente seleziona il tipo di bonifico o postagiro da effettuare
      | targetOperation   |
      | <targetOperation> |
    And Utente compila i dati per il bonifico
      | iban   | targetUser   | importo   | comment   | cardNumber   | name |
      | <iban> | <targetUser> | <importo> | <comment> | <cardNumber> |<name>|
    And Utente clicca su PROCEDI della Bonifico SEPA
      | targetOperation   |
      | <targetOperation> |
    Then Una Popup di errore avvisa che il CC o l IBAN è errato
    And Utente chiude App PP

    @PPINPROAND-3413_fortunato
    Examples: 
      | username                | password        | driver               | posteId | cardNumber | myIban                      | iban                        | targetUser          | importo | comment    | testIdCheck     | landingPage | site | language | targetOperation | discrepanza | name     | userNumberCards |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | **** 4679  | IT70M3608105138207391907401 | IT36I0301503200000006010000 | Carmine Di Vincenzo |    5.00 | IbanErrato | PPINPROAND-3413 | TnkGeneric  | test | site     | SEPA            |         0,0 | Vincenzo |               2 |

    @PPINPROAND-3413_musella
    Examples: 
      | username             | password  | driver               | posteId | cardNumber | myIban                      | iban                        | targetUser          | importo | comment    | testIdCheck     | landingPage | site | language | targetOperation | discrepanza | name      | userNumberCards |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | **** 7177  | IT15E3608105138271495971507 | IT36I0301503200000006010000 | Carmine Di Vincenzo |    5.00 | IbanErrato | PPINPROAND-3413 | TnkGeneric  | test | site     | SEPA            |         0,0 | Salvatore |               1 |

  @PPINPROAND-3417
  Scenario Outline: Bonifico - Verificare validazione del campo IBAN
    L utente verifica che il campo rispetti le regole di validazione

    Given App PP viene avviata
      | testIdJira    | driver   | site   | language   |
      | <testIdCheck> | <driver> | <site> | <language> |
    When Utente inserisce le credenziali nella login page
      | username   | password   | device   | testIdCheck   | name   | discrepanza   |
      | <username> | <password> | <device> | <testIdCheck> | <name> | <discrepanza> |
    And Utente clicca su accedi
    And Utente inserisce il posteId
      | username   | password   | device   | testIdCheck   | posteId   | name   |
      | <username> | <password> | <device> | <testIdCheck> | <posteId> | <name> |
    And Utente clicca sul bottone conferma
    And Utente sulla homepage clicca sul tab PAGA
    And Utente clicca su PAGAMENTI
    And Utente clicca su Bonifico SEPA
    And Utente seleziona la carta da utilizzare per il pagamento su "targetOperation"
      | targetOperation   | cardNumber   | discrepanza   | userNumberCards   | name |
      | <targetOperation> | <cardNumber> | <discrepanza> | <userNumberCards> | <name> |
    And Utente seleziona il tipo di bonifico o postagiro da effettuare
      | targetOperation   |
      | <targetOperation> |
    And Utente compila i dati per il bonifico
      | iban                         | targetUser   | importo   | comment            |
      | IT15L0760103400001§_:;,.-022 | <targetUser> | <importo> | Caratteri Speciali |
    And Utente clicca su PROCEDI della Bonifico SEPA
      | targetOperation   |
      | <targetOperation> |
    And Una Popup di errore avvisa che il CC o l IBAN è errato
    And Utente compila i dati per il bonifico
      | iban                       | targetUser         | importo   | comment          |
      | IT15L076010340000104492022 | Vincenzo Fortunato | <importo> | Meno di 27 chars |
    And Utente clicca su PROCEDI della Bonifico SEPA
      | targetOperation   |
      | <targetOperation> |
    And Una Popup di errore avvisa che il CC o l IBAN è errato
    And Utente compila i dati per il bonifico
      | iban                         | targetUser         | importo   | comment         |
      | IT15L07601034000010449202298 | Vincenzo Fortunato | <importo> | Piu di 27 chars |
    And Utente clicca su PROCEDI della Bonifico SEPA
      | targetOperation   |
      | <targetOperation> |
    Then Una Popup di errore avvisa che il CC o l IBAN è errato
    And Utente chiude App PP

    @PPINPROAND-3417_fortunato
    Examples: 
      | username                | password        | driver               | posteId | cardNumber | myIban                      | iban                        | targetUser          | importo | comment    | testIdCheck     | landingPage | site | language | targetOperation | discrepanza | name     | userNumberCards |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | **** 4679  | IT70M3608105138207391907401 | IT36I0301503200000006010000 | Carmine Di Vincenzo |    5.00 | IbanErrato | PPINPROAND-3413 | TnkGeneric  | test | site     | SEPA            |         0,0 | Vincenzo |               2 |

    @PPINPROAND-3417_musella
    Examples: 
      | username             | password  | driver               | posteId | cardNumber | myIban                      | iban                        | targetUser          | importo | comment    | testIdCheck     | landingPage | site | language | targetOperation | discrepanza | name      | userNumberCards |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | **** 7177  | IT15E3608105138271495971507 | IT36I0301503200000006010000 | Carmine Di Vincenzo |    5.00 | IbanErrato | PPINPROAND-3413 | TnkGeneric  | test | site     | SEPA            |         0,0 | Salvatore |               1 |

  @PPINPROAND-3411
  Scenario Outline: Bonifico - Associazione CC / intestatario
    L'utente verifica che l'associazione CC/ intestatario sia corretta

    Given App PP viene avviata
      | testIdJira    | driver   | site   | language   |
      | <testIdCheck> | <driver> | <site> | <language> |
    When Utente inserisce le credenziali nella login page
      | username   | password   | device   | testIdCheck   | name   | discrepanza   |
      | <username> | <password> | <device> | <testIdCheck> | <name> | <discrepanza> |
    And Utente clicca su accedi
    And Utente inserisce il posteId
      | username   | password   | device   | testIdCheck   | posteId   | name   |
      | <username> | <password> | <device> | <testIdCheck> | <posteId> | <name> |
    And Utente clicca sul bottone conferma
    And Utente sulla homepage clicca sul tab PAGA
    And Utente clicca su PAGAMENTI
    And Utente clicca su Bonifico SEPA
    And Utente seleziona la carta da utilizzare per il pagamento su "targetOperation"
      | targetOperation   | cardNumber   | discrepanza   | userNumberCards   | name |
      | <targetOperation> | <cardNumber> | <discrepanza> | <userNumberCards> | <name> |
    And Utente seleziona il tipo di bonifico o postagiro da effettuare
      | targetOperation   |
      | <targetOperation> |
    And Utente compila i dati per il bonifico
      | iban   | targetUser   | importo   | comment   | cardNumber   | name |
      | <iban> | <targetUser> | <importo> | <comment> | <cardNumber> |<name>|
    And Utente clicca su PROCEDI della Bonifico SEPA
      | targetOperation   |
      | <targetOperation> |
    Then Una Popup di errore avvisa che il CC o l IBAN è errato
      | comment   |
      | <comment> |
    And Utente chiude App PP

    @PPINPROAND-3411_fortunato
    Examples: 
      | username                | password        | driver               | posteId | cardNumber | myIban                      | iban     | targetUser         | importo | comment  | testIdCheck     | landingPage | site | language | targetOperation | discrepanza | name     | userNumberCards |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | **** 4679  | IT70M3608105138207391907401 | 40512777 | croce rosa celeste |    5.00 | CCErrato | PPINPROAND-3411 | TnkGeneric  | test | site     | SEPA            |         0,0 | Vincenzo |               2 |

    @PPINPROAND-3411_musella
    Examples: 
      | username             | password  | driver               | posteId | cardNumber | myIban                      | iban     | targetUser         | importo | comment  | testIdCheck     | landingPage | site | language | targetOperation | discrepanza | name      | userNumberCards |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | **** 7177  | IT15E3608105138271495971507 | 40512777 | croce rosa celeste |    5.00 | CCErrato | PPINPROAND-3411 | TnkGeneric  | test | site     | SEPA            |         0,0 | Salvatore |               1 |
