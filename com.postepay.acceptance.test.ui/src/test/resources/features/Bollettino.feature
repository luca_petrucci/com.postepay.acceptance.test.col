@Bollettino
Feature: Bollettino

  @PPINPROAND-360 @PPINPROIOS-371
  Scenario Outline: Bollettino manuale
  
    Given App PP viene avviata
      | testIdJira    | driver   | site   | language   |
      | <testIdCheck> | <driver> | <site> | <language> |
    When Utente inserisce le credenziali nella login page
      | username   | password   | device   | testIdCheck   | name   | discrepanza   |
      | <username> | <password> | <device> | <testIdCheck> | <name> | <discrepanza> |
    And Utente esegue la login in App
      | username   | password   | device   | testIdCheck   | name   | discrepanza   | posteId   |
      | <username> | <password> | <device> | <testIdCheck> | <name> | <discrepanza> | <posteId> |
    And Utente sulla homepage clicca sul tab PAGA
    And Utente clicca su PAGAMENTI
    And Utente clicca su bollettino
    And Utente seleziona la carta da utilizzare per il pagamento su "targetOperation"
      | targetOperation   | cardNumber   | discrepanza   | userNumberCards   |  name |
      | <targetOperation> | <cardNumber> | <discrepanza> | <userNumberCards> | <name> |
    And Utente seleziona la tipologia di bollettino
      | comment   |
      | <comment> |
    And Utente compila i campi per il pagamento del bollettino e clicca su conferma
      | comment   | contoCorrente   | targetUser   | importo   | causale   | cardNumber   | name   | fortunatoNome   | fortunatoCognome   | fortunatoIndirizzo   | fortunatoCitta   | fortunatoCap   | fortunatoProvincia   |
      | <comment> | <contoCorrente> | <targetUser> | <importo> | <causale> | <cardNumber> | <name> | <fortunatoNome> | <fortunatoCognome> | <fortunatoIndirizzo> | <fortunatoCitta> | <fortunatoCap> | <fortunatoProvincia> |
    And Utente verifica le informazioni nel summary e clicca su paga
      | comment   | contoCorrente   | targetUser   | importo   | causale   | cardNumber   | name   | fortunatoNome   | fortunatoCognome   | fortunatoIndirizzo   | fortunatoCitta   | fortunatoCap   | fortunatoProvincia   |
      | <comment> | <contoCorrente> | <targetUser> | <importo> | <causale> | <cardNumber> | <name> | <fortunatoNome> | <fortunatoCognome> | <fortunatoIndirizzo> | <fortunatoCitta> | <fortunatoCap> | <fortunatoProvincia> |
    And Utente inserisce il posteId durante la "targetOperation" e clicca conferma
      | posteId   | targetOperation   |
      | <posteId> | <targetOperation> |
    Then Utente atterra sulla "TnkGeneric"
      | landingPage   |
      | <landingPage> |
    And Utente chiude App PP

    @PPINPROAND-360_fortunato
    Examples: 
      | username                | password        | driver        | posteId | testIdCheck    | language | site | name     | discrepanza | userNumberCards | targetOperation | cardNumber | comment           | contoCorrente | targetUser         | importo | causale   | landingPage   | fortunatoNome | fortunatoCognome | fortunatoIndirizzo               | fortunatoCitta | fortunatoCap | fortunatoProvincia |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PPINPROAND-360 | test     | site | Vincenzo |         0,0 |               2 | Bollettino      | **** 4679  | Bollettino Bianco |      40512204 | croce rosa celeste |    2.00 | Donazione | TypBollettino | VINCENZO      | FORTUNATO        | VIA GENERALE PIETRO DE LACLOS 13 | TARANTO        |        74121 | TA                 |

    @PPINPROAND-360_musella
    Examples: 
      | username             | password  | driver        | posteId | testIdCheck    | language | site | name      | discrepanza | userNumberCards | targetOperation | cardNumber | comment           | contoCorrente | targetUser         | importo | causale   | landingPage   | fortunatoNome | fortunatoCognome | fortunatoIndirizzo | fortunatoCitta        | fortunatoCap | fortunatoProvincia |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | PPINPROAND-360 | test     | site | Salvatore |         0,0 |               2 | Bollettino      | **** 7177  | Bollettino Bianco |      40512204 | croce rosa celeste |    2.00 | Donazione | TypBollettino | SALVATORE     | MUSELLA          | VIA ROMA 32 32     | GIUGLIANO IN CAMPANIA |        80014 | NA                 |

    @PPINPROAND-360_romani
    Examples: 
      | username          | password   | driver        | posteId | testIdCheck    | language | site | name         | discrepanza | userNumberCards | targetOperation | cardNumber | comment           | contoCorrente | targetUser              | importo | causale   | landingPage   | fortunatoNome | fortunatoCognome | fortunatoIndirizzo | fortunatoCitta | fortunatoCap | fortunatoProvincia |
      | prisma20@poste.it | Password2! | GalaxyS10Luca |  123123 | PPINPROAND-360 | test     | site | Maria Grazia |         0,0 |               2 | Bollettino      | **** 9007  | Bollettino Bianco | 1000000115    | TNOYDIRHA XI FPURSEREJE |    2.07 | Donazione | TypBollettino | MARIA GRAZIA  | ROMANI           | VIALE EUROPA 176   | ROMA           |     00144    | RM                 |

    @PPINPROIOS-371_musella
    Examples: 
      | username             | password  | driver        | posteId | testIdCheck    | language | site | name      | discrepanza | userNumberCards | targetOperation | cardNumber | comment           | contoCorrente | targetUser         | importo | causale   | landingPage   | fortunatoNome | fortunatoCognome | fortunatoIndirizzo | fortunatoCitta        | fortunatoCap | fortunatoProvincia |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | PPINPROIOS-371 | test     | site | Salvatore |         0,0 |               2 | Bollettino      | **** 7177  | Bollettino Bianco |      40512204 | croce rosa celeste |    2.00 | Donazione | TypBollettino | SALVATORE     | MUSELLA          | VIA ROMA 32 32     | GIUGLIANO IN CAMPANIA |        80014 | NA                 |

  @PPINPROAND-4094
  Scenario Outline: Bollettino bianco, inserimento manuale
    Given App PP viene avviata
      | testIdJira    | driver   | site   | language   |
      | <testIdCheck> | <driver> | <site> | <language> |
    When Utente inserisce le credenziali nella login page
      | username   | password   | device   | testIdCheck   | name   | discrepanza   |
      | <username> | <password> | <device> | <testIdCheck> | <name> | <discrepanza> |
    And Utente esegue la login in App
      | username   | password   | device   | testIdCheck   | name   | discrepanza   | posteId   |
      | <username> | <password> | <device> | <testIdCheck> | <name> | <discrepanza> | <posteId> |
    And Utente sulla homepage clicca sul tab PAGA
    And Utente clicca su PAGAMENTI
    And Utente clicca su bollettino
    And Utente seleziona la carta da utilizzare per il pagamento su "targetOperation"
      | targetOperation   | cardNumber   | discrepanza   | userNumberCards   |  name |
      | <targetOperation> | <cardNumber> | <discrepanza> | <userNumberCards> | <name> |
    And Utente seleziona la tipologia di bollettino
      | comment   |
      | <comment> |
    And Utente compila i campi per il pagamento del bollettino e clicca su conferma
      | comment   | contoCorrente   | targetUser   | importo   | causale   | cardNumber   | name   | fortunatoNome   | fortunatoCognome   | fortunatoIndirizzo   | fortunatoCitta   | fortunatoCap   | fortunatoProvincia   |
      | <comment> | <contoCorrente> | <targetUser> | <importo> | <causale> | <cardNumber> | <name> | <fortunatoNome> | <fortunatoCognome> | <fortunatoIndirizzo> | <fortunatoCitta> | <fortunatoCap> | <fortunatoProvincia> |
    And Utente verifica le informazioni nel summary e clicca su paga
      | comment   | contoCorrente   | targetUser   | importo   | causale   | cardNumber   | name   | fortunatoNome   | fortunatoCognome   | fortunatoIndirizzo   | fortunatoCitta   | fortunatoCap   | fortunatoProvincia   |
      | <comment> | <contoCorrente> | <targetUser> | <importo> | <causale> | <cardNumber> | <name> | <fortunatoNome> | <fortunatoCognome> | <fortunatoIndirizzo> | <fortunatoCitta> | <fortunatoCap> | <fortunatoProvincia> |
    And Utente inserisce il posteId durante la "targetOperation" e clicca conferma
      | posteId   | targetOperation   |
      | <posteId> | <targetOperation> |
    Then Utente atterra sulla "TnkGeneric"
      | landingPage   |
      | <landingPage> |
    And Utente chiude App PP

    @PPINPROAND-4094_fortunato
    Examples: 
      | username                | password        | driver        | posteId | testIdCheck     | language | site | name     | discrepanza | userNumberCards | targetOperation | cardNumber | comment           | contoCorrente | targetUser         | importo | causale   | landingPage   | fortunatoNome | fortunatoCognome | fortunatoIndirizzo               | fortunatoCitta | fortunatoCap | fortunatoProvincia |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PPINPROAND-4094 | test     | site | Vincenzo |         0,0 |               2 | Bollettino      | **** 4679  | Bollettino Bianco |      40512204 | croce rosa celeste |    2.00 | Donazione | TypBollettino | VINCENZO      | FORTUNATO        | VIA GENERALE PIETRO DE LACLOS 13 | TARANTO        |        74121 | TA                 |

    @PPINPROAND-4094_musella
    Examples: 
      | username             | password  | driver               | posteId | testIdCheck     | language | site | name      | discrepanza | userNumberCards | targetOperation | cardNumber | comment           | contoCorrente | targetUser         | importo | causale   | landingPage   | fortunatoNome | fortunatoCognome | fortunatoIndirizzo | fortunatoCitta        | fortunatoCap | fortunatoProvincia |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | PPINPROAND-4094 | test     | site | Salvatore |         0,0 |               2 | Bollettino      | **** 7177  | Bollettino Bianco |      40512204 | croce rosa celeste |    2.00 | Donazione | TypBollettino | SALVATORE     | MUSELLA          | VIA ROMA 32 32     | GIUGLIANO IN CAMPANIA |        80014 | NA                 |

  @PPINPROAND-4095
  Scenario Outline: Bollettino bianco, inserimento da rubrica
    Given App PP viene avviata
      | testIdJira    | driver   | site   | language   |
      | <testIdCheck> | <driver> | <site> | <language> |
    When Utente inserisce le credenziali nella login page
      | username   | password   | device   | testIdCheck   | name   | discrepanza   |
      | <username> | <password> | <device> | <testIdCheck> | <name> | <discrepanza> |
    And Utente esegue la login in App
      | username   | password   | device   | testIdCheck   | name   | discrepanza   | posteId   |
      | <username> | <password> | <device> | <testIdCheck> | <name> | <discrepanza> | <posteId> |
    And Utente sulla homepage clicca sul tab PAGA
    And Utente clicca su PAGAMENTI
    And Utente clicca su bollettino
    And Utente seleziona la carta da utilizzare per il pagamento su "targetOperation"
      | targetOperation   | cardNumber   | discrepanza   | userNumberCards   | name |
      | <targetOperation> | <cardNumber> | <discrepanza> | <userNumberCards> | <name> |
    And Utente seleziona la tipologia di bollettino
      | comment   |
      | <comment> |
    And Utente clicca su icona della rubrica e inserisce i dati
      | comment   | contoCorrente   | targetUser   | importo   | causale   | cardNumber   | name   | fortunatoNome   | fortunatoCognome   | fortunatoIndirizzo   | fortunatoCitta   | fortunatoCap   | fortunatoProvincia   |
      | <comment> | <contoCorrente> | <targetUser> | <importo> | <causale> | <cardNumber> | <name> | <fortunatoNome> | <fortunatoCognome> | <fortunatoIndirizzo> | <fortunatoCitta> | <fortunatoCap> | <fortunatoProvincia> |
    And Utente verifica le informazioni nel summary e clicca su paga
      | comment   | contoCorrente   | targetUser   | importo   | causale   | cardNumber   | name   | fortunatoNome   | fortunatoCognome   | fortunatoIndirizzo   | fortunatoCitta   | fortunatoCap   | fortunatoProvincia   |
      | <comment> | <contoCorrente> | <targetUser> | <importo> | <causale> | <cardNumber> | <name> | <fortunatoNome> | <fortunatoCognome> | <fortunatoIndirizzo> | <fortunatoCitta> | <fortunatoCap> | <fortunatoProvincia> |
    And Utente inserisce il posteId durante la "targetOperation" e clicca conferma
      | posteId   | targetOperation   |
      | <posteId> | <targetOperation> |
    Then Utente atterra sulla "TnkGeneric"
      | landingPage   |
      | <landingPage> |
    And Utente chiude App PP

    @PPINPROAND-4095_fortunato
    Examples: 
      | username                | password        | driver        | posteId | testIdCheck     | language | site | name     | discrepanza | userNumberCards | targetOperation | cardNumber | comment           | contoCorrente | targetUser         | importo | causale   | landingPage   | fortunatoNome | fortunatoCognome | fortunatoIndirizzo               | fortunatoCitta | fortunatoCap | fortunatoProvincia |
      | vincenzo.fortunato-4869 | D@niela22Lili10 | GalaxyS10Luca | prisma  | PPINPROAND-4095 | test     | site | Vincenzo |         0,0 |               2 | Bollettino      | **** 4679  | Bollettino Bianco |      40512204 | croce rosa celeste |    2.00 | Donazione | TypBollettino | VINCENZO      | FORTUNATO        | VIA GENERALE PIETRO DE LACLOS 13 | TARANTO        |        74121 | TA                 |

    @PPINPROAND-4095_musella
    Examples: 
      | username             | password  | driver               | posteId | testIdCheck     | language | site | name      | discrepanza | userNumberCards | targetOperation | cardNumber | comment           | contoCorrente | targetUser         | importo | causale   | landingPage   | fortunatoNome | fortunatoCognome | fortunatoIndirizzo | fortunatoCitta        | fortunatoCap | fortunatoProvincia |
      | musellasal@gmail.com | Musabp67! | GalaxyS10Luca | prisma  | PPINPROAND-4095 | test     | site | Salvatore |         0,0 |               2 | Bollettino      | **** 7177  | Bollettino Bianco |      40512204 | croce rossa celeste |    2.00 | Donazione | TypBollettino | SALVATORE     | MUSELLA          | VIA ROMA 32 32     | GIUGLIANO IN CAMPANIA |        80014 | NA                 |
